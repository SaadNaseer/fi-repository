package com.NavigationFragments;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import java.io.File;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.GetterSetters.ScanInventoryGetterSetter;
import com.Utils.CalendarDialogBuilder;
import com.Utils.ConnectionClass;
import com.Utils.EmailCredentials;
import com.Utils.FTPCredentials;
import com.Utils.GMailSender;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ScanInventory extends Fragment implements CalendarDialogBuilder.OnDateSetListener{
	static String scanContent="";
	static String scanFormat="";
	String enteredquantity="";
	String entereddescription="";
	String entereddate="";
	ListView gridView;
	static String ITEM="item";
	static String QUANTITY="quantity";
	static String DESCRIPTION="description";
	static String DATE="date";
	Button scanagain_button;
	Button manualaddition;
	Button submit;
	Databaseadapter db;
	String usernamefull="";
	String txtfilepath="";
	Boolean isSuccess = false;
	String errormessage="";
	public static boolean backpressed=false;
	TextView tvsdate;
	ArrayList<HashMap<String, String>> inventorylisting;
	ConnectionClass connectionClass;
	final CharSequence[] items = {"Enter Manually","Choose from products list"};
	AlertDialog levelDialog;
	RequestListener requestlistener;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView; 
		rootView = inflater.inflate(R.layout.scaninventory, container, false);
		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails,String emailmsg,String subject)
			{
				if(emails.contains(","))
				{
					String arr[] = emails.split(",");
					for(int i=0;i<arr.length;i++)
					{
						String emailaddr=arr[i];
						SendEmailNew(emailmsg,subject,emailaddr);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					SendEmailNew(emailmsg,subject,emails);
					Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
				}
			}
			@Override
			public void onError(String result) {
				Toast.makeText(getActivity(),"Error occured in sending smail ",Toast.LENGTH_LONG).show();
			}
		};
		connectionClass = new ConnectionClass();
		db=new Databaseadapter(getActivity());
		db.open();
		db.getdata();
		ImageView imageButton = (ImageView) rootView
				.findViewById(R.id.menuicon);
		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
				NavigationActivity.draweropened=true;
			}
		});
		ImageView homeButton = (ImageView) rootView
				.findViewById(R.id.homeicon);
		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				preferences p= new preferences(getActivity());
				p.SetLastScreen("");
				p.Settotalbids("");
				Databaseadapter db=new Databaseadapter(getActivity());
				db.DeleteRunningTransactions(); 
				getActivity().finishAffinity();
				Intent i= new Intent(getActivity(),NavigationActivity.class);
				i.putExtra("val", "");
				startActivity(i);
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			}
		});
		submit=(Button)rootView.findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if(inventorylisting.isEmpty())
				{
					Toast.makeText(getActivity(), "No Items Found For Submission", Toast.LENGTH_LONG).show();
				}
				else{
					
					generateemail();
					generatetxtfile();
					
				}
			}
		});
		scanagain_button=(Button)rootView.findViewById(R.id.scanagain_button);
		manualaddition=(Button)rootView.findViewById(R.id.manualaddition);
		manualaddition.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				
				 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	                builder.setTitle("Select Manual Addition Type");
	                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int item) {
	                   
	                    
	                    switch(item)
	                    {
	                        case 0:
	                        	AlertDialog.Builder builder=new AlertDialog.Builder(getActivity()); 
	                    		LayoutInflater li = LayoutInflater.from(getActivity());
	                    		View promptsView = li.inflate(R.layout.scanmanual, null);
	                    		builder.setView(promptsView);
	                    		builder.setCancelable(false);
	                    		builder.setTitle("Enter Manual Item Details ");
	                    		final EditText itemno = (EditText)promptsView.
	                    				findViewById(R.id.itemno);   
	                    		final EditText itemq = (EditText)promptsView.
	                    				findViewById(R.id.itemq);  
	                    		final EditText itemd = (EditText)promptsView.
	                    				findViewById(R.id.itemd);  
	                    		
	                    		tvsdate=(TextView)promptsView.findViewById(R.id.tvsdate);
	                    		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
	                    		int thisMonth = calendar.get(Calendar.MONTH);
	                    		int thisDay = calendar.get(Calendar.DAY_OF_MONTH);

	                    		tvsdate.setText(calendar.get(Calendar.YEAR)+"-"+String.valueOf(thisMonth+1)+"-"+String.valueOf(thisDay));
	                    		tvsdate.setOnClickListener(new OnClickListener() {

	                    			@Override
	                    			public void onClick(View view) {
	                 
	                    			}
	                    		});
	                    		builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
	                    			@Override
	                    			public void onClick(DialogInterface dialog, int which) {

	                    				if(itemno.getText().toString().isEmpty())
	                    				{
	                    					Toast.makeText(getActivity(), "Item No cannot be empty", Toast.LENGTH_LONG).show();

	                    					return;
	                    				}
	                    				if(itemd.getText().toString().isEmpty())
	                    				{
	                    					Toast.makeText(getActivity(), "Item Description cannot be empty", Toast.LENGTH_LONG).show();

	                    					return;
	                    				}
	                    				
	                    				if(itemq.getText().toString().isEmpty())
	                    				{
	                    					Toast.makeText(getActivity(), "Quantity cannot be empty", Toast.LENGTH_LONG).show();

	                    					return;
	                    				}

	                    				if(tvsdate.getText().toString().equals("Select Date"))
	                    				{
	                    					Toast.makeText(getActivity(), "Date cannot be empty", Toast.LENGTH_LONG).show();

	                    					return;
	                    				}
	                    				HashMap<String, String> map = new HashMap<String, String>();
	                    				map.put(ITEM, itemno.getText().toString());
	                    				map.put(QUANTITY, itemq.getText().toString());
	                    				map.put(DESCRIPTION, itemd.getText().toString());
	                    				map.put(DATE, tvsdate.getText().toString());
	                    				inventorylisting.add(map);
	                    				gridView.setBackgroundResource(Color.TRANSPARENT);
	                    				db.insertscaninventory(new ScanInventoryGetterSetter(itemno.getText().toString(),itemq.getText().toString(),itemd.getText().toString(),tvsdate.getText().toString()));
	                    				InventoryAdapter marketingadapter = new InventoryAdapter(getActivity(), inventorylisting);
	                    				gridView.setAdapter(marketingadapter);
	                    			}
	                    		});

	                    		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	                    			@Override
	                    			public void onClick(DialogInterface dialog, int which) {


	                    			}
	                    		});
	                    		builder.show(); 
	                                 break;
	                        case 1:
	                               
	                        	FragmentManager fragmentManager = getFragmentManager();
	            				fragmentManager.beginTransaction().addToBackStack(null)
	            				.replace(R.id.frame_container, new AddManuallyProductSelection()).commit();  
	                                break;
	                       
	                        
	                    }
	                    levelDialog.dismiss();    
	                    }
	                });
	                levelDialog = builder.create();
	                levelDialog.show();
				
				return;
				/*AlertDialog.Builder builder=new AlertDialog.Builder(getActivity()); 
		           		 LayoutInflater li = LayoutInflater.from(getActivity());
		           			View promptsView = li.inflate(R.layout.scanmanual, null);
		           			builder.setView(promptsView);
		           			builder.setCancelable(false);
		           			builder.setTitle("Enter Manual Item Details ");
		           			final EditText itemno = (EditText)promptsView.
	           		 				findViewById(R.id.itemno);   
		           			final EditText itemq = (EditText)promptsView.
		           		 				findViewById(R.id.itemq);  
		           			final EditText itemd = (EditText)promptsView.
	           		 				findViewById(R.id.itemd);  
		           			tvsdate=(TextView)promptsView.findViewById(R.id.tvsdate);
		           			tvsdate.setOnClickListener(new OnClickListener() {

		           				@Override
		           				public void onClick(View view) {
		           					ShowCalendar();
		           				}
		           			});
		           			builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
		           				@Override
		           				public void onClick(DialogInterface dialog, int which) {
		           						if(itemno.getText().toString().isEmpty())
		           						{
		           							Toast.makeText(getActivity(), "Item No cannot be empty", Toast.LENGTH_LONG).show();

		           							return;
		           						}
		           						if(itemq.getText().toString().isEmpty())
		           						{
		           							Toast.makeText(getActivity(), "Quantity cannot be empty", Toast.LENGTH_LONG).show();

		           							return;
		           						}
		           						if(itemd.getText().toString().isEmpty())
		           						{
		           							Toast.makeText(getActivity(), "Description cannot be empty", Toast.LENGTH_LONG).show();

		           							return;
		           						}
		           						if(tvsdate.getText().toString().equals("Select Date"))
		           						{
		           							Toast.makeText(getActivity(), "Date cannot be empty", Toast.LENGTH_LONG).show();

		           							return;
		           						}
		           						HashMap<String, String> map = new HashMap<String, String>();
		           						map.put(ITEM, itemno.getText().toString());
		           						map.put(QUANTITY, itemq.getText().toString());
		           						map.put(DESCRIPTION, itemd.getText().toString());
		           						map.put(DATE, tvsdate.getText().toString());
		           						inventorylisting.add(map);
		           						gridView.setBackgroundResource(Color.TRANSPARENT);
		           						db.insertscaninventory(new ScanInventoryGetterSetter(itemno.getText().toString(),itemq.getText().toString(),itemd.getText().toString(),tvsdate.getText().toString()));
		           						InventoryAdapter marketingadapter = new InventoryAdapter(getActivity(), inventorylisting);
		           						gridView.setAdapter(marketingadapter);
		           				}
		           			});

		           			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		           				@Override
		           				public void onClick(DialogInterface dialog, int which) {


		           				}
		           			});
		           			builder.show();*/
			}
		});
		scanagain_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				IntentIntegrator scanIntegrator = new IntentIntegrator(ScanInventory.this);
				scanIntegrator.initiateScan();
			}
		});
		inventorylisting = new ArrayList<HashMap<String, String>>();
		gridView = (ListView)rootView.findViewById(R.id.gridview);
		List<ScanInventoryGetterSetter> scanned = db.getscannedinventory();
		for (ScanInventoryGetterSetter inventories : scanned) 
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(ITEM, inventories.GetItemNo());
			map.put(QUANTITY, inventories.GetItemQuantity());
			map.put(DESCRIPTION, inventories.GetItemDescription());
			map.put(DATE, inventories.GetItemDate());
			inventorylisting.add(map);
		}
		if(inventorylisting.isEmpty())
		{

		}
		else
		{
			gridView.setBackgroundResource(Color.TRANSPARENT);
		}
		InventoryAdapter marketingadapter = new InventoryAdapter(getActivity(), inventorylisting);
		gridView.setAdapter(marketingadapter);
		/* IntentIntegrator scanIntegrator = new IntentIntegrator(ScanInventory.this);
			 scanIntegrator.initiateScan();*/
		if(AddManuallyProductSelection.productselected)
		{
			AddManuallyProductSelection.productselected=false;
			afterproductselection();
		}
		return rootView;
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanningResult != null) {
			//scanContent="38-453";
			scanContent = scanningResult.getContents();
			//scanFormat = scanningResult.getFormatName();
			Toast.makeText(getActivity(), 
					scanContent, Toast.LENGTH_SHORT).show();
			Toast.makeText(getActivity(), 
					scanFormat, Toast.LENGTH_SHORT).show();
			if(scanContent!=null)
			{
				try{
					if(scanContent.contains("-"))
					{
					 
					}
					else
					{
						
						scanContent=scanContent.substring(6);
						scanContent = scanContent.substring(0, 2) + "-" + scanContent.substring(2, scanContent.length());
					
					if(scanContent.length()>6)
					{
						scanContent=scanContent.substring(0, scanContent.length() - 1);
						
					}
						
					}}catch(Exception a)
					{
						Toast.makeText(getActivity(), "Invalid Bar Code", Toast.LENGTH_LONG).show();
						return;
					}
				scanContent = scanContent.trim().toString();
				GetArtDescription getdes = new GetArtDescription(); 
				getdes.execute("");

			}
			else{
				Toast toast = Toast.makeText(getActivity(), 
						"No Bar Code data received!", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
		else{
			Toast toast = Toast.makeText(getActivity(), 
					"No Bar Code data received!", Toast.LENGTH_SHORT);
			toast.show();
		}
	}
	public class InventoryAdapter extends BaseAdapter {
		Context context;
		String value;
		String fontPath2 = "segoepr.ttf";
		LayoutInflater inflater;
		ArrayList<HashMap<String, String>> data;
		public InventoryAdapter(Context context,
				ArrayList<HashMap<String, String>> arraylist) {
			this.context = context;
			data = arraylist;	
		}
		@Override
		public int getCount() {
			return data.size();
		}
		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {	  
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final View itemView = inflater.inflate(R.layout.layout_scan_inventory, parent, false);
			HashMap<String, String> results = new HashMap<String, String>();
			results = data.get(position);
			final TextView nameTextView = (TextView)itemView.findViewById(R.id.itemname);
			final TextView quantityTextView = (TextView)itemView.findViewById(R.id.itemquantity);
			final TextView desTextView = (TextView)itemView.findViewById(R.id.itemdescription);
			final String date=results.get(ScanInventory.DATE);
			nameTextView.setText(results.get(ScanInventory.ITEM));
			quantityTextView.setText(results.get(ScanInventory.QUANTITY));
			desTextView.setText(results.get(ScanInventory.DESCRIPTION));
			Button deleteBtn = (Button)itemView.findViewById(R.id.delete_btn);
			deleteBtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v) { 
					//do something

					db.DeleteInventory(nameTextView.getText().toString());
					inventorylisting.remove(position);
					notifyDataSetChanged();
					if(inventorylisting.isEmpty())
					{
						gridView.setBackgroundResource(R.drawable.emptylist);
					}
				}
			});
			Button editBtn = (Button)itemView.findViewById(R.id.edit_btn);
			editBtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v) { 
					//do something
					AlertDialog.Builder builder=new AlertDialog.Builder(getActivity()); 
					LayoutInflater li = LayoutInflater.from(getActivity());
					View promptsView = li.inflate(R.layout.scanmanual, null);
					builder.setView(promptsView);
					builder.setCancelable(false);
					builder.setTitle("Edit Item Details ");
					final EditText itemno = (EditText)promptsView.
							findViewById(R.id.itemno);  
					itemno.setText(nameTextView.getText().toString());
					final String olditemno=itemno.getText().toString();

					final EditText itemq = (EditText)promptsView.
							findViewById(R.id.itemq); 
					
					itemq.setText(quantityTextView.getText().toString());
					final EditText itemd = (EditText)promptsView.
							findViewById(R.id.itemd); 
					itemd.setText(desTextView.getText().toString());
					final TextView tvsdate = (TextView)promptsView.
							findViewById(R.id.tvsdate); 
					tvsdate.setText(date);
					builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							if(itemno.getText().toString().isEmpty())
							{
								Toast.makeText(getActivity(), "Item No cannot be empty", Toast.LENGTH_LONG).show();

								return;
							}
							if(itemq.getText().toString().isEmpty())
							{
								Toast.makeText(getActivity(), "Quantity cannot be empty", Toast.LENGTH_LONG).show();

								return;
							}
							inventorylisting.remove(position);

							HashMap<String, String> map = new HashMap<String, String>();
							map.put(ITEM, itemno.getText().toString());
							map.put(QUANTITY, itemq.getText().toString());
							map.put(DESCRIPTION, itemd.getText().toString());
							inventorylisting.add(position,map);

							db.UpdateInventory(olditemno, itemno.getText().toString(), itemq.getText().toString(),itemd.getText().toString());
							InventoryAdapter marketingadapter = new InventoryAdapter(getActivity(), inventorylisting);
							gridView.setAdapter(marketingadapter);
							notifyDataSetChanged();
						}
					});
					builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {


						}
					});

					builder.show();
				}
			});
			return itemView;
		}
	}




	@SuppressWarnings("unused")
	public void generatetxtfile()
	{
		try{
			SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);

			usernamefull =pref.getString(preferences.username,"");
			String currentdate=getTestTimeStampfortests();
			File ff=new File(Environment.getExternalStorageDirectory()+File.separator+usernamefull+"_"+currentdate+".txt");
			if(ff.exists()){ff.delete();}
			ff.createNewFile();
			txtfilepath=Environment.getExternalStorageDirectory()+File.separator+usernamefull+"_"+currentdate+".txt";
			RandomAccessFile file = new RandomAccessFile(txtfilepath, "rw");
			long pointer;

			file.write("Item".getBytes());
			//file.seek(22);
			for(int i=0;i<16;i++)
			{
				file.write(" ".getBytes());
			}
			file.write("Description".getBytes());
			for(int i=0;i<30;i++)
			{
				file.write(" ".getBytes());
			}
			file.write("Quantity On Hand".getBytes());
			for(int i=0;i<16;i++)
			{
				file.write(" ".getBytes());
			}
			file.write("Date Entered".getBytes());
			file.writeBytes("\r\n");

			List<ScanInventoryGetterSetter> scanned = db.getscannedinventory();
			for (ScanInventoryGetterSetter inventories : scanned) 
			{
				file.write(inventories.GetItemNo().getBytes());	
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());

				file.write(inventories.GetItemDescription().getBytes());	
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());

				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(inventories.GetItemQuantity().getBytes());	
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());
				file.write(" ".getBytes());

				file.write(inventories.GetItemDate().getBytes());	
				file.writeBytes("\r\n");
			}
			file.close();
		}catch(Exception q)
		{
			Toast.makeText(getActivity(), q.toString(), Toast.LENGTH_LONG).show();
		}
		UploadFile doUpload = new UploadFile(); 
		doUpload.execute("");
	}
	public  String getTestTimeStampfortests() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		String currentDate = sdf.format(new Date());
		return currentDate;
	}
	public  String getDateStampfortests() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = sdf.format(new Date());
		return currentDate;
	}
	public  void generateemail()
	{
		String subject="Scanned Inventory for "+usernamefull;
		String body="Item                "+"Description                              "+"Quantity On Hand                "+"Date Entered"+"\n";

		List<ScanInventoryGetterSetter> scanned = db.getscannedinventory();
		for (ScanInventoryGetterSetter inventories : scanned) 
		{
			body=body+inventories.GetItemNo()+"              "+inventories.GetItemDescription()+"               "+inventories.GetItemQuantity()+"                          "+inventories.GetItemDate()+"\n";

		}

		Log.e("body ",body);
		SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);

		String ccemail =pref.getString(preferences.AMEMAIL,"");
		if(ccemail.equals("")||ccemail.equalsIgnoreCase("NULL")){
		//SendEmail(body,subject);
			GetEmails webservice = new GetEmails(getActivity(), requestlistener,"scannedinventory", body, subject);
			webservice.execute();
		}
		else
		{
			GetEmails webservice = new GetEmails(getActivity(), requestlistener,"scannedinventory", body, subject);
			webservice.execute();
			//SendEmailwithcc(body,subject,ccemail);
		}
		Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();
	}
	private void SendEmailwithcc(final String msg, final String subject,final String ccemail) {
		// TODO Auto-generated method stub
		//Toast.makeText(getActivity(), ccemail, Toast.LENGTH_LONG).show();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendCCMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT,ccemail);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmailNew(final String msg, final String subject,final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}

	public class UploadFile extends AsyncTask<String,String,String>
	{

		String z = "";

		String replycode="";
		ProgressDialog p;

		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Uploading File ...");
			p.setCancelable(false);
			p.show();
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			db.DeleteInventories();
			if(isSuccess) {



				Toast.makeText(getActivity(),"File Uploaded SuccessFully",Toast.LENGTH_SHORT).show();

			}
			else{

				Toast.makeText(getActivity(),"File Uploading Failed",Toast.LENGTH_SHORT).show();
			}
			getActivity().finishAffinity();
			Intent i= new Intent(getActivity(),NavigationActivity.class);
			i.putExtra("val", "");
			startActivity(i);
			FragmentManager fragmentManager = getFragmentManager();
			/*fragmentManager.beginTransaction()
			.replace(R.id.frame_container, new HomeNew()).commit();*/
			fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);




		}

		@Override
		protected String doInBackground(String... params) {

			try {
				isSuccess=false;
				Log.e("File path ",txtfilepath);
				uploadFile(new File(txtfilepath));
			}
			catch (Exception ex)
			{
				isSuccess = false;
				replycode = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return replycode;
		}
	}
	public void uploadFile(File fileName){


		FTPClient client = new FTPClient();

		try {

			client.connect(FTPCredentials.FTP_HOST,21);
			client.login(FTPCredentials.FTP_USER, FTPCredentials.FTP_PASS);
			client.setType(FTPClient.TYPE_BINARY);

			//client.changeDirectory("bid_imports/Test");
			client.changeDirectory("App_Inventory");
			
			client.upload(fileName, new MyTransferListener());

		} catch (Exception e) {
			e.printStackTrace();
			errormessage=e.toString();
			Log.e("EXCEPTION ",errormessage);
			try {
				client.disconnect(true);    
			} catch (Exception e2) {
				errormessage=e.toString();
				e2.printStackTrace();
			}
		}

	}

	/*******  Used to file upload and show progress  **********/

	public class MyTransferListener implements FTPDataTransferListener {

		public void started() {

		}

		public void transferred(int length) {

		}

		public void completed() {

			isSuccess=true;


		}

		public void aborted() {

		}

		public void failed() {

		}

	}

	@Override
	public void onResume() {
		super.onResume();

		// Call the 'activateApp' method to log an app event for use in analytics and advertising
		// reporting.  Do so in the onResume methods of the primary Activities that an app may be
		// launched into.
		// setOnBackPressListener();



	}
	@SuppressWarnings("unused")
	private void setOnBackPressListener() {

		this.getView().setFocusableInTouchMode(true);
		this.getView().requestFocus();
		this.getView().setOnKeyListener(new View.OnKeyListener() {
			@SuppressLint("NewApi")
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {


				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.frame_container, new MyInventory()).commit();

				backpressed=true;
				return false;
			}
		});
	}
	@Override
	public void onDateSet(int Year, int Month, int Day) {
		// TODO Auto-generated method stub
		int m=Month+1;
		tvsdate.setText(/*Month+1 + "/" + Day + "/" + */Year+"-"+m+"-"+Day);
	}
	public void ShowCalendar()
	{
		CalendarDialogBuilder calendar;


		calendar = new CalendarDialogBuilder(getActivity(), this);



		calendar.showCalendar();
	}

	public void afterproductselection()
	{
		AlertDialog.Builder builder=new AlertDialog.Builder(getActivity()); 
		LayoutInflater li = LayoutInflater.from(getActivity());
		View promptsView = li.inflate(R.layout.scanmanual, null);
		builder.setView(promptsView);
		builder.setCancelable(false);
		builder.setTitle("Enter Manual Item Details ");
		final EditText itemno = (EditText)promptsView.
				findViewById(R.id.itemno);   
		final EditText itemq = (EditText)promptsView.
				findViewById(R.id.itemq);  
		final EditText itemd = (EditText)promptsView.
				findViewById(R.id.itemd);  
		itemno.setText(AddManuallyProductSelection.artno);
		itemd.setText(AddManuallyProductSelection.artdes);
		tvsdate=(TextView)promptsView.findViewById(R.id.tvsdate);
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		int thisMonth = calendar.get(Calendar.MONTH);
		int thisDay = calendar.get(Calendar.DAY_OF_MONTH);

		tvsdate.setText(calendar.get(Calendar.YEAR)+"-"+String.valueOf(thisMonth+1)+"-"+String.valueOf(thisDay));
		tvsdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				//ShowCalendar();
			}
		});
		builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {


				if(itemq.getText().toString().isEmpty())
				{
					Toast.makeText(getActivity(), "Quantity cannot be empty", Toast.LENGTH_LONG).show();

					return;
				}

				if(tvsdate.getText().toString().equals("Select Date"))
				{
					Toast.makeText(getActivity(), "Date cannot be empty", Toast.LENGTH_LONG).show();

					return;
				}
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(ITEM, itemno.getText().toString());
				map.put(QUANTITY, itemq.getText().toString());
				map.put(DESCRIPTION, itemd.getText().toString());
				map.put(DATE, tvsdate.getText().toString());
				inventorylisting.add(map);
				gridView.setBackgroundResource(Color.TRANSPARENT);
				db.insertscaninventory(new ScanInventoryGetterSetter(itemno.getText().toString(),itemq.getText().toString(),itemd.getText().toString(),tvsdate.getText().toString()));
				InventoryAdapter marketingadapter = new InventoryAdapter(getActivity(), inventorylisting);
				gridView.setAdapter(marketingadapter);
			}
		});

		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {


			}
		});
		builder.show();
	}
	
	public class GetArtDescription extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = true;
		ProgressDialog p;
		String artdes="";
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			AlertDialog.Builder builder=new AlertDialog.Builder(getActivity()); 
			LayoutInflater li = LayoutInflater.from(getActivity());
			View promptsView = li.inflate(R.layout.scaninventorydetail_layout, null);
			builder.setView(promptsView);
			builder.setCancelable(false);
			builder.setTitle("Enter Details ");
			final EditText qval = (EditText)promptsView.
					findViewById(R.id.qval);  
			final EditText qdes = (EditText)promptsView.
					findViewById(R.id.qdes);  
			qdes.setText(artdes);
			tvsdate=(TextView)promptsView.findViewById(R.id.tvsdate);
			Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
			int thisMonth = calendar.get(Calendar.MONTH);
			int thisDay = calendar.get(Calendar.DAY_OF_MONTH);

			tvsdate.setText(calendar.get(Calendar.YEAR)+"-"+String.valueOf(thisMonth+1)+"-"+String.valueOf(thisDay));
			builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					enteredquantity=qval.getText().toString();
					entereddescription=qdes.getText().toString();
					entereddate=tvsdate.getText().toString();
					if(enteredquantity.isEmpty())
					{
						Toast.makeText(getActivity(), "Quantity cannot be empty", Toast.LENGTH_LONG).show();
						return;
					}
					if(entereddescription.isEmpty())
					{
						Toast.makeText(getActivity(), "Description cannot be empty", Toast.LENGTH_LONG).show();
						return;
					}
					if(entereddate.equals("Select Date"))
					{
						Toast.makeText(getActivity(), "Date cannot be empty", Toast.LENGTH_LONG).show();

						return;
					}
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(ITEM, scanContent);
					map.put(QUANTITY, enteredquantity);
					map.put(DESCRIPTION, entereddescription);
					map.put(DATE, entereddate);
					inventorylisting.add(map);
					gridView.setBackgroundResource(Color.TRANSPARENT);
					db.insertscaninventory(new ScanInventoryGetterSetter(scanContent,enteredquantity,entereddescription,entereddate));
					InventoryAdapter marketingadapter = new InventoryAdapter(getActivity(), inventorylisting);
					gridView.setAdapter(marketingadapter);
				}
			});builder.show();

		}

		@Override
		protected String doInBackground(String... params) {
		
				try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						//String query = "select * from dbo.vw_PAS_LiveEasels" ;
						String query = "select item_desc_1 from dbo.vw_PAS_AllProducts where item_no='" + scanContent + "'";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						 while (rs.next()) {

							if(rs.getString("item_desc_1")!=null){
								
								artdes=rs.getString("item_desc_1");
							}
								else
								{
									artdes="";
									
								}
						
		                    }
						
					}
				}
				catch (Exception ex)
				{
					artdes="";
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;
		}
	}
}