package com.NavigationFragments;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Database.Databaseadapter;
import com.Utils.ConnectionClass;
import com.Utils.Easels;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class LookUpEasel extends Fragment {
	private ArrayList<Easels> Easelss = new ArrayList<Easels>();
	ArrayList<HashMap<String, String>> listing;
	static boolean backpressed = false;
	ConnectionClass connectionClass;
	ListView listView;
	EditText search;
	Button confirmproductbtn;
	String artno = "";
	String artdes = "";
	String Art = "Art";
	String Location = "Location";
	String Charity = "Charity";
	String Quantity = "Quantity";
	String Sponsor = "Sponsor";
	String StreetAddress = "StreetAddress";
	String City = "City";
	String Province = "Province";
	String PostalCode = "PostalCode";
	String ArtDescription = "ArtDescription";
	String Easel = "Easel";
	String EaselID = "EaselID";
	String ProductID = "ProductID";
	String APProductID = "APProductID";
	String FROMLOCATIONID = "FROMLOCATIONID";
	public static String SelectedArt = "SelectedArt";
	public String SelectedLocation = "SelectedLocation";
	public String SelectedCharity = "SelectedCharity";
	public String SelectedSponsor = "SelectedSponsor";
	public String SelectedEasel = "SelectedEasel";
	public String SelectedEaselID = "SelectedEaselID";
	public String SelectedProductID = "SelectedProductID";
	public String SelectedAPProductID = "SelectedAPProductID";
	public String SelectedQuantity = "SelectedQuantity";
	public String SelectedStreetAddress = "SelectedStreetAddress";
	public String SelectedCity = "SelectedCity";
	public String SelectedProvince = "SelectedProvince";
	public String SelectedPostCode = "SelectedPostCode";
	public String SelectedArtDescription = "SelectedArtDescription";
	public String SelectedFROMLOCATIONID = "SelectedFROMLOCATIONID";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView;
		rootView = inflater.inflate(R.layout.lookup_easel, container, false);
		confirmproductbtn = (Button) rootView.findViewById(R.id.confirm_product_button);

		confirmproductbtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().addToBackStack(null)
						.replace(R.id.frame_container, new EaselConfirmation()).commit();

			}
		});
		ImageView imageButton = (ImageView) rootView
				.findViewById(R.id.menuicon);
		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				/*if(!NavigationActivity.draweropened){*/
				NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
				NavigationActivity.draweropened = true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
			}
		});
		ImageView homeButton = (ImageView) rootView
				.findViewById(R.id.homeicon);
		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Cancel ?");
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setMessage("Are you sure you wish to cancel the transaction ?");
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
												int arg1) {

							}
						});
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
												int arg1) {
								preferences p = new preferences(getActivity());
								p.SetLastScreen("");
								p.Settotalbids("");
								Databaseadapter db = new Databaseadapter(getActivity());
								db.DeleteRunningTransactions();
								getActivity().finishAffinity();
								Intent i = new Intent(getActivity(), NavigationActivity.class);
								i.putExtra("val", "");
								startActivity(i);
								FragmentManager fragmentManager = getFragmentManager();
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
								fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
							}
						});

				builder.create().show();
			}
		});
		listing = new ArrayList<HashMap<String, String>>();
		listView = (ListView) rootView.findViewById(R.id.listview);
		listView.setCacheColorHint(Color.TRANSPARENT);
		connectionClass = new ConnectionClass();
		search = (EditText) rootView.findViewById(R.id.editText1);
		Easelss.clear();
		listing.clear();
		//Lookup Easel through advancepro apis
		GetEaselsList geteasels = new GetEaselsList();
		geteasels.execute("");
		return rootView;
	}

	public class GetDetails extends AsyncTask<String, String, String> {
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber = "";
		String itemdesc = "";

		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(getActivity());
			p.setMessage("Loading Details ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if (isSuccess) {
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().addToBackStack(null)
						.replace(R.id.frame_container, new EaselConfirmation()).commit();
			} else {
				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				//get selected data from salesforce
				HttpClient httpclient = new DefaultHttpClient();
				String sfprodid="";
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME, 0);
				String conid = settings.getString(preferences.useridd, "");
				String EaselsURL = SFAPI.baseUri + "/query/?q=" + URLEncoder.encode("SELECT Id,Fundraising_Consultant__c,Charity_partner__c,Location_name__c,Address__c,City__c,Province__c,Postal_Code__c,Name FROM Easel__c Where Easel_Number__c = '" +Home.SelectedEasel + "'", "UTF-8");

				Log.e("URL ", EaselsURL);
				Log.e("Header ", SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(EaselsURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;

				// Execute the login POST request
				response = httpclient.execute(httpPost);
				String alleasels = EntityUtils.toString(response.getEntity());
				JSONObject jobj = new JSONObject(alleasels);
				JSONArray jarr = jobj.getJSONArray("records");
				for (int i = 0; i < jarr.length(); i++) {

					JSONObject eitems = jarr.getJSONObject(i);
					SelectedEaselID=eitems.getString("Id");
					Home.SelectedEaselID=SelectedEaselID;
					SelectedSponsor=eitems.getString("Location_name__c");
					Home.SelectedSponsor=SelectedSponsor;
					SelectedLocation=SelectedSponsor;
					Home.SelectedLocation=SelectedLocation;
					SelectedStreetAddress=eitems.getString("Address__c");
					Home.SelectedStreetAddress=SelectedStreetAddress;
					SelectedCity=eitems.getString("City__c");
					Home.SelectedCity=SelectedCity;
					SelectedProvince=eitems.getString("Province__c");
					Home.SelectedProvince=SelectedProvince;
					SelectedPostCode=eitems.getString("Postal_Code__c");
					Home.SelectedPostCode=SelectedPostCode;
					// Assemble the login request URL
					String AccountURL33 = SFAPI.baseUri + "/sobjects" + SFAPI.Account + "/" + eitems.getString("Charity_partner__c");
					Log.e("URL ", AccountURL33);

					// Login requests must be POSTs
					HttpGet httpPost33 = new HttpGet(AccountURL33);
					httpPost33.addHeader(SFAPI.oauthHeader);
					httpPost33.addHeader(SFAPI.prettyPrintHeader);


					HttpResponse response33 = null;

					// Execute the login POST request
					response33 = httpclient.execute(httpPost33);

					// verify response is HTTP OK
					final int statusCode33 = response33.getStatusLine().getStatusCode();
					if (statusCode33 != HttpStatus.SC_OK) {
						isSuccess=true;
					} else {
						JSONObject jsonObjectt3 = new JSONObject(EntityUtils.toString(response33.getEntity()));

						//map.put(Charity, jsonObjectt3.getString("Name"));
						SelectedCharity = jsonObjectt3.getString("Name");
						Home.SelectedCharity=SelectedCharity;
						Log.e("Charity ",SelectedCharity);
						String ProductURL = SFAPI.baseUri +"/query/?q="+URLEncoder.encode("SELECT Id FROM Product2 Where StockKeepingUnit= '"+Home.SelectedArt+"'", "UTF-8");
						Log.e("URL ",ProductURL);
						// Login requests must be POSTs
						HttpGet httpPostgp = new HttpGet(ProductURL);
						httpPostgp.addHeader(SFAPI.oauthHeader);
						httpPostgp.addHeader(SFAPI.prettyPrintHeader);


						HttpResponse responsegp = null;

							// Execute the login POST request
							responsegp = httpclient.execute(httpPostgp);

						// verify response is HTTP OK
						final int statusCode = responsegp.getStatusLine().getStatusCode();
						if (statusCode != HttpStatus.SC_OK) {
						}
						else

						{
							JSONObject prodidjobj=new JSONObject(EntityUtils.toString(responsegp.getEntity()));
							JSONArray jarrpi=prodidjobj.getJSONArray("records");
							for(int ii=0;ii<jarrpi.length();ii++)
							{
								JSONObject pitem = jarrpi.getJSONObject(ii);
								Log.e("PID ",pitem.getString("Id"));
								SelectedProductID=pitem.getString("Id");
								sfprodid=pitem.getString("Id");
								Home.SelectedProductID=SelectedProductID;
							}
						}
						isSuccess=true;
						if(sfprodid.equalsIgnoreCase(""))
						{
							isSuccess=false;
							z="Product Not Found on SalesForce";
						}
						else{
							isSuccess=true;}
					}
				}

			}catch (Exception aaa){	Log.e("URL ", aaa.toString());isSuccess=false;z="Salesforce Session Expired, Please Relogin.";}
			return z;
		}

	}
	/*public class GetEaselsList extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber="";
		String itemdesc="";
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading All Easels ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {

				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();


				final EaselsAdapter   adapter = new EaselsAdapter(getActivity(),Easelss, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			try
			{
				//get locations based on logged in user initials
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String conid=  	settings.getString(preferences.useridd,"");
				String initials=  	settings.getString(preferences.initials,"");
				String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
				String storedusername=  	settings.getString(preferences.username,"");
			*//*	String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+initials+"&&limit=10000";
				Log.e("Locations Url ",locationsapurl);
				HttpGet locations = new HttpGet(locationsapurl);
				locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				locations.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse locationsresponse = httpclient.execute(locations);
				JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
				if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
					JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

					for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
						JSONObject items = locationpayloadarray.getJSONObject(lp);
						String id = items.getString("id");
						String name = items.getString("name");
						//Log.e("Location ID ", id);
						//ignore default bin
						if(name.equalsIgnoreCase(initialsdef)){continue;}*//*
						//get inventory locations only
						//String locationinventoryapurl = SFAPI.advanceprourl + "/api/locationInventory?location_id=" + id;
				        String locationinventoryapurl = SFAPI.advanceprourl + "/api/locationInventory?tag="+ URLEncoder.encode(storedusername,"UTF-8");
						Log.e("LocationsInventory Url ", locationinventoryapurl);

						HttpGet locationInventory = new HttpGet(locationinventoryapurl);
						locationInventory.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
						locationInventory.addHeader(SFAPI.prettyPrintHeader);
						HttpResponse locationInventoryresponse = httpclient.execute(locationInventory);
						JSONObject locationInventoryresponseobject=new JSONObject(EntityUtils.toString(locationInventoryresponse.getEntity()));
						Log.e("resp ", locationInventoryresponseobject.toString());
						if(locationInventoryresponseobject.getString("success").equalsIgnoreCase("true"))
						{
							JSONArray locationInventorypayloadarray = locationInventoryresponseobject.getJSONArray("payload");
							for (int lip = 0; lip < locationInventorypayloadarray.length(); lip++) {
								JSONObject lipitems = locationInventorypayloadarray.getJSONObject(lip);
								String product_id = lipitems.getString("product_id");
								String location_id = lipitems.getString("location_id");
								String location_name = lipitems.getString("location_name");
								Log.e("product id ",product_id);
								//Log.e("location_id ",location_id);
								//Log.e("location_name ",location_name);
								if(location_name.equalsIgnoreCase(initialsdef)){continue;}
								//get products data from product id
								String productsapurl = SFAPI.advanceprourl + "/api/products/" + product_id;
								Log.e("Products Url ", productsapurl);
								HttpGet product = new HttpGet(productsapurl);
								product.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
								product.addHeader(SFAPI.prettyPrintHeader);
								HttpResponse productresponse = httpclient.execute(product);
								JSONObject productobject=new JSONObject(EntityUtils.toString(productresponse.getEntity()));
								if(productobject.getString("success").equalsIgnoreCase("true"))
								{
									JSONObject jsonproductobject=productobject.getJSONObject("payload");
									String productsku = jsonproductobject.getString("sku");
									String productname = jsonproductobject.getString("name");
									String productdescription = jsonproductobject.getString("description");
									String productavailable_quantity = jsonproductobject.getString("available_quantity");

									//Log.e("product sku ",productsku);
									//Log.e("product name ",productname);
									//Log.e("location id ",location_id);
									//Log.e("product quantity ",productavailable_quantity);

									//ignore 0 quantity products
									if(productavailable_quantity.equalsIgnoreCase("")||Float.parseFloat(productavailable_quantity)<1.0)
									{}
									else {
										HashMap<String, String> map = new HashMap<String, String>();
										String art = productsku;

										map.put(Art, art);
										SelectedArt = art;

										map.put(Location, productname);
										SelectedLocation = productname;
										map.put(APProductID, product_id);
										SelectedAPProductID = product_id;
										map.put(FROMLOCATIONID, location_id);
										SelectedFROMLOCATIONID = location_id;
										SelectedQuantity = "";
										map.put(Quantity, "");

										SelectedSponsor = productname;
										map.put(Sponsor, productname);

										SelectedStreetAddress = ""*//*eitems.getString("Address__c")*//*;
										map.put(StreetAddress, "" *//*eitems.getString("Address__c")*//*);


										SelectedCity = ""*//*eitems.getString("City__c")*//*;
										map.put(City, ""*//*eitems.getString("City__c")*//*);


										SelectedProvince = ""*//*eitems.getString("Province__c")*//*;
										map.put(Province, ""*//*eitems.getString("Province__c")*//*);


										SelectedPostCode = ""*//*eitems.getString("Postal_Code__c")*//*;
										map.put(PostalCode, "" *//*eitems.getString("Postal_Code__c")*//*);
										SelectedCharity = "";
										map.put(SelectedCharity, "" *//*eitems.getString("Postal_Code__c")*//*);
										SelectedArtDescription = productname;

										map.put(ArtDescription, SelectedArtDescription);
										SelectedQuantity = productavailable_quantity;
										map.put(Quantity, SelectedQuantity);
										SelectedEaselID = ""*//*easelid*//*;
										map.put(EaselID, SelectedEaselID);
										SelectedEasel = location_name*//*eitems.getString("Name")*//*;
										map.put(Easel, SelectedEasel);
										Easels easels = new Easels();
										easels.setArt(SelectedArt);
										easels.setArtDescription(SelectedArtDescription);
										easels.setCity(SelectedCity);
										easels.setEasel(SelectedEasel);
										easels.setLocation(SelectedLocation);

										easels.setCharity(SelectedCharity);
										easels.setPostalCode(SelectedPostCode);
										easels.setProvince(SelectedProvince);
										easels.setQuantity(SelectedQuantity);
										easels.setSponsor(SelectedSponsor);
										easels.setStreetAddress(SelectedStreetAddress);
										easels.setEaseld(SelectedEaselID);
										easels.setProductId(SelectedProductID);
										easels.setAPProductid(SelectedAPProductID);
										easels.setFROMLOCATIONID(SelectedFROMLOCATIONID);
										//add data to list for listview display
										Easelss.add(easels);
										listing.add(map);
									}
								}
							}
						//}
					//}
				}
				if(listing.isEmpty())
				{
					isSuccess = false;
					z = "No Easels Found";
				}
				else
					{
						isSuccess = true;
					}
			}
			catch (Exception aa)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + aa.toString();

			}
			return z;
			}

		}*/
	public class GetEaselsList extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber="";
		String itemdesc="";
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading All Easels ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {

				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();


				final EaselsAdapter   adapter = new EaselsAdapter(getActivity(),Easelss, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
						adapter.getFilter().filter(arg0);
					}

					@Override
					public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
												  int arg3) {
					}

					@Override
					public void afterTextChanged(Editable arg0) {

					}
				});
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			try
			{
				//get locations based on logged in user initials
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String conid=  	settings.getString(preferences.useridd,"");
				String initials=  	settings.getString(preferences.initials,"");
				String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
				//String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+initials+"&&limit=10000";
				String storedusername=  	settings.getString(preferences.username,"");
				String locationsapurl = SFAPI.advanceprourl + "/api/locations?tag="+ URLEncoder.encode(storedusername,"UTF-8");
				Log.e("Locations Url ",locationsapurl);
				HttpGet locations = new HttpGet(locationsapurl);
				locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				locations.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse locationsresponse = httpclient.execute(locations);
				JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
				if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
					JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

					for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
						JSONObject items = locationpayloadarray.getJSONObject(lp);
						String id = items.getString("id");
						String name = items.getString("name");
						//Log.e("Location ID ", id);
						//ignore default bin
						if(name.equalsIgnoreCase(initialsdef)){continue;}
						//get inventory locations only
						String locationinventoryapurl = SFAPI.advanceprourl + "/api/locationInventory?location_id=" + id;
						Log.e("LocationsInventory Url ", locationinventoryapurl);

						HttpGet locationInventory = new HttpGet(locationinventoryapurl);
						locationInventory.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
						locationInventory.addHeader(SFAPI.prettyPrintHeader);
						HttpResponse locationInventoryresponse = httpclient.execute(locationInventory);
						JSONObject locationInventoryresponseobject=new JSONObject(EntityUtils.toString(locationInventoryresponse.getEntity()));
						Log.e("resp ", locationInventoryresponseobject.toString());
						if(locationInventoryresponseobject.getString("success").equalsIgnoreCase("true"))
						{
							JSONArray locationInventorypayloadarray = locationInventoryresponseobject.getJSONArray("payload");
							for (int lip = 0; lip < locationInventorypayloadarray.length(); lip++) {
								JSONObject lipitems = locationInventorypayloadarray.getJSONObject(lip);
								String product_id = lipitems.getString("product_id");
								String location_id = lipitems.getString("location_id");
								String location_name = lipitems.getString("location_name");
								Log.e("product id ",product_id);
								//Log.e("location_id ",location_id);
								//Log.e("location_name ",location_name);

								//get products data from product id
								String productsapurl = SFAPI.advanceprourl + "/api/products/" + product_id;
								Log.e("Products Url ", locationinventoryapurl);
								HttpGet product = new HttpGet(productsapurl);
								product.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
								product.addHeader(SFAPI.prettyPrintHeader);
								HttpResponse productresponse = httpclient.execute(product);
								JSONObject productobject=new JSONObject(EntityUtils.toString(productresponse.getEntity()));
								if(productobject.getString("success").equalsIgnoreCase("true"))
								{
									JSONObject jsonproductobject=productobject.getJSONObject("payload");
									String productsku = jsonproductobject.getString("sku");
									String productname = jsonproductobject.getString("name");
									String productdescription = jsonproductobject.getString("description");
									String productavailable_quantity = jsonproductobject.getString("available_quantity");

									//Log.e("product sku ",productsku);
									//Log.e("product name ",productname);
									//Log.e("location id ",location_id);
									//Log.e("product quantity ",productavailable_quantity);

									//ignore 0 quantity products
									if(productavailable_quantity.equalsIgnoreCase("")||Float.parseFloat(productavailable_quantity)<1.0)
									{}
									else {
										HashMap<String, String> map = new HashMap<String, String>();
										String art = productsku;

										map.put(Art, art);
										SelectedArt = art;

										map.put(Location, productname);
										SelectedLocation = productname;
										map.put(APProductID, product_id);
										SelectedAPProductID = product_id;
										map.put(FROMLOCATIONID, location_id);
										SelectedFROMLOCATIONID = location_id;
										SelectedQuantity = "";
										map.put(Quantity, "");

										SelectedSponsor = productname;
										map.put(Sponsor, productname);

										SelectedStreetAddress = ""/*eitems.getString("Address__c")*/;
										map.put(StreetAddress, "" /*eitems.getString("Address__c")*/);


										SelectedCity = ""/*eitems.getString("City__c")*/;
										map.put(City, ""/*eitems.getString("City__c")*/);


										SelectedProvince = ""/*eitems.getString("Province__c")*/;
										map.put(Province, ""/*eitems.getString("Province__c")*/);


										SelectedPostCode = ""/*eitems.getString("Postal_Code__c")*/;
										map.put(PostalCode, "" /*eitems.getString("Postal_Code__c")*/);
										SelectedCharity = "";
										map.put(SelectedCharity, "" /*eitems.getString("Postal_Code__c")*/);
										SelectedArtDescription = productname;

										map.put(ArtDescription, SelectedArtDescription);
										SelectedQuantity = productavailable_quantity;
										map.put(Quantity, SelectedQuantity);
										SelectedEaselID = ""/*easelid*/;
										map.put(EaselID, SelectedEaselID);
										SelectedEasel = location_name/*eitems.getString("Name")*/;
										map.put(Easel, SelectedEasel);
										Easels easels = new Easels();
										easels.setArt(SelectedArt);
										easels.setArtDescription(SelectedArtDescription);
										easels.setCity(SelectedCity);
										easels.setEasel(SelectedEasel);
										easels.setLocation(SelectedLocation);

										easels.setCharity(SelectedCharity);
										easels.setPostalCode(SelectedPostCode);
										easels.setProvince(SelectedProvince);
										easels.setQuantity(SelectedQuantity);
										easels.setSponsor(SelectedSponsor);
										easels.setStreetAddress(SelectedStreetAddress);
										easels.setEaseld(SelectedEaselID);
										easels.setProductId(SelectedProductID);
										easels.setAPProductid(SelectedAPProductID);
										easels.setFROMLOCATIONID(SelectedFROMLOCATIONID);
										//add data to list for listview display
										Easelss.add(easels);
										listing.add(map);
									}
								}
							}
						}
					}
				}
				if(listing.isEmpty())
				{
					isSuccess = false;
					z = "No Easels Found";
				}
				else
				{
					isSuccess = true;
				}
			}
			catch (Exception aa)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + aa.toString();

			}
			return z;
		}

	}


	//listview adapter
	public class EaselsAdapter extends BaseAdapter implements Filterable{
		Context context;
		LayoutInflater inflater;
		int pos=-1;
		 RadioButton mSelectedRB;
		 private ValueFilter valueFilter;
		 @SuppressWarnings("unused")
		private ArrayList<Easels> Products;
		 private ArrayList<Easels> mStringFilterList;
		   int mSelectedPosition = -1;
		ArrayList<HashMap<String, String>> listt;
		public EaselsAdapter(Context context,
				ArrayList<Easels> pro,ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			 this.Products = pro;
			    mStringFilterList =  pro;
			    getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Easelss.size();
		}
		@Override
		public Object getItem(int position) {
		    return Easelss.get(position).getArt();
		}
		

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			final View  itemView = inflater.inflate(R.layout.manualproductslist, parent, false);
			if(pos==position)
			{
				itemView.setBackgroundColor(Color.parseColor("#666666"));
			}
			else
			{
				itemView.setBackgroundColor(Color.parseColor("#EEEEEE"));
			}
			ImageView imageView2=(ImageView)itemView.findViewById(R.id.imageView2);
			final TextView itemdescription=(TextView)itemView.findViewById(R.id.itemdescription);
			final TextView itemno=(TextView)itemView.findViewById(R.id.itemno);
			final TextView itemname=(TextView) itemView.findViewById(R.id.itemname);
				imageView2.setVisibility(View.INVISIBLE);
				itemno.setText("" +Easelss.get(position).getEasel());
				itemdescription.setText("" +Easelss.get(position).getLocation());
			itemname.setVisibility(View.VISIBLE);
			itemname.setText("" +Easelss.get(position).getCharity());
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Home.SelectedArt=Easelss.get(position).getArt();
					Home.SelectedArtDescription=Easelss.get(position).getArtDescription();
					Home.SelectedCharity=Easelss.get(position).getCharity();
					Home.SelectedCity=Easelss.get(position).getCity();
					Home.SelectedEasel=Easelss.get(position).getEasel();
					Home.SelectedLocation=Easelss.get(position).getLocation();
					Home.SelectedPostCode=Easelss.get(position).getPostalCode();
					Home.SelectedProvince=Easelss.get(position).getProvince();
					Home.SelectedQuantity=Easelss.get(position).getQuantity();
					Home.SelectedSponsor=Easelss.get(position).getSponsor();
					Home.SelectedStreetAddress=Easelss.get(position).getStreetAddress();
					Home.SelectedEaselID=Easelss.get(position).getEaselid();
					Home.SelectedProductID=Easelss.get(position).getProductid();
					Home.SelectedAPProductID=Easelss.get(position).getAPProductid();
					Home.SelectedFROMLOCATIONID=Easelss.get(position).getFROMLOCATIONID();
					pos=position;
					itemView.setBackgroundColor(Color.parseColor("#666666"));
					//	confirmproductbtn.setVisibility(View.VISIBLE);
					listView.invalidateViews();

					//get details of selected easel from salesforce
					GetDetails getDetails = new GetDetails();
					getDetails.execute("");

				}
			});
			return itemView;}
		
		@Override
		public Filter getFilter() {
		    if(valueFilter==null) {

		        valueFilter=new ValueFilter();
		    }

		    return valueFilter;
		}
		private class ValueFilter extends Filter {

		    //Invoked in a worker thread to filter the data according to the constraint.
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		        FilterResults results=new FilterResults();
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<Easels> filterList=new ArrayList<Easels>();
		        	
		            for(int i=0;i<mStringFilterList.size();i++){
		                if((mStringFilterList.get(i).getEasel().toUpperCase())
		                        .contains(constraint.toString().toUpperCase())) {
		                	Easels easels = new Easels();
		                	easels.setEasel(mStringFilterList.get(i).getEasel());
		                	easels.setLocation(mStringFilterList.get(i).getLocation());
		                	easels.setArt(mStringFilterList.get(i).getArt());
							easels.setArtDescription(mStringFilterList.get(i).getArtDescription());
							easels.setCity(mStringFilterList.get(i).getCity());
							easels.setEasel(mStringFilterList.get(i).getEasel());
							easels.setCharity(mStringFilterList.get(i).getCharity());
							easels.setPostalCode(mStringFilterList.get(i).getPostalCode());
							easels.setProvince(mStringFilterList.get(i).getProvince());
							easels.setQuantity(mStringFilterList.get(i).getQuantity());
							easels.setSponsor(mStringFilterList.get(i).getSponsor());
							easels.setStreetAddress(mStringFilterList.get(i).getStreetAddress());
							easels.setFROMLOCATIONID(mStringFilterList.get(i).getFROMLOCATIONID());
							easels.setAPProductid(mStringFilterList.get(i).getAPProductid());
							easels.setProductId(mStringFilterList.get(i).getProductid());
							easels.setEaseld(mStringFilterList.get(i).getEaselid());
		                    filterList.add(easels);
		                }
		            }
		            results.count=filterList.size();
		            results.values=filterList;
		        }else{
		            results.count=mStringFilterList.size();
		            results.values=mStringFilterList;
		        }
		        return results;
		    }


		    //Invoked in the UI thread to publish the filtering results in the user interface.
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint,
		            FilterResults results) {
		    	Easelss=(ArrayList<Easels>) results.values;
		        notifyDataSetChanged();
		    }
		}
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       
            //setOnBackPressListener();
         
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                	backpressed=true;
            		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new Home()).commit();}
                
                return false;
            }
        });
    }
}
