package com.NavigationFragments;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.GetterSetters.ProductsGetterSetter;
import com.Utils.ConnectionClass;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.Products;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class ManualProductSelectionForNewPicture extends Fragment{
	private ArrayList<Products> Productss = new ArrayList<Products>();
	ArrayList<HashMap<String, String>> listing;
	static String itemno = "itemno";
	static String itemdescription = "itemdescription";
	ConnectionClass connectionClass;
	ListView listView;
	EditText search;
	Button confirmproductbtn;
	static String artno="";
	static String artdes="";
	String newproduct="";
	String oldproduct="";
	RequestListener requestlistener;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 View rootView;
		 rootView = inflater.inflate(R.layout.manual_products_directory, container, false);
		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails,String emailmsg,String subject)
			{
				if(emails.contains(","))
				{
					String arr[] = emails.split(",");
					for(int i=0;i<arr.length;i++)
					{
						String emailaddr=arr[i];
						SendEmailNew(emailmsg,subject,emailaddr);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					SendEmailNew(emailmsg,subject,emails);
					Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
				}
			}
			@Override
			public void onError(String result) {
				Toast.makeText(getActivity(),"Error occured in sending smail ",Toast.LENGTH_LONG).show();
			}
		};
		 confirmproductbtn	 = (Button) rootView.findViewById(R.id.confirm_product_button);	
		 oldproduct=NewEasel.SelectedArt;
		 confirmproductbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					NewEasel.SelectedArt=artno;
					NewEasel.SelectedArtDescription=artdes;
					if(NewEasel.getvaluesfromproducts)
					{
						FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction().addToBackStack(null)
						.replace(R.id.frame_container, new NewEasel()).commit();
					}
					else{
						newproduct=artno;
						preferences pref=new preferences(getActivity());
						pref.SetNewArt(artno);
						pref.SetNewArtDescription(artdes);
						SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
						
					String	userid =settings.getString(preferences.username,"");

					 
					String emailvalues="Easel   : "+NewEasel.SelectedEasel+"\nUser   : "+userid+"\nProduct system says is on easel   : "+oldproduct+"\nProduct you picked   : "+newproduct;
					 
					//SendEmail(emailvalues, "Change product for userid: "+userid);
						GetEmails webservice = new GetEmails(getActivity(), requestlistener,"changeproduct", emailvalues, "Change product for userid: "+userid);
						webservice.execute();
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new EaselConfirmationNew()).commit();
					}
				}
			});
				 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();
				}
			});
		 listing = new ArrayList<HashMap<String, String>>();
		 listView=(ListView)rootView.findViewById(R.id.listview);
			listView.setCacheColorHint(Color.TRANSPARENT);
			connectionClass = new ConnectionClass();
			search=(EditText)rootView.findViewById(R.id.editText1);
			GetProducts getproducts = new GetProducts(); 
			getproducts.execute("");
		 return rootView;
	}
	public class GetProducts extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber="";
		String itemdesc="";


		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading All Products ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {
				
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				
				final ProductsAdapter   adapter = new ProductsAdapter(getActivity(),Productss, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {

		/*	try {
				HttpClient httpclient = new DefaultHttpClient();
				// Assemble the login request URL
				String EaselsURL = SFAPI.baseUri +"/sobjects"+SFAPI.Products
						;
				Log.e("URL ",EaselsURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(EaselsURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;
				try {
					// Execute the login POST request
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

				}
				// verify response is HTTP OK
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					//System.out.println("Error authenticating to Force.com: "+statusCode);
					//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;

				}
				else
				{
					isSuccess=true;
					z ="Success";
					String getResult = null;
					try {
						getResult = EntityUtils.toString(response.getEntity());
						//Log.e("RESULT ",getResult);
						JSONObject jobj=new JSONObject(getResult);
						JSONArray jarr=jobj.getJSONArray("recentItems");
						for(int i=0;i<jarr.length();i++)
						{
							JSONObject eitems=jarr.getJSONObject(i);
							String id=eitems.getString("Id");
							String Name=eitems.getString("Name");

							HttpClient httpclient2 = new DefaultHttpClient();
							// Assemble the login request URL
							String ProductsURL2 = SFAPI.baseUri +"/sobjects"+SFAPI.Products+"/"+id
									;
							Log.e("URL ",ProductsURL2);

							// Login requests must be POSTs
							HttpGet httpPost2 = new HttpGet(ProductsURL2);
							httpPost2.addHeader(SFAPI.oauthHeader);
							httpPost2.addHeader(SFAPI.prettyPrintHeader);


							HttpResponse response2 = null;
							try {
								// Execute the login POST request
								response2 = httpclient2.execute(httpPost2);
							} catch (ClientProtocolException cpException) {
								cpException.printStackTrace();

							} catch (IOException ioException) {
								ioException.printStackTrace();

							}
							// verify response is HTTP OK
							final int statusCode2 = response2.getStatusLine().getStatusCode();
							if (statusCode2 != HttpStatus.SC_OK) {

							}
							else {
								JSONObject jsonObjectt=new JSONObject(EntityUtils.toString(response2.getEntity()));
								String ProductCode=jsonObjectt.getString("ProductCode");
								String Description=jsonObjectt.getString("Description");
								itemnumber=ProductCode;
								itemdesc=Description;
								Log.e("ProductCode ",ProductCode);
								Log.e("Description ",Description);
								String pic="";
								HashMap<String, String> map = new HashMap<String, String>();
								map.put(itemno, ProductCode);
								map.put(itemdescription, Description);
								Products products = new Products();
								products.setItemNo(itemnumber);
								products.setItemDesc(itemdesc);
								products.setPicture(pic);
								products.setNewProductID(id);
								Productss.add(products);
								listing.add(map);

							}
						}
						if(!listing.isEmpty())
						{

							z = "Products Found";
							isSuccess=true;
						}
						else
						{
							z = "No Products Found";
							isSuccess = false;
						}
					} catch (IOException e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
					JSONObject jsonObject = null;

					try {

					} catch (Exception e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
				}



			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}*/

			try
			{
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String conid=  	settings.getString(preferences.useridd,"");
				String initials=  	settings.getString(preferences.initials,"");
				String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
				String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+initials;
				//Log.e("Locations Url ",locationsapurl);
				HttpGet locations = new HttpGet(locationsapurl);
				locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				locations.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse locationsresponse = httpclient.execute(locations);
				JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
				if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
					JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

					for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
						JSONObject items = locationpayloadarray.getJSONObject(lp);
						String id = items.getString("id");
						String name = items.getString("name");
						if(!name.equalsIgnoreCase(initialsdef)){continue;}
						String locationinventoryapurl = SFAPI.advanceprourl + "/api/locationInventory?location_id=" + id;
						//Log.e("LocationsInventory Url ", locationinventoryapurl);

						HttpGet locationInventory = new HttpGet(locationinventoryapurl);
						locationInventory.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
						locationInventory.addHeader(SFAPI.prettyPrintHeader);
						HttpResponse locationInventoryresponse = httpclient.execute(locationInventory);
						JSONObject locationInventoryresponseobject=new JSONObject(EntityUtils.toString(locationInventoryresponse.getEntity()));
						if(locationInventoryresponseobject.getString("success").equalsIgnoreCase("true"))
						{
							JSONArray locationInventorypayloadarray = locationInventoryresponseobject.getJSONArray("payload");
							for (int lip = 0; lip < locationInventorypayloadarray.length(); lip++) {
								JSONObject lipitems = locationInventorypayloadarray.getJSONObject(lip);
								String product_id = lipitems.getString("product_id");
								String location_id = lipitems.getString("location_id");
								String location_name = lipitems.getString("location_name");
								//Log.e("product id ",product_id);
								//Log.e("location_id ",location_id);
								//Log.e("location_name ",location_name);
								String productsapurl = SFAPI.advanceprourl + "/api/products/" + product_id;
								HttpGet product = new HttpGet(productsapurl);
								product.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
								product.addHeader(SFAPI.prettyPrintHeader);
								HttpResponse productresponse = httpclient.execute(product);
								JSONObject productobject=new JSONObject(EntityUtils.toString(productresponse.getEntity()));
								if(productobject.getString("success").equalsIgnoreCase("true"))
								{
									JSONObject jsonproductobject=productobject.getJSONObject("payload");
									String productsku = jsonproductobject.getString("sku");
									String productname = jsonproductobject.getString("name");
									String productdescription = jsonproductobject.getString("description");
									String productavailable_quantity = jsonproductobject.getString("available_quantity");
									Log.e("product sku ",productsku);
									Log.e("product name ",productname);
									Log.e("product description ",productdescription);
									if(productavailable_quantity.equalsIgnoreCase("")||Float.parseFloat(productavailable_quantity)<1.0)
									{}
									else {
										Log.e("product quantity ", productavailable_quantity);
										String art = productsku;
										itemnumber = art;
										itemdesc = productname;
										HashMap<String, String> map = new HashMap<String, String>();
										map.put(itemno, itemnumber);
										map.put(itemdescription, itemdesc);
										map.put("tolocid", location_id);
										map.put("pic", "");
										Log.e("itemno ", itemnumber);
										Log.e("itemdes ", itemdesc);
										Log.e("toloc ", location_id);
										Products products = new Products();
										products.setItemNo(itemnumber);
										products.setItemDesc(itemdesc);
										products.settoLocationId(location_id);
										products.setNewProductID(product_id);
										products.setPicture("");
										Productss.add(products);
										listing.add(map);
									}
								}
							}
						}
					}
				}
				if(listing.isEmpty())
				{
					isSuccess = false;
					z = "No Products Found";
				}
				else
				{
					z = "Products Found";
					isSuccess = true;
				}
			}
			catch (Exception aa)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + aa.toString();

			}

			return z;
		}
	}
	public class ProductsAdapter extends BaseAdapter implements Filterable{
		Context context;
		LayoutInflater inflater;
		 RadioButton mSelectedRB;
		 private ValueFilter valueFilter;
		 @SuppressWarnings("unused")
		private ArrayList<Products> Products;
		 private ArrayList<Products> mStringFilterList;
		   int mSelectedPosition = -1;
		   int pos=-1;
		ArrayList<HashMap<String, String>> listt;
		public ProductsAdapter(Context context,
				ArrayList<Products> pro,ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			 this.Products = pro;
			    mStringFilterList =  pro;
			    getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Productss.size();
		}
		@Override
		public Object getItem(int position) {
		    return Productss.get(position).getItemNo();
		}
		

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			final View  itemView = inflater.inflate(R.layout.manualproductslist, parent, false);
			if(pos==position)
			{
				itemView.setBackgroundColor(Color.parseColor("#666666"));
			}
			else
			{
				itemView.setBackgroundColor(Color.parseColor("#EEEEEE"));
			}
			ImageView imageView2=(ImageView)itemView.findViewById(R.id.imageView2);
			final TextView itemdescription=(TextView)itemView.findViewById(R.id.itemdescription);
			final TextView itemno=(TextView)itemView.findViewById(R.id.itemno);
			final TextView newproductidtv=(TextView)itemView.findViewById(R.id.tonew2productid);
				imageView2.setVisibility(View.INVISIBLE);
				itemno.setText("" +Productss.get(position).getItemNo());
				itemdescription.setText("" +Productss.get(position).getItemDesc());
			newproductidtv.setText(""+Productss.get(position).getNewProductID().toString());
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					pos=position;
					itemView.setBackgroundColor(Color.parseColor("#666666"));
					confirmproductbtn.setVisibility(View.VISIBLE);
					artno=itemno.getText().toString();
					artdes=itemdescription.getText().toString();
					//Home.SelectedNEWAPProductID=Productss.get(position).getNewProductID().toString();
					Home.SelectedNEWAPProductID=newproductidtv.getText().toString();
					listView.invalidateViews();
					GetDetails getDetails = new GetDetails();
					getDetails.execute("");
				}
			});
			return itemView;}
		
		@Override
		public Filter getFilter() {
		    if(valueFilter==null) {

		        valueFilter=new ValueFilter();
		    }

		    return valueFilter;
		}
		private class ValueFilter extends Filter {

		    //Invoked in a worker thread to filter the data according to the constraint.
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		        FilterResults results=new FilterResults();
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<Products> filterList=new ArrayList<Products>();
		        	
		            for(int i=0;i<mStringFilterList.size();i++){
		            	 if((mStringFilterList.get(i).getItemNo().toUpperCase()).contains(constraint.toString().toUpperCase())||mStringFilterList.get(i).getItemDesc().toUpperCase()
			                        .contains(constraint.toString().toUpperCase())) {
		                	Products pro = new Products();
		                    pro.setItemNo(mStringFilterList.get(i).getItemNo());
		                    pro.setItemDesc(mStringFilterList.get(i).getItemDesc());
		                    pro.setNewProductID(mStringFilterList.get(i).getNewProductID());
							 pro.settoLocationId(mStringFilterList.get(i).gettoLocationId());
		                    filterList.add(pro);
		                }
		            }
		            results.count=filterList.size();
		            results.values=filterList;
		        }else{
		            results.count=mStringFilterList.size();
		            results.values=mStringFilterList;
		        }
		        return results;
		    }


		    //Invoked in the UI thread to publish the filtering results in the user interface.
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint,
		            FilterResults results) {
		    	Productss=(ArrayList<Products>) results.values;
		        notifyDataSetChanged();
		    }
		}
	}
	
	@Override
 public void onResume() {
     super.onResume();

     // Call the 'activateApp' method to log an app event for use in analytics and advertising
     // reporting.  Do so in the onResume methods of the primary Activities that an app may be
     // launched into.
     setOnBackPressListener();
     
 }
 private void setOnBackPressListener() {
 	 
     this.getView().setFocusableInTouchMode(true);
     this.getView().requestFocus();
     this.getView().setOnKeyListener(new View.OnKeyListener() {
         @SuppressLint("NewApi")
         @Override
         public boolean onKey(View v, int keyCode, KeyEvent event) {
             if (keyCode == KeyEvent.KEYCODE_BACK) {
            	 NewEasel.getvaluesfromproducts=false;
         		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new NewEasel()).commit();
             }
             return false;
         }
     });
 }
	private void SendEmailNew(final String msg, final String subject,final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
 
 private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	public class GetDetails extends AsyncTask<String, String, String> {
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber = "";
		String itemdesc = "";

		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(getActivity());
			p.setMessage("Loading Details ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if (isSuccess) {
				if(NewEasel.getvaluesfromproducts)
				{
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new NewEasel()).commit();
				}
				else {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new EaselConfirmationNew()).commit();
				}
			} else {
				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME, 0);
				String conid = settings.getString(preferences.useridd, "");

						String ProductURL = SFAPI.baseUri +"/query/?q="+URLEncoder.encode("SELECT Id FROM Product2 Where StockKeepingUnit= '"+artno+"'", "UTF-8");
						Log.e("URL ",ProductURL);
						// Login requests must be POSTs
						HttpGet httpPostgp = new HttpGet(ProductURL);
						httpPostgp.addHeader(SFAPI.oauthHeader);
						httpPostgp.addHeader(SFAPI.prettyPrintHeader);


						HttpResponse responsegp = null;

						// Execute the login POST request
						responsegp = httpclient.execute(httpPostgp);

						// verify response is HTTP OK
						final int statusCode = responsegp.getStatusLine().getStatusCode();
						if (statusCode != HttpStatus.SC_OK) {
						}
						else

						{
							JSONObject prodidjobj=new JSONObject(EntityUtils.toString(responsegp.getEntity()));
							JSONArray jarrpi=prodidjobj.getJSONArray("records");
							for(int ii=0;ii<jarrpi.length();ii++)
							{
								JSONObject pitem = jarrpi.getJSONObject(ii);
								Log.e("PID ",pitem.getString("Id"));
								Home.SelectedNewProductID=pitem.getString("Id");
							}
						}
						isSuccess=true;
						/*if(SelectedProductID.equalsIgnoreCase(""))
						{
							isSuccess=false;
							z="Product Not Found";
						}
						else{
							isSuccess=true;}*/



			}catch (Exception aaa){	Log.e("URL ", aaa.toString());isSuccess=false;z="Salesforce Session Expired, Please Relogin.";}
			return z;
		}

	}
}
	

