package com.NavigationFragments;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.Utils.Easels;
import com.Utils.SFAPI;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import com.Database.Databaseadapter;
import com.GetterSetters.CharitiesGetterSetter;
import com.Utils.Charities;
import com.Utils.ConnectionClass;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CharityDirectory extends Fragment{
	private ArrayList<Charities> Charities = new ArrayList<Charities>();
	ArrayList<HashMap<String, String>> listing;
	static String charityname = "charityname";
	static String charityid = "charityid";
	static String charitylogo = "charitylogo";
	static String charityaddress = "charityaddress";
	static String charitycity = "charitycity";
	static String charityprovince = "charityprovince";
	static String charitypostcode = "charitypostcode";
	static String charitytelephone = "charitytelephone";
	static String charitycontactname = "charitycontactname";
	static String charitycontactphone = "charitycontactphone";
	static String charitycontactcell = "charitycontactcell";
	static String charitycontactemail = "charitycontactemail";
	ConnectionClass connectionClass;
	ListView listView;
	EditText search;
	Databaseadapter db;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.charity_directory, container, false);
		 db=new Databaseadapter(getActivity());
			db.open();
			db.getdata();
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					getActivity().finishAffinity();
					Intent i= new Intent(getActivity(),NavigationActivity.class);
					i.putExtra("val", "");
					startActivity(i);
					FragmentManager fragmentManager = getFragmentManager();
					/*fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();*/
					fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}
			});
		 TextView ch=(TextView)rootView.findViewById(R.id.textView1);
		 ch.setVisibility(View.GONE);
		 ch.setText("");
		 listing = new ArrayList<HashMap<String, String>>();
		 listView=(ListView)rootView.findViewById(R.id.listview);
			listView.setCacheColorHint(Color.TRANSPARENT);
			connectionClass = new ConnectionClass();
			search=(EditText)rootView.findViewById(R.id.editText1);
			if(db.getCount()>1)
			{
				List<CharitiesGetterSetter> charities = db.getcharities(); 
				for (CharitiesGetterSetter charity : charities) 
				{
					HashMap<String, String> map = new HashMap<String, String>();
				
					Charities charityy = new Charities();
					charityy.setId(charity.GetCharityid());
					charityy.setName(charity.GetCharityName());
					charityy.setAddress(charity.GetAddress()+","+charity.GetCity()+","+charity.GetProvince()+","+charity.GetPostCode());
					charityy.setTelephoneNo(charity.GetTelephone());
					charityy.setContactName(charity.GetContactName());
					charityy.setContactPhone(charity.Getcnt_f_tel());
					charityy.setContactCell(charity.GetCellPhone());
					charityy.setContactEmail(charity.GetEmail());
					charityy.setLogo(charity.GetLogo());
				        Charities.add(charityy);
			
					listing.add(map);
				}
				final CharityAdapter   adapter = new CharityAdapter(getActivity(),Charities, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
			GetCharities getcharities = new GetCharities(); 
			getcharities.execute("");
			}
		 return rootView;
	}
	public class GetCharities extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String name = "";
		String id = "";
		String address = "";
		String city = "";
		String province = "";
		String postcode = "";
		String logo = "";
		String telephone = "";
		String cname = "";
		String cphone = "";
		String ccell = "";
		String cemail = "";
		


		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Charity Directory ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {
				
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				
				final CharityAdapter   adapter = new CharityAdapter(getActivity(),Charities, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
		/*		try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						//String query = "select * from dbo.vw_PAS_LiveEasels" ;
						String query = "select * from dbo.vw_PAS_CharityDirectory";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);

						 while (rs.next()) {
							HashMap<String, String> map = new HashMap<String, String>();
							if(rs.getString("CharityID")!=null){
							map.put(charityid, rs.getString("CharityID").trim().toString());
							id=rs.getString("CharityID").trim().toString();
							}
							else
							{
								map.put(charityid, "");
								id="";
							}
							if(rs.getString("Name")!=null){
								map.put(charityname, rs.getString("Name"));
								name=rs.getString("Name");
							}
								else
								{
									map.put(charityname, "");
								name="";
								}
							
							if(rs.getString("Address1")!=null){
								//map.put(charityaddress, rs.getString("Address1"));
								address=rs.getString("Address1");
							}
								else
								{
									address="";
									//map.put(charityaddress, "");
								}
							
							if(rs.getString("City")!=null){
								//map.put(charitycity, rs.getString("City"));
								city=rs.getString("City");
							}
								else
								{
									city="";
									//map.put(charitycity, "");
									
								}
							if(rs.getString("Province")!=null){
								//map.put(charityprovince, rs.getString("Province").trim().toString());
								province=rs.getString("Province").trim().toString();
							}
								else
								{
									province="";
									//map.put(charityprovince, "");
								}
							if(rs.getString("PostCode")!=null){
								//map.put(charitypostcode, rs.getString("PostCode"));
								postcode=rs.getString("PostCode");
							}
								else
								{
									postcode="";
									//map.put(charitypostcode, "");
								}
							map.put(charityaddress, address+","+city+","+province);

							if(rs.getString("Telephone")!=null){
								telephone=rs.getString("Telephone");
							}
								else
								{
									telephone="";
								
								}
							
							if(rs.getString("Contact Name")!=null){
								cname=rs.getString("Contact Name");
							}
								else
								{
									cname="";
								}
							
							if(rs.getString("cnt_f_tel")!=null){
								cphone=rs.getString("cnt_f_tel");
							}
								else
								{
									cphone="";
								}
							
							if(rs.getString("Cellphone")!=null){
								ccell=rs.getString("Cellphone");
							}
								else
								{
									ccell="";
								}
							
							if(rs.getString("EMail")!=null){
								cemail=rs.getString("EMail");
							}
								else
								{
									cemail="";
								}
							
							String pic="";
							if(rs.getBlob("Logo")!=null){
							Blob blob = rs.getBlob("Logo");

							int blobLength = (int) blob.length();  
							byte[] blobAsBytes = blob.getBytes(1, blobLength);
							pic = Base64.encodeToString(blobAsBytes,Base64.NO_WRAP);
							}
							else{pic="";}
					

							Charities charity = new Charities();
							charity.setId(id);
							charity.setName(name);
							charity.setAddress(address+","+city+","+province+","+postcode);
							charity.setTelephoneNo(telephone);
							charity.setContactName(cname);
							charity.setContactPhone(cphone);
							charity.setContactCell(ccell);
							charity.setContactEmail(cemail);
							charity.setLogo(pic);
						        Charities.add(charity);
					
							listing.add(map);
							 db.insertcharities(new CharitiesGetterSetter(id,name,address,city,province,postcode,telephone,cname,cphone,ccell,cemail,pic));
		                    }
						if(!listing.isEmpty())
						{

							z = "Charities Found";
							isSuccess=true;
							
						}
						else
						{
							z = "No Charities Found";
							isSuccess = false;
						}

					}
					if(!con.isClosed())
					{
						con.close();
						con=null;
					}
				}
				catch (Exception ex)
				{
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;*/

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Assemble the login request URL
				String AccountURL = SFAPI.baseUri +"/sobjects"+SFAPI.Account
						;
				Log.e("URL ",AccountURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(AccountURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;
				try {
					// Execute the login POST request
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

				}
				// verify response is HTTP OK
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					//System.out.println("Error authenticating to Force.com: "+statusCode);
					//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;

				}
				else
				{
					isSuccess=true;
					z ="Success";
					String getResult = null;
					try {
						getResult = EntityUtils.toString(response.getEntity());
						//Log.e("RESULT ",getResult);
						JSONObject jobj=new JSONObject(getResult);
						JSONArray jarr=jobj.getJSONArray("recentItems");
						for(int i=0;i<jarr.length();i++)
						{
							JSONObject eitems=jarr.getJSONObject(i);
							String id=eitems.getString("Id");
							String Name=eitems.getString("Name");

							HttpClient httpclient2 = new DefaultHttpClient();
							// Assemble the login request URL
							String AccountURL2 = SFAPI.baseUri +"/sobjects"+SFAPI.Account+"/"+id
									;
							Log.e("URL ",AccountURL2);

							// Login requests must be POSTs
							HttpGet httpPost2 = new HttpGet(AccountURL2);
							httpPost2.addHeader(SFAPI.oauthHeader);
							httpPost2.addHeader(SFAPI.prettyPrintHeader);


							HttpResponse response2 = null;
							try {
								// Execute the login POST request
								response2 = httpclient2.execute(httpPost2);
							} catch (ClientProtocolException cpException) {
								cpException.printStackTrace();

							} catch (IOException ioException) {
								ioException.printStackTrace();

							}
							// verify response is HTTP OK
							final int statusCode2 = response2.getStatusLine().getStatusCode();
							if (statusCode2 != HttpStatus.SC_OK) {

							}
							else {
								JSONObject jsonObjectt=new JSONObject(EntityUtils.toString(response2.getEntity()));

								HashMap<String, String> map = new HashMap<String, String>();
								map.put(charityid, jsonObjectt.getString("Id"));
								id=jsonObjectt.getString("Id");
								map.put(charityname, jsonObjectt.getString("Name"));
								name=jsonObjectt.getString("Name");
								address=jsonObjectt.getString("BillingStreet");
								map.put(charitycity, jsonObjectt.getString("BillingCity"));
								city=jsonObjectt.getString("BillingCity");
								map.put(charityprovince, jsonObjectt.getString("BillingCountry"));
								province=jsonObjectt.getString("BillingCountry");
								map.put(charitypostcode, jsonObjectt.getString("BillingPostalCode"));
								postcode=jsonObjectt.getString("BillingPostalCode");
								map.put(charityaddress, address+","+city+","+province);
								telephone=jsonObjectt.getString("Phone");
								cname=jsonObjectt.getString("Name");
								cphone=jsonObjectt.getString("Phone");
								ccell=jsonObjectt.getString("Phone");
								cemail=jsonObjectt.getString("PersonEmail");
								String pic=jsonObjectt.getString("PhotoUrl");
								Charities charity = new Charities();
								charity.setId(id);
								charity.setName(name);
								charity.setAddress(address+","+city+","+province+","+postcode);
								charity.setTelephoneNo(telephone);
								charity.setContactName(cname);
								charity.setContactPhone(cphone);
								charity.setContactCell(ccell);
								charity.setContactEmail(cemail);
								charity.setLogo(pic);
								Charities.add(charity);

								listing.add(map);
								db.insertcharities(new CharitiesGetterSetter(id,name,address,city,province,postcode,telephone,cname,cphone,ccell,cemail,pic));

							}

						}
					} catch (IOException ioException) {
						ioException.printStackTrace();
					}
					JSONObject jsonObject = null;

					try {

					} catch (Exception jsonException) {
						jsonException.printStackTrace();
					}
				}



			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return z;
		}
	}
	public class CharityAdapter extends BaseAdapter implements Filterable{
		Context context;
		LayoutInflater inflater;
		 RadioButton mSelectedRB;
		 private ValueFilter valueFilter;
		 private ArrayList<Charities> Charities;
		 private ArrayList<Charities> mStringFilterList;
		   int mSelectedPosition = -1;
		ArrayList<HashMap<String, String>> listt;
		public CharityAdapter(Context context,
				ArrayList<Charities> Charities,ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			 this.Charities = Charities;
			    mStringFilterList =  Charities;
			    getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Charities.size();
		}
		@Override
		public Object getItem(int position) {
		    return Charities.get(position).getName();
		}
		

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		public class ViewHolder {
		    TextView tname, tplace,tid;
		    ImageView imageView2;
		    RadioButton radioBtn;
		    String groups;
		}
		@SuppressWarnings("unused")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewHolder holder;
			  holder = new ViewHolder();
		
			View  itemView = inflater.inflate(R.layout.charitylist, parent, false);
			
			ImageView imageView2=(ImageView)itemView.findViewById(R.id.imageView2);
			ImageView imgbox=(ImageView)itemView.findViewById(R.id.imageView1);
			

			final TextView charityid=(TextView)itemView.findViewById(R.id.charityid);
			final TextView charityaddress=(TextView)itemView.findViewById(R.id.charityaddress);
			final TextView charityname=(TextView)itemView.findViewById(R.id.charityname);
			if (imgbox.getTag() == null ||
					!imgbox.getTag().equals(Charities.get(position).getLogo())) {
			try
			{
				//byte[] decodedString = Base64.decode(Charities.get(position).getLogo(), Base64.DEFAULT);
				//BitmapFactory.Options options = new BitmapFactory.Options();
				//options.inPreferredConfig = Config.RGB_565;
				//Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
				//imgbox.setImageBitmap(bmp);
				OkHttpClient client = new OkHttpClient.Builder()
						.addInterceptor(new Interceptor() {
							@Override
							public Response intercept(Chain chain) throws IOException {
								Request newRequest = chain.request().newBuilder()
										.addHeader("Authorization", "OAuth "+SFAPI.loginAccessToken)
										.build();
								return chain.proceed(newRequest);
							}
						})
						.build();
				Picasso picasso = new Picasso.Builder(context)
						.downloader(new OkHttp3Downloader(client))
						.build();
				picasso.load("https://fundinginnovation--majente.my.salesforce.com"+Charities.get(position).getLogo()).into(imgbox);
			}
			catch(Exception t)
			{
				
			}
			}
			else
			{
				imgbox.setTag(Charities.get(position).getLogo());
			}
				imageView2.setVisibility(View.INVISIBLE);
				charityid.setText("" + Charities.get(position).getId());
			
				charityname.setText("" +Charities.get(position).getName());
				charityaddress.setText("" +Charities.get(position).getAddress());
		
			
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
					LayoutInflater inflater = getActivity().getLayoutInflater();
					View convertView = (View) inflater.inflate(R.layout.customdialog_charity, null);
					builder.setView(convertView);
					final AlertDialog alertDialog = builder.create();
					alertDialog.setCancelable(false);
					
					ImageView	dimageView1 = (ImageView) convertView.findViewById(R.id.dimageView1);
					TextView	mtitle = (TextView) convertView.findViewById(R.id.ditemno);
					mtitle.setText(charityname.getText().toString());
					
					TextView	mdes = (TextView) convertView.findViewById(R.id.ditemdescription);
					mdes.setText(charityaddress.getText().toString());
					
					TextView	mtel = (TextView) convertView.findViewById(R.id.ditemtelephone);
					mtel.setText("Tel # : "+Charities.get(position).getTelephoneNo());
					
					TextView	mcname = (TextView) convertView.findViewById(R.id.ditemcname);
					mcname.setText("Name : "+Charities.get(position).getContactName());
					
					TextView	mcphone = (TextView) convertView.findViewById(R.id.ditemcphone);
					mcphone.setText("Phone # : "+Charities.get(position).getContactPhone());
					
					TextView	mccell = (TextView) convertView.findViewById(R.id.ditemccell);
					mccell.setText("Cell # : "+Charities.get(position).getContactCell());
					
					TextView	mcemail = (TextView) convertView.findViewById(R.id.ditemcemail);
					mcemail.setText("Email : "+Charities.get(position).getContactEmail());
					
				/*	try
					{
						byte[] decodedString = Base64.decode(Charities.get(position).getLogo(), Base64.DEFAULT);
						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig = Config.RGB_565;
						Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
						dimageView1.setImageBitmap(bmp);
					}
					catch(Exception t)
					{
						
					}*/
					try
					{
						//byte[] decodedString = Base64.decode(Charities.get(position).getLogo(), Base64.DEFAULT);
						//BitmapFactory.Options options = new BitmapFactory.Options();
						//options.inPreferredConfig = Config.RGB_565;
						//Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
						//imgbox.setImageBitmap(bmp);
						OkHttpClient client = new OkHttpClient.Builder()
								.addInterceptor(new Interceptor() {
									@Override
									public Response intercept(Chain chain) throws IOException {
										Request newRequest = chain.request().newBuilder()
												.addHeader("Authorization", "OAuth "+SFAPI.loginAccessToken)
												.build();
										return chain.proceed(newRequest);
									}
								})
								.build();
						Picasso picasso = new Picasso.Builder(context)
								.downloader(new OkHttp3Downloader(client))
								.build();
						picasso.load("https://fundinginnovation--majente.my.salesforce.com"+Charities.get(position).getLogo()).into(dimageView1);
					}
					catch(Exception t)
					{

					}
					
					
					Button	close = (Button) convertView.findViewById(R.id.okbtn);
					
					close.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
						
							alertDialog.dismiss();
						}
					});
					alertDialog.show();
				}
			});
			return itemView;}
		
		@Override
		public Filter getFilter() {
		    if(valueFilter==null) {

		        valueFilter=new ValueFilter();
		    }

		    return valueFilter;
		}
		private class ValueFilter extends Filter {

		    //Invoked in a worker thread to filter the data according to the constraint.
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		        FilterResults results=new FilterResults();
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<Charities> filterList=new ArrayList<Charities>();
		        	
		            for(int i=0;i<mStringFilterList.size();i++){
		                if((mStringFilterList.get(i).getName().toUpperCase())
		                        .contains(constraint.toString().toUpperCase())) {
		                	Charities contacts = new Charities();
		                    contacts.setName(mStringFilterList.get(i).getName());
		                    contacts.setId(mStringFilterList.get(i).getId());
		                    filterList.add(contacts);
		                }
		            }
		            results.count=filterList.size();
		            results.values=filterList;
		        }else{
		            results.count=mStringFilterList.size();
		            results.values=mStringFilterList;
		        }
		        return results;
		    }


		    //Invoked in the UI thread to publish the filtering results in the user interface.
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint,
		            FilterResults results) {
		        Charities=(ArrayList<Charities>) results.values;
		        notifyDataSetChanged();
		    }
		}
	}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        //setOnBackPressListener();
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressWarnings("unused")
			@SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	boolean move=false;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                
            		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();
                }
                return false;
            }
        });
    }
}
