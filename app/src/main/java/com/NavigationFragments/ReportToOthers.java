package com.NavigationFragments;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.Utils.ConnectionClass;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.Products;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class ReportToOthers extends Fragment{
	ListView listView;
	EditText search;
	Button confirmproductbtn;
	private ArrayList<Products> Productss = new ArrayList<Products>();
	ArrayList<HashMap<String, String>> listing;
	static String itemno = "itemno";
	static String itemdescription = "itemdescription";
	ConnectionClass connectionClass;
	String artno="";
	String artdes="";
	EditText enterdamagedescription;
	Button submit;
	EditText etproductt;
	RequestListener requestlistener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.reporttoother, container, false);
		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails,String emailmsg,String subject)
			{
				if(emails.contains(","))
				{
					String arr[] = emails.split(",");
					for(int i=0;i<arr.length;i++)
					{
						String emailaddr=arr[i];
						SendEmailNew(emailmsg,subject,emailaddr);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					SendEmailNew(emailmsg,subject,emails);
					Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
				}
			}
			@Override
			public void onError(String result) {
				Toast.makeText(getActivity(),"Error occured in sending smail ",Toast.LENGTH_LONG).show();
			}
		};
		 etproductt=(EditText)rootView.findViewById(R.id.etproductt);
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					 preferences p= new preferences(getActivity());
				    	p.SetLastScreen("");
						p.Settotalbids("");
						Databaseadapter db=new Databaseadapter(getActivity());
						db.DeleteRunningTransactions(); 
						getActivity().finishAffinity();
						Intent i= new Intent(getActivity(),NavigationActivity.class);
						i.putExtra("val", "");
						startActivity(i);
					FragmentManager fragmentManager = getFragmentManager();
					/*fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();*/
					fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
/*					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
									fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();*/
				}
			});
			enterdamagedescription=(EditText)rootView.findViewById(R.id.enterdamagedescription);
			enterdamagedescription.setImeOptions(EditorInfo.IME_ACTION_DONE);
			enterdamagedescription.setRawInputType(InputType.TYPE_CLASS_TEXT);
			submit = (Button) rootView
					.findViewById(R.id.confirm_product_button);
		 listing = new ArrayList<HashMap<String, String>>();
		 listView=(ListView)rootView.findViewById(R.id.listview);
			listView.setCacheColorHint(Color.TRANSPARENT);
			connectionClass = new ConnectionClass();
			search=(EditText)rootView.findViewById(R.id.editText1);
			GetProducts getproducts = new GetProducts(); 
			getproducts.execute("");
			submit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
				
					if(enterdamagedescription.getText().toString().equals(""))
					{
						enterdamagedescription.setError("Damage Description is required");
						return;
					}
					if(listView.getVisibility()!=View.VISIBLE)
					{
						
						/*if(etproductt.getText().toString().equals(""))
						{
							etproductt.setError("Product is required");
							return;
						}*/
						artno=etproductt.getText().toString();
					}
					SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
					String uname =pref.getString(preferences.username,"");
					String damagevalues="";
					if(artno.equals(""))
					{
						damagevalues="Damage Explanation  : "+enterdamagedescription.getText().toString()+"\n";
					}
					else
					{
					 damagevalues="Damage Reported For Product  : "+artno+"\nDamage Explanation  : "+enterdamagedescription.getText().toString()+"\n";
					 }
					//SendEmail(damagevalues,"Damage Reported For "+uname);
					//Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();

					GetEmails webservice = new GetEmails(getActivity(), requestlistener,"damagereportedforproduct", damagevalues, "Damage Reported For "+uname);
					webservice.execute();
					
		                	
					getFragmentManager().popBackStack();
		               
				}
			});
	return rootView;	 
	}
	
	public class GetProducts extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber="";
		String itemdesc="";


		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading All Products ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {
				
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				
				final ProductsAdapter   adapter = new ProductsAdapter(getActivity(),Productss, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        	
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
			/*	try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						//String query = "select * from dbo.vw_PAS_LiveEasels" ;
					
						SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
						String uname =pref.getString(preferences.username,"");
						//String query ="select * from dbo.vw_PAS_LiveEasels where AreaManager='" + uname+ "'";
						
						
					    String query = "select distinct item_no,item_desc_1 from dbo.vw_PAS_SalesPersonInventory where usr_id='" + uname + "'"+" order by item_no";
							
						
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						
						 while (rs.next()) {
							HashMap<String, String> map = new HashMap<String, String>();
							if(rs.getString("item_no")!=null){
							map.put(itemno, rs.getString("item_no").trim().toString());
							itemnumber=rs.getString("item_no").trim().toString();
							}
							else
							{
								map.put(itemno, "");
								itemnumber="";
							}
							if(rs.getString("item_desc_1")!=null){
								map.put(itemdescription, rs.getString("item_desc_1"));
								itemdesc=rs.getString("item_desc_1");
							}
								else
								{
									map.put(itemdescription, "");
									itemdesc="";
								}
							
							Products products = new Products();
							products.setItemNo(itemnumber);
							products.setItemDesc(itemdesc);
							
							Productss.add(products);
					
							listing.add(map);
		                    }
						if(!listing.isEmpty())
						{

							z = "Products Found";
							isSuccess=true;
							if(!con.isClosed())
							{
								con.close();
								con=null;
							}
						}
						else
						{
							z = "No Products Found";
							isSuccess = false;
						}

					}
				}
				catch (Exception ex)
				{
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;*/
			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Assemble the login request URL
				String EaselsURL = SFAPI.baseUri +"/sobjects"+SFAPI.Products
						;
				Log.e("URL ",EaselsURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(EaselsURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;
				try {
					// Execute the login POST request
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

				}
				// verify response is HTTP OK
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					//System.out.println("Error authenticating to Force.com: "+statusCode);
					//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;

				}
				else
				{
					isSuccess=true;
					z ="Success";
					String getResult = null;
					try {
						getResult = EntityUtils.toString(response.getEntity());
						//Log.e("RESULT ",getResult);
						JSONObject jobj=new JSONObject(getResult);
						JSONArray jarr=jobj.getJSONArray("recentItems");
						for(int i=0;i<jarr.length();i++)
						{
							JSONObject eitems=jarr.getJSONObject(i);
							String id=eitems.getString("Id");
							String Name=eitems.getString("Name");

							HttpClient httpclient2 = new DefaultHttpClient();
							// Assemble the login request URL
							String ProductsURL2 = SFAPI.baseUri +"/sobjects"+SFAPI.Products+"/"+id
									;
							Log.e("URL ",ProductsURL2);

							// Login requests must be POSTs
							HttpGet httpPost2 = new HttpGet(ProductsURL2);
							httpPost2.addHeader(SFAPI.oauthHeader);
							httpPost2.addHeader(SFAPI.prettyPrintHeader);


							HttpResponse response2 = null;
							try {
								// Execute the login POST request
								response2 = httpclient2.execute(httpPost2);
							} catch (ClientProtocolException cpException) {
								cpException.printStackTrace();

							} catch (IOException ioException) {
								ioException.printStackTrace();

							}
							// verify response is HTTP OK
							final int statusCode2 = response2.getStatusLine().getStatusCode();
							if (statusCode2 != HttpStatus.SC_OK) {

							}
							else {
								JSONObject jsonObjectt=new JSONObject(EntityUtils.toString(response2.getEntity()));
								String ProductCode=jsonObjectt.getString("ProductCode");
								String Description=jsonObjectt.getString("Description");
								itemnumber=ProductCode;
								itemdesc=Description;
								Log.e("ProductCode ",ProductCode);
								Log.e("Description ",Description);
								HashMap<String, String> map = new HashMap<String, String>();
								map.put(itemno, ProductCode);
								map.put(itemdescription, Description);
								Products products = new Products();
								products.setItemNo(itemnumber);
								products.setItemDesc(itemdesc);

								Productss.add(products);

								listing.add(map);
							}
						}
						if(!listing.isEmpty())
						{

							z = "Products Found";
							isSuccess=true;
						}
						else
						{
							z = "No Products Found";
							isSuccess = false;
						}
					} catch (IOException e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
					JSONObject jsonObject = null;

					try {

					} catch (Exception e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
				}



			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return z;
		}
	}
	public class ProductsAdapter extends BaseAdapter implements Filterable{
		Context context;
		LayoutInflater inflater;
		int pos=-1;
		 RadioButton mSelectedRB;
		 private ValueFilter valueFilter;
		 @SuppressWarnings("unused")
		private ArrayList<Products> Products;
		 private ArrayList<Products> mStringFilterList;
		   int mSelectedPosition = -1;
		ArrayList<HashMap<String, String>> listt;
		public ProductsAdapter(Context context,
				ArrayList<Products> pro,ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			 this.Products = pro;
			    mStringFilterList =  pro;
			    getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Productss.size();
		}
		@Override
		public Object getItem(int position) {
		    return Productss.get(position).getItemNo();
		}
		

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			final View  itemView = inflater.inflate(R.layout.manualproductslist, parent, false);
			if(pos==position)
			{
				itemView.setBackgroundColor(Color.parseColor("#666666"));
			}
			else
			{
				itemView.setBackgroundColor(Color.parseColor("#EEEEEE"));
			}
			ImageView imageView2=(ImageView)itemView.findViewById(R.id.imageView2);
			final TextView itemdescription=(TextView)itemView.findViewById(R.id.itemdescription);
			final TextView itemno=(TextView)itemView.findViewById(R.id.itemno);
				imageView2.setVisibility(View.INVISIBLE);
				itemno.setText("" +Productss.get(position).getItemNo());
				itemdescription.setText("" +Productss.get(position).getItemDesc());

			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					pos=position;
					itemView.setBackgroundColor(Color.parseColor("#666666"));
					artno=itemno.getText().toString();
					artdes=itemdescription.getText().toString();
					listView.invalidateViews(); 
				}
			});
			return itemView;}
		
		@Override
		public Filter getFilter() {
		    if(valueFilter==null) {

		        valueFilter=new ValueFilter();
		    }

		    return valueFilter;
		}
		private class ValueFilter extends Filter {

		    //Invoked in a worker thread to filter the data according to the constraint.
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		        FilterResults results=new FilterResults();
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<Products> filterList=new ArrayList<Products>();
		        	
		            for(int i=0;i<mStringFilterList.size();i++){
		                if((mStringFilterList.get(i).getItemNo().toUpperCase()).contains(constraint.toString().toUpperCase())||mStringFilterList.get(i).getItemDesc().toUpperCase()
	                        .contains(constraint.toString().toUpperCase())) {
		                	Products pro = new Products();
		                    pro.setItemNo(mStringFilterList.get(i).getItemNo());
		                    pro.setItemDesc(mStringFilterList.get(i).getItemDesc());
		                    filterList.add(pro);
		                }
		            }
		            results.count=filterList.size();
		            results.values=filterList;
		        }else{
		            results.count=mStringFilterList.size();
		            results.values=mStringFilterList;
		            
		        }
		        return results;
		    }


		    //Invoked in the UI thread to publish the filtering results in the user interface.
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint,
		            FilterResults results) {
		    	Productss=(ArrayList<Products>) results.values;
		        notifyDataSetChanged();
		        if(results.count==0)
		        {
		        	listView.setVisibility(View.INVISIBLE);
		        	
		        	etproductt.setVisibility(View.VISIBLE);
		        }
		        else
		        {
		        	etproductt.setVisibility(View.INVISIBLE);
		        	listView.setVisibility(View.VISIBLE);
		        }
		    }
		}
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmailNew(final String msg, final String subject,final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
}
