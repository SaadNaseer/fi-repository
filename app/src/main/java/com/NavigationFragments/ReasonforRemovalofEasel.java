package com.NavigationFragments;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class ReasonforRemovalofEasel extends Fragment{
	RadioGroup radioreasons;
	EditText providedetails;
	Button next;
	TextView q;
	String reasonselected="";
	boolean other=false;
	RequestListener requestlistener;
	boolean emailsent=false;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final View rootView;

		rootView = inflater.inflate(R.layout.reason_remove_easel, container, false);
		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails,String emailmsg,String subject)
			{
				emailsent=true;
				if(emails.contains(","))
				{
					String arr[] = emails.split(",");
					for(int i=0;i<arr.length;i++)
					{
						String emailaddr=arr[i];
						SendEmailNew(emailmsg,subject,emailaddr);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					SendEmailNew(emailmsg,subject,emails);
					Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
				}
			}
			@Override
			public void onError(String result) {
				Toast.makeText(getActivity(),"Error occured in sending smail ",Toast.LENGTH_LONG).show();
			}
		};

		radioreasons=(RadioGroup)rootView.findViewById(R.id.radioreasons);
		providedetails=(EditText)rootView.findViewById(R.id.providedetails);
		next=(Button)rootView.findViewById(R.id.next);
		q=(TextView)rootView.findViewById(R.id.textView1);
		ImageView imageButton = (ImageView) rootView
				.findViewById(R.id.menuicon);
		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				/*if(!NavigationActivity.draweropened){*/
				NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
				NavigationActivity.draweropened=true;
				/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
			}
		});
		ImageView homeButton = (ImageView) rootView
				.findViewById(R.id.homeicon);
		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if(emailsent)
				{
					preferences p= new preferences(getActivity());
					p.SetLastScreen("");
					p.Settotalbids("");
					Databaseadapter db=new Databaseadapter(getActivity());
					db.DeleteRunningTransactions();
					getActivity().finishAffinity();
					Intent i= new Intent(getActivity(),NavigationActivity.class);
					i.putExtra("val", "");
					startActivity(i);
					FragmentManager fragmentManager = getFragmentManager();
								/*fragmentManager.beginTransaction()
								.replace(R.id.frame_container, new HomeNew()).commit();*/
					fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					return;
				}

				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Cancel ?");
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setMessage("Are you sure you wish to cancel the transaction ?");
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0,
							int arg1) {
						
					}
				});
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
									int arg1) {
								 preferences p= new preferences(getActivity());
							    	p.SetLastScreen("");
									p.Settotalbids("");
									Databaseadapter db=new Databaseadapter(getActivity());
									db.DeleteRunningTransactions(); 
									getActivity().finishAffinity();
									Intent i= new Intent(getActivity(),NavigationActivity.class);
									i.putExtra("val", "");
									startActivity(i);
								FragmentManager fragmentManager = getFragmentManager();
								/*fragmentManager.beginTransaction()
								.replace(R.id.frame_container, new HomeNew()).commit();*/
								fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
							}
						});

				builder.create().show();
			}
		});
		radioreasons.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				int id=group.getCheckedRadioButtonId();
				RadioButton rb=(RadioButton)rootView.findViewById(id);
				rb.setChecked(true);
				final String reason=rb.getText().toString();

				if(reason.equals("Other"))
				{
					providedetails.setVisibility(View.VISIBLE);
					q.setText("Please Provide Detail");
					next.setVisibility(View.VISIBLE);
					next.setText("Submit");
					other=true;
					radioreasons.setVisibility(View.INVISIBLE);
				}
				else
				{
					next.setVisibility(View.VISIBLE);
					next.setText("Submit");
					reasonselected=reason;
				}
				


				


				next.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) 
					{

						if(emailsent)
						{
							Toast.makeText(getActivity(),"Email is already generated",Toast.LENGTH_LONG).show();
							return;
						}
						SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
						
					
						String usernamefull =pref.getString(preferences.username,"");
						if(other)
						{
							 reasonselected=providedetails.getText().toString();
						}
						if(other)
						{
							//SendEmail(RemoveEasel.removeeaselvalues+"\nReason   : "+reasonselected,"Remove easel "+RemoveEasel.selectedeaseltoremove+" for user: "+usernamefull+" with reason: Other");
							GetEmails webservice = new GetEmails(getActivity(), requestlistener,"easelremovalreason", RemoveEasel.removeeaselvalues+"\nReason   : "+reasonselected, "Remove easel "+RemoveEasel.selectedeaseltoremove+" for user: "+usernamefull+" with reason: Other");
							webservice.execute();
						}
						else
						{
						
						//SendEmail(RemoveEasel.removeeaselvalues+"\nReason   : "+reasonselected,"Remove an easel for "+usernamefull);}
						//Toast.makeText(getActivity(), "Email has been sent to Head office.", Toast.LENGTH_LONG).show();
						//SendEmailtoMeghan(RemoveEasel.removeeaselvalues+"\nReason   : "+reasonselected,"Remove an easel for "+usernamefull);
						//SendEmailtoKathy(RemoveEasel.removeeaselvalues+"\nReason   : "+reasonselected,"Remove an easel for "+usernamefull);
						//Toast.makeText(getActivity(), "Email has been sent to Megan.", Toast.LENGTH_LONG).show();
						GetEmails webservice = new GetEmails(getActivity(), requestlistener,"easelremovalreason", RemoveEasel.removeeaselvalues+"\nReason   : "+reasonselected, "Remove an easel for "+usernamefull);
						webservice.execute();
						FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction()
						.replace(R.id.frame_container, new LiveEasels()).commit();

					}

					}
				});

			}
		});
		return rootView;
	}
	@SuppressWarnings("unused")
	public double getscreensize()
	{
		DisplayMetrics metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		float scaleFactor = metrics.density;
		float widthDp = widthPixels / scaleFactor;
		float heightDp = heightPixels / scaleFactor;

		float widthDpi = metrics.xdpi;
		float heightDpi = metrics.ydpi;
		float widthInches = widthPixels / widthDpi;
		float heightInches = heightPixels / heightDpi;
		double diagonalInches = Math.sqrt(
				(widthInches * widthInches) 
				+ (heightInches * heightInches));

		if (diagonalInches >= 10) {
			//Device is a 10" tablet
		} 
		else if (diagonalInches >= 7) {
			//Device is a 7" tablet
		}

		return diagonalInches;
	}
		private void SendEmailNew(final String msg, final String subject,final String recepient) {
			// TODO Auto-generated method stub
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
								EmailCredentials.EMAIL_PASS);

						sender.sendMail(/*"Test "+*/subject, msg,
								EmailCredentials.EMAIL_USER, recepient);
					} catch (Exception e) {
						Log.e("SendMail", e.getMessage(), e);
						Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
					}
				}

			}).start();
		}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmailtoMeghan(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT_MEGAN);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmailtoKathy(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT_KATHY);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
}
