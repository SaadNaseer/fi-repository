package com.NavigationFragments;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.Utils.CalendarDialogBuilder;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;

public class AddnewEasel extends Fragment implements CalendarDialogBuilder.OnDateSetListener{
	//EditText enterneweaselnumber,enterneweaseldescription;
	Button submitneweasel;
	TextView tvclosedate;
	EditText eteasel,etart,etartdesc,etcharity,etsponsor,etstaddress,etcity,etprovince,etpostalcode;
	RequestListener requestlistener;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView;
		rootView = inflater.inflate(R.layout.add_new_easel, container, false);

		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails,String emailmsg,String subject)
			{
				if(emails.contains(","))
				{
					String arr[] = emails.split(",");
					for(int i=0;i<arr.length;i++)
					{
						String emailaddr=arr[i];
						SendEmailNew(emailmsg,subject,emailaddr);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
				}
				else
					{
						SendEmailNew(emailmsg,subject,emails);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
			}
			@Override
			public void onError(String result) {
			Toast.makeText(getActivity(),"Error occured in sending smail ",Toast.LENGTH_LONG).show();
			}
		};

		eteasel=(EditText)rootView.findViewById(R.id.eteasel);
		etart=(EditText)rootView.findViewById(R.id.etart);
		etartdesc=(EditText)rootView.findViewById(R.id.etartdesc);
		etcharity=(EditText)rootView.findViewById(R.id.etcharity);
		etsponsor=(EditText)rootView.findViewById(R.id.etsponsor);
		etstaddress=(EditText)rootView.findViewById(R.id.etstaddress);
		etcity=(EditText)rootView.findViewById(R.id.etcity);
		etprovince=(EditText)rootView.findViewById(R.id.etprovince);
		etpostalcode=(EditText)rootView.findViewById(R.id.etpostalcode);
		tvclosedate=(TextView)rootView.findViewById(R.id.tvclosedate);
		SharedPreferences	settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
		String lasteaselused=  	settings.getString(preferences.lastusedeasel,"");
		if(lasteaselused.equals(""))
		{
			eteasel.setText("");
		}
		else{
			try{
		Pattern digitPattern = Pattern.compile("(\\d+)"); 

		Matcher matcher = digitPattern.matcher(lasteaselused);
		StringBuffer result = new StringBuffer();
		while (matcher.find())
		{
		    matcher.appendReplacement(result, String.valueOf(Integer.parseInt(matcher.group(1)) + 1));
		}
		matcher.appendTail(result);
		//Toast.makeText(getActivity(), result.toString(), Toast.LENGTH_LONG).show();
		eteasel.setText(result.toString());
			}catch(Exception q){eteasel.setText("");}
			}
		tvclosedate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				ShowCalendar();
			}
		});
		/*enterneweaselnumber=(EditText)rootView.findViewById(R.id.enterneweaselnumber);
		enterneweaseldescription=(EditText)rootView.findViewById(R.id.enterneweaseldescription);*/
		ImageView imageButton = (ImageView) rootView
				.findViewById(R.id.menuicon);
		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
				NavigationActivity.draweropened=true;
			}
		});
		ImageView homeButton = (ImageView) rootView
				.findViewById(R.id.homeicon);
		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				/*AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Cancel ?");
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setMessage("Are you sure you wish to cancel the transaction ?");
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0,
							int arg1) {
						
					}
				});
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
									int arg1) {
								 preferences p= new preferences(getActivity());
							    	p.SetLastScreen("");
									p.Settotalbids("");
									Databaseadapter db=new Databaseadapter(getActivity());
									db.DeleteRunningTransactions(); 
								FragmentManager fragmentManager = getFragmentManager();
								fragmentManager.beginTransaction()
								.replace(R.id.frame_container, new HomeNew()).commit();
								fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
							}
						});

				builder.create().show();*/
				 preferences p= new preferences(getActivity());
			    	p.SetLastScreen("");
					p.Settotalbids("");
					Databaseadapter db=new Databaseadapter(getActivity());
					db.DeleteRunningTransactions(); 
					getActivity().finishAffinity();
					Intent i= new Intent(getActivity(),NavigationActivity.class);
					i.putExtra("val", "");
					startActivity(i);
				FragmentManager fragmentManager = getFragmentManager();
				/*fragmentManager.beginTransaction()
				.replace(R.id.frame_container, new HomeNew()).commit();*/
				fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		submitneweasel = (Button) rootView
				.findViewById(R.id.submitneweasel);
		submitneweasel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				/*if(enterneweaselnumber.getText().toString().equals(""))
				{
					enterneweaselnumber.setError("Easel # is required");
					return;
				}
				if(enterneweaseldescription.getText().toString().equals(""))
				{
					enterneweaseldescription.setError("Easel Description is required");
					return;
				}
				String neweaselvalues="New Easel Number   : "+enterneweaselnumber.getText().toString()+"\nNew Easel Description   : "+enterneweaseldescription.getText().toString();
				SendEmail(neweaselvalues,"Add");
				Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.frame_container, new LiveEasels()).commit();*/
				
				if(eteasel.getText().toString().length()==0)
				{
					eteasel.setError("Please Enter Easel First");
					return;
				}
				if(etart.getText().toString().length()==0)
				{
					etart.setError("Please Enter Art Number First");
					return;
				}
				if(etartdesc.getText().toString().length()==0)
				{
					etartdesc.setError("Please Enter Art Description First");
					return;
				}
				if(etcharity.getText().toString().length()==0)
				{
					etcharity.setError("Please Enter Charity Name First");
					return;
				}
				if(etsponsor.getText().toString().length()==0)
				{
					etsponsor.setError("Please Enter Location Name First");
					return;
				}
				if(etstaddress.getText().toString().length()==0)
				{
					etstaddress.setError("Please Enter Street Address First");
					return;
				}
				if(etcity.getText().toString().length()==0)
				{
					etcity.setError("Please Enter City Name First");
					return;
				}
				if(etprovince.getText().toString().length()==0)
				{
					etprovince.setError("Please Enter Province Name First");
					return;
				}
				if(etpostalcode.getText().toString().length()==0)
				{
					etpostalcode.setError("Please Enter Postal Code First");
					return;
				}
				if(tvclosedate.getText().toString().equals("Select Close Date"))
				{
				Toast.makeText(getActivity(), "Select Close Date First", Toast.LENGTH_LONG).show();
					return;
				}
				SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);

				String usernamefull =pref.getString(preferences.username,"");
				String neweaselvalues="Easel   : "+eteasel.getText().toString()+"\nCharity   : "+etcharity.getText().toString()+"\nSponsor   : "+etsponsor.getText().toString()+"\nStreet Address   : "+etstaddress.getText().toString()+"\nCity   : "+etcity.getText().toString()+"\nProvince   : "+etprovince.getText().toString()+"\nPostal Code   : "+etpostalcode.getText().toString()+"\nArt   : "+etart.getText().toString()+"\nArt Description   : "+etartdesc.getText().toString()+"\nClose Date   : "+tvclosedate.getText().toString()+"\nUser ID   : "+usernamefull;



				//call webservice and generate email


				//SendEmail(neweaselvalues,"Add Product for Easel for "+usernamefull);
				//Toast.makeText(getActivity(), "Email has been sent to Head office. You will be contacted with when the Easel and Product is ready for placement", Toast.LENGTH_LONG).show();
				//SendEmailtoMeghan(neweaselvalues,"Add Product for Easel for "+usernamefull);
				//SendEmailtoKathy(neweaselvalues,"Add Product for Easel for "+usernamefull);
				//Toast.makeText(getActivity(), "Email has been sent to Megan. You will be contacted with when the Easel and Product is ready for placement", Toast.LENGTH_LONG).show();
				GetEmails webservice = new GetEmails(getActivity(), requestlistener,"addeasel", neweaselvalues, "Add Product for Easel for "+usernamefull);
				webservice.execute();
				
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().addToBackStack(null)
				.replace(R.id.frame_container, new LiveEasels()).commit();
			}
		});

		return rootView;
	}

	private void SendEmailNew(final String msg, final String subject,final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}

	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmailtoMeghan(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT_MEGAN);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	
	private void SendEmailtoKathy(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT_KATHY);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
      //  setOnBackPressListener();
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressWarnings("unused")
			@SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	boolean move=false;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                
            		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new LiveEasels()).commit();
                }
                return false;
            }
        });
    }
	@Override
	public void onDateSet(int Year, int Month, int Day) {
		// TODO Auto-generated method stub
		tvclosedate.setText(Month+1 + "/" + Day + "/" + Year);
	}
    
    public void ShowCalendar()
    {
    	CalendarDialogBuilder calendar;

	      
        calendar = new CalendarDialogBuilder(getActivity(), this);
    


    calendar.showCalendar();
    }
}
