package com.NavigationFragments;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import com.Database.Databaseadapter;
import com.GetterSetters.RunningtransactionsGetterSetter;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.CommonUtils;
import com.fundinginnovation.LoginActivity;
import com.fundinginnovation.NewEaselConfirmationQuestion;
import com.fundinginnovation.Questionaire;
import com.fundinginnovation.RefreshService;
import com.fundinginnovation.SummaryActivity;
import com.fundinginnovation.TakePictures;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class HomeNew extends Fragment{
	Button homestartmyturns,homeliveeasellocations,homemyinventory,homecharitydirectory,homemarketingmaterials,homeallproducts,homerequestsupplies,homerefreshall,homeexit;
	TextView currentdate;
	Databaseadapter db;
	public static final int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 0;
	boolean permissionFlag = true;
	List<String> permissionDenied;
	SharedPreferences settings;
	preferences  pref;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 View rootView;
		 rootView = inflater.inflate(R.layout.activity_home, container, false);
		  pref=new preferences(getActivity());
		settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
		String baseUri=  	settings.getString(preferences.baseUri,"");
		String loginAccessToken=  	settings.getString(preferences.loginAccessToken,"");
		String initials=  	settings.getString(preferences.initials,"");
		//Toast.makeText(getActivity(),initials,Toast.LENGTH_LONG).show();
		SFAPI.baseUri = baseUri;
		SFAPI.loginAccessToken=loginAccessToken;
		SFAPI.oauthHeader = new BasicHeader("Authorization", "OAuth " + SFAPI.loginAccessToken) ;
		 db=new Databaseadapter(getActivity());
		 db.open();
		 db.getdata();
		preferences pre = new preferences(getActivity());
		pre.SetChangedartno("");
		pre.SetChangedartdes("");
		// NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
		// String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
		 currentdate = (TextView) rootView.findViewById(R.id.currentdate);
		 currentdate.setText("2020-12-02 - Version 9.0");
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					
				}
			});
		  homestartmyturns = (Button) rootView.findViewById(R.id.homestartmyturns);
		  homestartmyturns.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(db.getRunningTransactionsCount()>0)
					{
						List<RunningtransactionsGetterSetter> rt = db.getRunningTransactions(); 
						for (RunningtransactionsGetterSetter runningtransaction : rt) 
						{
							Home.SelectedArt=runningtransaction.GetArt();
							Home.SelectedArtDescription=runningtransaction.GetArtDescription();
							Home.SelectedEasel=runningtransaction.GetEasel();
							Home.SelectedLocation=runningtransaction.GetLocation();
							Home.SelectedProvince=runningtransaction.GetProvince();
							Home.SelectedPostCode=runningtransaction.GetPostalCode();
							Home.SelectedQuantity=runningtransaction.GetQuantity();
							Home.SelectedSponsor=runningtransaction.GetSponsor();
							Home.SelectedCharity=runningtransaction.GetCharity();
							Home.SelectedStreetAddress=runningtransaction.GetStreeAddress();
							Home.SelectedCity=runningtransaction.GetCity();
						}
						SharedPreferences	settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
						String lastscreen=  	settings.getString(preferences.LastScreen,"");
						String bids=  	settings.getString(preferences.TotalBids,"");
						if(lastscreen.equals("easelconfirm"))
						{
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new EaselConfirmation()).commit();
							
						}
						else if(lastscreen.equals("questionaire"))
						{
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new Questionaire()).commit();
						}
						else if(lastscreen.equals("takepictures"))
						{
						Questionaire.zerobids=false;
						Intent i= new Intent(getActivity(),TakePictures.class);
						i.putExtra("total", bids);
						Questionaire.Bids=bids;
						startActivity(i);
						}
						else if(lastscreen.equals("neweaselquestion"))
						{
							Intent intent=new Intent(getActivity(),NewEaselConfirmationQuestion.class);
							startActivity(intent);
						}
						else if(lastscreen.equals("neweaselscreen"))
						{
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new NewEasel()).commit();
						}
						else if(lastscreen.equals("easelconfirmnew"))
						{
							NewEasel.SelectedArt=settings.getString(preferences.NewArt,"");
							NewEasel.SelectedArtDescription=settings.getString(preferences.NewArtDescription,"");
							NewEasel.SelectedEasel=settings.getString(preferences.NewEasel,"");
							NewEasel.SelectedLocation=settings.getString(preferences.NewLocation,"");
							NewEasel.SelectedProvince=settings.getString(preferences.NewProvince,"");
							NewEasel.SelectedPostCode=settings.getString(preferences.NewPostalCode,"");
							NewEasel.SelectedQuantity=settings.getString(preferences.NewQuantity,"");
							NewEasel.SelectedSponsor=settings.getString(preferences.NewSponsor,"");
							NewEasel.SelectedCharity=settings.getString(preferences.NewCharity,"");
							NewEasel.SelectedStreetAddress=settings.getString(preferences.NewStreetAddress,"");
							NewEasel.SelectedCity=settings.getString(preferences.NewCity,"");
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new EaselConfirmationNew()).commit();
						}
						else if(lastscreen.equals("summaryactivity"))
						{
							if(!bids.equals(""))
							{
								Questionaire.zerobids=false;
								Intent i= new Intent(getActivity(),SummaryActivity.class);
								Questionaire.Bids=bids;
								startActivity(i);
							}
							else
							{
								Questionaire.zerobids=true;
								Intent i= new Intent(getActivity(),SummaryActivity.class);
								startActivity(i);
							}
							
							
						}
						else
						{
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new Home()).commit();
						}
					}
					else
					{*/
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new Home()).commit();
					//}
				}
			});
		  homeliveeasellocations = (Button) rootView.findViewById(R.id.homeliveeasellocations);
		  homeliveeasellocations.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new LiveEasels()).commit();
				}
			});
		  homemyinventory = (Button) rootView.findViewById(R.id.homemyinventory);
		  homemyinventory.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new MyInventory()).commit();
				}
			});
		  homecharitydirectory = (Button) rootView.findViewById(R.id.homecharitydirectory);
		  homecharitydirectory.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new CharityDirectory()).commit();
				}
			});
		  homemarketingmaterials = (Button) rootView.findViewById(R.id.homemarketingmaterials);
		  homemarketingmaterials.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new MarketingMaterials()).commit();
				}
			});
		  homeallproducts = (Button) rootView.findViewById(R.id. homeallproducts);
		  homeallproducts.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new AllProducts()).commit();
				}
			});
		  homerequestsupplies = (Button) rootView.findViewById(R.id.homerquestsupplies);
		  homerequestsupplies.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new ContactHeadOfficeMenu()).commit();
				}

				
			});
		  homerefreshall = (Button) rootView.findViewById(R.id.homerefreshall);
		  homerefreshall.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					Toast.makeText(getActivity(), "Data Refresh Started", Toast.LENGTH_LONG).show();
					getActivity().stopService(new Intent(getActivity(), RefreshService.class));
					getActivity().startService(new Intent(getActivity(), RefreshService.class));
				}

				
			});
		  homeexit = (Button) rootView.findViewById(R.id.homeexit);
		  homeexit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					clearValues();
					getActivity().finishAffinity();
				}

				
			});
		 getpermissions();

		//validate salesforce session
		new Login().execute();
		return rootView;
		}
	public class Login extends AsyncTask<String,String,String>
	{
		ProgressDialog p;
		String z = "";
		Boolean isSuccess = false;
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Checking Session ...");
			p.show();
			p.setCancelable(false);
		}
		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			//login advancepro
			new LoginAdvancepro().execute();
			if(isSuccess) {

				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}
			else
			{
				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}
		}
		@Override
		protected String doInBackground(String... params) {
			try {
				HttpClient httpclient = new DefaultHttpClient();
				String loginURL = SFAPI.LOGINURL +
						SFAPI.GRANTSERVICE +
						"&client_id=" + SFAPI.CLIENTID +
						"&client_secret=" + SFAPI.CLIENTSECRET +
						"&username=" + SFAPI.USERNAME +
						"&password=" + SFAPI.PASSWORD;
				Log.e("nfdjfdn",loginURL);
				HttpPost httpPost = new HttpPost(loginURL);
				HttpResponse response = null;
				try {
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();
				}
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;
				}
				else
				{
					isSuccess=true;
					z ="Login Successful";
				}
				String getResult = null;
				try {
					getResult = EntityUtils.toString(response.getEntity());
				} catch (IOException ioException) {
					ioException.printStackTrace();
				}
				JSONObject jsonObject = null;
				try {
					jsonObject = (JSONObject) new JSONTokener(getResult).nextValue();
					SFAPI.loginAccessToken = jsonObject.getString("access_token");
					SFAPI.loginInstanceUrl = jsonObject.getString("instance_url");
				} catch (JSONException jsonException) {
					jsonException.printStackTrace();
				}
				SFAPI.baseUri = SFAPI.loginInstanceUrl + SFAPI.REST_ENDPOINT + SFAPI.API_VERSION ;
				SFAPI.oauthHeader = new BasicHeader("Authorization", "OAuth " + SFAPI.loginAccessToken) ;

				pref.SetbaseUri(SFAPI.baseUri);
				pref.SetloginAccessToken(SFAPI.loginAccessToken);
				pref.SetloginInstanceUrl(SFAPI.loginInstanceUrl);
			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "Invalid User";	}
			return z;
		}
	}
	private void clearValues() {
		// TODO Auto-generated method stub
		preferences pref= new preferences(getActivity());
		pref.SetUsername("");
		pref.SetUserId("");
		pref.Set5digitnumber("");
		pref.SetLastUsedEasel("");
		pref.SetName("");
		pref.SetWareHouse("");
		Home.SelectedArt="";
		Home.SelectedArtDescription="";
		Home.SelectedCharity="";
		Home.SelectedCity="";
		Home.SelectedEasel="";
		Home.SelectedLocation="";
		Home.SelectedPostCode="";
		Home.SelectedProvince="";
		Home.SelectedQuantity="";
		Home.SelectedSponsor="";
		Home.SelectedStreetAddress="";
		NewEasel.SelectedArt="";
		NewEasel.SelectedArtDescription="";
		NewEasel.SelectedCharity="";
		NewEasel.SelectedCity="";
		NewEasel.SelectedEasel="";
		NewEasel.SelectedLocation="";
		NewEasel.SelectedPostCode="";
		NewEasel.SelectedProvince="";
		NewEasel.SelectedQuantity="";
		NewEasel.SelectedSponsor="";
		NewEasel.SelectedStreetAddress="";
		SummaryActivity.SelectedArt="";
		SummaryActivity.SelectedArtDescription="";
		SummaryActivity.SelectedCharity="";
		SummaryActivity.SelectedCity="";
		SummaryActivity.SelectedEasel="";
		SummaryActivity.SelectedLocation="";
		SummaryActivity.SelectedPostCode="";
		SummaryActivity.SelectedProvince="";
		SummaryActivity.SelectedQuantity="";
		SummaryActivity.SelectedSponsor="";
		SummaryActivity.SelectedStreetAddress="";
	}
	@TargetApi(26)
	private void getpermissions() {
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			int hasContactPermission = ActivityCompat.checkSelfPermission(getContext(),android.Manifest.permission.READ_CONTACTS);

			int hasExternalStoragePermission = ActivityCompat.checkSelfPermission(getContext(),android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
			 int hasCameraPermission= ActivityCompat.checkSelfPermission(getContext(),android.Manifest.permission.CAMERA);

			List<String> permissions = new ArrayList<>();

			if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
				permissions.add(android.Manifest.permission.CAMERA);
			}
			if (hasContactPermission != PackageManager.PERMISSION_GRANTED) {
				permissions.add(android.Manifest.permission.READ_CONTACTS);
			}
			if (hasExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
				permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
			}
			if (!permissions.isEmpty()) {
				requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
			} else {
				//runApp();
			}
		} else {
			//runApp();
		}
	}

	@TargetApi(26)
	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		permissionDenied = new ArrayList<String>();
		switch (requestCode) {
			case REQUEST_CODE_SOME_FEATURES_PERMISSIONS: {
				for (int i = 0; i < permissions.length; i++) {
					if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
						Log.d("Permissions", "Permission Granted: " + permissions[i]);
					} else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
						permissionFlag = shouldShowRequestPermissionRationale(permissions[i]);
						permissionDenied.add(CommonUtils.checkPermissionName(permissions[i]));
						Log.d("Permissions", "Permission Denied: " + permissions[i]);
					}
				}
			}
			break;
			default: {
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
			}
		}
		if (permissionDenied != null && permissionDenied.size() > 0) {
			showDialogForDeniedPermissions();
		} else {
			//runApp();
		}
		// runApp();
		return;
	}

	private void showDialogForDeniedPermissions() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK);
		builder.setTitle(getResources().getString(R.string.denied_permissions));
		String[] items = new String[permissionDenied.size()];
		if (permissionFlag) {
			for (int i = 0; i < permissionDenied.size(); i++) {
				items[i] = permissionDenied.get(i);
			}

			builder.setItems(items, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					getpermissions();
					dialog.dismiss();
				}

			});
			builder.setNegativeButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							getpermissions();
							dialog.dismiss();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		} else {
			AlertDialog.Builder builderDialog = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK);
			builderDialog.setTitle(getResources().getString(R.string.denied_permissions_ever
			));
			builderDialog.setMessage("Dear User, In order to use the application you will have to grant permissions from Settings -> Applications -> Application Manager -> Mohafiz -> Permissions");

			builderDialog.setNegativeButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							getActivity().finish();
							System.exit(0);

						}
					});
			AlertDialog alert = builderDialog.create();
			alert.show();
		}
	}
	public class LoginAdvancepro extends AsyncTask<String,String,String> {
		ProgressDialog p;
		String z = "";
		Boolean isSuccess = false;


		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(getActivity());
			p.setMessage("Checking Session ...");
			p.show();
			p.setCancelable(false);
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();

			try
			{
				JSONObject jobj=new JSONObject(r);
				JSONObject jobj2=jobj.getJSONObject("successResonse");
				JSONObject mainobject=new JSONObject(jobj2.toString());
				JSONObject userobject=mainobject.getJSONObject("user");
				JSONObject userrobject=new JSONObject(userobject.toString());
				SFAPI.advanceprouserid=userrobject.getString("userID");
				SFAPI.advanceprotoken=mainobject.getString("token");
				Log.e("User ID ",SFAPI.advanceprouserid);
				Log.e("TOKEN  ",SFAPI.advanceprotoken);
			}
			catch
			(
					Exception a)
			{

			}
			p.dismiss();



		}

		@Override
		protected String doInBackground(String... params) {

			String apurl = SFAPI.advanceprourl + "/api/Authenticate/login";
			Log.e("Url :", apurl);
			//Log.e("currentimagebase64 :",currentimagebase64);
			try {
				//create the JSON object containing the new login details.
				JSONObject aplogin = new JSONObject();
				//aplogin.put("username", "sharplogician");
				//aplogin.put("password", "P35fflaX$&Y1Mg8E");
				aplogin.put("username", "sharplogician2");
				aplogin.put("password", "D3GR@hv#9D6BXx0n");
				Log.e("Req :", aplogin.toString());
				//Construct the objects needed for the request
				HttpClient httpClient2 = new DefaultHttpClient();

				HttpPost httpPost2 = new HttpPost(apurl);
				//httpPost2.addHeader(SFAPI.oauthHeader);
				httpPost2.addHeader(SFAPI.prettyPrintHeader);
				// The message we are going to post
				StringEntity body2 = new StringEntity(aplogin.toString(1));
				body2.setContentType("application/json");
				httpPost2.setEntity(body2);

				//Make the request
				HttpResponse response2 = httpClient2.execute(httpPost2);

				//Process the results
				int statusCode2 = response2.getStatusLine().getStatusCode();
				z=""+ EntityUtils.toString(response2.getEntity());
			} catch (Exception aa) {
			}
			return z;
		}
	}
}
