package com.NavigationFragments;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;
import java.io.File;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Database.Databaseadapter;
import com.GetterSetters.CharitiesGetterSetter;
import com.GetterSetters.ProductsGetterSetter;
import com.NavigationFragments.AllProducts.ProductsAdapter;
import com.Utils.ConnectionClass;
import com.Utils.FTPCredentials;
import com.Utils.Products;
import com.fundinginnovation.R;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class RefreshAll extends Fragment{
	static String charityname = "charityname";
	static String charityid = "charityid";
	static String charitylogo = "charitylogo";
	static String charityaddress = "charityaddress";
	static String charitycity = "charitycity";
	static String charityprovince = "charityprovince";
	static String charitypostcode = "charitypostcode";
	static String charitytelephone = "charitytelephone";
	static String charitycontactname = "charitycontactname";
	static String charitycontactphone = "charitycontactphone";
	static String charitycontactcell = "charitycontactcell";
	static String charitycontactemail = "charitycontactemail";
	static String itemno = "itemno";
	static String itemdescription = "itemdescription";
	ConnectionClass connectionClass;
	Databaseadapter db;
	Boolean isSuccess = false;
	String errormessage="";
	int filecount=1;
	static String NAME = "NAME";
	boolean refresh=false;
	String loadingmessage="";
	ArrayList<HashMap<String, String>> listing;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.refreshall, container, false);
		  listing = new ArrayList<HashMap<String, String>>();
		  GetProducts getproducts = new GetProducts(); 
			 getproducts.execute("");
		// GetCharities getcharities = new GetCharities(); 
		// getcharities.execute("");
	return rootView;	 
	}
	public class DownloadFile extends AsyncTask<String,String,String>
	{
		
		String z = "";
		
		  String replycode="";
		  ProgressDialog p;
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Support Materials ...");
			p.setCancelable(false);
			p.show();
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if(isSuccess) {
				
					//Toast.makeText(getActivity(),"Refresh Succesfull",Toast.LENGTH_SHORT).show();
				
				refresh=false;
			}
			
			else
			{
				//Toast.makeText(getActivity(),"Error in uploading File",Toast.LENGTH_SHORT).show();
				
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
				try {
					isSuccess=false;
					downloadFile(new File(Environment.getExternalStorageDirectory()+File.separator+"Funding_Innovation_1_Pager"+".pdf"));
					
				}
				catch (Exception ex)
				{
					isSuccess = false;
					replycode = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return replycode;
		}
	}
	public void downloadFile(File fileName){
	    
	    
	    FTPClient client = new FTPClient();
	     
	   try {
	        
	       client.connect(FTPCredentials.FTP_HOST,21);
	       client.login(FTPCredentials.FTP_USER, FTPCredentials.FTP_PASS);
	       client.setType(FTPClient.TYPE_BINARY);
	       client.changeDirectory("Marketing_Materials");
	       FTPFile[] list = client.list();
	       
	       for (int i = 0; i < list.length; i++)
	       {
	    	   HashMap<String, String> map = new HashMap<String, String>();
	    	   map.put(NAME, list[i].getName());
	    	   listing.add(map);
	    	   Log.e("NAME ",list[i].getName());
	          //client.download("localFile", new java.io.File("remotefile);
	    	   if(new java.io.File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()).exists())
	    	   {
	    		   if(refresh)
		    		  {
		    			  new java.io.File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()).delete();
		    			  
		    			  client.download(list[i].getName(), new java.io.File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()));     
		    		  }
		    		  else
		    		  {
		    			  
		    		  }
	    	   }
	    	   else
	    	   {
	           client.download(list[i].getName(), new java.io.File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()));    
	    	   }
	    	   
	       }
	       isSuccess=true;
	      // client.download("Funding_Innovation_1_Pager.pdf",fileName, new MyTransferListener());
	        
	   } catch (Exception e) {
	       e.printStackTrace();
	       errormessage=e.toString();
	       isSuccess=false;
	       Log.e("EXCEPTION ",errormessage);
	       try {
	           client.disconnect(true);    
	       } catch (Exception e2) {
	    	   errormessage=e.toString();
	           e2.printStackTrace();
	       }
	   }
	    
	}
	public class GetCharities extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		String name = "";
		String id = "";
		String address = "";
		String city = "";
		String province = "";
		String postcode = "";
		String logo = "";
		String telephone = "";
		String cname = "";
		String cphone = "";
		String ccell = "";
		String cemail = "";
		


		@Override
		protected void onPreExecute() {
		
		}

		@Override
		protected void onPostExecute(String r) {
			
			 
			if(isSuccess) {
				
				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				refresh=true;
				loadingmessage="Refreshing Support Materials ...";
				
				
			}
			else
			{
				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			
			db=new Databaseadapter(getActivity());
			db.open();
			db.getdata();
			db.DeleteCharities();
			connectionClass = new ConnectionClass();
				try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						//String query = "select * from dbo.vw_PAS_LiveEasels" ;
						String query = "select * from dbo.vw_PAS_CharityDirectory";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						
						 while (rs.next()) {
							HashMap<String, String> map = new HashMap<String, String>();
							if(rs.getString("CharityID")!=null){
							map.put(charityid, rs.getString("CharityID").trim().toString());
							id=rs.getString("CharityID").trim().toString();
							}
							else
							{
								map.put(charityid, "");
								id="";
							}
							if(rs.getString("Name")!=null){
								map.put(charityname, rs.getString("Name"));
								name=rs.getString("Name");
							}
								else
								{
									map.put(charityname, "");
								name="";
								}
							
							if(rs.getString("Address1")!=null){
								//map.put(charityaddress, rs.getString("Address1"));
								address=rs.getString("Address1");
							}
								else
								{
									address="";
									//map.put(charityaddress, "");
								}
							
							if(rs.getString("City")!=null){
								//map.put(charitycity, rs.getString("City"));
								city=rs.getString("City");
							}
								else
								{
									city="";
									//map.put(charitycity, "");
									
								}
							if(rs.getString("Province")!=null){
								//map.put(charityprovince, rs.getString("Province").trim().toString());
								province=rs.getString("Province").trim().toString();
							}
								else
								{
									province="";
									//map.put(charityprovince, "");
								}
							if(rs.getString("PostCode")!=null){
								//map.put(charitypostcode, rs.getString("PostCode"));
								postcode=rs.getString("PostCode");
							}
								else
								{
									postcode="";
									//map.put(charitypostcode, "");
								}
							map.put(charityaddress, address+","+city+","+province);
							
							if(rs.getString("Telephone")!=null){
								telephone=rs.getString("Telephone");
							}
								else
								{
									telephone="";
								
								}
							
							if(rs.getString("Contact Name")!=null){
								cname=rs.getString("Contact Name");
							}
								else
								{
									cname="";
								}
							
							if(rs.getString("cnt_f_tel")!=null){
								cphone=rs.getString("cnt_f_tel");
							}
								else
								{
									cphone="";
								}
							
							if(rs.getString("Cellphone")!=null){
								ccell=rs.getString("Cellphone");
							}
								else
								{
									ccell="";
								}
							
							if(rs.getString("EMail")!=null){
								cemail=rs.getString("EMail");
							}
								else
								{
									cemail="";
								}
							
							String pic="";
							if(rs.getBlob("Logo")!=null){
							Blob blob = rs.getBlob("Logo");

							int blobLength = (int) blob.length();  
							byte[] blobAsBytes = blob.getBytes(1, blobLength);
							pic = Base64.encodeToString(blobAsBytes,Base64.NO_WRAP);
							}
							else{pic="";}
					
							
							
							 db.insertcharities(new CharitiesGetterSetter(id,name,address,city,province,postcode,telephone,cname,cphone,ccell,cemail,pic));
		                    }
					

					}
				}
				catch (Exception ex)
				{
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;
		}
	}
	public class GetProducts extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean Success = false;
		String itemnumber="";
		String itemdesc="";

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected void onPostExecute(String r) {
		/*	if(Success) {
				
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}*/
			DownloadFile down = new DownloadFile(); 
			down.execute("");
		}

		@Override
		protected String doInBackground(String... params) {
		
					db=new Databaseadapter(getActivity());
					db.open();
					db.getdata();
					db.DeleteProducts();
					connectionClass = new ConnectionClass();
						try {
							Connection con = connectionClass.CONN();
							if (con == null) {
								z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
							} else {
						String query = "select  Art,Description from dbo.vw_pas_AllItems";
						//,Picture
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						
						 while (rs.next()) {
							 
						
							HashMap<String, String> map = new HashMap<String, String>();
							if(rs.getString("Art")!=null){
							map.put(itemno, rs.getString("Art").trim().toString());
							itemnumber=rs.getString("Art").trim().toString();
							}
							else
							{
								map.put(itemno, "");
								itemnumber="";
							}
							if(rs.getString("Description")!=null){
								map.put(itemdescription, rs.getString("Description"));
								itemdesc=rs.getString("Description");
							}
								else
								{
									map.put(itemdescription, "");
									itemdesc="";
								}
							String pic="";
						/*	if(rs.getBlob("Picture")!=null){
							Blob blob = rs.getBlob("Picture");

							int blobLength = (int) blob.length();  
							byte[] blobAsBytes = blob.getBytes(1, blobLength);
							pic = Base64.encodeToString(blobAsBytes,Base64.NO_WRAP);
							}
							else{pic="";}*/
							db.insertproducts(new ProductsGetterSetter(itemnumber,itemdesc,pic));
		                    
						
Success=true;
					}
							}
						}
				catch (Exception ex)
				{
					Success = false;
				Log.e("",ex.toString());	z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;
				
}
	}
}