package com.NavigationFragments;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Database.Databaseadapter;
import com.Utils.ConnectionClass;
import com.Utils.Easels;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class AddProductToEasel extends Fragment{
	private ArrayList<Easels> Easelss = new ArrayList<Easels>();
	ArrayList<HashMap<String, String>> listing;
	static boolean backpressed=false;
	ConnectionClass connectionClass;
	ListView listView;
	EditText search;
	Button confirmproductbtn;
	String artno="";
	String artdes="";
	 String Art = "Art";
	 String Location = "Location";
	 String Charity = "Charity";
	 String Quantity = "Quantity";
	 String Sponsor = "Sponsor";
	 String StreetAddress = "StreetAddress";
	 String City = "City";
	 String Province = "Province";
	 String PostalCode = "PostalCode";
	 String ArtDescription = "ArtDescription";
	 String Easel = "Easel";
	 public static String SelectedArt = "SelectedArt";
		public  String SelectedLocation = "SelectedLocation";
		public  String SelectedCharity = "SelectedCharity";
		public  String SelectedSponsor = "SelectedSponsor";
		public  String SelectedEasel = "SelectedEasel";
		public  String SelectedQuantity = "SelectedQuantity";
		public  String SelectedStreetAddress = "SelectedStreetAddress";
		public  String SelectedCity = "SelectedCity";
		public  String SelectedProvince= "SelectedProvince";
		public  String SelectedPostCode= "SelectedPostCode";
		public  String SelectedArtDescription="SelectedArtDescription";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.add_product_to_easel, container, false);
 confirmproductbtn	 = (Button) rootView.findViewById(R.id.confirm_product_button);	
		 
		 confirmproductbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new EaselConfirmation()).commit();
					
				}
			});
				 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();
				}
			});
		 listing = new ArrayList<HashMap<String, String>>();
		 listView=(ListView)rootView.findViewById(R.id.listview);
			listView.setCacheColorHint(Color.TRANSPARENT);
			connectionClass = new ConnectionClass();
			search=(EditText)rootView.findViewById(R.id.editText1);
			GetEaselsList geteasels = new GetEaselsList(); 
			geteasels.execute("");
	return rootView;	 
	}
	public class GetEaselsList extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber="";
		String itemdesc="";


		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading All Easels ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {
				
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				
				final EaselsAdapter   adapter = new EaselsAdapter(getActivity(),Easelss, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
			/*	try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
						String uname =pref.getString(preferences.useridd,"");
						String query = "select * from dbo.vw_PAS_LiveEasels where AreaManager='" + uname +  "'";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						
						 while (rs.next()) {
							 HashMap<String, String> map = new HashMap<String, String>();
								if(rs.getString("Art")!=null){
								map.put(Art, rs.getString("Art").trim().toString());
								SelectedArt=rs.getString("Art").trim().toString();
								Log.e("Art ", rs.getString("Art"));}
								else
								{
									map.put(Art, "");
									SelectedArt="";
								}
								if(SelectedArt.equalsIgnoreCase("NULL")||SelectedArt.equalsIgnoreCase("Unknown"))
								{
									
								}
								else
								{
								
									continue;
								}
								
								if(rs.getString("Location")!=null){
									map.put(Location, rs.getString("Location"));
									SelectedLocation=rs.getString("Location").trim().toString();
								}
									else
									{
										map.put(Location, "");
										SelectedLocation="";
									}
								
								if(rs.getString("Charity")!=null){
									map.put(Charity, rs.getString("Charity"));
									SelectedCharity=rs.getString("Charity").trim().toString();
								}
									else
									{
										SelectedCharity="";
										map.put(Charity, "");
									}
								
								if(rs.getString("Quantity")!=null){
									map.put(Quantity, rs.getString("Quantity"));
									SelectedQuantity=rs.getString("Quantity").trim().toString();
								}
									else
									{
										SelectedQuantity="";
										map.put(Quantity, "");
										
									}
								if(rs.getString("Sponsor")!=null){
									map.put(Sponsor, rs.getString("Sponsor").trim().toString());
									SelectedSponsor=rs.getString("Sponsor").trim().toString();
								}
									else
									{
										SelectedSponsor="";
										map.put(Sponsor, "");
									}
								if(rs.getString("Street_Address")!=null){
									map.put(StreetAddress, rs.getString("Street_Address"));
									SelectedStreetAddress=rs.getString("Street_Address").trim().toString();
								}
									else
									{
										SelectedStreetAddress="";
										map.put(StreetAddress, "");
									}
								if(rs.getString("City")!=null){
									map.put(City, rs.getString("City"));
									SelectedCity=rs.getString("City").trim().toString();
								}
									else
									{
										SelectedCity="";
										map.put(City, "");
									}
								if(rs.getString("Province")!=null){
									map.put(Province, rs.getString("Province"));
									SelectedProvince=rs.getString("Province").trim().toString();
								}
									else
									{
										SelectedProvince="";
										map.put(Province, "");
									}
								if(rs.getString("Postcode")!=null){
									map.put(PostalCode, rs.getString("Postcode"));
									SelectedPostCode=rs.getString("Postcode").trim().toString();
								}
									else
									{
										SelectedPostCode="";
										map.put(PostalCode, "");
									}
								if(rs.getString("ArtDescription")!=null){
									map.put(ArtDescription, rs.getString("ArtDescription"));
									SelectedArtDescription=rs.getString("ArtDescription").trim().toString();
								}
									else
									{
										SelectedArtDescription="";
										map.put(ArtDescription, "");
									}
								if(rs.getString("Easel")!=null){
									map.put(Easel, rs.getString("Easel"));
									SelectedEasel=rs.getString("Easel").trim().toString();
								}
									else
									{
										SelectedEasel="";
										map.put(Easel, "");
									}
								
								
								
								Easels easels = new Easels();
								easels.setArt(SelectedArt);
								easels.setArtDescription(SelectedArtDescription);
								easels.setCity(SelectedCity);
								easels.setEasel(SelectedEasel);
								easels.setLocation(SelectedLocation);
								easels.setCharity(SelectedCharity);
								easels.setPostalCode(SelectedPostCode);
								easels.setProvince(SelectedProvince);
								easels.setQuantity(SelectedQuantity);
								easels.setSponsor(SelectedSponsor);
								easels.setStreetAddress(SelectedStreetAddress);
								
								Easelss.add(easels);
								listing.add(map);
			                    }
						 if(!con.isClosed())
						 {
							 con.close();
						 }
							if(!listing.isEmpty())
							{

								z = "Data Found";
								isSuccess=true;
							}
							else
							{
								z = "No Data Found";
								isSuccess = false;
							}

						}
					}
					catch (Exception ex)
					{
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
					}
				
				return z;*/

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Assemble the login request URL
				String EaselsURL = SFAPI.baseUri +"/sobjects"+SFAPI.Easels
						;
				Log.e("URL ",EaselsURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(EaselsURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;
				try {
					// Execute the login POST request
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

				}
				// verify response is HTTP OK
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					//System.out.println("Error authenticating to Force.com: "+statusCode);
					//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;

				}
				else
				{
					isSuccess=true;
					z ="Success";
					String getResult = null;
					try {
						getResult = EntityUtils.toString(response.getEntity());
						//Log.e("RESULT ",getResult);
						JSONObject jobj=new JSONObject(getResult);
						JSONArray jarr=jobj.getJSONArray("recentItems");
						for(int i=0;i<jarr.length();i++)
						{
							JSONObject eitems=jarr.getJSONObject(i);
							String id=eitems.getString("Id");
							String Name=eitems.getString("Name");

							HttpClient httpclient2 = new DefaultHttpClient();
							// Assemble the login request URL
							String ProductsURL2 = SFAPI.baseUri +"/sobjects"+SFAPI.Easels+"/"+id
									;
							Log.e("URL ",ProductsURL2);

							// Login requests must be POSTs
							HttpGet httpPost2 = new HttpGet(ProductsURL2);
							httpPost2.addHeader(SFAPI.oauthHeader);
							httpPost2.addHeader(SFAPI.prettyPrintHeader);


							HttpResponse response2 = null;
							try {
								// Execute the login POST request
								response2 = httpclient2.execute(httpPost2);
							} catch (ClientProtocolException cpException) {
								cpException.printStackTrace();

							} catch (IOException ioException) {
								ioException.printStackTrace();

							}
							// verify response is HTTP OK
							final int statusCode2 = response2.getStatusLine().getStatusCode();
							if (statusCode2 != HttpStatus.SC_OK) {

							}
							else {
								JSONObject jsonObjectt=new JSONObject(EntityUtils.toString(response2.getEntity()));

								HashMap<String, String> map = new HashMap<String, String>();
								String art="NA";

								map.put(Art, art);
								SelectedArt=art;

								map.put(Location, jsonObjectt.getString("Location_name__c"));
								SelectedLocation=jsonObjectt.getString("Location_name__c");

								map.put(Charity, jsonObjectt.getString("Charity_partner__c"));
								SelectedCharity=jsonObjectt.getString("Charity_partner__c");

								SelectedQuantity="";
								map.put(Quantity, "");

								SelectedSponsor=jsonObjectt.getString("Location_name__c");
								map.put(Sponsor, jsonObjectt.getString("Location_name__c"));

								SelectedStreetAddress=jsonObjectt.getString("Address__c");
								map.put(StreetAddress, jsonObjectt.getString("Address__c"));


								SelectedCity=jsonObjectt.getString("City__c");
								map.put(City, jsonObjectt.getString("City__c"));


								SelectedProvince=jsonObjectt.getString("Province__c");
								map.put(Province, jsonObjectt.getString("Province__c"));


								SelectedPostCode=jsonObjectt.getString("Postal_Code__c");
								map.put(PostalCode, jsonObjectt.getString("Postal_Code__c"));

								SelectedArtDescription="NA";
								map.put(ArtDescription, SelectedArtDescription);

								SelectedEasel=jsonObjectt.getString("Name");
								map.put(Easel,SelectedEasel);
								Easels easels = new Easels();
								easels.setArt(SelectedArt);
								easels.setArtDescription(SelectedArtDescription);
								easels.setCity(SelectedCity);
								easels.setEasel(SelectedEasel);
								easels.setLocation(SelectedLocation);
								easels.setCharity(SelectedCharity);
								easels.setPostalCode(SelectedPostCode);
								easels.setProvince(SelectedProvince);
								easels.setQuantity(SelectedQuantity);
								easels.setSponsor(SelectedSponsor);
								easels.setStreetAddress(SelectedStreetAddress);

								Easelss.add(easels);
								listing.add(map);
							}

						}
					} catch (IOException ioException) {
						ioException.printStackTrace();
					}
					JSONObject jsonObject = null;

					try {

					} catch (Exception jsonException) {
						jsonException.printStackTrace();
					}
				}



			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return z;

			}
		}
	public class EaselsAdapter extends BaseAdapter implements Filterable{
		Context context;
		LayoutInflater inflater;
		int pos=-1;
		 RadioButton mSelectedRB;
		 private ValueFilter valueFilter;
		 @SuppressWarnings("unused")
		private ArrayList<Easels> Products;
		 private ArrayList<Easels> mStringFilterList;
		   int mSelectedPosition = -1;
		ArrayList<HashMap<String, String>> listt;
		public EaselsAdapter(Context context,
				ArrayList<Easels> pro,ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			 this.Products = pro;
			    mStringFilterList =  pro;
			    getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Easelss.size();
		}
		@Override
		public Object getItem(int position) {
		    return Easelss.get(position).getArt();
		}
		

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			final View  itemView = inflater.inflate(R.layout.manualproductslist, parent, false);
			if(pos==position)
			{
				itemView.setBackgroundColor(Color.parseColor("#666666"));
			}
			else
			{
				itemView.setBackgroundColor(Color.parseColor("#EEEEEE"));
			}
			ImageView imageView2=(ImageView)itemView.findViewById(R.id.imageView2);
			final TextView itemdescription=(TextView)itemView.findViewById(R.id.itemdescription);
			final TextView itemno=(TextView)itemView.findViewById(R.id.itemno);
				imageView2.setVisibility(View.INVISIBLE);
				itemno.setText("" +Easelss.get(position).getEasel());
				itemdescription.setText("" +Easelss.get(position).getLocation());

			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
					
					
					String username =pref.getString(preferences.username,"");
					String body="Easel Number   : "+Easelss.get(position).getEasel()+"\nLocation   : "+Easelss.get(position).getLocation();
				//	SendEmail(body,"Damaged or missing Easel Barcode for "+username);
					//Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();
					Home.SelectedArt=Easelss.get(position).getArt();
					Home.SelectedArtDescription=Easelss.get(position).getArtDescription();
					Home.SelectedCharity=Easelss.get(position).getCharity();
					Home.SelectedCity=Easelss.get(position).getCity();
					Home.SelectedEasel=Easelss.get(position).getEasel();
					Home.SelectedLocation=Easelss.get(position).getLocation();
					Home.SelectedPostCode=Easelss.get(position).getPostalCode();
					Home.SelectedProvince=Easelss.get(position).getProvince();
					Home.SelectedQuantity=Easelss.get(position).getQuantity();
					Home.SelectedSponsor=Easelss.get(position).getSponsor();
					Home.SelectedStreetAddress=Easelss.get(position).getStreetAddress();
					pos=position;
					itemView.setBackgroundColor(Color.parseColor("#666666"));
				//	confirmproductbtn.setVisibility(View.VISIBLE);
					listView.invalidateViews(); 
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new AddProductSelection()).commit();
				}
			});
			return itemView;}
		
		@Override
		public Filter getFilter() {
		    if(valueFilter==null) {

		        valueFilter=new ValueFilter();
		    }

		    return valueFilter;
		}
		private class ValueFilter extends Filter {

		    //Invoked in a worker thread to filter the data according to the constraint.
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		        FilterResults results=new FilterResults();
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<Easels> filterList=new ArrayList<Easels>();
		        	
		            for(int i=0;i<mStringFilterList.size();i++){
		                if((mStringFilterList.get(i).getEasel().toUpperCase())
		                        .contains(constraint.toString().toUpperCase())) {
		                	Easels easels = new Easels();
		                	easels.setEasel(mStringFilterList.get(i).getEasel());
		                	easels.setLocation(mStringFilterList.get(i).getLocation());
		                	easels.setArt(mStringFilterList.get(i).getArt());
							easels.setArtDescription(mStringFilterList.get(i).getArtDescription());
							easels.setCity(mStringFilterList.get(i).getCity());
							easels.setEasel(mStringFilterList.get(i).getEasel());
							easels.setCharity(mStringFilterList.get(i).getCharity());
							easels.setPostalCode(mStringFilterList.get(i).getPostalCode());
							easels.setProvince(mStringFilterList.get(i).getProvince());
							easels.setQuantity(mStringFilterList.get(i).getQuantity());
							easels.setSponsor(mStringFilterList.get(i).getSponsor());
							easels.setStreetAddress(mStringFilterList.get(i).getStreetAddress());
		                    filterList.add(easels);
		                }
		            }
		            results.count=filterList.size();
		            results.values=filterList;
		        }else{
		            results.count=mStringFilterList.size();
		            results.values=mStringFilterList;
		        }
		        return results;
		    }


		    //Invoked in the UI thread to publish the filtering results in the user interface.
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint,
		            FilterResults results) {
		    	Easelss=(ArrayList<Easels>) results.values;
		        notifyDataSetChanged();
		    }
		}
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       
           // setOnBackPressListener();
         
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                	backpressed=true;
            		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new Home()).commit();}
                
                return false;
            }
        });
    }
}
