package com.NavigationFragments;

import com.Database.Databaseadapter;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class LiveEasels extends Fragment{
	Button liveeasels,addeasel,removeeasel,easelsHistory,addproducttoeasel;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.live_easels_home, container, false);
		 
	
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					 preferences p= new preferences(getActivity());
				    	p.SetLastScreen("");
						p.Settotalbids("");
						Databaseadapter db=new Databaseadapter(getActivity());
						db.DeleteRunningTransactions(); 
						getActivity().finishAffinity();
						Intent i= new Intent(getActivity(),NavigationActivity.class);
						i.putExtra("val", "");
						startActivity(i);
					FragmentManager fragmentManager = getFragmentManager();
					/*fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();*/
					fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				/*	AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									
								}
							});

					builder.create().show();*/
				}
			});
			Button live = (Button) rootView
						.findViewById(R.id.liveeaselbtn);
				live.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction().addToBackStack(null)
						.replace(R.id.frame_container, new LiveEaselLocations()).commit();
					}
				});
				Button addeasel = (Button) rootView
							.findViewById(R.id.addeaselbtn);
				 addeasel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View view) {
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new AddnewEasel()).commit();
						}
					});
				 Button removeeasel = (Button) rootView
								.findViewById(R.id.removeeaselbtn);
					 removeeasel.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {
								FragmentManager fragmentManager = getFragmentManager();
								fragmentManager.beginTransaction().addToBackStack(null)
								.replace(R.id.frame_container, new RemoveEasel()).commit();
							}
						});
					 Button easelsHistory = (Button) rootView
								.findViewById(R.id.easelsHistory);
					 easelsHistory.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {
								FragmentManager fragmentManager = getFragmentManager();
								fragmentManager.beginTransaction().addToBackStack(null)
								.replace(R.id.frame_container, new EaselHistory()).commit();
								EaselHistory.backpressed=false;
							}
						});
					 
					 addproducttoeasel= (Button) rootView
								.findViewById(R.id.addproducttoeasel);
					 
					 addproducttoeasel.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {
							/*	FragmentManager fragmentManager = getFragmentManager();
								fragmentManager.beginTransaction().addToBackStack(null)
								.replace(R.id.frame_container, new AddProductToEasel()).commit();
								EaselHistory.backpressed=false;*/
								Toast.makeText(getActivity(),"Not Available",Toast.LENGTH_LONG).show();
							}
						});
					 
		 return rootView;
	}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       // setOnBackPressListener();
        
    }
    @SuppressWarnings("unused")
	private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	boolean move=false;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                	 if(EaselHistory.backpressed)
                     {
                		 EaselHistory.backpressed=false;
                     	
                     }
                	 else{
            		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();}
                }
                return false;
            }
        });
    }
}
