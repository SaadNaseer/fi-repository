package com.NavigationFragments;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Database.Databaseadapter;
import com.Utils.ConnectionClass;
import com.Utils.Inventory;
import com.Utils.SFAPI;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import com.Utils.preferences;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class MyInventory extends Fragment{
	private ArrayList<Inventory> Inventories = new ArrayList<Inventory>();
	ArrayList<HashMap<String, String>> listing;
	static String Easel = "Easel";
	static String Artno = "Artno";
	static String ArtDes = "ArtDes";
	static String ArtQty = "ArtQty";
	TextView linktoallproducts;
	ConnectionClass connectionClass;
	ListView listView;
	EditText search;
	TextView inventory;
	Button reportdamages;
	static boolean myinventory=false;
	Button scaninventory;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.inventory_menu, container, false);
		 scaninventory= (Button) rootView.findViewById(R.id.scaninventory);
		 scaninventory.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					myinventory=true;
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new ScanInventory()).commit();
				}
			});
		 linktoallproducts= (TextView) rootView.findViewById(R.id.linktoallproducts);
		 reportdamages= (Button) rootView.findViewById(R.id.reportdamages);
		 SpannableString content = new SpannableString("Looking For Products ?");
		 content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		 linktoallproducts.setText(content);
		 linktoallproducts.setTextColor(Color.parseColor("#0645AD"));
		 reportdamages.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					myinventory=true;
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new ReportDamages()).commit();
				}
			});
		 linktoallproducts.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new AllProducts()).commit();
				}
			});
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					preferences p= new preferences(getActivity());
			    	p.SetLastScreen("");
					p.Settotalbids("");
					Databaseadapter db=new Databaseadapter(getActivity());
					db.DeleteRunningTransactions();
					getActivity().finishAffinity();
					Intent i= new Intent(getActivity(),NavigationActivity.class);
					i.putExtra("val", "");
					startActivity(i);
					FragmentManager fragmentManager = getFragmentManager();
					/*fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();*/
					fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				/*	AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									
								}
							});

					builder.create().show();*/
				}
			});
		 listing = new ArrayList<HashMap<String, String>>();
		 listView=(ListView)rootView.findViewById(R.id.listview);
		 inventory=(TextView)rootView.findViewById(R.id.textView1);
		 inventory.setText("");
			listView.setCacheColorHint(Color.TRANSPARENT);
			connectionClass = new ConnectionClass();
			search=(EditText)rootView.findViewById(R.id.editText1);
		Inventories.clear();
		listing.clear();
		    GetInventoryAdvancepro getinventory = new GetInventoryAdvancepro();
			getinventory.execute("");
		 return rootView;
	}
	public class GetInventoryAdvancepro extends AsyncTask<String,String,String> {
		ProgressDialog p;
		String z = "";
		Boolean isSuccess = false;
		String artnum = "";
		String easel = "";
		String artdesc = "";
		String city = "";
		String province = "";
		String postcode = "";
		String logo = "";
		String qtyonhand = "";

		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(getActivity());
			p.setMessage("Loading Inventory ...");
			p.show();
			p.setCancelable(false);
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();
				Log.e("result",r);

			p.dismiss();

			if(isSuccess) {

				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();


				final InventoryAdapter   adapter = new InventoryAdapter(getActivity(),Inventories, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
						adapter.getFilter().filter(arg0);
					}

					@Override
					public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
												  int arg3) {
					}

					@Override
					public void afterTextChanged(Editable arg0) {

					}
				});
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}


		}

		@Override
		protected String doInBackground(String... params) {

			String apurl = SFAPI.advanceprourl + "/api/"+SFAPI.locationInventory;
			Log.e("Url :", apurl);
			//Log.e("currentimagebase64 :",currentimagebase64);
			try {
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String conid=  	settings.getString(preferences.useridd,"");
				String initials=  	settings.getString(preferences.initials,"");
				String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
				String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+initials;
				//Log.e("Locations Url ",locationsapurl);
				HttpGet locations = new HttpGet(locationsapurl);
				locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				locations.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse locationsresponse = httpclient.execute(locations);
				JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
				if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
					JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

					for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
						JSONObject items = locationpayloadarray.getJSONObject(lp);
						String id = items.getString("id");
						String name = items.getString("name");
						if(!name.equalsIgnoreCase(initialsdef)){continue;}
						String locationinventoryapurl = SFAPI.advanceprourl + "/api/locationInventory?location_id=" + id;
						//Log.e("LocationsInventory Url ", locationinventoryapurl);

						HttpGet locationInventory = new HttpGet(locationinventoryapurl);
						locationInventory.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
						locationInventory.addHeader(SFAPI.prettyPrintHeader);
						HttpResponse locationInventoryresponse = httpclient.execute(locationInventory);
						JSONObject locationInventoryresponseobject=new JSONObject(EntityUtils.toString(locationInventoryresponse.getEntity()));
						if(locationInventoryresponseobject.getString("success").equalsIgnoreCase("true"))
						{
							JSONArray locationInventorypayloadarray = locationInventoryresponseobject.getJSONArray("payload");
							for (int lip = 0; lip < locationInventorypayloadarray.length(); lip++) {
								JSONObject lipitems = locationInventorypayloadarray.getJSONObject(lip);
								String product_id = lipitems.getString("product_id");
								String location_id = lipitems.getString("location_id");
								String location_name = lipitems.getString("location_name");
								//Log.e("product id ",product_id);
								//Log.e("location_id ",location_id);
								//Log.e("location_name ",location_name);
								String productsapurl = SFAPI.advanceprourl + "/api/products/" + product_id;
								HttpGet product = new HttpGet(productsapurl);
								product.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
								product.addHeader(SFAPI.prettyPrintHeader);
								HttpResponse productresponse = httpclient.execute(product);
								JSONObject productobject=new JSONObject(EntityUtils.toString(productresponse.getEntity()));
								if(productobject.getString("success").equalsIgnoreCase("true"))
								{
									JSONObject jsonproductobject=productobject.getJSONObject("payload");
									String productsku = jsonproductobject.getString("sku");
									String productname = jsonproductobject.getString("name");
									String productdescription = jsonproductobject.getString("description");
									String productavailable_quantity = jsonproductobject.getString("available_quantity");
									Log.e("product sku ",productsku);
									Log.e("product name ",productname);
									Log.e("product description ",productdescription);
									Log.e("product quantity ",productavailable_quantity);
									String art=productsku;
									HashMap<String, String> map = new HashMap<String, String>();
									map.put(Artno, art);
									artnum=art;
									map.put(ArtDes, productname);
									artdesc=productname;
									map.put(ArtQty, productavailable_quantity);
									qtyonhand=productavailable_quantity;
									Inventory inv = new Inventory();
									//inv.setEasel(easel);
									inv.setArtNumber(artnum);
									inv.setArtDes(artdesc);
									inv.setArtQty(qtyonhand);

									Inventories.add(inv);

									listing.add(map);

								}
							}
						}
					}
				}

			} catch (Exception aa) {
			}


			if(!listing.isEmpty())
			{

				z = "Inventory Found";
				isSuccess=true;

			}
			else
			{
				z = "No Inventory Found";
				isSuccess = false;
			}
			return z;
		}
	}
	public class GetInventory extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String artnum = "";
		String easel = "";
		String artdesc = "";
		String city = "";
		String province = "";
		String postcode = "";
		String logo = "";
		String qtyonhand = "";


		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Inventory Menu ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			
			if(isSuccess) {
				
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				
				final InventoryAdapter   adapter = new InventoryAdapter(getActivity(),Inventories, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
				try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
						
					String	userid =settings.getString(preferences.username,"");
						String query = "select distinct item_no,item_desc_1,qty_on_hand from dbo.vw_PAS_SalesPersonInventory where qty_on_hand>=1 and usr_id='" + userid + "'"+" order by item_no";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						
						 while (rs.next()) {
							HashMap<String, String> map = new HashMap<String, String>();
							if(rs.getString("item_no")!=null){
							map.put(Artno, rs.getString("item_no").trim().toString());
							artnum=rs.getString("item_no").trim().toString();
							}
							else
							{
								map.put(Artno, "");
								artnum="";
							}
							if(rs.getString("item_desc_1")!=null){
								map.put(ArtDes, rs.getString("item_desc_1"));
								artdesc=rs.getString("item_desc_1");
							}
								else
								{
									map.put(ArtDes, "");
									artdesc="";
								}
							if(rs.getString("qty_on_hand")!=null){
								map.put(ArtQty, rs.getString("qty_on_hand"));
								qtyonhand=rs.getString("qty_on_hand");
							}
								else
								{
									map.put(ArtQty, "");
									qtyonhand="";
								}
							
							
							
						/*	if(rs.getString("ArtDescription")!=null){
								map.put(ArtDes, rs.getString("ArtDescription"));
								artdesc=rs.getString("ArtDescription");
							}
								else
								{
									artdesc="";
									map.put(ArtDes, "");
								}*/
							
							
							
							Inventory inv = new Inventory();
							//inv.setEasel(easel);
							inv.setArtNumber(artnum);
							inv.setArtDes(artdesc);
							inv.setArtQty(qtyonhand);
							
						        Inventories.add(inv);
					
							listing.add(map);
		                    }
						if(!listing.isEmpty())
						{

							z = "Inventory Found";
							isSuccess=true;
							
						}
						else
						{
							z = "No Inventory Found";
							isSuccess = false;
						}

					}
					if(!con.isClosed())
					{
						con.close();
						con=null;
					}
				}
				catch (Exception ex)
				{
					
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;
		}
	}
	public class InventoryAdapter extends BaseAdapter implements Filterable{
		Context context;
		LayoutInflater inflater;
		 RadioButton mSelectedRB;
		 private ValueFilter valueFilter;
		 private ArrayList<Inventory> Inventories;
		 private ArrayList<Inventory> mStringFilterList;
		   int mSelectedPosition = -1;
		ArrayList<HashMap<String, String>> listt;
		public InventoryAdapter(Context context,
				ArrayList<Inventory> inventoriess,ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			 this.Inventories = inventoriess;
			    mStringFilterList =  inventoriess;
			    getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Inventories.size();
		}
		@Override
		public Object getItem(int position) {
		    return Inventories.get(position).getEasel();
		}
		

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		public class ViewHolder {
		    TextView tname, tplace,tid;
		    ImageView imageView2;
		    RadioButton radioBtn;
		    String groups;
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewHolder holder;
			  holder = new ViewHolder();
		
			View  itemView = inflater.inflate(R.layout.inventorylist, parent, false);
			
			ImageView imageView2=(ImageView)itemView.findViewById(R.id.imageView2);
	

			
			final TextView easel=(TextView)itemView.findViewById(R.id.easel);
			final TextView art=(TextView)itemView.findViewById(R.id.artdetails);
			final TextView qty=(TextView)itemView.findViewById(R.id.artqty);
		
		
			
				imageView2.setVisibility(View.INVISIBLE);
				//charityid.setText("" + Inventories.get(position).getId());
			
				//easel.setText("Easel " +Inventories.get(position).getEasel());
				easel.setText(Html.fromHtml("<b>"+"Art # : " + "</b> "+Inventories.get(position).getArtNumber()));
				
				art.setText(Html.fromHtml("<b>"+"Art Description : " + "</b> "+Inventories.get(position).getArtDes()));
		
				qty.setText(Html.fromHtml("<b>"+"Qty : " + "</b> "+Inventories.get(position).getArtQty()));
			
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
	
				}
			});
			return itemView;}
		
		@Override
		public Filter getFilter() {
		    if(valueFilter==null) {

		        valueFilter=new ValueFilter();
		    }

		    return valueFilter;
		}
		private class ValueFilter extends Filter {

		    //Invoked in a worker thread to filter the data according to the constraint.
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		        FilterResults results=new FilterResults();
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<Inventory> filterList=new ArrayList<Inventory>();
		        	
		            for(int i=0;i<mStringFilterList.size();i++){
		                if((mStringFilterList.get(i).getArtNumber().toUpperCase())
		                        .contains(constraint.toString().toUpperCase())) {
		                	Inventory inventory = new Inventory();
		                	inventory.setEasel("");
		                	inventory.setArtNumber(mStringFilterList.get(i).getArtNumber());
		                	inventory.setArtDes(mStringFilterList.get(i).getArtDes());
		                    filterList.add(inventory);
		                }
		            }
		            results.count=filterList.size();
		            results.values=filterList;
		        }else{
		            results.count=mStringFilterList.size();
		            results.values=mStringFilterList;
		        }
		        return results;
		    }


		    //Invoked in the UI thread to publish the filtering results in the user interface.
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint,
		            FilterResults results) {
		    	Inventories=(ArrayList<Inventory>) results.values;
		        notifyDataSetChanged();
		    }
		}
	}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
      //  setOnBackPressListener();
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	boolean move=false;
            	 if(ScanInventory.backpressed)
                 {
            		 ScanInventory.backpressed=false;
                 	
                 }
                 else{
             		FragmentManager fragmentManager = getFragmentManager();
 					fragmentManager.beginTransaction()
 					.replace(R.id.frame_container, new HomeNew()).commit();}
                 
              
                return false;
            }
        });
    }
}
