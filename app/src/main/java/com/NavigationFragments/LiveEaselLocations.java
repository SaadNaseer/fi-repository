package com.NavigationFragments;

import com.Database.Databaseadapter;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class LiveEaselLocations extends Fragment{
	private ProgressDialog progDailog; 
	 WebView w;
	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
			 View rootView;
			 rootView = inflater.inflate(R.layout.live_easels, container, false);
			 ImageView imageButton = (ImageView) rootView
						.findViewById(R.id.menuicon);
				imageButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						/*if(!NavigationActivity.draweropened){*/
							NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
							NavigationActivity.draweropened=true;
						/*}
						else{
							NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
							NavigationActivity.draweropened=false;
						}*/
					}
				});
				ImageView homeButton = (ImageView) rootView
						.findViewById(R.id.homeicon);
				homeButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getActivity());
						builder.setTitle("Cancel ?");
						builder.setIcon(android.R.drawable.ic_dialog_alert);
						builder.setMessage("Are you sure you wish to cancel the transaction ?");
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
									int arg1) {
								
							}
						});
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										 preferences p= new preferences(getActivity());
									    	p.SetLastScreen("");
											p.Settotalbids("");
											Databaseadapter db=new Databaseadapter(getActivity());
											db.DeleteRunningTransactions(); 
											getActivity().finishAffinity();
											Intent i= new Intent(getActivity(),NavigationActivity.class);
											i.putExtra("val", "");
											startActivity(i);
										FragmentManager fragmentManager = getFragmentManager();
										/*fragmentManager.beginTransaction()
										.replace(R.id.frame_container, new HomeNew()).commit();*/
										fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
									}
								});

						builder.create().show();
					}
				});
				SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String username =pref.getString(preferences.username,"");
			 String url ="http://fundinginnovation.ca/easelmap/index.html?user="+username;
				if(isNetworkAvailable()==true) 
				{
			   
				progDailog = ProgressDialog.show(getActivity(), "Loading Live Easels","Please wait...", true);
		     progDailog.setCancelable(false);
			     w = (WebView)rootView. findViewById(R.id.webview);
			    w.getSettings().setJavaScriptEnabled(true);     
			       w.getSettings().setLoadWithOverviewMode(true);
			       w.getSettings().setUseWideViewPort(true);   
			       w.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);  
			    w.setWebViewClient(new WebViewClient(){

		         @Override
		         public boolean shouldOverrideUrlLoading(WebView view, String url) {
		             progDailog.show();
		             view.loadUrl(url);

		             return true;                
		         }
		         @Override
		         public void onPageFinished(WebView view, final String url) {
		             progDailog.dismiss();
		         }
		     });

		     w.loadUrl(url);}
				else{
					AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
					dialog.setTitle("No Internet Connection!");
					
					dialog.setMessage("Live Easels could not be loaded because your internet connection is weak ");
					dialog.setCancelable(false);
					
					dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							
							
						}
					});
					dialog.show();
				}
			return rootView; 
	 }
		private boolean isNetworkAvailable() {
		    ConnectivityManager connectivityManager 
		          = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
		} 
		@Override
	    public void onResume() {
	        super.onResume();

	        // Call the 'activateApp' method to log an app event for use in analytics and advertising
	        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
	        // launched into.
	        //setOnBackPressListener();
	        
	    }
	    private void setOnBackPressListener() {
	    	 
	        this.getView().setFocusableInTouchMode(true);
	        this.getView().requestFocus();
	        this.getView().setOnKeyListener(new View.OnKeyListener() {
	            @SuppressWarnings("unused")
				@SuppressLint("NewApi")
	            @Override
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	            	boolean move=false;
	                if (keyCode == KeyEvent.KEYCODE_BACK) {

	                	FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction()
						.replace(R.id.frame_container, new LiveEasels()).commit();
	                }
	                return false;
	            }
	        });
	    }
}
