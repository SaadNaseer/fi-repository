package com.NavigationFragments;

import java.util.List;

import com.Database.Databaseadapter;
import com.GetterSetters.RunningtransactionsGetterSetter;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.preferences;
import com.fundinginnovation.NewEaselConfirmationQuestion;
import com.fundinginnovation.Questionaire;
import com.fundinginnovation.SummaryActivity;
import com.fundinginnovation.TakePictures;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EaselRemovalError extends Fragment{
	TextView confirmationvalues;
	String values="";
	Button confirmbtn;
	Databaseadapter db;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 db=new Databaseadapter(getActivity());
		 db.open();
		 db.getdata();
		 rootView = inflater.inflate(R.layout.remove_easel_error, container, false);
		 confirmbtn=(Button)rootView.findViewById(R.id.confirm_button);
		 confirmationvalues=(TextView)rootView.findViewById(R.id.values);
		 values="<b>Easel Name #   : </b>"+Home.SelectedEasel+"<br>"+"<b>Easel Location   : </b>"+Home.SelectedLocation+"<br>"+"<b>Product currently on the easel   : </b>"+Home.SelectedArt+"<br />";
		 confirmationvalues.setText(Html.fromHtml(values));
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();
				}
			});
			confirmbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new EaselConfirmation()).commit();
					/*db.insertrunningtransactions(new RunningtransactionsGetterSetter(Home.SelectedArt,Home.SelectedLocation,Home.SelectedCharity,Home.SelectedSponsor,Home.SelectedEasel,Home.SelectedQuantity,Home.SelectedStreetAddress,Home.SelectedCity,Home.SelectedProvince,Home.SelectedPostCode,Home.SelectedArtDescription));
					preferences p=new preferences(getActivity());
					p.SetLastScreen("easelconfirm");
					if(db.getRunningTransactionsCount()>0)
					{
						List<RunningtransactionsGetterSetter> rt = db.getRunningTransactions(); 
						for (RunningtransactionsGetterSetter runningtransaction : rt) 
						{
							Home.SelectedArt=runningtransaction.GetArt();
							Home.SelectedArtDescription=runningtransaction.GetArtDescription();
							Home.SelectedEasel=runningtransaction.GetEasel();
							Home.SelectedLocation=runningtransaction.GetLocation();
							Home.SelectedProvince=runningtransaction.GetProvince();
							Home.SelectedPostCode=runningtransaction.GetPostalCode();
							Home.SelectedQuantity=runningtransaction.GetQuantity();
							Home.SelectedSponsor=runningtransaction.GetSponsor();
							Home.SelectedCharity=runningtransaction.GetCharity();
							Home.SelectedStreetAddress=runningtransaction.GetStreeAddress();
							Home.SelectedCity=runningtransaction.GetCity();
						}
						SharedPreferences	settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
						String lastscreen=  	settings.getString(preferences.LastScreen,"");
						String bids=  	settings.getString(preferences.TotalBids,"");
						if(lastscreen.equals("easelconfirm"))
						{
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new EaselConfirmation()).commit();
						}
						else if(lastscreen.equals("questionaire"))
						{
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new Questionaire()).commit();
						}
						else if(lastscreen.equals("takepictures"))
						{
						Questionaire.zerobids=false;
						Intent i= new Intent(getActivity(),TakePictures.class);
						i.putExtra("total", bids);
						Questionaire.Bids=bids;
						startActivity(i);
						}
						else if(lastscreen.equals("neweaselquestion"))
						{
							Intent intent=new Intent(getActivity(),NewEaselConfirmationQuestion.class);
							startActivity(intent);
						}
						else if(lastscreen.equals("neweaselscreen"))
						{
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new NewEasel()).commit();
						}
						else if(lastscreen.equals("easelconfirmnew"))
						{
							NewEasel.SelectedArt=settings.getString(preferences.NewArt,"");
							NewEasel.SelectedArtDescription=settings.getString(preferences.NewArtDescription,"");
							NewEasel.SelectedEasel=settings.getString(preferences.NewEasel,"");
							NewEasel.SelectedLocation=settings.getString(preferences.NewLocation,"");
							NewEasel.SelectedProvince=settings.getString(preferences.NewProvince,"");
							NewEasel.SelectedPostCode=settings.getString(preferences.NewPostalCode,"");
							NewEasel.SelectedQuantity=settings.getString(preferences.NewQuantity,"");
							NewEasel.SelectedSponsor=settings.getString(preferences.NewSponsor,"");
							NewEasel.SelectedCharity=settings.getString(preferences.NewCharity,"");
							NewEasel.SelectedStreetAddress=settings.getString(preferences.NewStreetAddress,"");
							NewEasel.SelectedCity=settings.getString(preferences.NewCity,"");
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new EaselConfirmationNew()).commit();
						}
						else if(lastscreen.equals("summaryactivity"))
						{
							if(!bids.equals(""))
							{
								Questionaire.zerobids=false;
								Intent i= new Intent(getActivity(),SummaryActivity.class);
								Questionaire.Bids=bids;
								startActivity(i);
							}
							else
							{
								Questionaire.zerobids=true;
								Intent i= new Intent(getActivity(),SummaryActivity.class);
								startActivity(i);
							}
							
							
						}
						else
						{
							FragmentManager fragmentManager = getFragmentManager();
							fragmentManager.beginTransaction()
							.replace(R.id.frame_container, new Home()).commit();
						}
					}
					else
					{
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new Home()).commit();
					}
					SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);

					String usernamefull =pref.getString(preferences.username,"");*/
					//SendEmail("This easel was attempted to be closed, but could not as there was still art loaded on it","Attempted Closing of easel: "+Home.SelectedArt+" for user "+usernamefull);
					//Toast.makeText(getActivity(), "Email has been sent to Head office.", Toast.LENGTH_LONG).show();
					
					//SendEmailtoMeghan("This easel was attempted to be closed, but could not as there was still art loaded on it","Attempted Closing of easel: "+Home.SelectedArt+" for user "+usernamefull);
					//Toast.makeText(getActivity(), "Email has been sent to Megan.", Toast.LENGTH_LONG).show();
					
				}
			});
			
		 return rootView;
	}
	private void SendEmailNew(final String msg, final String subject,final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmailtoMeghan(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT_MEGAN);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	
	private void SendEmailtoKathy(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT_KATHY);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
}
