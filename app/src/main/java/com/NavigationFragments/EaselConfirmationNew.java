package com.NavigationFragments;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Database.Databaseadapter;
import com.NavigationFragments.EaselConfirmation.GetProductPicture;
import com.Utils.ConnectionClass;
import com.Utils.preferences;
import com.fundinginnovation.SelectDate;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;

public class EaselConfirmationNew extends Fragment{
	TextView confirmationvalues;
	String values="";
	Button confirmbtn,cancelbtn,otherbtn;
	Connection con ;
	ConnectionClass connectionClass;
	String findpictureofart="";
	ImageView productpicture;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 View rootView;
		
		 rootView = inflater.inflate(R.layout.easel_confirmation, container, false);
		 productpicture= (ImageView) rootView.findViewById(R.id.productpicture);
		 connectionClass = new ConnectionClass();
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();
				}
			});
		 otherbtn=(Button)rootView.findViewById(R.id.other_button);
		 confirmbtn=(Button)rootView.findViewById(R.id.confirm_button);
		 cancelbtn=(Button)rootView.findViewById(R.id.cancel_button);
		 confirmationvalues=(TextView)rootView.findViewById(R.id.values);
	SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
			
			String	newArtDescription =settings.getString(preferences.NewArtDescription,"");
			String	changedart =settings.getString(preferences.NewArt,"");
			String	changedquantitiy =settings.getString(preferences.NewQuantity,"");
			
			
			String date="";
			try{
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date datef = new Date();
				date=sdf.format(datef);
			}catch(Exception w){}
		 if(newArtDescription.equals("")&&changedart.equals("")&&changedquantitiy.equals(""))
		 {
			 findpictureofart=NewEasel.SelectedArt;
		 values=/*"<b>Easel Location #   : </b>"+NewEasel.SelectedEasel+"<br>"+*/"<b>Please confirm this is the correct product going up ("+date+") ?</b><br>"+"<br>"+"<b>Product #   : </b>"+NewEasel.SelectedArt+"<br>"+"<b>Product Name   : </b>"+NewEasel.SelectedArtDescription+"<br>"+"<b>Quantity   : </b>"+NewEasel.SelectedQuantity+"<br />";
		 }
		 else
		 {
			 findpictureofart=changedart;
			 values="<b>Please confirm this is the correct product going up ("+date+") ?</b><br>"+"<br>"+"<b>Product #   : </b>"+changedart+"<br>"+"<b>Product Name   : </b>"+newArtDescription+"<br>"+"<b>Quantity   : </b>"+changedquantitiy+"<br />";
			 
		 }
		 confirmationvalues.setText(Html.fromHtml(values));
		 //cancelbtn.setVisibility(View.INVISIBLE);
		 confirmbtn.setOnClickListener(new View.OnClickListener() {
				@Override 
				public void onClick(View v) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new SelectDate()).commit();

				}
			});
		 cancelbtn.setOnClickListener(new View.OnClickListener() {
				@Override 
				public void onClick(View v) {
					
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the easel entry ?");
					builder.setNegativeButton("No", null);
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									NavigationActivity.startturn=false;
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();
				}
			});
		 otherbtn.setOnClickListener(new View.OnClickListener() {
				@Override 
				public void onClick(View v) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new ManualProductSelectionForNewPicture()).commit();

				}
			});
/*		 GetProductPicture getproducts = new GetProductPicture();
			getproducts.execute("");*/
		return rootView;
		}
	
	public class GetProductPicture extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber="";
		String itemdesc="";
		int count=0;
		String pic="";
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Picture ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {

			try {
				if (p != null && p.isShowing()) {
					p.dismiss();
				}

			}catch (Exception a){}
			con=null;
			if(isSuccess) {
				
				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				

					Thread t = new Thread(){
						public void run(){
							try
							{
							byte[] decodedString = Base64.decode(pic, Base64.DEFAULT);
							BitmapFactory.Options options = new BitmapFactory.Options();
							options.inPreferredConfig = Config.RGB_565;
							Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
							productpicture.setImageBitmap(bmp);
							}
							catch(Exception t)
							{
								new AlertDialog.Builder(getActivity())
										.setTitle("Delete entry")
										.setMessage(t.toString())

										// Specifying a listener allows you to take an action before dismissing the dialog.
										// The dialog is automatically dismissed when a dialog button is clicked.
										.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int which) {
												// Continue with delete operation
											}
										});

							}
						}
					};
					t.start();


			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
				try {
					if(con==null){
					 con = connectionClass.CONN();}
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						//String query = "select * from dbo.vw_PAS_LiveEasels" ;
						//String query="SELECT  * FROM (SELECT  TOP "+currentrowcount+5+" t.*, ROW_NUMBER() OVER (ORDER BY Art) AS rn FROM  dbo.vw_pas_AllItems t ORDER BY Art) t WHERE   rn > "+currentrowcount+"";
						//String query = "select top 1 Art , Description,Picture from dbo.vw_pas_AllItems";
						//String query="SELECT  * FROM (SELECT  TOP 20 t.*, ROW_NUMBER() OVER (ORDER BY Art) AS rn FROM  dbo.vw_pas_AllItems t ORDER BY Art) t WHERE   rn > 10";
						//String query="SELECT  Art,Description,ImageFreeField1 FROM (SELECT  TOP "+currentrowcount+5+" t.*, ROW_NUMBER() OVER (ORDER BY Art) AS rn FROM  dbo.vw_pas_AllItems t ORDER BY Art) t WHERE   rn > "+currentrowcount+"";
						String query = "select  ImageFreeField1 from dbo.vw_pas_AllItems where Art='" + findpictureofart + "'";
						Statement stmt = con.createStatement();
						stmt.setQueryTimeout(0);
						ResultSet rs = stmt.executeQuery(query);
						
						 while (rs.next()) {
							 
							 /*if(count==5)
							 {
								 break;
							 }*/
						
							
							try{
							if(rs.getBlob("ImageFreeField1")!=null){
							Blob blob = rs.getBlob("ImageFreeField1");

							int blobLength = (int) blob.length();  
							byte[] blobAsBytes = blob.getBytes(1, blobLength);
							pic = Base64.encodeToString(blobAsBytes,Base64.NO_WRAP);
							}
							else{pic="";}
							}catch(Exception w)
							{
								pic="";
							}
							
							
		                    }
					
						 if(pic.equals(""))
						 {
							 isSuccess = false;
						 }
						 else
						 {
							 isSuccess = true;;
						 }
					}
					if(!con.isClosed())
					{
						con.close();
						con=null;
					}
				}
				catch (Exception ex)
				{
					
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;
		}
	}
}
