package com.NavigationFragments;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.Utils.ConnectionClass;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class ReportDamages extends Fragment{
	EditText enterproductnumber,enterdamagedescription;
	Button submitreport;
	Spinner  selecteaselnumber;
	List<String> spinnerArray =  new ArrayList<String>();
	ConnectionClass connectionClass;
	RequestListener requestlistener;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView;
		rootView = inflater.inflate(R.layout.report_damages, container, false);
		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails,String emailmsg,String subject)
			{
				if(emails.contains(","))
				{
					String arr[] = emails.split(",");
					for(int i=0;i<arr.length;i++)
					{
						String emailaddr=arr[i];
						SendEmailNew(emailmsg,subject,emailaddr);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					SendEmailNew(emailmsg,subject,emails);
					Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
				}
			}
			@Override
			public void onError(String result) {
				Toast.makeText(getActivity(),"Error occured in sending smail ",Toast.LENGTH_LONG).show();
			}
		};
		 connectionClass = new ConnectionClass();
		selecteaselnumber=(Spinner)rootView.findViewById(R.id.selecteaselnumber);
		enterproductnumber=(EditText)rootView.findViewById(R.id.enterproductnumber);
		enterdamagedescription=(EditText)rootView.findViewById(R.id.enterdamagedescription);
		enterdamagedescription.setImeOptions(EditorInfo.IME_ACTION_DONE);
		enterdamagedescription.setRawInputType(InputType.TYPE_CLASS_TEXT);
		ImageView imageButton = (ImageView) rootView
				.findViewById(R.id.menuicon);
		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
				NavigationActivity.draweropened=true;
			}
		});
		ImageView homeButton = (ImageView) rootView
				.findViewById(R.id.homeicon);
		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				preferences p= new preferences(getActivity());
		    	p.SetLastScreen("");
				p.Settotalbids("");
				Databaseadapter db=new Databaseadapter(getActivity());
				db.DeleteRunningTransactions(); 
				getActivity().finishAffinity();
				Intent i= new Intent(getActivity(),NavigationActivity.class);
				i.putExtra("val", "");
				startActivity(i);
				FragmentManager fragmentManager = getFragmentManager();
				/*fragmentManager.beginTransaction()
				.replace(R.id.frame_container, new HomeNew()).commit();*/
				fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				/*AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Cancel ?");
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setMessage("Are you sure you wish to cancel the transaction ?");
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0,
							int arg1) {
						
					}
				});
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
									int arg1) {
							
							}
						});

				builder.create().show();*/
			}
		});
		submitreport = (Button) rootView
				.findViewById(R.id.submitreport);
		submitreport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				if(enterproductnumber.getText().toString().equals(""))
				{
					enterproductnumber.setError("Product # is required");
					return;
				}
				if(enterdamagedescription.getText().toString().equals(""))
				{
					enterdamagedescription.setError("Damage Description is required");
					return;
				}
				SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String uname =pref.getString(preferences.username,"");
				String damagevalues="Product Number   : "+enterproductnumber.getText().toString()+"\nEasel Number   : "+selecteaselnumber.getSelectedItem().toString()+"\nDamage Explanation   : "+enterdamagedescription.getText().toString()+"\nArea Manager   : "+uname;
				//SendEmail(damagevalues,"Report Damage of "+enterproductnumber.getText().toString());
				//Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();

				GetEmails webservice = new GetEmails(getActivity(), requestlistener,"reportdamages", damagevalues, "Report Damage of "+enterproductnumber.getText().toString());
				webservice.execute();

				if(MyInventory.myinventory)
	                {
	                	MyInventory.myinventory=false;
	                	/*FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction().addToBackStack(null)
						.replace(R.id.frame_container, new MyInventory()).commit();*/
	                	getFragmentManager().popBackStack();
	                }
	                else
	                {
	                	getFragmentManager().popBackStack();
	                	/*FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction().addToBackStack(null)
						.replace(R.id.frame_container, new Home()).commit();*/
	                }
			}
		});
		GetEasels geteasels = new GetEasels(); 
		geteasels.execute("");
		return rootView;
	}
	private void SendEmailNew(final String msg, final String subject,final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}

	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       // setOnBackPressListener();
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	boolean move=false;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                if(MyInventory.myinventory)
                {
                	MyInventory.myinventory=false;
                	FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new MyInventory()).commit();
					ManualEntry.backpressed=true;
                }
                else
                {
                	ManualEntry.backpressed=true;
                	FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new Home()).commit();
                }
            		
                }
                return false;
            }
        });
    }
    public class GetEasels extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
	


		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Easels ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {
				
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					    getActivity(), R.layout.spinner_item, spinnerArray);

					adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
					selecteaselnumber.setAdapter(adapter);
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
			/*	try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
						String uname =pref.getString(preferences.useridd,"");
						String query = "select Easel from dbo.vw_PAS_LiveEasels where AreaManager='" + uname+ "'";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						
						 while (rs.next()) {
							if(rs.getString("Easel")!=null){
								spinnerArray.add(rs.getString("Easel"));
						
							}
							else
							{
								spinnerArray.add(rs.getString("Easel"));
								
							}
							
		                    }
						if(!spinnerArray.isEmpty())
						{

							z = "Easels Found";
							isSuccess=true;
						}
						else
						{
							z = "No Easels Found";
							isSuccess = false;
						}

					}
					if(!con.isClosed())
					{
						con.close();
						con=null;
					}
				}
				catch (Exception ex)
				{
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}*/

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Assemble the login request URL
				String EaselsURL = SFAPI.baseUri +"/sobjects"+SFAPI.Easels
						;
				Log.e("URL ",EaselsURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(EaselsURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;
				try {
					// Execute the login POST request
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

				}
				// verify response is HTTP OK
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					//System.out.println("Error authenticating to Force.com: "+statusCode);
					//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;

				}
				else
				{
					isSuccess=true;
					z ="Success";
					String getResult = null;
					try {
						getResult = EntityUtils.toString(response.getEntity());
						//Log.e("RESULT ",getResult);
						JSONObject jobj=new JSONObject(getResult);
						JSONArray jarr=jobj.getJSONArray("recentItems");
						for(int i=0;i<jarr.length();i++)
						{
							JSONObject eitems=jarr.getJSONObject(i);
							String id=eitems.getString("Id");
							String Name=eitems.getString("Name");
							Log.e("id ",id);
							Log.e("Name ",Name);
							spinnerArray.add(Name);

						}
						if(!spinnerArray.isEmpty())
						{

							z = "Easels Found";
							isSuccess=true;
						}
						else
						{
							z = "No Easels Found";
							isSuccess = false;
						}
					} catch (IOException e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
					JSONObject jsonObject = null;

					try {

					} catch (Exception e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
				}



			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return z;
		}
	}
}
