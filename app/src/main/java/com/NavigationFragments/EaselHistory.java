package com.NavigationFragments;

import java.net.URLEncoder;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Database.Databaseadapter;
import com.Utils.ConnectionClass;
import com.Utils.EaselsHistory;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class EaselHistory extends Fragment {
	private ArrayList<EaselsHistory> EaselHistory = new ArrayList<EaselsHistory>();
	ArrayList<HashMap<String, String>> listing;
	static String Bin_No = "binno";
	static String Item_N0 = "itemno";
	static String Item_DESCRIPTION= "itemdescription";
	static String Turn_Start_Date = "turnstartdate";
	static String Turn_End_Date = "turnenddate";
	static String Turn_Image="turnimage";
	ConnectionClass connectionClass;
	ListView listView;
	EditText search;
	Databaseadapter db;
	static boolean backpressed=false;
	String searchitem="";
	String searcheasel="";
	String CountConfirmedBids="";
	String CountSales="";
	String TotalSales="";
	String popupbinno="";
	String popupturnstartdate="";
	String popupturnenddate="";
	String popupitemno="";
	String popupdescription="";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.easel_history, container, false);
		 	db=new Databaseadapter(getActivity());
			db.open();
			db.getdata();
			 TextView ch=(TextView)rootView.findViewById(R.id.textView1);
			 ch.setVisibility(View.GONE);
			 ch.setText("");
			 listing = new ArrayList<HashMap<String, String>>();
			 listView=(ListView)rootView.findViewById(R.id.listview);
				listView.setCacheColorHint(Color.TRANSPARENT);
				connectionClass = new ConnectionClass();
				search=(EditText)rootView.findViewById(R.id.editText1);
				GetEaselHistory geteaselhistory = new GetEaselHistory(); 
				geteaselhistory.execute("");
				ImageView imageButton = (ImageView) rootView
						.findViewById(R.id.menuicon);
				imageButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					}
				});
				ImageView homeButton = (ImageView) rootView
						.findViewById(R.id.homeicon);
				homeButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						 preferences p= new preferences(getActivity());
					    	p.SetLastScreen("");
							p.Settotalbids("");
							Databaseadapter db=new Databaseadapter(getActivity());
							db.DeleteRunningTransactions(); 
							getActivity().finishAffinity();
							Intent i= new Intent(getActivity(),NavigationActivity.class);
							i.putExtra("val", "");
							startActivity(i);
						FragmentManager fragmentManager = getFragmentManager();
						/*fragmentManager.beginTransaction()
						.replace(R.id.frame_container, new HomeNew()).commit();*/
						fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
						/*AlertDialog.Builder builder = new AlertDialog.Builder(
								getActivity());
						builder.setTitle("Cancel ?");
						builder.setIcon(android.R.drawable.ic_dialog_alert);
						builder.setMessage("Are you sure you wish to cancel the transaction ?");
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
									int arg1) {
								
							}
						});
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										 preferences p= new preferences(getActivity());
									    	p.SetLastScreen("");
											p.Settotalbids("");
											Databaseadapter db=new Databaseadapter(getActivity());
											db.DeleteRunningTransactions(); 
										FragmentManager fragmentManager = getFragmentManager();
										fragmentManager.beginTransaction()
										.replace(R.id.frame_container, new HomeNew()).commit();
									}
								});

						builder.create().show();*/
					}
				});
		 return rootView;
			}
	public class GetEaselHistory extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String binno = "";
		String itemno = "";
		String itemdes = "";
		String startdate = "";
		String enddate = "";
		String turnimage="";
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Easels History ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if(isSuccess) {	
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				final EaselHistoryAdapter   adapter = new EaselHistoryAdapter(getActivity(),EaselHistory, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {
			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }
			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }
			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}
		}
		@Override
		protected String doInBackground(String... params) {
		
				try {

					HttpClient httpclient = new DefaultHttpClient();
					String sfprodid="";
					String estatus="Closed";
					SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME, 0);
					String conid = settings.getString(preferences.useridd, "");
					String AuctionaURL = SFAPI.baseUri + "/query/?q=" + URLEncoder.encode("SELECT Id,Easel_Number__c,Auction_End_Date__c,Product_Name__c,Product_SKU__c,Easel_Location__c,Name FROM Auction__c Where Status__c = '" +estatus + "'", "UTF-8");

					Log.e("URL ", AuctionaURL);
					Log.e("Header ", SFAPI.oauthHeader.toString());
					// Login requests must be POSTs
					HttpGet httpPost = new HttpGet(AuctionaURL);
					httpPost.addHeader(SFAPI.oauthHeader);
					httpPost.addHeader(SFAPI.prettyPrintHeader);


					HttpResponse response = null;

					// Execute the login POST request
					response = httpclient.execute(httpPost);
					String allauctiona = EntityUtils.toString(response.getEntity());
					JSONObject jobj=new JSONObject(allauctiona);
					JSONArray jarr=jobj.getJSONArray("records");
					for(int i=0;i<jarr.length();i++) {

						JSONObject eitems = jarr.getJSONObject(i);
						String id = eitems.getString("Id");
						String Auction_End_Date__c=eitems.getString("Auction_End_Date__c");
						String Product_SKU__c=eitems.getString("Product_SKU__c");
						String Easel_Location__c=eitems.getString("Easel_Location__c");
						String Easel_Number__c=eitems.getString("Easel_Number__c");
						String Product_Name__c=eitems.getString("Product_Name__c");
						String ProductName="";
						String EaselNumber="";
						String ProductURL = SFAPI.baseUri +"/query/?q="+URLEncoder.encode("SELECT Name FROM Product2 Where StockKeepingUnit= '"+Product_SKU__c+"'", "UTF-8");
						Log.e("URL ",ProductURL);
						// Login requests must be POSTs
						HttpGet httpPostgp = new HttpGet(ProductURL);
						httpPostgp.addHeader(SFAPI.oauthHeader);
						httpPostgp.addHeader(SFAPI.prettyPrintHeader);


						HttpResponse responsegp = null;

						// Execute the login POST request
						responsegp = httpclient.execute(httpPostgp);

						// verify response is HTTP OK
						final int statusCode = responsegp.getStatusLine().getStatusCode();
						if (statusCode != HttpStatus.SC_OK) {
						}
						else

						{
							JSONObject prodidjobj=new JSONObject(EntityUtils.toString(responsegp.getEntity()));
							JSONArray jarrpi=prodidjobj.getJSONArray("records");
							for(int ii=0;ii<jarrpi.length();ii++)
							{
								JSONObject pitem = jarrpi.getJSONObject(ii);
								Log.e("Name ",pitem.getString("Name"));
								ProductName=pitem.getString("Name");
							}
						}
						String EaselsURL = SFAPI.baseUri + "/query/?q=" + URLEncoder.encode("SELECT Easel_Number__c FROM Easel__c Where Id = '" +Easel_Number__c + "'", "UTF-8");

						Log.e("URL ", EaselsURL);
						Log.e("Header ", SFAPI.oauthHeader.toString());
						// Login requests must be POSTs
						HttpGet httpPost2 = new HttpGet(EaselsURL);
						httpPost2.addHeader(SFAPI.oauthHeader);
						httpPost2.addHeader(SFAPI.prettyPrintHeader);


						HttpResponse response2 = null;

						// Execute the login POST request
						response2 = httpclient.execute(httpPost2);
						String alleasels = EntityUtils.toString(response2.getEntity());
						JSONObject jobj2 = new JSONObject(alleasels);
						JSONArray jarr2 = jobj2.getJSONArray("records");
						for (int i2 = 0; i2 < jarr2.length(); i2++)
						{

							JSONObject eitems2 = jarr2.getJSONObject(i2);
							EaselNumber=eitems2.getString("Easel_Number__c");
						}
						HashMap<String, String> map = new HashMap<String, String>();

							map.put(Bin_No, EaselNumber);
							binno=EaselNumber;

							map.put(Item_N0, Product_SKU__c);
							itemno=Product_SKU__c;


							map.put(Item_DESCRIPTION, ProductName);
							itemdes=ProductName;


							map.put(Turn_Start_Date, "N/A");
							startdate="N/A";


							map.put(Turn_End_Date, Auction_End_Date__c);
							enddate=Auction_End_Date__c;
						EaselsHistory eh = new EaselsHistory();
						eh.setBinno(binno);
						eh.setItemno(itemno);
						eh.setItemdescription(itemdes);
						eh.setTurnstartdate(startdate);
						eh.setTurnenddate(enddate);
						eh.setTurnimage(turnimage);

						EaselHistory.add(eh);

						listing.add(map);
					}
					if(!listing.isEmpty())
					{

						z = "Easels History Found";
						isSuccess=true;

					}
					else
					{
						z = "No Easels History Found";
						isSuccess = false;
					}
				}
				catch (Exception ex)
				{
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;
		}
	}
	public class EaselHistoryAdapter extends BaseAdapter implements Filterable{
		Context context;
		LayoutInflater inflater;
		 RadioButton mSelectedRB;
		 private ValueFilter valueFilter;
		 private ArrayList<EaselsHistory> History;
		 private ArrayList<EaselsHistory> mStringFilterList;
		   int mSelectedPosition = -1;
		ArrayList<HashMap<String, String>> listt;
		public EaselHistoryAdapter(Context context,
				ArrayList<EaselsHistory> eh,ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			 this.History = eh;
			    mStringFilterList =  eh;
			    getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return History.size();
		}
		@Override
		public Object getItem(int position) {
		    return History.get(position).getBinno();
		}
		

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		public class ViewHolder {

		}
		@SuppressWarnings("unused")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewHolder holder;
			  holder = new ViewHolder();
		
			View  itemView = inflater.inflate(R.layout.historylist, parent, false);
			
			final TextView binno=(TextView)itemView.findViewById(R.id.binno);
			final TextView itemno=(TextView)itemView.findViewById(R.id.itemno);
			final TextView itemdes=(TextView)itemView.findViewById(R.id.itemdes);
			final TextView startdate=(TextView)itemView.findViewById(R.id.startdate);
			final TextView enddate=(TextView)itemView.findViewById(R.id.enddate);
			ImageView imgbox=(ImageView)itemView.findViewById(R.id.imageView1);
			binno.setText(Html.fromHtml("<b>"+"Easel # : " + "</b> "+History.get(position).getBinno()));
			itemno.setText(Html.fromHtml("<b>"+"Current Product : " + "</b> "+History.get(position).getItemno()));
			itemdes.setText(Html.fromHtml("<b>"+"Description : "+ "</b> " +History.get(position).getItemdescription()));
			startdate.setText(Html.fromHtml("<b>"+"Start Date : "+ "</b> " +History.get(position).getTurnstartdate()));
			enddate.setText(Html.fromHtml("<b>"+"End Date : " + "</b> "  +History.get(position).getTurnenddate()));
			try
			{
				byte[] decodedString = Base64.decode(History.get(position).getItemImage(), Base64.DEFAULT);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig = Config.RGB_565;
				Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
				imgbox.setImageBitmap(bmp);
			}
			catch(Exception t)
			{
				
			}
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					CountConfirmedBids="";
					CountSales="";
					TotalSales="";
					popupbinno=History.get(position).getBinno();
					popupturnstartdate=History.get(position).getTurnstartdate();
					popupturnenddate=History.get(position).getTurnenddate();
					popupitemno=History.get(position).getItemno();
					popupdescription=History.get(position).getItemdescription();
					searchitem=History.get(position).getItemno();
					searcheasel=History.get(position).getBinno();
					AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
					LayoutInflater inflater = getActivity().getLayoutInflater();
					View convertView = (View) inflater.inflate(R.layout.customdialog_easelhistory, null);
					builder.setView(convertView);
					final AlertDialog alertDialog = builder.create();
					alertDialog.setCancelable(false);




					TextView	dprod = (TextView) convertView.findViewById(R.id.dproduct);
					dprod.setText("Current Product : "+popupitemno);

					TextView	ddescription = (TextView) convertView.findViewById(R.id.ddescription);
					ddescription.setText("Description : "+popupdescription);

					TextView	dstartdate = (TextView) convertView.findViewById(R.id.dstartdate);
					dstartdate.setText("Start Date : "+"N/A");

					TextView	denddate = (TextView) convertView.findViewById(R.id.denddate);
					denddate.setText("End Date : "+popupturnenddate);

					/*TextView	dconfirmedbids = (TextView) convertView.findViewById(R.id.dconfirmedbids);
					dconfirmedbids.setText("Count of confirmed bids : "+CountConfirmedBids);

					TextView	dcountsales = (TextView) convertView.findViewById(R.id.dcountsales);
					dcountsales.setText("Count of sales : "+CountSales);

					TextView	dtotalsales = (TextView) convertView.findViewById(R.id.dtotalsales);
					dtotalsales.setText("Total sales : $"+TotalSales);*/

					TextView	deasel = (TextView) convertView.findViewById(R.id.deasel);
					deasel.setText("Easel # : "+popupbinno);





					Button	close = (Button) convertView.findViewById(R.id.okbtn);

					close.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							alertDialog.dismiss();
						}
					});
					alertDialog.show();
					//GetEaselHistoryExtended geteaselextended = new GetEaselHistoryExtended();
					//geteaselextended.execute("");
				}
			});
			return itemView;}
		
		@Override
		public Filter getFilter() {
		    if(valueFilter==null) {

		        valueFilter=new ValueFilter();
		    }

		    return valueFilter;
		}
		private class ValueFilter extends Filter {

		    //Invoked in a worker thread to filter the data according to the constraint.
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		        FilterResults results=new FilterResults();
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<EaselsHistory> filterList=new ArrayList<EaselsHistory>();
		        	
		            for(int i=0;i<mStringFilterList.size();i++){
		                if((mStringFilterList.get(i).getItemno().toUpperCase()).contains(constraint.toString().toUpperCase())||(mStringFilterList.get(i).getBinno().toUpperCase()).contains(constraint.toString().toUpperCase())) 
		                {
		                	EaselsHistory contacts = new EaselsHistory();
		                    contacts.setBinno(mStringFilterList.get(i).getBinno());
		                    contacts.setItemno((mStringFilterList.get(i).getItemno()));
		                    contacts.setItemdescription((mStringFilterList.get(i).getItemdescription()));
		                    contacts.setTurnstartdate((mStringFilterList.get(i).getTurnstartdate()));
		                    contacts.setTurnenddate((mStringFilterList.get(i).getTurnenddate()));
		                    filterList.add(contacts);
		                }
		            }
		            results.count=filterList.size();
		            results.values=filterList;
		        }else{
		            results.count=mStringFilterList.size();
		            results.values=mStringFilterList;
		        }
		        return results;
		    }


		    //Invoked in the UI thread to publish the filtering results in the user interface.
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint,
		            FilterResults results) {
		        History=(ArrayList<EaselsHistory>) results.values;
		        notifyDataSetChanged();
		    }
		}
	}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       
         //   setOnBackPressListener();
         
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                	
            		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new LiveEasels()).commit();
					
                }
                backpressed=true;
                return false;
            }
        });
    }
    public class GetEaselHistoryExtended extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		
		
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Details ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if(isSuccess) {	
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
				LayoutInflater inflater = getActivity().getLayoutInflater();
				View convertView = (View) inflater.inflate(R.layout.customdialog_easelhistory, null);
				builder.setView(convertView);
				final AlertDialog alertDialog = builder.create();
				alertDialog.setCancelable(false);

		
	
				
				TextView	dprod = (TextView) convertView.findViewById(R.id.dproduct);
				dprod.setText("Current Product : "+popupitemno);
				
				TextView	ddescription = (TextView) convertView.findViewById(R.id.ddescription);
				ddescription.setText("Description : "+popupdescription);
				
				TextView	dstartdate = (TextView) convertView.findViewById(R.id.dstartdate);
				dstartdate.setText("Start Date : "+popupturnstartdate);
				
				TextView	denddate = (TextView) convertView.findViewById(R.id.denddate);
				denddate.setText("End Date : "+popupturnenddate);
				
				TextView	dconfirmedbids = (TextView) convertView.findViewById(R.id.dconfirmedbids);
				dconfirmedbids.setText("Count of confirmed bids : "+CountConfirmedBids);
				
				TextView	dcountsales = (TextView) convertView.findViewById(R.id.dcountsales);
				dcountsales.setText("Count of sales : "+CountSales);
				
				TextView	dtotalsales = (TextView) convertView.findViewById(R.id.dtotalsales);
				dtotalsales.setText("Total sales : $"+TotalSales);
				
				TextView	deasel = (TextView) convertView.findViewById(R.id.deasel);
				deasel.setText("Easel # : "+popupbinno);
				
				
		
				
				
				Button	close = (Button) convertView.findViewById(R.id.okbtn);
				
				close.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
					
						alertDialog.dismiss();
					}
				});
				alertDialog.show();
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}
		}
		@Override
		protected String doInBackground(String... params) {
		
				try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
					
						String query = "select * from vw_LS_EaselTurnHistoryExtended where item_no='" + searchitem + "' and bin_no='" + searcheasel + "'";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						 while (rs.next()) {
							HashMap<String, String> map = new HashMap<String, String>();
						
						
							if(rs.getString("CountConfirmedBids")!=null){
							
								CountConfirmedBids=rs.getString("CountConfirmedBids");
							}
								else
								{
									
									CountConfirmedBids="";
								}
							if(rs.getString("CountSales")!=null){
								CountSales=rs.getString("CountSales");
							}
								else
								{
									CountSales="";
									
								}
							
							if(rs.getString("TotalSales")!=null){
							
								TotalSales=rs.getString("TotalSales");
							}
								else
								{
									TotalSales="";
									
									
								}
			
							
		                    }
						 isSuccess=true;

					}
					if(!con.isClosed())
					{
						con.close();
						con=null;
					}
				}
				catch (Exception ex)
				{
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;
		}
	}
}
