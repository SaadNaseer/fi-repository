package com.NavigationFragments;

import com.Database.Databaseadapter;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class ContactHeadOfficeMenu extends Fragment

{	
	Button requestsuppliesbtn,reportdamagesbtn,reportotherbtn;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.contact_head_office, container, false);
		 requestsuppliesbtn=(Button)rootView.findViewById(R.id.requestsuppliesbtn);
		 reportdamagesbtn=(Button)rootView.findViewById(R.id.reportdamagesbtn);
		 reportotherbtn=(Button)rootView.findViewById(R.id.reportotherbtn);
		 requestsuppliesbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new RequestSupplies()).commit();
				}
			});
		 reportdamagesbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new ReportDamages()).commit();
				}
			});
		 reportotherbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new ReportToOthers()).commit();
				}
			});
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					preferences p= new preferences(getActivity());
			    	p.SetLastScreen("");
					p.Settotalbids("");
					Databaseadapter db=new Databaseadapter(getActivity());
					db.DeleteRunningTransactions(); 
					getActivity().finishAffinity();
					Intent i= new Intent(getActivity(),NavigationActivity.class);
					i.putExtra("val", "");
					startActivity(i);
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.frame_container, new HomeNew()).commit();
				fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					/*AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
									fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();*/
				}
			});
		 return rootView;
			
	}
}
