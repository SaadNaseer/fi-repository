package com.NavigationFragments;

import com.Database.Databaseadapter;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ManualEntry extends Fragment{
static boolean backpressed=false;
	Button nextbtn;
	EditText eteasel,etart,etartdesc,etcharity,etsponsor,etstaddress,etcity,etprovince,etpostalcode;
	Button homestartmyturns,homeliveeasellocations,homemyinventory,homecharitydirectory,homemarketingmaterials,homeallproducts,homeexit;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 View rootView;
		 rootView = inflater.inflate(R.layout.activity_manual_entry, container, false);
		// NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
		 
			eteasel=(EditText)rootView.findViewById(R.id.eteasel);
			etart=(EditText)rootView.findViewById(R.id.etart);
			etartdesc=(EditText)rootView.findViewById(R.id.etartdesc);
			etcharity=(EditText)rootView.findViewById(R.id.etcharity);
			etsponsor=(EditText)rootView.findViewById(R.id.etsponsor);
			etstaddress=(EditText)rootView.findViewById(R.id.etstaddress);
			etcity=(EditText)rootView.findViewById(R.id.etcity);
			etprovince=(EditText)rootView.findViewById(R.id.etprovince);
			etpostalcode=(EditText)rootView.findViewById(R.id.etpostalcode);
			nextbtn = (Button)rootView.findViewById(R.id.manualnextbtn);
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
				
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();
				}
			});
			nextbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					if(eteasel.getText().toString().length()==0)
					{
						eteasel.setError("Please Enter Easel First");
						return;
					}
					if(etart.getText().toString().length()==0)
					{
						etart.setError("Please Enter Art Number First");
						return;
					}
					if(etartdesc.getText().toString().length()==0)
					{
						etartdesc.setError("Please Enter Art Description First");
						return;
					}
					if(etcharity.getText().toString().length()==0)
					{
						etcharity.setError("Please Enter Charity Name First");
						return;
					}
					if(etsponsor.getText().toString().length()==0)
					{
						etsponsor.setError("Please Enter Location Name First");
						return;
					}
					if(etstaddress.getText().toString().length()==0)
					{
						etstaddress.setError("Please Enter Street Address First");
						return;
					}
					if(etcity.getText().toString().length()==0)
					{
						etcity.setError("Please Enter City Name First");
						return;
					}
					if(etprovince.getText().toString().length()==0)
					{
						etprovince.setError("Please Enter Province Name First");
						return;
					}
					if(etpostalcode.getText().toString().length()==0)
					{
						etpostalcode.setError("Please Enter Postal Code First");
						return;
					}
					
					Home.SelectedArt = etart.getText().toString();
					Home.SelectedArtDescription = etartdesc.getText().toString();
					Home.SelectedCharity = etcharity.getText().toString();
					Home.SelectedSponsor = etsponsor.getText().toString();
					Home.SelectedEasel = eteasel.getText().toString();
					Home.SelectedStreetAddress = etstaddress.getText().toString();
					Home.SelectedCity = etcity.getText().toString();
					Home.SelectedProvince = etprovince.getText().toString();
					Home.SelectedPostCode = etpostalcode.getText().toString();
					
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new EaselConfirmation()).commit();
				}
			});
		 
			
		return rootView;
		}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       
       // setOnBackPressListener();
       
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                	backpressed=true;
                	FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new Home()).commit();
                }
                return false;
            }
        });
    }
}
