package com.NavigationFragments;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import it.sauronsoftware.ftp4j.FTPFile;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import com.Database.Databaseadapter;
import com.Utils.FTPCredentials;
import com.Utils.FileUtils;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MarketingMaterials extends Fragment{
	Boolean isSuccess = false;
	String errormessage="";
	int filecount=1;
	GridView gridView;
	ArrayList<HashMap<String, String>> listing;
	static String NAME = "NAME";
	ImageView refreshbtn;
	boolean refresh=false;
	String loadingmessage="";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.marketing_materials, container, false);
		 gridView = (GridView)rootView.findViewById(R.id.gridview);
		  listing = new ArrayList<HashMap<String, String>>();
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
									FragmentManager fragmentManager = getFragmentManager();
									getActivity().finishAffinity();
									Intent i= new Intent(getActivity(),NavigationActivity.class);
									i.putExtra("val", "");
									startActivity(i);
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();
				}
			});
			refreshbtn = (ImageView) rootView
					.findViewById(R.id.RefreshBtn);

			refreshbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					listing.clear();
					DownloadFile down = new DownloadFile(); 
					down.execute("");
					refresh=true;
					loadingmessage="Refreshing Support Materials ...";
				}
			});
			loadingmessage="Loading Support Materials ...";
			DownloadFile down = new DownloadFile(); 
			down.execute("");
		 return rootView;
	}
	public class DownloadFile extends AsyncTask<String,String,String>
	{
		
		String z = "";
		
		  String replycode="";
		  ProgressDialog p;
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Support Materials ...");
			p.setCancelable(false);
			p.show();
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if(isSuccess) {
				
					//Toast.makeText(getActivity(),"File Downloaded SuccessFully",Toast.LENGTH_SHORT).show();
				MarketingAdapter marketingadapter = new MarketingAdapter(getActivity(), listing);
				gridView.setAdapter(marketingadapter);
				refresh=false;
			}
			
			else
			{
				Toast.makeText(getActivity(),"Error in downloading File",Toast.LENGTH_SHORT).show();
				
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
				try {
					isSuccess=false;
					downloadFile(new File(Environment.getExternalStorageDirectory()+File.separator+"Funding_Innovation_1_Pager"+".pdf"));
					
				}
				catch (Exception ex)
				{
					isSuccess = false;
					replycode = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return replycode;
		}
	}
	public void downloadFile(File fileName){
	    
	    
	    FTPClient client = new FTPClient();
	     
	   try {
	        
	       client.connect(FTPCredentials.FTP_HOST,21);
	       client.login(FTPCredentials.FTP_USER, FTPCredentials.FTP_PASS);
	       client.setType(FTPClient.TYPE_BINARY);
	       client.changeDirectory("Marketing_Materials");
	       FTPFile[] list = client.list();
	       
	       for (int i = 0; i < list.length; i++)
	       {
	    	   HashMap<String, String> map = new HashMap<String, String>();
	    	   map.put(NAME, list[i].getName());
	    	   listing.add(map);
	    	   Log.e("NAME ",list[i].getName());
	          //client.download("localFile", new java.io.File("remotefile);
	    	   if(new java.io.File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()).exists())
	    	   {
	    		   if(refresh)
		    		  {
		    			  new java.io.File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()).delete();
		    			  
		    			  client.download(list[i].getName(), new java.io.File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()));     
		    		  }
		    		  else
		    		  {
		    			  
		    		  }
	    	   }
	    	   else
	    	   {
	           client.download(list[i].getName(), new java.io.File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()));    
	    	   }
	    	   
	       }
	       isSuccess=true;
	      // client.download("Funding_Innovation_1_Pager.pdf",fileName, new MyTransferListener());
	        
	   } catch (Exception e) {
	       e.printStackTrace();
	       errormessage=e.toString();
	       isSuccess=false;
	       Log.e("EXCEPTION ",errormessage);
	       try {
	           client.disconnect(true);    
	       } catch (Exception e2) {
	    	   errormessage=e.toString();
	           e2.printStackTrace();
	       }
	   }
	    
	}

	/*******  Used to file upload and show progress  **********/

	public class MyTransferListener implements FTPDataTransferListener {

	   public void started() {
	        
		 //  p.setMessage("Upload Started");
	       // Transfer started
	       //Toast.makeText(getBaseContext(), " Upload Started ...", Toast.LENGTH_SHORT).show();
	       //System.out.println(" Upload Started ...");
	   }

	   public void transferred(int length) {
		//   p.setMessage("Transferred ... " + String.valueOf(length));
	       // Yet other length bytes has been transferred since the last time this
	       // method was called
	     //  Toast.makeText(getBaseContext(), " transferred ..." + length, Toast.LENGTH_SHORT).show();
	       //System.out.println(" transferred ..." + length);
	   }

	   public void completed() {
	        
		   isSuccess=true;
		  // p.setMessage("completed");
	       // Transfer completed
		 ///  isSuccess=true;
	     //  Toast.makeText(getBaseContext(), " completed ...", Toast.LENGTH_SHORT).show();
	       //System.out.println(" completed ..." );
	   }

	   public void aborted() {
		//   p.setMessage("aborted");
		  // isSuccess=false;
	       // Transfer aborted
	       //Toast.makeText(getBaseContext()," transfer aborted ", Toast.LENGTH_SHORT).show();
	       //System.out.println(" aborted ..." );
	   }

	   public void failed() {
//		   p.setMessage("failed");
		  // isSuccess=false;
	       // Transfer failed
	     //  System.out.println(" failed ..." );
	   }

	}
	public class MarketingAdapter extends BaseAdapter {

		Context context;
		String value;
		String fontPath2 = "segoepr.ttf";
		LayoutInflater inflater;
		ArrayList<HashMap<String, String>> data;
		
		public MarketingAdapter(Context context,
				ArrayList<HashMap<String, String>> arraylist) {
			this.context = context;
			data = arraylist;
			

		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
		  // 5
		  @SuppressWarnings("unused")
		@Override
		  public View getView(int position, View convertView, ViewGroup parent) {
			
			  
			  inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			  final View itemView = inflater.inflate(R.layout.marketing_materials_detail, parent, false);
			  HashMap<String, String> results = new HashMap<String, String>();
				results = data.get(position);
			  // 3
			  final ImageView imageView = (ImageView)itemView.findViewById(R.id.imageview_cover_art);
			  final TextView nameTextView = (TextView)itemView.findViewById(R.id.file_name);
			 

			  // 4
			 
			  nameTextView.setText(results.get(MarketingMaterials.NAME));
			  itemView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						String filepath=Environment.getExternalStorageDirectory()+File.separator + nameTextView.getText().toString();
						FileUtils.openAnyFile(getActivity(),
								filepath);
					}
				});
			  
			  return itemView;
		  }

		}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        //setOnBackPressListener();
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressWarnings("unused")
			@SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	boolean move=false;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                
            		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();
                }
                return false;
            }
        });
    }
}
