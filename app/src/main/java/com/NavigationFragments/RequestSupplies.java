package com.NavigationFragments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.GetterSetters.RequestSuppliesGetterSetter;
import com.TextFileFormatting.Block;
import com.TextFileFormatting.Board;
import com.TextFileFormatting.Table;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.TableBuilder;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;

import dnl.utils.text.table.TextTable;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class RequestSupplies extends Fragment{
	
	ArrayList<HashMap<String, String>> list;
	//Spinner items;
	ListView listview;
	Button submitsupplies;
	boolean onstart=true;
	//EditText etquantity;
	//EditText etdescription;
	static String ITEM="item";
	static String QUANTITY="quantity";
	static String DESCRIPTION="description";
	Button selectitems;
	final CharSequence[] Supplyitems = {"Pens","Pads","Velcro - Female (roll)","Velcro - Male (roll)","Bid Corners","Protective Corners","Charity Stickers","Dates","Months","Brochures","Standard Easel","Double sided Easel","Double Up Easel","Wall Mounted Easel","Easel Barcodes","Easel Boards","Double-sided Tape (foam tape)","Packing Tape (6 in a package)","Box Well Boxes","Other"};
	AlertDialog rquestsuppliesDialog;
	Databaseadapter db;
	String usernamefull="";
	RequestListener requestlistener;
	@SuppressWarnings("unused")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView;
		rootView = inflater.inflate(R.layout.request_supplies, container, false);
		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails,String emailmsg,String subject)
			{
				if(emails.contains(","))
				{
					String arr[] = emails.split(",");
					for(int i=0;i<arr.length;i++)
					{
						String emailaddr=arr[i];
						SendEmailNew(emailmsg,subject,emailaddr);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					SendEmailNew(emailmsg,subject,emails);
					Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
				}
			}
			@Override
			public void onError(String result) {
				Toast.makeText(getActivity(),"Error occured in sending smail ",Toast.LENGTH_LONG).show();
			}
		};
		list = new ArrayList<HashMap<String, String>>();
		db=new Databaseadapter(getActivity());
		db.open();
		db.getdata();
		selectitems=(Button)rootView.findViewById(R.id.selectitems);
		selectitems.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Select Item");
				builder.setSingleChoiceItems(Supplyitems, -1, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {

						String  selected = Supplyitems[item].toString();
						//list.add(selected);
						//MyCustomAdapter adapter = new MyCustomAdapter(list, getActivity());
						//listview.setAdapter(adapter);
						rquestsuppliesDialog.dismiss();    
						//listview.setBackgroundResource(Color.TRANSPARENT);
						getotherfieldsandsave(selected);
					}
				});
				rquestsuppliesDialog = builder.create();
				rquestsuppliesDialog.show();
			}
		});
		//items= (Spinner) rootView.findViewById(R.id.items);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getActivity(), R.layout.spinner_item, R.array.supplies);


		//	items.setAdapter(adapter);
		listview=(ListView) rootView.findViewById(R.id.listview);
		List<RequestSuppliesGetterSetter> scanned = db.getrequestsupplies();
		for (RequestSuppliesGetterSetter inventories : scanned) 
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(ITEM, inventories.GetItemName());
			map.put(QUANTITY, inventories.GetItemQuantity());
			map.put(DESCRIPTION, inventories.GetItemDescription());
			list.add(map);
		}
		if(list.isEmpty())
		{

		}
		else
		{
			listview.setBackgroundResource(Color.TRANSPARENT);
		}
		MyCustomAdapter marketingadapter = new MyCustomAdapter(list,getActivity());
		listview.setAdapter(marketingadapter);
		submitsupplies=(Button) rootView.findViewById(R.id.submitsupplies);
		//etquantity=(EditText) rootView.findViewById(R.id.etquantity);
		//etdescription=(EditText) rootView.findViewById(R.id.etdescription);
		submitsupplies.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);


				 usernamefull =pref.getString(preferences.username,"");

				/*if(etquantity.getText().toString().isEmpty())
				{
					etquantity.setError("Quantity can't be empty");
					return;
				}
				if(etdescription.getText().toString().isEmpty())
				{
					etdescription.setError("Description can't be empty");
					return;
				}*/
				/*String items="";
				for(int i=0;i<list.size();i++)
				{
					if(items.equals(""))
					{
						items=list.get(i);
					}
					else
					{
						items=items+","+list.get(i);
					}
				}
				Log.e("","Items "+items);*/
				//SendEmail("Items : "+items+"\nQuantity : "+etquantity.getText().toString()+"\nDescription : "+etdescription.getText().toString(),"Order Supplies for "+usernamefull);

				 generateemail();
				
				//etquantity.setText("");
				//etdescription.setText("");
				list.clear();
				listview.setAdapter(null);
				listview.setBackgroundResource(R.drawable.emptylist);
				

			}
		});
		ImageView imageButton = (ImageView) rootView
				.findViewById(R.id.menuicon);
		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				/*if(!NavigationActivity.draweropened){*/
				NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
				NavigationActivity.draweropened=true;
				/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
			}
		});
		ImageView homeButton = (ImageView) rootView
				.findViewById(R.id.homeicon);
		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				preferences p= new preferences(getActivity());
				p.SetLastScreen("");
				p.Settotalbids("");
				Databaseadapter db=new Databaseadapter(getActivity());
				db.DeleteRunningTransactions(); 
				getActivity().finishAffinity();
				Intent i= new Intent(getActivity(),NavigationActivity.class);
				i.putExtra("val", "");
				startActivity(i);
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});


		return rootView;
	}
	public void getotherfieldsandsave(String selecteditemm)
	{
		AlertDialog.Builder builder=new AlertDialog.Builder(getActivity()); 
		LayoutInflater li = LayoutInflater.from(getActivity());
		View promptsView = li.inflate(R.layout.reqsupdetails, null);
		builder.setView(promptsView);
		builder.setCancelable(false);
		builder.setTitle("Enter Item Details ");
		final TextView selectditem = (TextView)promptsView.
				findViewById(R.id.selecteditem); 
		selectditem.setText(selecteditemm);
		final EditText itemq = (EditText)promptsView.
				findViewById(R.id.itemq);  
		final EditText itemd = (EditText)promptsView.
				findViewById(R.id.itemd);  
		
		
		builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				if(selectditem.getText().toString().isEmpty())
				{
					Toast.makeText(getActivity(), "Item must be selected", Toast.LENGTH_LONG).show();

					return;
				}
				/*if(itemd.getText().toString().isEmpty())
				{
					Toast.makeText(getActivity(), "Item Description cannot be empty", Toast.LENGTH_LONG).show();

					return;
				}*/
				
				if(itemq.getText().toString().isEmpty())
				{
					Toast.makeText(getActivity(), "Item Quantity cannot be empty", Toast.LENGTH_LONG).show();

					return;
				}

				
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(ITEM, selectditem.getText().toString());
				map.put(QUANTITY, itemq.getText().toString());
				map.put(DESCRIPTION, itemd.getText().toString());
				
				list.add(map);
				listview.setBackgroundResource(Color.TRANSPARENT);
				db.insertrequestsupplies(new RequestSuppliesGetterSetter(selectditem.getText().toString(),itemq.getText().toString(),itemd.getText().toString()));
				MyCustomAdapter marketingadapter = new MyCustomAdapter(list,getActivity());
				listview.setAdapter(marketingadapter);
			}
		});

		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {


			}
		});
		builder.show(); 
	}
	public class MyCustomAdapter extends BaseAdapter implements ListAdapter { 
		 
		ArrayList<HashMap<String, String>> list;
		private Context context; 



		public MyCustomAdapter(ArrayList<HashMap<String, String>> arraylist, Context context) { 
			this.list = arraylist; 
			this.context = context; 
		} 

		@Override
		public int getCount() { 
			return list.size(); 
		} 

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
				view = inflater.inflate(R.layout.my_custom_list_layout, null);
			} 
			HashMap<String, String> results = new HashMap<String, String>();
			results = list.get(position);
			final TextView nameTextView = (TextView)view.findViewById(R.id.itemname);
			final TextView quantityTextView = (TextView)view.findViewById(R.id.itemquantity);
			final TextView desTextView = (TextView)view.findViewById(R.id.itemdescription);
			
			nameTextView.setText(results.get(RequestSupplies.ITEM));
			quantityTextView.setText(results.get(RequestSupplies.QUANTITY));
			desTextView.setText(results.get(RequestSupplies.DESCRIPTION));
			Button deleteBtn = (Button)view.findViewById(R.id.delete_btn);
			deleteBtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v) { 
					//do something

					db.DeleteRequestSuplies(nameTextView.getText().toString());
					list.remove(position);
					notifyDataSetChanged();
					if(list.isEmpty())
					{
						listview.setBackgroundResource(R.drawable.emptylist);
					}
				}
			});
			Button editBtn = (Button)view.findViewById(R.id.edit_btn);
			editBtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v) { 
					//do something
					AlertDialog.Builder builder=new AlertDialog.Builder(getActivity()); 
					LayoutInflater li = LayoutInflater.from(getActivity());
					View promptsView = li.inflate(R.layout.reqsupdetails, null);
					builder.setView(promptsView);
					builder.setCancelable(false);
					builder.setTitle("Edit Item Details ");
					final TextView itemno = (TextView)promptsView.
							findViewById(R.id.selecteditem);  
					itemno.setText(nameTextView.getText().toString());
					final String olditemno=itemno.getText().toString();

					final EditText itemq = (EditText)promptsView.
							findViewById(R.id.itemq); 
					
					itemq.setText(quantityTextView.getText().toString());
					final EditText itemd = (EditText)promptsView.
							findViewById(R.id.itemd); 
					itemd.setText(desTextView.getText().toString());
					
					builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							if(itemno.getText().toString().isEmpty())
							{
								Toast.makeText(getActivity(), "Item  must be selected", Toast.LENGTH_LONG).show();

								return;
							}
							if(itemq.getText().toString().isEmpty())
							{
								Toast.makeText(getActivity(), "Quantity cannot be empty", Toast.LENGTH_LONG).show();

								return;
							}
							list.remove(position);

							HashMap<String, String> map = new HashMap<String, String>();
							map.put(ITEM, itemno.getText().toString());
							map.put(QUANTITY, itemq.getText().toString());
							map.put(DESCRIPTION, itemd.getText().toString());
							list.add(position,map);

							db.UpdateRequestSupplies(olditemno, itemno.getText().toString(), itemq.getText().toString(),itemd.getText().toString());
							MyCustomAdapter marketingadapter = new MyCustomAdapter(list,getActivity());
							listview.setAdapter(marketingadapter);
							notifyDataSetChanged();
						}
					});
					builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {


						}
					});

					builder.show();
				}
			});


			return view; 
		} 
	}
	public  void generateemail()
	{
		
		
		
		
		
		String subject="Order Supplies for "+usernamefull;
		List<RequestSuppliesGetterSetter> scanned = db.getrequestsupplies();
		/*String body="Items                                                    "+"Quantity                              "+"Description                "+"\n";

		List<RequestSuppliesGetterSetter> scanned = db.getrequestsupplies();
		for (RequestSuppliesGetterSetter inventories : scanned) 
		{
			body=body+inventories.GetItemName()+"                                       "+inventories.GetItemQuantity()+"                                     "+inventories.GetItemDescription()+"                          "+"\n";

		}*/
		/*List<String> headersList = Arrays.asList("Items", "Quantity", "Description");
		List<RequestSuppliesGetterSetter> scanned = db.getrequestsupplies();
		List<List<String>> rowsListt = new ArrayList<List<String>>();
		
		for (RequestSuppliesGetterSetter inventories : scanned) 
		{
			String[] rsdetails = {inventories.GetItemName(),inventories.GetItemQuantity(),inventories.GetItemDescription()};
			rowsListt.add(Arrays.asList(rsdetails));
		}
		
		Board board = new Board(280);
		Table table = new Table(board, 280, headersList, rowsListt);
		List<Integer> colAlignList = Arrays.asList(
			    Block.DATA_CENTER, 
			    Block.DATA_CENTER, 
			    Block.DATA_CENTER);
			table.setColAlignsList(colAlignList);
		//table.setGridMode(Table.GRID_FULL).setColWidthsList(colWidthsListEdited);
		Block tableBlock = table.tableToBlocks();
		board.setInitialBlock(tableBlock);
		board.build();
		String body = board.getPreview();*/
	
		String body="";
		//body=body+String.format("%-22s%-22s%-22s", "Items", "Quantity", "Description"+"\n");
		//body=body+String.format("%-22s%-22s%-22s", "", "", ""+"\n");
		
		StringBuilder buf = new StringBuilder();
		buf.append("<html>" +
		           "<body>" +
		           "<table  style='border: 1px solid #495a8a;width: 100%;'>" +
		           "<tr>" +
		           "<th style='background: #495a8a;color: white;padding: 6px;text-align:  left;'>Items</th>" +
		           "<th style='background: #495a8a;color: white;padding: 6px;text-align:  left;'>Quantity</th>" +
		           "<th style='background: #495a8a;color: white;padding: 6px;text-align:  left;'>Description</th>" +
		           "</tr>");
		
		for (RequestSuppliesGetterSetter inventories : scanned) 
		{
			 buf.append("<tr><td style='border-right: 1px solid #495a8a;'>")
		       .append(inventories.GetItemName())
		       .append("</td><td style='border-right: 1px solid #495a8a;'>")
		       .append(inventories.GetItemQuantity())
		       .append("</td><td style='border-right: 1px solid #495a8a;'>")
		       .append(inventories.GetItemDescription())
		       .append("</td></tr>");
		}
		buf.append("</table>" +
		           "</body>" +
		           "</html>");
		 body = buf.toString();
		Log.e("body ",body);
		SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);

		String ccemail =pref.getString(preferences.AMEMAIL,"");
		if(ccemail.equals("")||ccemail.equalsIgnoreCase("NULL")){
		//SendEmail(body,subject);
			GetEmails webservice = new GetEmails(getActivity(), requestlistener,"requestsupplies", body, subject);
			webservice.execute();
		}
		else
		{
			//SendEmailwithcc(body,subject,ccemail);
			//SendEmail(body,subject);
			GetEmails webservice = new GetEmails(getActivity(), requestlistener,"requestsupplies", body, subject);
			webservice.execute();
		}
		db.DeleteRequestSupplies();
		//Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();
	}
	private void SendEmailwithcc(final String msg, final String subject,final String ccemail) {
		// TODO Auto-generated method stub
		//Toast.makeText(getActivity(), ccemail, Toast.LENGTH_LONG).show();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);
					
					sender.sendCCMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT,ccemail);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendOrderSuppliesMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmailNew(final String msg, final String subject,final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);
					sender.sendOrderSuppliesMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	@Override
	public void onResume() {
		super.onResume();



	}

}
