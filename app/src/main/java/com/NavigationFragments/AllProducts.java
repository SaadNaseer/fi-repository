package com.NavigationFragments;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.Database.Databaseadapter;
import com.GetterSetters.ProductsGetterSetter;
import com.Utils.ConnectionClass;
import com.Utils.Products;
import com.Utils.SFAPI;
import com.fundinginnovation.R;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class AllProducts extends Fragment{
	private ArrayList<Products> Productss = new ArrayList<Products>();
	ArrayList<HashMap<String, String>> listing;
	static String itemno = "itemno";
	static String itemdescription = "itemdescription";
	ConnectionClass connectionClass;
	ListView listView;
	EditText search;
	Connection con ;
	int imagepos=0;
	int currentrowcount=0;
	int countlocal=0;
	int dblimitcount=5;
	Databaseadapter db;
	@SuppressWarnings("unused")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View rootView;
		 rootView = inflater.inflate(R.layout.products_directory, container, false);
		 
		 TextView ch=(TextView)rootView.findViewById(R.id.textView1);
		 
		 ch.setText("");
		 db=new Databaseadapter(getActivity());
			db.open();
			db.getdata();
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					getActivity().finishAffinity();
					Intent i= new Intent(getActivity(),NavigationActivity.class);
					i.putExtra("val", "");
					startActivity(i);
					FragmentManager fragmentManager = getFragmentManager();
					/*fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();*/
					fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}
			});
			
			Button loadnext = (Button) rootView
					.findViewById(R.id.load_next);
			loadnext.setVisibility(View.INVISIBLE);
			loadnext.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					listing.clear();
					Productss.clear();
					int pcount=0;
					int countt=0;
					if(db.getProductsCount()>1)
					{
						
						List<ProductsGetterSetter> products = db.get5products(dblimitcount); 
						if(products.size()<1)
						{
							GetProducts getproducts = new GetProducts(); 
							getproducts.execute("");
							
						}
					
						for (ProductsGetterSetter product : products) 
						{
							HashMap<String, String> map = new HashMap<String, String>();
						
							Products productss = new Products();
							productss.setItemNo(product.GetProductName());
							productss.setItemDesc(product.GetProductDescription());
							productss.setPicture(product.GetProductPicture());
							
							Productss.add(productss);
					
							listing.add(map);
							
							countlocal++;
							countt++;
							currentrowcount++;
						}
						
						dblimitcount=dblimitcount+5;
						final ProductsAdapter   adapter = new ProductsAdapter(getActivity(),Productss, listing);
						// Binds the Adapter to the ListView
						listView.setAdapter(adapter);
						search.addTextChangedListener(new TextWatcher() {

					        @Override
					        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
					        	adapter.getFilter().filter(arg0);
					        }

					        @Override
					        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					                int arg3) {
					        }

					        @Override
					        public void afterTextChanged(Editable arg0) {

					        }
					    });
					}
					else
					{
						GetProducts getproducts = new GetProducts(); 
						getproducts.execute("");
					}
				}
			});
			/*Button loadnext = (Button) rootView
					.findViewById(R.id.load_next);
			loadnext.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					listing.clear();
					Productss.clear();
					GetProducts getproducts = new GetProducts(); 
					getproducts.execute("");
				}
			});*/
		 listing = new ArrayList<HashMap<String, String>>();
		 listView=(ListView)rootView.findViewById(R.id.listview);
			listView.setCacheColorHint(Color.TRANSPARENT);
			connectionClass = new ConnectionClass();
			search=(EditText)rootView.findViewById(R.id.editText1);
			try{
			if(db.getProductsCount()>1)
			{
				
				countlocal=0;
				int lcount=0;
				List<ProductsGetterSetter> products = db.getproducts(); 
				
				for (ProductsGetterSetter product : products) 
				{
					if(products.size()<1)
					{
						
						
					}
					/*if(lcount<5){*/
					HashMap<String, String> map = new HashMap<String, String>();
				
					Products productss = new Products();
					productss.setItemNo(product.GetProductName());
					productss.setItemDesc(product.GetProductDescription());
					productss.setPicture(product.GetProductPicture());
					
					Productss.add(productss);
			
					listing.add(map);
					lcount++;
					countlocal++;
					currentrowcount++;
					
					/*}*/
					
				}
				final ProductsAdapter   adapter = new ProductsAdapter(getActivity(),Productss, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
			}
			else
			{
				GetProducts getproducts = new GetProducts(); 
				getproducts.execute("");
			}
			}
			catch(Exception y){
				db.DeleteProducts();
				GetProducts getproducts = new GetProducts(); 
			getproducts.execute("");}
		 return rootView;
	}
	
	public class GetProducts extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber="";
		String itemdesc="";
		int count=0;

		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading Products ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			con=null;
			if(isSuccess) {
				
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				
				//currentrowcount+=5;;
				final ProductsAdapter   adapter = new ProductsAdapter(getActivity(),Productss, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

			        @Override
			        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			        	adapter.getFilter().filter(arg0);
			        }

			        @Override
			        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			                int arg3) {
			        }

			        @Override
			        public void afterTextChanged(Editable arg0) {

			        }
			    });
				/*GetPictures getpics = new GetPictures(); 
				getpics.execute("");*/
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Assemble the login request URL
				String EaselsURL = SFAPI.baseUri +"/sobjects"+SFAPI.Products
						;
				Log.e("URL ",EaselsURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(EaselsURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;
				try {
					// Execute the login POST request
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

				}
				// verify response is HTTP OK
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					//System.out.println("Error authenticating to Force.com: "+statusCode);
					//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;

				}
				else
				{
					isSuccess=true;
					z ="Success";
					String getResult = null;
					try {
						getResult = EntityUtils.toString(response.getEntity());
						//Log.e("RESULT ",getResult);
						JSONObject jobj=new JSONObject(getResult);
						JSONArray jarr=jobj.getJSONArray("recentItems");
						for(int i=0;i<jarr.length();i++)
						{
							JSONObject eitems=jarr.getJSONObject(i);
							String id=eitems.getString("Id");
							String Name=eitems.getString("Name");

							HttpClient httpclient2 = new DefaultHttpClient();
							// Assemble the login request URL
							String ProductsURL2 = SFAPI.baseUri +"/sobjects"+SFAPI.Products+"/"+id
									;
							Log.e("URL ",ProductsURL2);

							// Login requests must be POSTs
							HttpGet httpPost2 = new HttpGet(ProductsURL2);
							httpPost2.addHeader(SFAPI.oauthHeader);
							httpPost2.addHeader(SFAPI.prettyPrintHeader);


							HttpResponse response2 = null;
							try {
								// Execute the login POST request
								response2 = httpclient2.execute(httpPost2);
							} catch (ClientProtocolException cpException) {
								cpException.printStackTrace();

							} catch (IOException ioException) {
								ioException.printStackTrace();

							}
							// verify response is HTTP OK
							final int statusCode2 = response2.getStatusLine().getStatusCode();
							if (statusCode2 != HttpStatus.SC_OK) {

							}
							else {
								JSONObject jsonObjectt=new JSONObject(EntityUtils.toString(response2.getEntity()));
								String ProductCode=jsonObjectt.getString("StockKeepingUnit");
								String Description=jsonObjectt.getString("Name");
								itemnumber=ProductCode;
								itemdesc=Description;
								Log.e("ProductCode ",ProductCode);
								Log.e("Description ",Description);
								String pic="";
								HashMap<String, String> map = new HashMap<String, String>();
								map.put(itemno, ProductCode);
								map.put(itemdescription, Description);
								Products products = new Products();
								products.setItemNo(itemnumber);
								products.setItemDesc(itemdesc);
								products.setPicture(pic);
								Productss.add(products);
								listing.add(map);
								db.insertproducts(new ProductsGetterSetter(itemnumber,itemdesc,pic));
							}
						}
						if(!listing.isEmpty())
						{

							z = "Products Found";
							isSuccess=true;
						}
						else
						{
							z = "No Products Found";
							isSuccess = false;
						}
					} catch (IOException e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
					JSONObject jsonObject = null;

					try {

					} catch (Exception e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
				}



			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}
return z;
		}
	}
	public class ProductsAdapter extends BaseAdapter implements Filterable{
		Context context;
		LayoutInflater inflater;
		 RadioButton mSelectedRB;
		 private ValueFilter valueFilter;
		 @SuppressWarnings("unused")
		private ArrayList<Products> Products;
		 private ArrayList<Products> mStringFilterList;
		   int mSelectedPosition = -1;
		ArrayList<HashMap<String, String>> listt;
		public ProductsAdapter(Context context,
				ArrayList<Products> pro,ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			 this.Products = pro;
			    mStringFilterList =  pro;
			    getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Productss.size();
		}
		@Override
		public Object getItem(int position) {
		    return Productss.get(position).getItemNo();
		}
		

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			View  itemView = inflater.inflate(R.layout.productslist, parent, false);
			
			ImageView imageView2=(ImageView)itemView.findViewById(R.id.imageView2);
			ImageView imgbox=(ImageView)itemView.findViewById(R.id.imageView1);

			
			final TextView itemdescription=(TextView)itemView.findViewById(R.id.itemdescription);
			final TextView itemno=(TextView)itemView.findViewById(R.id.itemno);
			
				
					// TODO Auto-generated method stub
				/*	new Thread(new Runnable() {
						 
						  @Override
						  public void run() {
						    try 
						    {
						    	String query = "select Picture from dbo.vw_pas_AllItems where Art='"+itemno.getText().toString()+"'";
								//String query="SELECT  * FROM (SELECT  TOP 20 t.*, ROW_NUMBER() OVER (ORDER BY Art) AS rn FROM  dbo.vw_pas_AllItems t ORDER BY Art) t WHERE   rn > 10";
						    	Log.e("Query ", query);
						    	Statement stmt = con.createStatement();
								ResultSet rs = stmt.executeQuery(query);
								String pic="";
								if(rs.getBlob("Picture")!=null){
								Blob blob = rs.getBlob("Picture");

								int blobLength = (int) blob.length();  
								byte[] blobAsBytes = blob.getBytes(1, blobLength);
								pic = Base64.encodeToString(blobAsBytes,Base64.NO_WRAP);
								}
								else{pic="";}
								Productss.get(position).setPicture(pic);
						    	
						    } catch (Exception e) {
						      Log.e("Pictures ", e.getMessage(), e);
						      Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
						  }
						}
						 
						}).start();
				}*/
			
			
			/*if (imgbox.getTag() == null ||
					!imgbox.getTag().equals(Productss.get(position).getPicture())) {*/
			try
			{
				byte[] decodedString = Base64.decode(Productss.get(position).getPicture(), Base64.DEFAULT);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig = Config.RGB_565;
				Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
				imgbox.setImageBitmap(bmp);
			}
			catch(Exception t)
			{
				
			}
			/*}
			else
			{
				imgbox.setTag(Productss.get(position).getPicture());
			}*/
		
			
				imageView2.setVisibility(View.INVISIBLE);
				
			
				itemno.setText("" +Productss.get(position).getItemNo());
				itemdescription.setText("" +Productss.get(position).getItemDesc());
		
			
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
					LayoutInflater inflater = getActivity().getLayoutInflater();
					View convertView = (View) inflater.inflate(R.layout.customdialog, null);
					builder.setView(convertView);
					final AlertDialog alertDialog = builder.create();
					alertDialog.setCancelable(false);
					
					ImageView	dimageView1 = (ImageView) convertView.findViewById(R.id.dimageView1);
					TextView	mtitle = (TextView) convertView.findViewById(R.id.ditemno);
					mtitle.setText(itemno.getText().toString());
					
					TextView	mdes = (TextView) convertView.findViewById(R.id.ditemdescription);
					mdes.setText(itemdescription.getText().toString());
					
					try
					{
						byte[] decodedString = Base64.decode(Productss.get(position).getPicture(), Base64.DEFAULT);
						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig = Config.RGB_565;
						Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
						dimageView1.setImageBitmap(bmp);
					}
					catch(Exception t)
					{
						
					}
					
					
					Button	close = (Button) convertView.findViewById(R.id.okbtn);
					
					close.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
						
							alertDialog.dismiss();
						}
					});
					alertDialog.show();
				}
			});
			return itemView;}
		
		@Override
		public Filter getFilter() {
		    if(valueFilter==null) {

		        valueFilter=new ValueFilter();
		    }

		    return valueFilter;
		}
		private class ValueFilter extends Filter {

		    //Invoked in a worker thread to filter the data according to the constraint.
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		        FilterResults results=new FilterResults();
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<Products> filterList=new ArrayList<Products>();
		        	
		            for(int i=0;i<mStringFilterList.size();i++){
		               /* if((mStringFilterList.get(i).getItemDesc().toUpperCase())
		                        .contains(constraint.toString().toUpperCase()))*/ 
		            	 if((mStringFilterList.get(i).getItemNo().toUpperCase()).contains(constraint.toString().toUpperCase())||mStringFilterList.get(i).getItemDesc().toUpperCase()
			                        .contains(constraint.toString().toUpperCase())){
		                	Products pro = new Products();
		                    pro.setItemNo(mStringFilterList.get(i).getItemNo());
		                    pro.setItemDesc(mStringFilterList.get(i).getItemDesc());
		                    filterList.add(pro);
		                }
		            }
		            results.count=filterList.size();
		            results.values=filterList;
		        }else{
		            results.count=mStringFilterList.size();
		            results.values=mStringFilterList;
		        }
		        return results;
		    }


		    //Invoked in the UI thread to publish the filtering results in the user interface.
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint,
		            FilterResults results) {
		    	Productss=(ArrayList<Products>) results.values;
		        notifyDataSetChanged();
		    }
		}
	}
	
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        //setOnBackPressListener();
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressWarnings("unused")
			@SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	boolean move=false;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                
            		FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();
                }
                return false;
            }
        });
    }
}
	

