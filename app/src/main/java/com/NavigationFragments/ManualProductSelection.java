package com.NavigationFragments;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.GetterSetters.ProductsGetterSetter;
import com.Utils.ConnectionClass;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.Products;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class ManualProductSelection extends Fragment {
	private ArrayList<Products> Productss = new ArrayList<Products>();
	ArrayList<HashMap<String, String>> listing;
	static String itemno = "itemno";
	static String itemdescription = "itemdescription";
	ConnectionClass connectionClass;
	ListView listView;
	EditText search;
	Button confirmproductbtn;
	String artno = "";
	String artdes = "";
	String tolocationid = "";
	String tonewsproductid = "";
	String oldproduct = "";
	String newproduct = "";
	RequestListener requestlistener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView;
		rootView = inflater.inflate(R.layout.manual_products_directory, container, false);
		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails, String emailmsg, String subject) {
				if (emails.contains(",")) {
					String arr[] = emails.split(",");
					for (int i = 0; i < arr.length; i++) {
						String emailaddr = arr[i];
						SendEmailNew(emailmsg, subject, emailaddr);
						Toast.makeText(getActivity(), "Email has been sent", Toast.LENGTH_LONG).show();
					}
				} else {
					SendEmailNew(emailmsg, subject, emails);
					Toast.makeText(getActivity(), "Email has been sent", Toast.LENGTH_LONG).show();
				}
			}

			@Override
			public void onError(String result) {
				Toast.makeText(getActivity(), "Error occured in sending smail ", Toast.LENGTH_LONG).show();
			}
		};
		oldproduct = Home.SelectedArt;
		confirmproductbtn = (Button) rootView.findViewById(R.id.confirm_product_button);

		confirmproductbtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				//Home.SelectedArt=artno;
				//Home.SelectedArtDescription=artdes;
				newproduct = artno;

				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME, 0);

				String userid = settings.getString(preferences.username, "");


				String emailvalues = "Easel   : " + Home.SelectedEasel + "\nUser   : " + userid + "\nProduct system says is on easel   : " + oldproduct + "\nProduct you picked   : " + newproduct;

				//SendEmail(emailvalues, "Change product for userid: "+userid);

				GetEmails webservice = new GetEmails(getActivity(), requestlistener, "changeproduct", emailvalues, "Change product for userid: " + userid);
				webservice.execute();
				//InsertRecord doInsert = new InsertRecord();
				//doInsert.execute("");\

				//change product
				ChangeProduct changeproduct = new ChangeProduct();
				changeproduct.execute("");


			}
		});
		ImageView imageButton = (ImageView) rootView
				.findViewById(R.id.menuicon);
		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				/*if(!NavigationActivity.draweropened){*/
				NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
				NavigationActivity.draweropened = true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
			}
		});
		ImageView homeButton = (ImageView) rootView
				.findViewById(R.id.homeicon);
		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Cancel ?");
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setMessage("Are you sure you wish to cancel the transaction ?");
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
												int arg1) {

							}
						});
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
												int arg1) {
								preferences p = new preferences(getActivity());
								p.SetLastScreen("");
								p.Settotalbids("");
								Databaseadapter db = new Databaseadapter(getActivity());
								db.DeleteRunningTransactions();
								getActivity().finishAffinity();
								Intent i = new Intent(getActivity(), NavigationActivity.class);
								i.putExtra("val", "");
								startActivity(i);
								FragmentManager fragmentManager = getFragmentManager();
									/*fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
								fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
							}
						});

				builder.create().show();
			}
		});
		listing = new ArrayList<HashMap<String, String>>();
		listView = (ListView) rootView.findViewById(R.id.listview);
		listView.setCacheColorHint(Color.TRANSPARENT);
		connectionClass = new ConnectionClass();
		search = (EditText) rootView.findViewById(R.id.editText1);
		GetProducts getproducts = new GetProducts();
		getproducts.execute("");
		return rootView;
	}

	public class GetProducts extends AsyncTask<String, String, String> {
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber = "";
		String itemdesc = "";


		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(getActivity());
			p.setMessage("Loading All Products ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if (isSuccess) {

				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();


				final ProductsAdapter adapter = new ProductsAdapter(getActivity(), Productss, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
				search.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
						adapter.getFilter().filter(arg0);
					}

					@Override
					public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
												  int arg3) {
					}

					@Override
					public void afterTextChanged(Editable arg0) {

					}
				});
			} else {
				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {


			try
			{
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String conid=  	settings.getString(preferences.useridd,"");
				String initials=  	settings.getString(preferences.initials,"");
				String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
				String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+initials;
				//Log.e("Locations Url ",locationsapurl);
				HttpGet locations = new HttpGet(locationsapurl);
				locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				locations.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse locationsresponse = httpclient.execute(locations);
				JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
				if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
					JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

					for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
						JSONObject items = locationpayloadarray.getJSONObject(lp);
						String id = items.getString("id");
						String name = items.getString("name");
						if(!name.equalsIgnoreCase(initialsdef)){continue;}
						String locationinventoryapurl = SFAPI.advanceprourl + "/api/locationInventory?location_id=" + id;
						//Log.e("LocationsInventory Url ", locationinventoryapurl);

						HttpGet locationInventory = new HttpGet(locationinventoryapurl);
						locationInventory.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
						locationInventory.addHeader(SFAPI.prettyPrintHeader);
						HttpResponse locationInventoryresponse = httpclient.execute(locationInventory);
						JSONObject locationInventoryresponseobject=new JSONObject(EntityUtils.toString(locationInventoryresponse.getEntity()));
						if(locationInventoryresponseobject.getString("success").equalsIgnoreCase("true"))
						{
							JSONArray locationInventorypayloadarray = locationInventoryresponseobject.getJSONArray("payload");
							for (int lip = 0; lip < locationInventorypayloadarray.length(); lip++) {
								JSONObject lipitems = locationInventorypayloadarray.getJSONObject(lip);
								String product_id = lipitems.getString("product_id");
								String location_id = lipitems.getString("location_id");
								String location_name = lipitems.getString("location_name");
								//Log.e("product id ",product_id);
								//Log.e("location_id ",location_id);
								//Log.e("location_name ",location_name);
								String productsapurl = SFAPI.advanceprourl + "/api/products/" + product_id;
								HttpGet product = new HttpGet(productsapurl);
								product.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
								product.addHeader(SFAPI.prettyPrintHeader);
								HttpResponse productresponse = httpclient.execute(product);
								JSONObject productobject=new JSONObject(EntityUtils.toString(productresponse.getEntity()));
								if(productobject.getString("success").equalsIgnoreCase("true"))
								{
									JSONObject jsonproductobject=productobject.getJSONObject("payload");
									String productsku = jsonproductobject.getString("sku");
									String productname = jsonproductobject.getString("name");
									String productdescription = jsonproductobject.getString("description");
									String productavailable_quantity = jsonproductobject.getString("available_quantity");
									Log.e("product sku ",productsku);
									Log.e("product name ",productname);
									Log.e("product description ",productdescription);

									if(productavailable_quantity.equalsIgnoreCase("")||Float.parseFloat(productavailable_quantity)<1.0)
									{}
									else {
										Log.e("product quantity ",productavailable_quantity);
										String art = productsku;
										itemnumber = art;
										itemdesc = productname;
										HashMap<String, String> map = new HashMap<String, String>();
										map.put(itemno, itemnumber);
										map.put(itemdescription, itemdesc);
										map.put("tolocid", location_id);
										map.put("pic", "");
										Log.e("itemno ", itemnumber);
										Log.e("itemdes ", itemdesc);
										Log.e("toloc ", location_id);
										Products products = new Products();
										products.setItemNo(itemnumber);
										products.setItemDesc(itemdesc);
										products.settoLocationId(location_id);
										products.setNewProductID(product_id);
										products.setPicture("");
										Productss.add(products);
										listing.add(map);
									}
								}
							}
						}
					}
				}
				if(listing.isEmpty())
				{
					isSuccess = false;
					z = "No Products Found";
				}
				else
				{
					z = "Products Found";
					isSuccess = true;
				}
			}
			catch (Exception aa)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + aa.toString();

			}
			return z;
		}
	}

	public class ProductsAdapter extends BaseAdapter implements Filterable {
		Context context;
		LayoutInflater inflater;
		int pos = -1;
		RadioButton mSelectedRB;
		private ValueFilter valueFilter;
		@SuppressWarnings("unused")
		private ArrayList<Products> Products;
		private ArrayList<Products> mStringFilterList;
		int mSelectedPosition = -1;
		ArrayList<HashMap<String, String>> listt;

		public ProductsAdapter(Context context,
							   ArrayList<Products> pro, ArrayList<HashMap<String, String>> vulist) {
			// TODO Auto-generated constructor stub
			this.context = context;
			listt = vulist;
			this.Products = pro;
			mStringFilterList = pro;
			getFilter();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Productss.size();
		}

		@Override
		public Object getItem(int position) {
			return Productss.get(position).getItemNo();
		}


		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			final View itemView = inflater.inflate(R.layout.manualproductslist, parent, false);
			if (pos == position) {
				itemView.setBackgroundColor(Color.parseColor("#666666"));
			} else {
				itemView.setBackgroundColor(Color.parseColor("#EEEEEE"));
			}
			ImageView imageView2 = (ImageView) itemView.findViewById(R.id.imageView2);
			final TextView itemdescription = (TextView) itemView.findViewById(R.id.itemdescription);
			final TextView itemno = (TextView) itemView.findViewById(R.id.itemno);
			final TextView tonewproductidtv = (TextView) itemView.findViewById(R.id.tonewproductid);
			final TextView tolocationidtv = (TextView) itemView.findViewById(R.id.tolocationid);
			imageView2.setVisibility(View.INVISIBLE);
			itemno.setText("" + Productss.get(position).getItemNo());
			itemdescription.setText("" + Productss.get(position).getItemDesc());
			tonewproductidtv.setText("" + Productss.get(position).getNewProductID());
			tolocationidtv.setText("" + Productss.get(position).gettoLocationId());

			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					pos = position;
					itemView.setBackgroundColor(Color.parseColor("#666666"));
					confirmproductbtn.setVisibility(View.VISIBLE);
					artno = itemno.getText().toString();
					artdes = itemdescription.getText().toString();
					//tolocationid = Productss.get(position).gettoLocationId();
					//tonewsproductid=Productss.get(position).getNewProductID();
					tolocationid=tolocationidtv.getText().toString();
					tonewsproductid=tonewproductidtv.getText().toString();
					listView.invalidateViews();
				}
			});
			return itemView;
		}

		@Override
		public Filter getFilter() {
			if (valueFilter == null) {

				valueFilter = new ValueFilter();
			}

			return valueFilter;
		}

		private class ValueFilter extends Filter {

			//Invoked in a worker thread to filter the data according to the constraint.
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				if (constraint != null && constraint.length() > 0) {
					ArrayList<Products> filterList = new ArrayList<Products>();

					for (int i = 0; i < mStringFilterList.size(); i++) {
						if ((mStringFilterList.get(i).getItemNo().toUpperCase()).contains(constraint.toString().toUpperCase()) || mStringFilterList.get(i).getItemDesc().toUpperCase()
								.contains(constraint.toString().toUpperCase())) {
							Products pro = new Products();
							pro.setItemNo(mStringFilterList.get(i).getItemNo());
							pro.setItemDesc(mStringFilterList.get(i).getItemDesc());
							pro.setNewProductID(mStringFilterList.get(i).getNewProductID());
							pro.settoLocationId(mStringFilterList.get(i).gettoLocationId());
							filterList.add(pro);
						}
					}
					results.count = filterList.size();
					results.values = filterList;
				} else {
					results.count = mStringFilterList.size();
					results.values = mStringFilterList;
				}
				return results;
			}


			//Invoked in the UI thread to publish the filtering results in the user interface.
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
										  FilterResults results) {
				Productss = (ArrayList<Products>) results.values;
				notifyDataSetChanged();
			}
		}
	}

	private void SendEmailNew(final String msg, final String subject, final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}

	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}

	@Override
	public void onResume() {
		super.onResume();

		// Call the 'activateApp' method to log an app event for use in analytics and advertising
		// reporting.  Do so in the onResume methods of the primary Activities that an app may be
		// launched into.
		//setOnBackPressListener();

	}

	private void setOnBackPressListener() {

		this.getView().setFocusableInTouchMode(true);
		this.getView().requestFocus();
		this.getView().setOnKeyListener(new View.OnKeyListener() {
			@SuppressLint("NewApi")
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {

					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.frame_container, new EaselConfirmation()).commit();
				}
				return false;
			}
		});
	}

	public class ChangeProduct extends AsyncTask<String, String, String> {
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;

		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(getActivity());
			p.setMessage("Changing Product ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if (isSuccess) {
				//preferences pre = new preferences(getActivity());
				//pre.SetChangedartno(artno);
				//pre.SetChangedartdes(artdes);
			/*	Home.SelectedArt=artno;
				Home.SelectedArtDescription=artdes;
				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
				Home.SelectedAPProductID=tonewsproductid;
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().addToBackStack(null)
						.replace(R.id.frame_container, new EaselConfirmation()).commit();*/
			new ChangeProduct2().execute();
			} else {
				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
			}

		}

		@SuppressWarnings("unused")
		@Override
		protected String doInBackground(String... params) {


			String uri = SFAPI.advanceprourl + "/api/" + SFAPI.advanceproinventoryTransfer;
			Log.e("Url :", uri);
			try {

				//create the JSON object containing the new lead details.
				JSONObject auction = new JSONObject();
				auction.put("product_id", Integer.parseInt(tonewsproductid));
				auction.put("variant_id", 0);
				auction.put("from_location_id", Integer.parseInt(tolocationid));
				auction.put("to_location_id", Integer.parseInt(Home.SelectedFROMLOCATIONID));
				auction.put("quantity", 1);
				auction.put("reason", "Change Product");

				Log.e("", "JSON for CP record to be inserted:\n" + auction.toString(1));
				//Toast.makeText(LoginActivity.this,"JSON for lead record to be inserted:\n" + lead.toString(1),Toast.LENGTH_LONG).show();

				//Construct the objects needed for the request
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(uri);
				httpPost.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				httpPost.addHeader(SFAPI.prettyPrintHeader);
				// The message we are going to post
				StringEntity body = new StringEntity(auction.toString(1));
				body.setContentType("application/json");
				httpPost.setEntity(body);

				//Make the request
				HttpResponse response = httpClient.execute(httpPost);

				//Process the results
				int statusCode = response.getStatusLine().getStatusCode();
				Log.e("StatusCode ",""+statusCode);
				String changedfirstresponse=EntityUtils.toString(response.getEntity());
				if (statusCode == 201) {

						isSuccess=true;//z="Successful";
                    z="Successful";

				} else {
					//Toast.makeText(LoginActivity.this,"Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity()),Toast.LENGTH_LONG).show();
					//System.out.println("Insertion unsuccessful. Status code returned is " + statusCode);
					z = "Insertion unsuccessful. Status response " + changedfirstresponse;
					isSuccess = false;
				}

			} catch (Exception ex) {
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return z;

		}
		public class ChangeProduct2 extends AsyncTask<String, String, String> {
			String z = "";
			Boolean isSuccess = false;
			ProgressDialog p;

			@Override
			protected void onPreExecute() {
				p = new ProgressDialog(getActivity());
				p.setMessage("Processing ...");
				p.setCancelable(false);
				p.show();
			}

			@Override
			protected void onPostExecute(String r) {
				p.dismiss();
				if (isSuccess) {
					//preferences pre = new preferences(getActivity());
					//pre.SetChangedartno(artno);
					//pre.SetChangedartdes(artdes);
					Home.SelectedArt=artno;
					Home.SelectedArtDescription=artdes;
					Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
					Home.SelectedAPProductID=tonewsproductid;
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new EaselConfirmation()).commit();
				} else {
					Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
				}

			}

			@SuppressWarnings("unused")
			@Override
			protected String doInBackground(String... params) {

				try {
					HttpClient httpClient = new DefaultHttpClient();
					SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
					String conid=  	settings.getString(preferences.useridd,"");
					String initials=  	settings.getString(preferences.initials,"");
					String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
					String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+initialsdef;
					//Log.e("Locations Url ",locationsapurl);
					String tolocationid="";
					HttpGet locations = new HttpGet(locationsapurl);
					locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
					locations.addHeader(SFAPI.prettyPrintHeader);
					HttpResponse locationsresponse = httpClient.execute(locations);
					JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
					if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
						JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

						for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
							JSONObject items = locationpayloadarray.getJSONObject(lp);
							tolocationid = items.getString("id");

						}
					}

					String uri = SFAPI.advanceprourl + "/api/" + SFAPI.advanceproinventoryTransfer;
					Log.e("Url :", uri);


					//create the JSON object containing the new lead details.
					JSONObject auction = new JSONObject();
					auction.put("product_id", Integer.parseInt(Home.SelectedAPProductID));
					auction.put("variant_id", 0);
					auction.put("from_location_id", Integer.parseInt(Home.SelectedFROMLOCATIONID));
					auction.put("to_location_id", Integer.parseInt(tolocationid));
					auction.put("unassign_after_transfer", true);
					auction.put("quantity", 1);
					auction.put("reason", "Change Product");


					Log.e("", "JSON for CP record to be inserted:\n" + auction.toString(1));
					//Toast.makeText(LoginActivity.this,"JSON for lead record to be inserted:\n" + lead.toString(1),Toast.LENGTH_LONG).show();

					//Construct the objects needed for the request

					HttpPost httpPost = new HttpPost(uri);
					httpPost.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
					httpPost.addHeader(SFAPI.prettyPrintHeader);
					// The message we are going to post
					StringEntity body = new StringEntity(auction.toString(1));
					body.setContentType("application/json");
					httpPost.setEntity(body);

					//Make the request
					HttpResponse response = httpClient.execute(httpPost);
					String changedfirstresponse=EntityUtils.toString(response.getEntity());
					//Process the results
					int statusCode = response.getStatusLine().getStatusCode();
					if (statusCode == 201) {

						String ProductURL = SFAPI.baseUri +"/query/?q="+ URLEncoder.encode("SELECT Id FROM Product2 Where StockKeepingUnit= '"+artno+"'", "UTF-8");
						Log.e("URL ",ProductURL);
						// Login requests must be POSTs
						HttpGet httpPostgp = new HttpGet(ProductURL);
						httpPostgp.addHeader(SFAPI.oauthHeader);
						httpPostgp.addHeader(SFAPI.prettyPrintHeader);


						HttpResponse responsegp = null;

						// Execute the login POST request
						responsegp = httpClient.execute(httpPostgp);

						// verify response is HTTP OK
						final int statusCodesp = responsegp.getStatusLine().getStatusCode();
						String newProductID="";
						if(statusCodesp!= HttpStatus.SC_OK)
						{

						}
						else
						{

							JSONObject prodidjobj=new JSONObject(EntityUtils.toString(responsegp.getEntity()));
							JSONArray jarrpi=prodidjobj.getJSONArray("records");
							for(int ii=0;ii<jarrpi.length();ii++)
							{
								JSONObject pitem = jarrpi.getJSONObject(ii);
								Log.e("PID ",pitem.getString("Id"));
								newProductID=pitem.getString("Id");
								Home.SelectedProductID=newProductID;
							}
						}
						if(newProductID.equalsIgnoreCase(""))
						{
							isSuccess=false;
							z="Product Not Found on SalesForce";
						}
						else
						{
							isSuccess=true;//z="Successful";
							z="Successful";
						}
					} else {
						//Toast.makeText(LoginActivity.this,"Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity()),Toast.LENGTH_LONG).show();
						//System.out.println("Insertion unsuccessful. Status code returned is " + statusCode);
						z = "Insertion unsuccessful. Status response " + changedfirstresponse;
						isSuccess = false;
					}

				} catch (Exception ex) {
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}

				return z;

			}

		}
		public class InsertRecord extends AsyncTask<String, String, String> {
			String z = "";
			Boolean Success = false;
			ProgressDialog p;

			@Override
			protected void onPreExecute() {
				p = new ProgressDialog(getActivity());
				p.setMessage("Inserting Record ...");
				p.setCancelable(false);
				p.show();
			}

			@Override
			protected void onPostExecute(String r) {
				p.dismiss();
				if (Success) {

					Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();

					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new EaselConfirmation()).commit();
				} else {
					Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
				}

			}

			@SuppressWarnings("unused")
			@Override
			protected String doInBackground(String... params) {

				try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME, 0);
						String usernamefull = pref.getString(preferences.username, "");
						String m = "";
						String d = "";
						String modifiedmonth = "";
						String modifiedday = "";
						Date date = new Date(); // your date
						Calendar cal = Calendar.getInstance();
						cal.setTime(date);
						int year = cal.get(Calendar.YEAR);
						int month = cal.get(Calendar.MONTH);
						int day = cal.get(Calendar.DAY_OF_MONTH);
						month = month + 1;
						if (day < 10) {
							modifiedday = "0" + String.valueOf(day);
						} else if (day >= 10) {
							modifiedday = String.valueOf(day);
						}
						if (month < 10) {
							modifiedmonth = "0" + String.valueOf(month);
						} else if (month >= 10) {
							modifiedmonth = String.valueOf(month);
						}
						if (day < 10) {
							d = "0" + String.valueOf(day);
						} else {
							d = String.valueOf(day);
						}

						if (month <= 9) {
							m = String.valueOf(month);
						} else if (month == 10) {
							m = "O";

						} else if (month == 11) {
							m = "N";

						} else if (month == 12) {
							m = "D";

						}

						String turndate = String.valueOf(year) + String.valueOf(modifiedmonth) + String.valueOf(modifiedday);
						String query = "Insert into dbo.EaselTurnChange (loginid,easel,original_item,actual_item,turn_date) values ('"
								+ usernamefull + "','" + Home.SelectedEasel + "','" + oldproduct + "','" + newproduct + "','" + turndate

								+ "')";
						PreparedStatement preStmt = con.prepareStatement(query);

						preStmt.executeUpdate();

						z = "Inserted Successfully";

						Success = true;

					}
				} catch (Exception ex) {
					Success = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}

				return z;
			}
		}
	}
}
	

