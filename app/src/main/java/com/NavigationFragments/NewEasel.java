package com.NavigationFragments;

import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Database.Databaseadapter;
import com.Utils.ConnectionClass;
import com.Utils.Products;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.navigationdrawer.NavigationActivity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class NewEasel extends Fragment implements OnClickListener{
	private Button scanBtn,manualentryBtn;
	ConnectionClass connectionClass;
	ListView listView;
	ArrayList<HashMap<String, String>> listing;
//////////////////////////////////////////New Usable Strings ///////////////////////////////////////////////////////////////////
public static String SelectedArt = "SelectedArt";
public static String SelectedLocation = "SelectedLocation";
public static String SelectedCharity = "SelectedCharity";
public static String SelectedSponsor = "SelectedSponsor";
public static String SelectedEasel = "SelectedEasel";
public static String SelectedQuantity = "SelectedQuantity";
public static String SelectedStreetAddress = "SelectedStreetAddress";
public static String SelectedCity = "SelectedCity";
public static String SelectedProvince= "SelectedProvince";
public static String SelectedPostCode= "SelectedPostCode";
public static String SelectedArtDescription="SelectedArtDescription";
static String scanContent="";
static String scanFormat="";

///////////////////////////////////Old Listing Strings/////////////////////////////////////////////////////////
static String Art = "Art";
static String Location = "Location";
static String Charity = "Charity";
static String Quantity = "Quantity";
static String Sponsor = "Sponsor";
static String StreetAddress = "StreetAddress";
static String City = "City";
static String Province = "Province";
static String PostalCode = "PostalCode";
static String ArtDescription = "ArtDescription";
static String Easel = "Easel";
TextView linktoallproducts;
static boolean getvaluesfromproducts=false;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 View rootView;
		 rootView = inflater.inflate(R.layout.activity_neweasel, container, false);
		// Toast.makeText(getActivity(),String.valueOf(getvaluesfromproducts),Toast.LENGTH_LONG).show();
		 linktoallproducts= (TextView) rootView.findViewById(R.id.linktoallproducts);
		 SpannableString content = new SpannableString("No BarCode ?");
		 content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		 linktoallproducts.setText(content);
		 linktoallproducts.setTextColor(Color.parseColor("#0645AD"));
		 linktoallproducts.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					getvaluesfromproducts=true;
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new ManualProductSelectionForNewPicture()).commit();
				}
			});
		 ImageView imageButton = (ImageView) rootView
					.findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if(!NavigationActivity.draweropened){*/
						NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=true;
					/*}
					else{
						NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
						NavigationActivity.draweropened=false;
					}*/
				}
			});
			ImageView homeButton = (ImageView) rootView
					.findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(getActivity());
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(getActivity());
										db.DeleteRunningTransactions(); 
										getActivity().finishAffinity();
										Intent i= new Intent(getActivity(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
									FragmentManager fragmentManager = getFragmentManager();
								/*	fragmentManager.beginTransaction()
									.replace(R.id.frame_container, new HomeNew()).commit();*/
									fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								}
							});

					builder.create().show();
				}
			});
		 scanBtn = (Button)rootView.findViewById(R.id.scan_button);
			scanBtn.setOnClickListener(this);
			manualentryBtn = (Button)rootView.findViewById(R.id.manualentry_button);
			manualentryBtn.setOnClickListener(this);
			listing = new ArrayList<HashMap<String, String>>();
			listView=(ListView)rootView.findViewById(R.id.listview);
			listView.setCacheColorHint(Color.TRANSPARENT);
			connectionClass = new ConnectionClass();
		if(getvaluesfromproducts)
		{
			
			getvaluesfromproducts=false;
			scanContent=NewEasel.SelectedArt;
			NewEasel.SelectedCharity=Home.SelectedCharity;
			NewEasel.SelectedEasel=Home.SelectedEasel;
			NewEasel.SelectedCity=Home.SelectedCity;
			NewEasel.SelectedLocation=Home.SelectedLocation;
			NewEasel.SelectedPostCode=Home.SelectedPostCode;
			NewEasel.SelectedQuantity=Home.SelectedQuantity;
			NewEasel.SelectedSponsor=Home.SelectedSponsor;
			NewEasel.SelectedStreetAddress=Home.SelectedStreetAddress;
			NewEasel.SelectedProvince=Home.SelectedProvince;
			NewEasel.SelectedArt=ManualProductSelectionForNewPicture.artno;
			NewEasel.ArtDescription=ManualProductSelectionForNewPicture.artdes;
			preferences pref=new preferences(getActivity());
			pref.SetNewArt(NewEasel.SelectedArt);
			pref.SetNewArtDescription(NewEasel.ArtDescription);
			pref.SetNewCharity(NewEasel.SelectedCharity);
			pref.SetNewEasel(NewEasel.SelectedEasel);
			pref.SetNewCity(NewEasel.SelectedCity);
			pref.SetNewQuantity(NewEasel.SelectedQuantity);
			pref.SetNewLocation(NewEasel.SelectedLocation);
			pref.SetNewProvince(NewEasel.SelectedProvince);
			pref.SetNewSponsor(NewEasel.SelectedSponsor);
			pref.SetNewStreetAddress(NewEasel.SelectedStreetAddress);
			pref.SetNewPostalCode(NewEasel.SelectedPostCode);
			pref.SetLastScreen("easelconfirmnew");
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().addToBackStack(null)
			.replace(R.id.frame_container, new EaselConfirmationNew()).commit();
			/*GetEasels geteasels = new GetEasels(); 
			geteasels.execute("");*/
		}
		return rootView;
		}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.scan_button){
			IntentIntegrator scanIntegrator = new IntentIntegrator(this);

			scanIntegrator.initiateScan();
		}
			if(v.getId()==R.id.manualentry_button){
			
			Intent i = new Intent(getActivity(),ManualEntry.class);
			startActivity(i);
		}
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanningResult != null) {
			//artnumberbar="38-453";
			scanContent = scanningResult.getContents();
			scanFormat = scanningResult.getFormatName();
			Toast.makeText(getActivity(), 
					scanContent, Toast.LENGTH_SHORT).show();

			Toast.makeText(getActivity(), 
					scanFormat, Toast.LENGTH_SHORT).show();
			
			/*GetEasels geteasels = new GetEasels(); 
			geteasels.execute("");*/
			//scanContent="17-001";
			
			if(scanContent!=null)
			{
				scanContent = scanContent.trim().toString();
				//scanFormat = scanningResult.getFormatName().trim().toString();
				//artnumberbar=scanContent;
				try{
					if(scanContent.contains("-"))
					{
					 
					}
					else
					{
						
						scanContent=scanContent.substring(6);
						scanContent = scanContent.substring(0, 2) + "-" + scanContent.substring(2, scanContent.length());
					
					if(scanContent.length()>6)
					{
						scanContent=scanContent.substring(0, scanContent.length() - 1);
						
					}
						
					}}catch(Exception a)
					{
						Toast.makeText(getActivity(), "Invalid Bar Code", Toast.LENGTH_LONG).show();
						return;
					}
				GetEasels geteasels = new GetEasels(); 
				geteasels.execute("");
				
				
			}
			else{
				Toast toast = Toast.makeText(getActivity(), 
						"No Bar Code data received!", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
		else{
			Toast toast = Toast.makeText(getActivity(), 
					"No Bar Code data received!", Toast.LENGTH_SHORT);
			toast.show();
		}
		

	}
	public class GetDetails extends AsyncTask<String, String, String> {
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber = "";
		String itemdesc = "";

		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(getActivity());
			p.setMessage("Loading Details ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if (isSuccess) {
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
				NewEasel.SelectedCharity=Home.SelectedCharity;
				NewEasel.SelectedEasel=Home.SelectedEasel;
				NewEasel.SelectedCity=Home.SelectedCity;
				NewEasel.SelectedLocation=Home.SelectedLocation;
				NewEasel.SelectedPostCode=Home.SelectedPostCode;
				NewEasel.SelectedQuantity=Home.SelectedQuantity;
				NewEasel.SelectedSponsor=Home.SelectedSponsor;
				NewEasel.SelectedStreetAddress=Home.SelectedStreetAddress;
				NewEasel.SelectedProvince=Home.SelectedProvince;
				preferences pref=new preferences(getActivity());
				pref.SetNewArt(NewEasel.SelectedArt);
				pref.SetNewArtDescription(SelectedArtDescription);
				pref.SetNewCharity(NewEasel.SelectedCharity);
				pref.SetNewEasel(NewEasel.SelectedEasel);
				pref.SetNewCity(NewEasel.SelectedCity);
				pref.SetNewQuantity(NewEasel.SelectedQuantity);
				pref.SetNewLocation(NewEasel.SelectedLocation);
				pref.SetNewProvince(NewEasel.SelectedProvince);
				pref.SetNewSponsor(NewEasel.SelectedSponsor);
				pref.SetNewStreetAddress(NewEasel.SelectedStreetAddress);
				pref.SetNewPostalCode(NewEasel.SelectedPostCode);
				pref.SetLastScreen("easelconfirmnew");

				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().addToBackStack(null)
						.replace(R.id.frame_container, new EaselConfirmationNew()).commit();
			} else {
				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME, 0);
				String conid = settings.getString(preferences.useridd, "");

				String ProductURL = SFAPI.baseUri +"/query/?q="+ URLEncoder.encode("SELECT Id FROM Product2 Where StockKeepingUnit= '"+scanContent+"'", "UTF-8");
				Log.e("URL ",ProductURL);
				// Login requests must be POSTs
				HttpGet httpPostgp = new HttpGet(ProductURL);
				httpPostgp.addHeader(SFAPI.oauthHeader);
				httpPostgp.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse responsegp = null;

				// Execute the login POST request
				responsegp = httpclient.execute(httpPostgp);

				// verify response is HTTP OK
				final int statusCode = responsegp.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
				}
				else

				{
					JSONObject prodidjobj=new JSONObject(EntityUtils.toString(responsegp.getEntity()));
					JSONArray jarrpi=prodidjobj.getJSONArray("records");
					for(int ii=0;ii<jarrpi.length();ii++)
					{
						JSONObject pitem = jarrpi.getJSONObject(ii);
						Log.e("PID ",pitem.getString("Id"));
						Home.SelectedNewProductID=pitem.getString("Id");
					}
				}
				isSuccess=true;
						/*if(SelectedProductID.equalsIgnoreCase(""))
						{
							isSuccess=false;
							z="Product Not Found";
						}
						else{
							isSuccess=true;}*/



			}catch (Exception aaa){	Log.e("URL ", aaa.toString());isSuccess=false;z="Salesforce Session Expired, Please Relogin.";}
			return z;
		}

	}
	public class GetEasels extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;

		/*String userid = edtuserid.getText().toString();
		String password = edtpass.getText().toString();*/


		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Loading ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {
				

				GetDetails getDetails = new GetDetails();
				getDetails.execute("");
				/*listView.setVisibility(View.VISIBLE);
				EaselAdapter   adapter = new EaselAdapter(getActivity(), listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);*/
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			try
			{
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String conid=  	settings.getString(preferences.useridd,"");
				String initials=  	settings.getString(preferences.initials,"");
				String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
				String locationsapurl= SFAPI.advanceprourl+"/api/locations?name="+initials;
				//Log.e("Locations Url ",locationsapurl);
				HttpGet locations = new HttpGet(locationsapurl);
				locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				locations.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse locationsresponse = httpclient.execute(locations);
				JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
				if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
					JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

					for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
						JSONObject items = locationpayloadarray.getJSONObject(lp);
						String id = items.getString("id");
						String name = items.getString("name");
						if(!name.equalsIgnoreCase(initialsdef)){continue;}
						String locationinventoryapurl = SFAPI.advanceprourl + "/api/locationInventory?location_id=" + id;
						//Log.e("LocationsInventory Url ", locationinventoryapurl);

						HttpGet locationInventory = new HttpGet(locationinventoryapurl);
						locationInventory.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
						locationInventory.addHeader(SFAPI.prettyPrintHeader);
						HttpResponse locationInventoryresponse = httpclient.execute(locationInventory);
						JSONObject locationInventoryresponseobject=new JSONObject(EntityUtils.toString(locationInventoryresponse.getEntity()));
						if(locationInventoryresponseobject.getString("success").equalsIgnoreCase("true"))
						{
							JSONArray locationInventorypayloadarray = locationInventoryresponseobject.getJSONArray("payload");
							for (int lip = 0; lip < locationInventorypayloadarray.length(); lip++) {
								JSONObject lipitems = locationInventorypayloadarray.getJSONObject(lip);
								String product_id = lipitems.getString("product_id");
								String location_id = lipitems.getString("location_id");
								String location_name = lipitems.getString("location_name");
								//Log.e("product id ",product_id);
								//Log.e("location_id ",location_id);
								//Log.e("location_name ",location_name);
								String productsapurl = SFAPI.advanceprourl + "/api/products/" + product_id;
								HttpGet product = new HttpGet(productsapurl);
								product.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
								product.addHeader(SFAPI.prettyPrintHeader);
								HttpResponse productresponse = httpclient.execute(product);
								JSONObject productobject=new JSONObject(EntityUtils.toString(productresponse.getEntity()));
								if(productobject.getString("success").equalsIgnoreCase("true"))
								{
									JSONObject jsonproductobject=productobject.getJSONObject("payload");
									String productsku = jsonproductobject.getString("sku");
									String productname = jsonproductobject.getString("name");
									String productdescription = jsonproductobject.getString("description");
									String productavailable_quantity = jsonproductobject.getString("available_quantity");
									Log.e("product sku ",productsku);
									Log.e("product name ",productname);
									Log.e("product description ",productdescription);
									Log.e("product quantity ",productavailable_quantity);
									if(scanContent.equalsIgnoreCase(productsku)) {
										HashMap<String, String> map = new HashMap<String, String>();
										String art = productsku;
										map.put(Art, art);
										SelectedArt = art;

										map.put(ArtDescription, productname);
										SelectedArtDescription = productname;
										SelectedQuantity = productavailable_quantity;
										map.put("tolocid", location_id);
										map.put("pic", "");
										Home.SelectedNEWAPProductID=product_id;
										listing.add(map);
									}

								}
							}
						}
					}
				}
				if(listing.isEmpty())
				{
					isSuccess = false;
					z = "Product Not Found";
				}
				else
				{
					z = "Product Found";
					isSuccess = true;
				}
			}
			catch (Exception aa)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + aa.toString();

			}
				/*try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						//String query = "select * from dbo.vw_PAS_LiveEasels" ;
						String query = "select * from dbo.vw_PAS_AllProducts where item_no='" + scanContent + "'";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						 while (rs.next()) {
							HashMap<String, String> map = new HashMap<String, String>();
							if(rs.getString("item_no")!=null){
							map.put(Art, rs.getString("item_no").trim().toString());
							SelectedArt=rs.getString("item_no").trim().toString();}
							else
							{
								map.put(Art, "");
								SelectedArt="";
							}
							*//*if(rs.getString("Location")!=null){
								map.put(Location, rs.getString("Location"));
								SelectedLocation=rs.getString("Location").trim().toString();
							}
								else
								{
									map.put(Location, "");
									SelectedLocation="";
								}
							
							if(rs.getString("Charity")!=null){
								map.put(Charity, rs.getString("Charity"));
								SelectedCharity=rs.getString("Charity").trim().toString();
							}
								else
								{
									SelectedCharity="";
									map.put(Charity, "");
								}
							
							if(rs.getString("Quantity")!=null){
								map.put(Quantity, rs.getString("Quantity"));
								SelectedQuantity=rs.getString("Quantity").trim().toString();
							}
								else
								{
									SelectedQuantity="";
									map.put(Quantity, "");
									
								}
							if(rs.getString("Sponsor")!=null){
								map.put(Sponsor, rs.getString("Sponsor").trim().toString());
								SelectedSponsor=rs.getString("Sponsor").trim().toString();
							}
								else
								{
									SelectedSponsor="";
									map.put(Sponsor, "");
								}
							if(rs.getString("Street_Address")!=null){
								map.put(StreetAddress, rs.getString("Street_Address"));
								SelectedStreetAddress=rs.getString("Street_Address").trim().toString();
							}
								else
								{
									SelectedStreetAddress="";
									map.put(StreetAddress, "");
								}
							if(rs.getString("City")!=null){
								map.put(City, rs.getString("City"));
								SelectedCity=rs.getString("City").trim().toString();
							}
								else
								{
									SelectedCity="";
									map.put(City, "");
								}
							if(rs.getString("Province")!=null){
								map.put(Province, rs.getString("Province"));
								SelectedProvince=rs.getString("Province").trim().toString();
							}
								else
								{
									SelectedProvince="";
									map.put(Province, "");
								}
							if(rs.getString("Postcode")!=null){
								map.put(PostalCode, rs.getString("Postcode"));
								SelectedPostCode=rs.getString("Postcode").trim().toString();
							}
								else
								{
									SelectedPostCode="";
									map.put(PostalCode, "");
								}*//*
							if(rs.getString("item_desc_1")!=null){
								map.put(ArtDescription, rs.getString("item_desc_1"));
								SelectedArtDescription=rs.getString("item_desc_1").trim().toString();
							}
								else
								{
									SelectedArtDescription="";
									map.put(ArtDescription, "");
								}
							*//*if(rs.getString("Easel")!=null){
								map.put(Easel, rs.getString("Easel"));
								SelectedEasel=rs.getString("Easel").trim().toString();
							}
								else
								{
									SelectedEasel="";
									map.put(Easel, "");
								}*//*
							
							
							
					
							listing.add(map);
		                    }
						if(!listing.isEmpty())
						{

							z = "Data Found";
							isSuccess=true;
						}
						else
						{
							z = "No Data Found";
							isSuccess = false;
						}

					}
				}
				catch (Exception ex)
				{
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}*/
			
			return z;
		}
	}
}
