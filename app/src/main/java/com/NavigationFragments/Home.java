package com.NavigationFragments;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Utils.ConnectionClass;
import com.Utils.Easels;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.Database.Databaseadapter;
import com.GetterSetters.RunningtransactionsGetterSetter;
import com.NavigationFragments.EaselConfirmation;
import com.fundinginnovation.Questionaire;
import com.fundinginnovation.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.navigationdrawer.NavigationActivity;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class Home extends Fragment implements OnClickListener{
	private Button scanBtn,manualentryBtn;
	static String scanContent="";
	static String scanFormat="";
	ConnectionClass connectionClass;
	ListView listView;
	ArrayList<HashMap<String, String>> listing;
	String foundid="";
	String foundname="";
	///////////////////////////////////Old Listing Strings/////////////////////////////////////////////////////////
	static String Art = "Art";
	static String Location = "Location";
	static String Charity = "Charity";
	static String Quantity = "Quantity";
	static String Sponsor = "Sponsor";
	static String StreetAddress = "StreetAddress";
	static String City = "City";
	static String Province = "Province";
	static String PostalCode = "PostalCode";
	static String ArtDescription = "ArtDescription";
	static String Easel = "Easel";
	 //String artnumberbar = "";

	//////////////////////////////////////////New Usable Strings ///////////////////////////////////////////////////////////////////
	public static String SelectedArt = "SelectedArt";
	public static String SelectedLocation = "SelectedLocation";
	public static String SelectedCharity = "SelectedCharity";
	public static String SelectedSponsor = "SelectedSponsor";
	public static String SelectedEasel = "SelectedEasel";
	public static String SelectedQuantity = "SelectedQuantity";
	public static String SelectedStreetAddress = "SelectedStreetAddress";
	public static String SelectedCity = "SelectedCity";
	public static String SelectedProvince= "SelectedProvince";
	public static String SelectedPostCode= "SelectedPostCode";
	public static String SelectedArtDescription="SelectedArtDescription";
	public static String SelectedEaselID = "SelectedEaselID";
	public  static String SelectedProductID = "SelectedProductID";
	public  static String SelectedNewProductID = "SelectedNewProductID";
	public  static String SelectedAPProductID = "SelectedAPProductID";
	public  static String SelectedNEWAPProductID = "SelectedNewAPProductID";
	public static String SelectedFROMLOCATIONID="SelectedFROMLOCATIONID";
	String APProductID = "APProductID";
	String FROMLOCATIONID = "FROMLOCATIONID";
	String EaselID = "EaselID";
	Button reportdamages;
	Databaseadapter db;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 View rootView;
		 rootView = inflater.inflate(R.layout.activity_main, container, false);
		 db=new Databaseadapter(getActivity());
		 db.open();
		 db.getdata();
		 reportdamages= (Button) rootView.findViewById(R.id.reportdamages);
		 reportdamages.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
					.replace(R.id.frame_container, new ReportDamages()).commit();
				}
			});
		 scanBtn = (Button)rootView.findViewById(R.id.scan_button);
			scanBtn.setOnClickListener(this);
			manualentryBtn = (Button)rootView.findViewById(R.id.manualentry_button);
			manualentryBtn.setOnClickListener(this);
			listing = new ArrayList<HashMap<String, String>>();
			listView=(ListView)rootView.findViewById(R.id.listview);
			listView.setCacheColorHint(Color.TRANSPARENT);
			connectionClass = new ConnectionClass();
			 ImageView imageButton = (ImageView) rootView
						.findViewById(R.id.menuicon);
				imageButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						/*if(!NavigationActivity.draweropened){*/
							NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
							NavigationActivity.draweropened=true;
						/*}
						else{
							NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
							NavigationActivity.draweropened=false;
						}*/
					}
				});
				ImageView homeButton = (ImageView) rootView
						.findViewById(R.id.homeicon);
				homeButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						getActivity().finishAffinity();
						Intent i= new Intent(getActivity(),NavigationActivity.class);
						i.putExtra("val", "");
						startActivity(i);
						
						FragmentManager fragmentManager = getFragmentManager();
						/*fragmentManager.beginTransaction()
						.replace(R.id.frame_container, new HomeNew()).commit();*/
						fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					}
				});
		if(NavigationActivity.startturn)
		{
			IntentIntegrator scanIntegrator = new IntentIntegrator(this);

			scanIntegrator.initiateScan();
		}

		return rootView;
		}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.scan_button){
			//scan easel barcode
			IntentIntegrator scanIntegrator = new IntentIntegrator(this);

			scanIntegrator.initiateScan();
		}
			if(v.getId()==R.id.manualentry_button){
			
				/*FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.frame_container, new ManualEntry()).commit();*/
				
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().addToBackStack(null)
				.replace(R.id.frame_container, new LookUpEasel()).commit();
				
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		//GetEasels geteaselss = new GetEasels();
		//geteaselss.execute("");
		//scanContent="AK0000";

		//get scanned easel number
		if (scanningResult != null) {
			//artnumberbar="38-453";
			scanContent = scanningResult.getContents();
			scanFormat = scanningResult.getFormatName();
			Toast.makeText(getActivity(),
					scanContent, Toast.LENGTH_SHORT).show();

			Toast.makeText(getActivity(),
					scanFormat, Toast.LENGTH_SHORT).show();

			/*GetEasels geteasels = new GetEasels();
			geteasels.execute("");*/
			//scanContent="BILL704";

			if(scanContent!=null)
			{
				scanContent = scanContent.trim().toString();
				//scanFormat = scanFormat.trim().toString();
				//artnumberbar=scanContent;
				/*try{
					if(scanContent.contains("-"))
					{

					}
					else
					{

						scanContent=scanContent.substring(6);
						scanContent = scanContent.substring(0, 2) + "-" + scanContent.substring(2, scanContent.length());

					if(scanContent.length()>6)
					{
						scanContent=scanContent.substring(0, scanContent.length() - 1);

					}

					}}catch(Exception a)
					{
						Toast.makeText(getActivity(), "Invalid Bar Code", Toast.LENGTH_LONG).show();
						return;
					}*/

				//get scanned easel data from advancepro apis
				GetEasels geteasels = new GetEasels();
				geteasels.execute("");


			}
			else{
				Toast toast = Toast.makeText(getActivity(),
						"No Bar Code data received!", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
		else{
			Toast toast = Toast.makeText(getActivity(),
					"No Bar Code data received!", Toast.LENGTH_SHORT);
			toast.show();
		}
		

	}
	public class GetEasels extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog prog;

		/*String userid = edtuserid.getText().toString();
		String password = edtpass.getText().toString();*/


		@Override
		protected void onPreExecute() {
			prog=new ProgressDialog(getActivity());
			prog.setMessage("Loading ...");
			prog.setCancelable(false);
			prog.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			try {
				if (prog != null && prog.isShowing()) {
					prog.dismiss();
				}
			}
			catch (Exception a)
			{
				Toast.makeText(getActivity(), "Unfortunately Someething went wrong . please try again later.", Toast.LENGTH_SHORT).show();
			}
				if (isSuccess) {

					Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
					db.insertrunningtransactions(new RunningtransactionsGetterSetter(SelectedArt, SelectedLocation, SelectedCharity, SelectedSponsor, SelectedEasel, SelectedQuantity, SelectedStreetAddress, SelectedCity, SelectedProvince, SelectedPostCode, SelectedArtDescription));
					preferences p = new preferences(getActivity());
					p.SetLastScreen("easelconfirm");
					// take user to next screen
					FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction().addToBackStack(null)
							.replace(R.id.frame_container, new EaselConfirmation()).commit();
			
				
				/*listView.setVisibility(View.VISIBLE);
				EaselAdapter   adapter = new EaselAdapter(getActivity(), listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);*/
				} else {
					Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
				}

		}

		@Override
		protected String doInBackground(String... params) {

			try
			{
				//scanContent="AK213";
				HttpClient httpclient = new DefaultHttpClient();
				String sfprodid="";
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String conid=  	settings.getString(preferences.useridd,"");
				String initials=  	settings.getString(preferences.initials,"");

				//get scanned easel location
				String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+scanContent;
				Log.e("Locations Url ",locationsapurl);
				HttpGet locations = new HttpGet(locationsapurl);
				locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				locations.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse locationsresponse = httpclient.execute(locations);
				JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
				if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
					JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

					for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
						JSONObject items = locationpayloadarray.getJSONObject(lp);
						String id = items.getString("id");
						String name = items.getString("name");

						//check if scanned easel location exists in location inventory
						String locationinventoryapurl = SFAPI.advanceprourl + "/api/locationInventory?location_id=" + id;
						//Log.e("LocationsInventory Url ", locationinventoryapurl);

						HttpGet locationInventory = new HttpGet(locationinventoryapurl);
						locationInventory.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
						locationInventory.addHeader(SFAPI.prettyPrintHeader);
						HttpResponse locationInventoryresponse = httpclient.execute(locationInventory);
						JSONObject locationInventoryresponseobject=new JSONObject(EntityUtils.toString(locationInventoryresponse.getEntity()));
						if(locationInventoryresponseobject.getString("success").equalsIgnoreCase("true"))
						{
							JSONArray locationInventorypayloadarray = locationInventoryresponseobject.getJSONArray("payload");
							for (int lip = 0; lip < locationInventorypayloadarray.length(); lip++) {
								JSONObject lipitems = locationInventorypayloadarray.getJSONObject(lip);
								String product_id = lipitems.getString("product_id");
								String location_id = lipitems.getString("location_id");
								String location_name = lipitems.getString("location_name");
								//Log.e("product id ",product_id);
								//Log.e("location_id ",location_id);
								//Log.e("location_name ",location_name);

								//get product data from product id
								String productsapurl = SFAPI.advanceprourl + "/api/products/" + product_id;
								HttpGet product = new HttpGet(productsapurl);
								product.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
								product.addHeader(SFAPI.prettyPrintHeader);
								HttpResponse productresponse = httpclient.execute(product);
								JSONObject productobject=new JSONObject(EntityUtils.toString(productresponse.getEntity()));
								if(productobject.getString("success").equalsIgnoreCase("true"))
								{
									JSONObject jsonproductobject=productobject.getJSONObject("payload");
									String productsku = jsonproductobject.getString("sku");
									String productname = jsonproductobject.getString("name");
									String productdescription = jsonproductobject.getString("description");
									String productavailable_quantity = jsonproductobject.getString("available_quantity");
									Log.e("product sku ",productsku);
									Log.e("product name ",productname);
									Log.e("product description ",productdescription);
									Log.e("product quantity ",productavailable_quantity);

									HashMap<String, String> map = new HashMap<String, String>();
									String art=productsku;

									map.put(Art, art);
									SelectedArt=art;


									map.put(APProductID, product_id);
									SelectedAPProductID=product_id;
									map.put(FROMLOCATIONID, location_id);
									SelectedFROMLOCATIONID=location_id;
									SelectedQuantity="";
									map.put(Quantity, "");

									SelectedSponsor=location_name;
									map.put(Sponsor, location_name);

									SelectedStreetAddress=""/*eitems.getString("Address__c")*/;
									map.put(StreetAddress,"" /*eitems.getString("Address__c")*/);


									SelectedCity=""/*eitems.getString("City__c")*/;
									map.put(City, ""/*eitems.getString("City__c")*/);


									SelectedProvince=""/*eitems.getString("Province__c")*/;
									map.put(Province, ""/*eitems.getString("Province__c")*/);


									SelectedPostCode=""/*eitems.getString("Postal_Code__c")*/;
									map.put(PostalCode,"" /*eitems.getString("Postal_Code__c")*/);
									SelectedCharity="";
									map.put(SelectedCharity,"" /*eitems.getString("Postal_Code__c")*/);
									SelectedArtDescription=productname;

									map.put(ArtDescription, SelectedArtDescription);
									SelectedQuantity=productavailable_quantity;
									map.put(Quantity,SelectedQuantity);
									SelectedEaselID=""/*easelid*/;
									map.put(EaselID,SelectedEaselID);
									SelectedEasel=location_name/*eitems.getString("Name")*/;
									map.put(Easel,SelectedEasel);
									//get other data based on scanned easel from salesforce
									String EaselsURL = SFAPI.baseUri + "/query/?q=" + URLEncoder.encode("SELECT Id,Fundraising_Consultant__c,Charity_partner__c,Location_name__c,Address__c,City__c,Province__c,Postal_Code__c,Name FROM Easel__c Where Easel_Number__c = '" +Home.SelectedEasel + "'", "UTF-8");

									Log.e("URL ", EaselsURL);
									Log.e("Header ", SFAPI.oauthHeader.toString());
									// Login requests must be POSTs
									HttpGet httpPost = new HttpGet(EaselsURL);
									httpPost.addHeader(SFAPI.oauthHeader);
									httpPost.addHeader(SFAPI.prettyPrintHeader);


									HttpResponse response = null;

									// Execute the login POST request
									response = httpclient.execute(httpPost);
									String alleasels = EntityUtils.toString(response.getEntity());
									JSONObject jobj = new JSONObject(alleasels);
									JSONArray jarr = jobj.getJSONArray("records");
									for (int i = 0; i < jarr.length(); i++) {

										JSONObject eitems = jarr.getJSONObject(i);
										SelectedEaselID=eitems.getString("Id");
										Home.SelectedEaselID=SelectedEaselID;
										SelectedSponsor=eitems.getString("Location_name__c");
										Home.SelectedSponsor=SelectedSponsor;
										map.put(Location, eitems.getString("Location_name__c"));
										SelectedLocation=eitems.getString("Location_name__c");
										SelectedStreetAddress=eitems.getString("Address__c");
										Home.SelectedStreetAddress=SelectedStreetAddress;
										SelectedCity=eitems.getString("City__c");
										Home.SelectedCity=SelectedCity;
										SelectedProvince=eitems.getString("Province__c");
										Home.SelectedProvince=SelectedProvince;
										SelectedPostCode=eitems.getString("Postal_Code__c");
										Home.SelectedPostCode=SelectedPostCode;
										// Assemble the login request URL
										String AccountURL33 = SFAPI.baseUri + "/sobjects" + SFAPI.Account + "/" + eitems.getString("Charity_partner__c");
										Log.e("URL ", AccountURL33);

										// Login requests must be POSTs
										HttpGet httpPost33 = new HttpGet(AccountURL33);
										httpPost33.addHeader(SFAPI.oauthHeader);
										httpPost33.addHeader(SFAPI.prettyPrintHeader);


										HttpResponse response33 = null;

										// Execute the login POST request
										response33 = httpclient.execute(httpPost33);

										// verify response is HTTP OK
										final int statusCode33 = response33.getStatusLine().getStatusCode();
										if (statusCode33 != HttpStatus.SC_OK) {
											isSuccess=true;
										} else {
											JSONObject jsonObjectt3 = new JSONObject(EntityUtils.toString(response33.getEntity()));

											//map.put(Charity, jsonObjectt3.getString("Name"));
											SelectedCharity = jsonObjectt3.getString("Name");
											Home.SelectedCharity=SelectedCharity;
											Log.e("Charity ",SelectedCharity);
											String ProductURL = SFAPI.baseUri +"/query/?q="+URLEncoder.encode("SELECT Id FROM Product2 Where StockKeepingUnit= '"+Home.SelectedArt+"'", "UTF-8");
											Log.e("URL ",ProductURL);
											// Login requests must be POSTs
											HttpGet httpPostgp = new HttpGet(ProductURL);
											httpPostgp.addHeader(SFAPI.oauthHeader);
											httpPostgp.addHeader(SFAPI.prettyPrintHeader);


											HttpResponse responsegp = null;

											// Execute the login POST request
											responsegp = httpclient.execute(httpPostgp);

											// verify response is HTTP OK
											final int statusCode = responsegp.getStatusLine().getStatusCode();
											if (statusCode != HttpStatus.SC_OK) {
											}
											else

											{

												JSONObject prodidjobj=new JSONObject(EntityUtils.toString(responsegp.getEntity()));
												JSONArray jarrpi=prodidjobj.getJSONArray("records");
												for(int ii=0;ii<jarrpi.length();ii++)
												{
													JSONObject pitem = jarrpi.getJSONObject(ii);
													Log.e("PID ",pitem.getString("Id"));
													SelectedProductID=pitem.getString("Id");
													sfprodid=pitem.getString("Id");
													Home.SelectedProductID=SelectedProductID;
												}
											}
											isSuccess=true;

										}
									}
									if(sfprodid.equalsIgnoreCase(""))
									{
										isSuccess=false;
										z="Product Not Found on SalesForce";
									}
									else{
										isSuccess=true;listing.add(map);}


								}
							}
						}
					}
				}
				if(listing.isEmpty())
				{
					isSuccess = false;
					z = "No Easels Found";
				}
				else
				{
					isSuccess = true;
				}
			}
			catch (Exception aa)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + aa.toString();

			}
			return z;

		}
	}
	public class EaselAdapter extends BaseAdapter {

		// Declare Variables
		Context context;
		String value;
		String fontPath2 = "segoepr.ttf";
		LayoutInflater inflater;
		ArrayList<HashMap<String, String>> data;
		
		public EaselAdapter(Context context,
				ArrayList<HashMap<String, String>> arraylist) {
			this.context = context;
			data = arraylist;
			

		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@SuppressWarnings("unused")
		public View getView(final int position, View convertView, ViewGroup parent) {
			// Declare Variables
			final TextView Art;
			final TextView Location;
			final TextView Charity;
			final TextView Quantity;
			final TextView Sponsor;
			TextView StreetAddress;
			TextView City;
			TextView Province;
			TextView PostalCode;
			TextView ArtDescription;
			final TextView Easel;
			TextView listtext;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			Typeface tf2 = Typeface.createFromAsset(context.getAssets(), fontPath2);

			final View itemView = inflater.inflate(R.layout.easeldisplay, parent, false);

			// Get the position from the results
			HashMap<String, String> results = new HashMap<String, String>();
			results = data.get(position);
			Art=(TextView)itemView.findViewById(R.id.art);
			Art.setText(results.get(Home.Art));
			Easel=(TextView)itemView.findViewById(R.id.easel);
			Easel.setText(results.get(Home.Easel));
			Location=(TextView)itemView.findViewById(R.id.location);
			Location.setText(results.get(Home.Location));
			Charity=(TextView)itemView.findViewById(R.id.charity);
			Charity.setText(results.get(Home.Charity));
			Quantity=(TextView)itemView.findViewById(R.id.quantity);
			Quantity.setText(results.get(Home.Quantity));
			Sponsor=(TextView)itemView.findViewById(R.id.sponsor);
			Sponsor.setText(results.get(Home.Sponsor));
			StreetAddress=(TextView)itemView.findViewById(R.id.streetaddress);
			StreetAddress.setText(results.get(Home.StreetAddress));
			City=(TextView)itemView.findViewById(R.id.city);
			City.setText(results.get(Home.City));
			Province=(TextView)itemView.findViewById(R.id.province);
			Province.setText(results.get(Home.Province));
			PostalCode=(TextView)itemView.findViewById(R.id.postalcode);
			PostalCode.setText(results.get(Home.PostalCode));
			ArtDescription=(TextView)itemView.findViewById(R.id.artdescription);
			ArtDescription.setText(results.get(Home.ArtDescription));
			listtext=(TextView)itemView.findViewById(R.id.listtext);
			listtext.setText(results.get(Home.Easel.trim().toString())+"   "+results.get(Home.Location.trim().toString())+" / "+results.get(Home.Sponsor.trim().toString())+" / "+results.get(Home.Art.trim().toString()));
			
	
			// Capture button clicks on ListView items
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					
					SelectedArt = Art.getText().toString();
					SelectedLocation = Location.getText().toString();
					SelectedCharity = Charity.getText().toString();
					SelectedSponsor = Sponsor.getText().toString();
					SelectedEasel = Easel.getText().toString();
					SelectedQuantity = Quantity.getText().toString();
					Intent i=new Intent(getActivity(),Questionaire.class);
					startActivity(i);
				}
			});

			return itemView;
		}
	}
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       
          //  setOnBackPressListener();
         
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                if(ManualEntry.backpressed)
                {
                	ManualEntry.backpressed=false;
                	
                }
                else if(LookUpEasel.backpressed)
                {
                	LookUpEasel.backpressed=false;
                	
                }
                else{
                	getActivity().finishAffinity();
					Intent i= new Intent(getActivity(),NavigationActivity.class);
					i.putExtra("val", "");
					startActivity(i);
            		FragmentManager fragmentManager = getFragmentManager();
					/*fragmentManager.beginTransaction()
					.replace(R.id.frame_container, new HomeNew()).commit();*/}
                }
                return false;
            }
        });
    }
}
