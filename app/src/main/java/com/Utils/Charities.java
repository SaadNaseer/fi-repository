package com.Utils;

public class Charities {

private String name;
private String id;
private String address;
private String logo;
private String telephone;
private String cname;
private String cphone;
private String ccell;
private String cemail;

public String getName() {
    return name;
}
public void setName(String name) {
    this.name = name;
}
public String getId() {
    return id;
}
public void setId(String id) {
    this.id = id;
}

public String getAddress() {
    return address;
}
public void setAddress(String tv) {
    this.address = tv;
}
public String getLogo() {
    return logo;
}
public void setLogo(String garr) {
    this.logo = garr;
}
public String getTelephoneNo() {
    return telephone;
}
public void setTelephoneNo(String telephone) {
	// TODO Auto-generated method stub
	this.telephone = telephone;
}
public String getContactName() {
    return cname;
}
public void setContactName(String name) {
	// TODO Auto-generated method stub
	this.cname = name;
}
public String getContactCell() {
    return ccell;
}
public void setContactCell(String cell) {
	// TODO Auto-generated method stub
	this.ccell = cell;
}
public String getContactEmail() {
    return cemail;
}
public void setContactEmail(String email) {
	// TODO Auto-generated method stub
	this.cemail = email;
}
public String getContactPhone() {
    return cphone;
}
public void setContactPhone(String phone) {
	// TODO Auto-generated method stub
	this.cphone = phone;
}
}
