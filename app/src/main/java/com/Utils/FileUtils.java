package com.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

public class FileUtils {

	public static String convertStream(InputStream is) {

		// TODO Auto-generated method stub
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append((line + "\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public static void openAnyFile(Context context, String path) {
		if(Build.VERSION.SDK_INT>=24){
			try{
				Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
				m.invoke(null);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		File file = new File(path);
		MimeTypeMap map = MimeTypeMap.getSingleton();
		String ext = MimeTypeMap.getFileExtensionFromUrl(file.getName());
		if (ext.isEmpty()) {
			ext = FileUtils.fileExt(path);
		}

		String type = map.getMimeTypeFromExtension(ext);
		if (type == null)
			type = "*/*";

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		Uri data = Uri.fromFile(file);

		intent.setDataAndType(data, type);

		try {
			context.startActivity(intent);
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(context, "Sorry ! Cannot open this type of file!"+e.toString(),
					Toast.LENGTH_SHORT).show();
		}

	}
	private static String fileExt(String url) {
		if (url.indexOf("?") > -1) {
			url = url.substring(0, url.indexOf("?"));
		}
		if (url.lastIndexOf(".") == -1) {
			return null;
		} else {
			String ext = url.substring(url.lastIndexOf(".") + 1);
			if (ext.indexOf("%") > -1) {
				ext = ext.substring(0, ext.indexOf("%"));
			}
			if (ext.indexOf("/") > -1) {
				ext = ext.substring(0, ext.indexOf("/"));
			}
			return ext.toLowerCase();

		}
	}
	public static void CreateAllDirectories() {
		String ParentDirectory = Environment.getExternalStorageDirectory()
				+ File.separator + "Bid Imports";
		File filee = new File(ParentDirectory);
		if (!filee.exists()) {
			filee.mkdir();
			CreateDirectory(ParentDirectory);
			

			/**/
		} 
		

	}
	public static void CreateDirectory(String ParentDirectory/*,
			String childdirectory*/) {
		File folder = new File(ParentDirectory /*+ File.separator*/
				/*+ childdirectory*/);
		boolean success = true;
		File parentdirectoryfile = new File(ParentDirectory);
		if (!parentdirectoryfile.exists()) {
			parentdirectoryfile.mkdir();
		}
		if (!folder.exists()) {
			success = folder.mkdir();
		}
		if (success) {
			// Do something on success
		} else {
			// Do something else on failure
		}
	}
	
	public static void MoveFile(String inputPath, String outputPath) {

		File from = new File(inputPath);
		File to = new File(outputPath);
		from.renameTo(to);
	}
}
