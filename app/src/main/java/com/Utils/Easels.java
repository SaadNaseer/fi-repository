package com.Utils;

public class Easels {
	private String Art;
	private String Location;
	private String Charity;
	private String Quantity;
	private String Sponsor;
	private String StreetAddress;
	private String City;
	private String Province;
	private String PostalCode;
	private String ArtDescription;
	private String Easel;
private String EaselID;
	private String ProductID;
	private String APProductID;
	private String FROMLOCATIONID;


	public String getFROMLOCATIONID() {
		return FROMLOCATIONID;
	}
	public void setFROMLOCATIONID(String appid) {
		this.FROMLOCATIONID = appid;
	}

	public String getAPProductid() {
		return APProductID;
	}
	public void setAPProductid(String appid) {
		this.APProductID = appid;
	}

	public String getEaselid() {
		return EaselID;
	}
	public void setEaseld(String eid) {
		this.EaselID = eid;
	}

	public String getProductid() {
		return ProductID;
	}
	public void setProductId(String pid) {
		this.ProductID = pid;
	}
 
	public String getArt() {
	    return Art;
	}
	public void setArt(String art) {
	    this.Art = art;
	}
	public String getLocation() {
	    return Location;
	}
	public void setLocation(String loc) {
	    this.Location = loc;
	}
	public String getCharity() {
	    return Charity;
	}
	public void setCharity(String ch) {
	    this.Charity = ch;
	}
	public String getQuantity() {
	    return Quantity;
	}
	public void setQuantity(String q) {
	    this.Quantity = q;
	}
	
	public String getSponsor() {
	    return Sponsor;
	}
	public void setSponsor(String q) {
	    this.Sponsor = q;
	}
	
	public String getStreetAddress() {
	    return StreetAddress;
	}
	public void setStreetAddress(String q) {
	    this.StreetAddress = q;
	}
	
	public String getCity() {
	    return City;
	}
	public void setCity(String q) {
	    this.City = q;
	}
	
	public String getProvince() {
	    return Province;
	}
	public void setProvince(String q) {
	    this.Province = q;
	}
	
	public String getPostalCode() {
	    return PostalCode;
	}
	public void setPostalCode(String q) {
	    this.PostalCode = q;
	}
	
	public String getArtDescription() {
	    return ArtDescription;
	}
	public void setArtDescription(String q) {
	    this.ArtDescription = q;
	}
	
	public String getEasel() {
	    return Easel;
	}
	public void setEasel(String q) {
	    this.Easel = q;
	}

	}

