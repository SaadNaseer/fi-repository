package com.Utils;

public class EaselsHistory {
	
	private String bin_no;
	private String item_no;
	private String item_des;
	private String turn_start_date;
	private String turn_end_date;
	private String turn_image;
	
	public String getBinno() {
	    return bin_no;
	}
	public void setBinno(String name) {
	    this.bin_no = name;
	}
	public String getItemno() {
	    return item_no;
	}
	public void setItemno(String id) {
	    this.item_no = id;
	}
	public String getItemdescription() {
	    return item_des;
	}
	public String getItemImage() {
	    return turn_image;
	}
	
	public void setItemdescription(String des)
	{
	    this.item_des = des;
	}
	public String getTurnstartdate() {
	    return turn_start_date;
	}
	public void setTurnstartdate(String tv) {
	    this.turn_start_date = tv;
	}
	public String getTurnenddate() {
	    return turn_end_date;
	}
	public void setTurnenddate(String tv) {
	    this.turn_end_date = tv;
	}
	public void setTurnimage(String img) {
	    this.turn_image = img;
	}
}
