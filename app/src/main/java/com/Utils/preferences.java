package com.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class preferences {
	SharedPreferences pref;
	SharedPreferences settings ;
	Editor editor;
	Context context;
	int PRIVATE_MODE = 0;
	public static final String PREF_NAME = "fundinginnovation";
	public static final String username = "username";
	public static final String useridd = "userid";
	public static final String uniquevalue = "uniquevalue";
	public static final String name = "name";
	public static final String lastusedeasel = "lastusedeasel";
	public static final String binno = "binno";
	public static final String location = "location";
	public static final String Warehouse="Warehouse";
	public static final String LastScreen="LastScreen";
	public static final String TotalBids="TotalBids";
	public static final String NewArt="NewArt";
	public static final String NewLocation="NewLocation";
	public static final String NewCharity="NewCharity";
	public static final String NewSponsor="NewSponsor";
	public static final String NewEasel="NewEasel";
	public static final String NewQuantity="NewQuantity";
	public static final String NewStreetAddress="NewStreetAddress";
	public static final String NewCity="NewCity";
	public static final String NewProvince="NewProvince";
	public static final String NewPostalCode="NewPostalCode";
	public static final String NewArtDescription="NewArtDescription";
	public static final String ChangedArtno="Changedartno";
	public static final String ChangedArtDes="Changedartdes";
	public static final String AMEMAIL="AMEMAIL";
	public static final String baseUri="baseUri";
	public static final String initials="initials";
	public static final String loginAccessToken ="loginAccessToken";
	public static final String loginInstanceUrl ="loginInstanceUrl";
	
	public preferences(Context context){
		this.context = context;
		pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	public void SetbaseUri(String bu){
		editor.putString(baseUri, bu);
		editor.commit();
	}
	public void SetloginAccessToken(String lat){
		editor.putString(loginAccessToken, lat);
		editor.commit();
	}
	public void SetloginInstanceUrl(String liu){
		editor.putString(loginInstanceUrl, liu);
		editor.commit();
	}
	public void SetUsername(String user){
		editor.putString(username, user);
		editor.commit();
	}
	public void Set5digitnumber(String num){
		editor.putString(uniquevalue, num);
		editor.commit();
	}
	public void SetInitals(String in){
		editor.putString(initials, in);
		editor.commit();
	}
	public void SetUserId(String userid){
		editor.putString(useridd, userid);
		editor.commit();
	}
	public void SetName(String na){
		editor.putString(name, na);
		editor.commit();
	}
	public void SetLastUsedEasel(String lue){
		editor.putString(lastusedeasel, lue);
		editor.commit();
	}
	public void SetBinNo(String bin){
		editor.putString(binno, bin);
		editor.commit();
	}
	public void SetLocation(String loc){
		editor.putString(location, loc);
		editor.commit();
	}
	public void SetWareHouse(String wh)
	{
		editor.putString(Warehouse, wh);
		editor.commit();
		
	}
	public void SetAremanageremail(String ame)
	{
		editor.putString(AMEMAIL, ame);
		editor.commit();
		
	}
	public void SetLastScreen(String ls)
	{
		editor.putString(LastScreen, ls);
		editor.commit();
		
	}

	public void SetNewArt(String tb)
	{
		editor.putString(NewArt, tb);
		editor.commit();
		
	}
	public void SetNewLocation(String tb)
	{
		editor.putString(NewLocation, tb);
		editor.commit();
		
	}
	public void SetNewCharity(String tb)
	{
		editor.putString(NewCharity, tb);
		editor.commit();
		
	}
	public void SetNewSponsor(String tb)
	{
		editor.putString(NewSponsor, tb);
		editor.commit();
		
	}
	public void SetNewEasel(String tb)
	{
		editor.putString(NewEasel, tb);
		editor.commit();
		
	}
	public void SetNewQuantity(String tb)
	{
		editor.putString(NewQuantity, tb);
		editor.commit();
		
	}
	public void SetNewStreetAddress(String tb)
	{
		editor.putString(NewStreetAddress, tb);
		editor.commit();
		
	}
	public void SetNewCity(String tb)
	{
		editor.putString(NewCity, tb);
		editor.commit();
		
	}
	public void SetNewProvince(String tb)
	{
		editor.putString(NewProvince, tb);
		editor.commit();
		
	}
	public void SetNewPostalCode(String tb)
	{
		editor.putString(NewPostalCode, tb);
		editor.commit();
		
	}
	public void SetNewArtDescription(String tb)
	{
		editor.putString(NewArtDescription, tb);
		editor.commit();
		
	}
	public void Settotalbids(String tb)
	{
		editor.putString(TotalBids, tb);
		editor.commit();
		
	}
	public void SetChangedartno(String tb)
	{
		editor.putString(ChangedArtno, tb);
		editor.commit();
		
	}
	public void SetChangedartdes(String tb)
	{
		editor.putString(ChangedArtDes, tb);
		editor.commit();
		
	}
}
