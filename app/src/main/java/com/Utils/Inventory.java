package com.Utils;

public class Inventory {

private String easel;
private String artno;
private String artdes;
private String artqty;

public String getEasel() {
    return easel;
}
public void setEasel(String name) {
    this.easel = name;
}
public String getArtNumber() {
    return artno;
}
public void setArtNumber(String id) {
    this.artno = id;
}

public String getArtDes() {
    return artdes;
}
public void setArtDes(String tv) {
    this.artdes = tv;
}
public String getArtQty() {
    return artqty;
}
public void setArtQty(String tv) {
    this.artqty = tv;
}

}
