package com.Utils;

public class Products {

private String itemno;
private String itemdesc;
private String picture;
    private String tolocationid;
    private String newproductid;

public String getItemNo() {
    return itemno;
}
public void setItemNo(String no) {
    this.itemno = no;
}
public String getItemDesc() {
    return itemdesc;
}
public void setItemDesc(String pic) {
    this.itemdesc = pic;
}
public String getPicture() {
    return picture;
}
public void setPicture(String pic) {
    this.picture = pic;
}

    public String getNewProductID() {
        return newproductid;
    }
    public void setNewProductID(String npi) {
        this.newproductid = npi;
    }

    public String gettoLocationId() {
        return tolocationid;
    }
    public void settoLocationId(String tli) {
        this.tolocationid = tli;
    }

}

