package com.navigationdrawer;

import java.util.ArrayList;
import java.util.List;
import com.Database.Databaseadapter;
import com.GetterSetters.RunningtransactionsGetterSetter;
import com.NavigationFragments.AllProducts;
import com.NavigationFragments.CharityDirectory;
import com.NavigationFragments.ContactHeadOfficeMenu;
import com.NavigationFragments.EaselConfirmation;
import com.NavigationFragments.EaselConfirmationNew;
import com.NavigationFragments.Home;
import com.NavigationFragments.HomeNew;
import com.NavigationFragments.LiveEasels;
import com.NavigationFragments.MarketingMaterials;
import com.NavigationFragments.MyInventory;
import com.NavigationFragments.NewEasel;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.fundinginnovation.NewEaselConfirmationQuestion;
import com.fundinginnovation.Questionaire;
import com.fundinginnovation.RefreshService;
import com.fundinginnovation.SummaryActivity;
import com.fundinginnovation.TakePictures;
import com.fundinginnovation.R;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class NavigationActivity extends Activity{
	/////////////////////////////////Navigation Drawer Items ///////////////////////////////////////////
	public static boolean startturn=false;
	public static DrawerLayout mDrawerLayout;
	public static ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	public static boolean draweropened=false;
	// nav drawer title
	@SuppressWarnings("unused")
	private CharSequence mDrawerTitle;

	// used to store app title
	@SuppressWarnings("unused")
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	//TextView mTitleTextView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	/*	ActionBar mActionBar = getSupportActionBar();
		
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.actionbarcus, null);
		mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
		mTitleTextView.setText(R.string.app_name);


		
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);*/
		
		setContentView(R.layout.navigationdrawer);
		//Log.e("Access Token ", SFAPI.loginAccessToken);
		Intent i= getIntent();
		String value=i.getStringExtra("val");
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();
	/*	ImageView imageButton = (ImageView) mCustomView
				.findViewById(R.id.imageView1);
		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				if(!draweropened){
					mDrawerLayout.openDrawer(mDrawerList);
					draweropened=true;
				}
				else{
					mDrawerLayout.closeDrawer(mDrawerList);
					draweropened=false;
				}
			}
		});
		ImageView homeButton = (ImageView) mCustomView
				.findViewById(R.id.imageView2);
		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				gotohome();
			}
		});*/
		mTitle = mDrawerTitle = getTitle();
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1)));
		
		


		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		/*// enabling action bar app icon and behaving it as toggle button
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
*/
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.menu, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
				) {
			public void onDrawerClosed(View view) {
			//	getSupportActionBar().setTitle("Settings");
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				//getSupportActionBar().setTitle("Settings");
				// calling onPrepareOptionsMenu() to hide action bar icons


				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
		//mDrawerLayout.openDrawer(mDrawerList);
		draweropened=false;
		if(value.equals("val"))
		{
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().addToBackStack(null)
			.replace(R.id.frame_container, new NewEasel()).commit();
		}
		if(value.equals("q"))
		{
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().addToBackStack(null)
			.replace(R.id.frame_container, new Questionaire()).commit();
		}
	}
	private class SlideMenuClickListener implements
	ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {

		/*case R.id.app_name:
			return true;
		default:
			return super.onOptionsItemSelected(item);*/
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@SuppressWarnings("unused")
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		//menu.findItem(R.id.app_name).setVisible(!drawerOpen);

		return super.onPrepareOptionsMenu(menu);
	}
	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		//getSupportActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	@SuppressWarnings("unused")
	private void hideMenuItems(Menu menu, boolean visible)
	{

		for(int i = 0; i < menu.size(); i++){

			menu.getItem(i).setVisible(visible);

		}}
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {

		case 0:
			startturn=false;
			fragment = new HomeNew();

			//mTitleTextView.setText("Home");
			break;
		case 1:
			startturn=false;
			Databaseadapter db=new Databaseadapter(NavigationActivity.this);
			 db.open();
			 db.getdata();
			 if(db.getRunningTransactionsCount()>0)
				{
					List<RunningtransactionsGetterSetter> rt = db.getRunningTransactions(); 
					for (RunningtransactionsGetterSetter runningtransaction : rt) 
					{
						Home.SelectedArt=runningtransaction.GetArt();
						Home.SelectedArtDescription=runningtransaction.GetArtDescription();
						Home.SelectedEasel=runningtransaction.GetEasel();
						Home.SelectedLocation=runningtransaction.GetLocation();
						Home.SelectedProvince=runningtransaction.GetProvince();
						Home.SelectedPostCode=runningtransaction.GetPostalCode();
						Home.SelectedQuantity=runningtransaction.GetQuantity();
						Home.SelectedSponsor=runningtransaction.GetSponsor();
						Home.SelectedCharity=runningtransaction.GetCharity();
						Home.SelectedStreetAddress=runningtransaction.GetStreeAddress();
						Home.SelectedCity=runningtransaction.GetCity();
					}
					SharedPreferences	settings = NavigationActivity.this.getSharedPreferences(preferences.PREF_NAME,0);
					String lastscreen=  	settings.getString(preferences.LastScreen,"");
					String bids=  	settings.getString(preferences.TotalBids,"");
					if(lastscreen.equals("easelconfirm"))
					{
						
						fragment = new EaselConfirmation();
					}
					else if(lastscreen.equals("questionaire"))
					{
					
					fragment = new Questionaire();
					}
					else if(lastscreen.equals("takepictures"))
					{
					Questionaire.zerobids=false;
					Intent i= new Intent(NavigationActivity.this,TakePictures.class);
					i.putExtra("total", bids);
					Questionaire.Bids=bids;
					startActivity(i);
					}
					else if(lastscreen.equals("neweaselquestion"))
					{
						Intent intent=new Intent(NavigationActivity.this,NewEaselConfirmationQuestion.class);
						startActivity(intent);
					}
					else if(lastscreen.equals("neweaselscreen"))
					{
						
						fragment = new NewEasel();
					}
					else if(lastscreen.equals("easelconfirmnew"))
					{
						NewEasel.SelectedArt=settings.getString(preferences.NewArt,"");
						NewEasel.SelectedArtDescription=settings.getString(preferences.NewArtDescription,"");
						NewEasel.SelectedEasel=settings.getString(preferences.NewEasel,"");
						NewEasel.SelectedLocation=settings.getString(preferences.NewLocation,"");
						NewEasel.SelectedProvince=settings.getString(preferences.NewProvince,"");
						NewEasel.SelectedPostCode=settings.getString(preferences.NewPostalCode,"");
						NewEasel.SelectedQuantity=settings.getString(preferences.NewQuantity,"");
						NewEasel.SelectedSponsor=settings.getString(preferences.NewSponsor,"");
						NewEasel.SelectedCharity=settings.getString(preferences.NewCharity,"");
						NewEasel.SelectedStreetAddress=settings.getString(preferences.NewStreetAddress,"");
						NewEasel.SelectedCity=settings.getString(preferences.NewCity,"");
						
						fragment = new EaselConfirmationNew();
					}
					else if(lastscreen.equals("summaryactivity"))
					{
						if(!bids.equals(""))
						{
							Questionaire.zerobids=false;
							Intent i= new Intent(NavigationActivity.this,SummaryActivity.class);
							Questionaire.Bids=bids;
							startActivity(i);
						}
						else
						{
							Questionaire.zerobids=true;
							Intent i= new Intent(NavigationActivity.this,SummaryActivity.class);
							startActivity(i);
						}
						
						
					}
					else
					{
						fragment = new Home();
					}
				}
				else
				{
					fragment = new Home();
				}
			

			//mTitleTextView.setText("Start My Turns");
		

			break;
		case 2:
			
			fragment = new LiveEasels();

			//mTitleTextView.setText("Live Easel Locations");

			break;
		case 3:
			fragment = new MyInventory();

			//mTitleTextView.setText("My Inventory");
			

			break;
		case 4:
			fragment = new CharityDirectory();

			//mTitleTextView.setText("Charity Directory");
			
			break;
		case 5:
			fragment = new MarketingMaterials();

		//	mTitleTextView.setText("Marketing Materials");

			break;
		case 6:
			fragment = new AllProducts();

		//	mTitleTextView.setText("All Products");

			break;
			
		case 7:
			//fragment = new RefreshAll();

		//	mTitleTextView.setText("All Products");
			Toast.makeText(NavigationActivity.this, "Data Refresh Started", Toast.LENGTH_LONG).show();
			stopService(new Intent(NavigationActivity.this, RefreshService.class));
			startService(new Intent(NavigationActivity.this, RefreshService.class));
			break;
			
		case 8:
			fragment = new ContactHeadOfficeMenu();

		//	mTitleTextView.setText("All Products");

			break;
			}
			if (fragment != null) {
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().addToBackStack(null)
				.replace(R.id.frame_container, fragment).commit();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				setTitle(navMenuTitles[position]);
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				// error in creating fragment
				Log.e("MainActivity", "Error in creating fragment");
			}
		}
	public void gotohome()
	{
		displayView(0);
		NavigationActivity.startturn=false;
	}
	@Override
	public void onBackPressed() {
	    // Do Here what ever you want do on back press;
		  if (getFragmentManager().getBackStackEntryCount() == 0) {
		        this.finish();
		    } else {
		    	if (getFragmentManager().getBackStackEntryCount() == 1)
		    	  {}
		    	  else{
		        getFragmentManager().popBackStack();}
		    }
	}
	}

