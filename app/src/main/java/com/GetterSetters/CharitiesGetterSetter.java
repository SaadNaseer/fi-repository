package com.GetterSetters;


public class CharitiesGetterSetter {
    
    //private variables
	
    String CharityID;
    String Name;
    String Address1;
    String City;
    String Province;
    String PostCode;
    String Telephone;
    String ContactName;
    String cnt_f_tel;
    String Cellphone;
    String EMail;
    String Logo;
   
    // Empty constructor
    public CharitiesGetterSetter(){
         
    }

    // constructor
    public CharitiesGetterSetter(String cid, String cname,String caddress,String ccity,String cprovince,String cpostcode,String ctelephone,String ccname,String ccnftel,String ccellphone,String cemail,String clogo){
        this.CharityID = cid;
        this.Name =cname ;
        this.Address1 =caddress ;
        this.City =ccity ;
        this.Province =cprovince ;
        this.PostCode=cpostcode;
        this.Telephone=ctelephone;
        this.ContactName=ccname;
        this.cnt_f_tel=ccnftel;
        this.Cellphone=ccellphone;
        this.EMail=cemail;
        this.Logo=clogo;
    }
   

    // getting ID
    public String GetCharityid(){
        return this.CharityID;
    }
     
    // setting ID
    public void SetCharityId(String charityid){
        this.CharityID = charityid;
    }
     
    // getting name
    public String GetCharityName(){
        return this.Name;
    }
     
    // setting name
    public void SetCharityName(String charityname){
        this.Name = charityname;
    }
    // getting Address
    public String GetAddress(){
        return this.Address1;
    }
     
    // setting Address
    public void SetAddress(String charityaddress){
        this.Address1 = charityaddress;
    }
	public String GetCity() {
		// TODO Auto-generated method stub
		return this.City;
	}
	public void SetCity(String charitycity) {
		// TODO Auto-generated method stub
		this.City=charitycity;
	}
	public String GetProvince() {
		// TODO Auto-generated method stub
		return this.Province;
	}
	public void SetProvine(String charityprovince) {
		// TODO Auto-generated method stub
		this.Province=charityprovince;
	}
	public String GetPostCode() {
		// TODO Auto-generated method stub
		return this.PostCode;
	}
	public void SetPostCode(String charitypostcode) {
		// TODO Auto-generated method stub
		this.PostCode=charitypostcode;
	}
	public String GetTelephone() {
		// TODO Auto-generated method stub
		return this.Telephone;
	}
	public void SetTelephone(String charitytelephone) {
		// TODO Auto-generated method stub
		this.Telephone=charitytelephone;
	}
	public String GetContactName() {
		// TODO Auto-generated method stub
		return this.ContactName;
	}
	public void SetContactName(String charitycontactname) {
		// TODO Auto-generated method stub
		this.ContactName=charitycontactname;
	} 
	public String Getcnt_f_tel() {
		// TODO Auto-generated method stub
		return this.cnt_f_tel;
	}
	public void Setcnt_f_tel(String charitycnt_f_tel) {
		// TODO Auto-generated method stub
		this.cnt_f_tel=charitycnt_f_tel;
	} 
	public String GetCellPhone() {
		// TODO Auto-generated method stub
		return this.Cellphone;
	}
	public void SetCellPhone(String charityCellphone) {
		// TODO Auto-generated method stub
		this.Cellphone=charityCellphone;
	} 
	public String GetEmail() {
		// TODO Auto-generated method stub
		return this.EMail;
	}
	public void SetEmail(String charityEMail) {
		// TODO Auto-generated method stub
		this.EMail=charityEMail;
	} 
	public String GetLogo() {
		// TODO Auto-generated method stub
		return this.Logo;
	}
	public void SetLogo(String charityLogo) {
		// TODO Auto-generated method stub
		this.Logo=charityLogo;
	} 
   
}
