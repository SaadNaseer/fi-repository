package com.GetterSetters;

public class RequestSuppliesGetterSetter {
	 String itemname;
	    String itemquantity;
	    String itemdescription;
	    public RequestSuppliesGetterSetter(){
	         
	    }
	    public RequestSuppliesGetterSetter(String item, String quan,String id){
	        this.itemname = item;
	        this.itemquantity =quan ;
	        this.itemdescription=id;
	
	    }
// setting itemno
public void SetItemName(String item){
    this.itemname = item;
}
 
// getting itemno
public String GetItemName(){
    return this.itemname;
}
 
// setting itemquantity
public void SetItemQuantity(String q){
    this.itemquantity = q;
}

// getting itemquantity
public String GetItemQuantity(){
    return this.itemquantity;
}

//setting itemdescription
public void SetItemDescription(String q){
 this.itemdescription = q;
}

//getting itemdescription
public String GetItemDescription(){
 return this.itemdescription;
}

}