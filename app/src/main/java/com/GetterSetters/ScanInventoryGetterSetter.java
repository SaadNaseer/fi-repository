package com.GetterSetters;

public class ScanInventoryGetterSetter {
	 String itemno;
	    String itemquantity;
	    String itemdescription;
	    String itemdate;
	    public ScanInventoryGetterSetter(){
	         
	    }
	    public ScanInventoryGetterSetter(String item, String quan,String id,String dte){
	        this.itemno = item;
	        this.itemquantity =quan ;
	        this.itemdescription=id;
	        this.itemdate=dte;
	
	    }
// setting itemno
public void SetItemNo(String item){
    this.itemno = item;
}
 
// getting itemno
public String GetItemNo(){
    return this.itemno;
}
 
// setting itemquantity
public void SetItemQuantity(String q){
    this.itemquantity = q;
}

// getting itemquantity
public String GetItemQuantity(){
    return this.itemquantity;
}

//setting itemdescription
public void SetItemDescription(String q){
 this.itemdescription = q;
}

//getting itemdescription
public String GetItemDescription(){
 return this.itemdescription;
}

//setting itemdescription
public void SetItemDate(String q){
this.itemdate = q;
}

//getting itemdescription
public String GetItemDate(){
return this.itemdate;
}

}