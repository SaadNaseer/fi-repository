package com.GetterSetters;

public class RunningtransactionsGetterSetter {
  //private variables
	
    String Art;
    String Location;
    String Charity;
    String Sponsor;
    String Easel;
    String Quantity;
    String StreetAddress;
    String City;
    String Province;
    String PostalCode;
    String ArtDescription;
    String LastScreen;
    // Empty constructor
    public RunningtransactionsGetterSetter(){
         
    }

    // constructor
    public RunningtransactionsGetterSetter(String art, String loc,String charity,String sponsor,String easel,String quantity,String stadd,String city,String province,String postalcode,String artdes/*,String lastscreen*/){
        this.Art = art;
        this.Location =loc ;
        this.Charity =charity ;
        this.Sponsor =sponsor ;
        this.Easel =easel ;
        this.Quantity=quantity;
        this.StreetAddress=stadd;
        this.City=city;
        this.Province=province;
        this.PostalCode=postalcode;
        this.ArtDescription=artdes;
        //this.LastScreen=lastscreen;
    }
   

    // getting art
    public String GetArt(){
        return this.Art;
    }
     
    // setting art
    public void SetArt(String ar){
        this.Art = ar;
    }
     
    // getting location
    public String GetLocation(){
        return this.Location;
    }
     
    // setting location
    public void SetLocation(String loc){
        this.Location = loc;
    }
    // getting charity
    public String GetCharity(){
        return this.Charity;
    }
     
    // setting charity
    public void SetCharity(String ch){
        this.Charity = ch;
    }
	public String GetSponsor() {
		// TODO Auto-generated method stub
		return this.Sponsor;
	}
	public void SetSponsor(String sp) {
		// TODO Auto-generated method stub
		this.Sponsor=sp;
	}
	public String GetEasel() {
		// TODO Auto-generated method stub
		return this.Easel;
	}
	public void SetEasel(String ea) {
		// TODO Auto-generated method stub
		this.Easel=ea;
	}
	public String GetQuantity() {
		// TODO Auto-generated method stub
		return this.Quantity;
	}
	public void SetQuantity(String q) {
		// TODO Auto-generated method stub
		this.Quantity=q;
	}
	public String GetStreeAddress() {
		// TODO Auto-generated method stub
		return this.StreetAddress;
	}
	public void SetStreeAddress(String stadd) {
		// TODO Auto-generated method stub
		this.StreetAddress=stadd;
	}
	public String GetCity() {
		// TODO Auto-generated method stub
		return this.City;
	}
	public void SetCity(String city) {
		// TODO Auto-generated method stub
		this.City=city;
	} 
	public String GetProvince() {
		// TODO Auto-generated method stub
		return this.Province;
	}
	public void SetProvince(String prov) {
		// TODO Auto-generated method stub
		this.Province=prov;
	} 
	public String GetPostalCode() {
		// TODO Auto-generated method stub
		return this.PostalCode;
	}
	public void SetPostalCode(String pc) {
		// TODO Auto-generated method stub
		this.PostalCode=pc;
	} 
	public String GetArtDescription() {
		// TODO Auto-generated method stub
		return this.ArtDescription;
	}
	public void SetArtDescription(String artdes) {
		// TODO Auto-generated method stub
		this.ArtDescription=artdes;
	} 
	public String getLastScreen() {
		// TODO Auto-generated method stub
		return this.LastScreen;
	}
	public void SetLastScreen(String ls) {
		// TODO Auto-generated method stub
		this.LastScreen=ls;
	} 
	
   
}
