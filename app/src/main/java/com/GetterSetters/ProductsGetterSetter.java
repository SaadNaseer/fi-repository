package com.GetterSetters;

public class ProductsGetterSetter {
	 String ProductName;
	    String ProductDescription;
	    String ProductPicture;
	    
	    public ProductsGetterSetter(){
	         
	    }

	    // constructor
	    public ProductsGetterSetter(String pname, String pdesc,String ppic){
	        this.ProductName = pname;
	        this.ProductDescription =pdesc ;
	        this.ProductPicture =ppic ;
	
	    }
	    
	    // getting Name
	    public String GetProductName(){
	        return this.ProductName;
	    }
	     
	    // setting Name
	    public void SetProductName(String name){
	        this.ProductName = name;
	    }
	     
	    // getting Description
	    public String GetProductDescription(){
	        return this.ProductDescription;
	    }
	     
	    // setting Description
	    public void SetProductDescription(String desc){
	        this.ProductDescription = desc;
	    }
	    
	    // getting Picture
	    public String GetProductPicture(){
	        return this.ProductPicture;
	    }
	     
	    // setting Picture
	    public void SetProductPicture(String pic){
	        this.ProductPicture = pic;
	    }
}
