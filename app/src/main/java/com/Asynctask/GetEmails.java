package com.Asynctask;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.Utils.FileUtils;
import com.Utils.ServerCredentials;
import com.listeners.RequestListener;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetEmails extends AsyncTask<String, String, String> {

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private Context context;
    boolean exception = false;
    String exceptionmessage = "";
    RequestListener _listener;
    private ProgressDialog mProgressDialog;
    String result = "";
    String emailtype="";
    String emailmsg="";
    String emailsubject="";

    public GetEmails(Context context2, RequestListener listener,String type,String emsg,String esub) {
        this.context = context2;
        this._listener = listener;
        this.mProgressDialog = new ProgressDialog(context2);
        this.mProgressDialog.setMessage("Sending Email ...");
        this.mProgressDialog.setCancelable(false);
        this.emailtype=type;
        this.emailmsg=emsg;
        this.emailsubject=esub;
    }
    @Override
    public void onPreExecute() {
        super.onPreExecute();
        this.mProgressDialog.show();
    }
    @Override
    public String doInBackground(String... aurl) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            StringBuilder sb = new StringBuilder();
            sb.append(ServerCredentials.API_URL);
            HttpPost httppost = new HttpPost(sb.toString());
            List<NameValuePair> nameValuePairs = new ArrayList<>(4);

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            this.result = FileUtils.convertStream(httpclient.execute(httppost).getEntity().getContent());
        } catch (Exception e) {
            Log.e("Exception ", e.getMessage());
            this.exception = true;
            this.exceptionmessage = "Unfortunately Connection with server has been aborted , Please try again later";
        }
        return null;
    }
    @TargetApi(19)
    @Override
    public void onPostExecute(String unused) {
        this.mProgressDialog.dismiss();
        Log.e("Result ", this.result);
        String str = "";
        String str2 = "";
        String str3 = "";
        String str4 = "";
        try {
            int i = 0;
            if(!this.exception) {
                JSONArray jarr = new JSONArray(this.result);
                while (i < jarr.length()) {
                    if(jarr.getJSONObject(i).getString("name").equalsIgnoreCase(this.emailtype)){
                        this._listener.onSuccess(jarr.getJSONObject(i).getString("email"),emailmsg,emailsubject);
                        break;
                    }
                    i++;
                }
            }
             else if (this.exception) {
                this._listener.onError(this.exceptionmessage);
            }
        } catch (Exception e) {
            this._listener.onError(e.toString());
        }
    }
}
