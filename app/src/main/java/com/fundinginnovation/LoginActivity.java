package com.fundinginnovation;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import com.Utils.ConnectionClass;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.navigationdrawer.NavigationActivity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class LoginActivity extends Activity{

	ConnectionClass connectionClass;
	EditText edtuserid,edtpass;
	static String  User="";
	static String  id="";
	static String  name="";
	preferences pref;
	SharedPreferences settings;
	String storedusername="";
	String location="";
	String binno="";
	String Warehouse="";
	String email="";

	ProgressDialog pr;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		//SFAPI.USERNAME="";
		//SFAPI.PASSWORD="";
        pr=new ProgressDialog(LoginActivity.this);
        pref=new preferences(getApplicationContext());
		settings = this.getSharedPreferences(preferences.PREF_NAME,0);
		storedusername=  	settings.getString(preferences.username,"");
		//get initials from username
		if(storedusername.equals("")){}
		else
		{

			String[] myName = storedusername.split(" ");
			String initials="";
			for (int i = 0; i < myName.length; i++) {
				String s = myName[i];
				initials+=s.charAt(0);
			}
			pref.SetInitals(initials);
			//already logged in
			Intent i = new Intent(getApplicationContext(),NavigationActivity.class);
			i.putExtra("val", "");
			startActivity(i);
			finish();
		}
		edtuserid=(EditText)findViewById(R.id.editText1);
		//edtuserid.setText("Adam Kooistra");
		edtpass=(EditText)findViewById(R.id.editText2);
		connectionClass = new ConnectionClass();
	}
	public void login(View a)
	{
		if(edtuserid.getText().toString().isEmpty())
		{
			edtuserid.setError("Username cannot be empty. Please try again and if the issue persists, please contact app@fundinginnovation.ca");
		}
		else
		{
			//Authenticate Salesforce user
			Login doLogin = new Login(); 
			doLogin.execute("");
		}
	}

	public class Login extends AsyncTask<String,String,String>
	{
		ProgressDialog p;
		String z = "";
		Boolean isSuccess = false;
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(LoginActivity.this);
			p.setMessage("Logging In ...");
			p.show();
			p.setCancelable(false);
		}
		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if(isSuccess) {
				pref.Set5digitnumber("00001");
				//Take user to main Naviagtion
				Intent i= new Intent(getApplicationContext(),NavigationActivity.class);
				i.putExtra("val", "");
				startActivity(i);
				Toast.makeText(LoginActivity.this,r,Toast.LENGTH_SHORT).show();
			}
			else
			{
				Toast.makeText(LoginActivity.this,r,Toast.LENGTH_SHORT).show();
			}
		}
		@Override
		protected String doInBackground(String... params) {
			try {
				HttpClient httpclient = new DefaultHttpClient();
				String loginURL = SFAPI.LOGINURL +
						SFAPI.GRANTSERVICE +
						"&client_id=" + SFAPI.CLIENTID +
						"&client_secret=" + SFAPI.CLIENTSECRET +
						"&username=" + SFAPI.USERNAME +
						"&password=" + SFAPI.PASSWORD;
				Log.e("nfdjfdn",loginURL);
				HttpPost httpPost = new HttpPost(loginURL);
				HttpResponse response = null;
				try {
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();
				}
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;
				}
				else
					{
						isSuccess=true;
						z ="Login Successful";
					}
				String getResult = null;
				try {
					getResult = EntityUtils.toString(response.getEntity());
				} catch (IOException ioException) {
					ioException.printStackTrace();
				}
				JSONObject jsonObject = null;
				try {
					jsonObject = (JSONObject) new JSONTokener(getResult).nextValue();
					SFAPI.loginAccessToken = jsonObject.getString("access_token");
					SFAPI.loginInstanceUrl = jsonObject.getString("instance_url");
				} catch (JSONException jsonException) {
					jsonException.printStackTrace();
				}
				SFAPI.baseUri = SFAPI.loginInstanceUrl + SFAPI.REST_ENDPOINT + SFAPI.API_VERSION ;
				SFAPI.oauthHeader = new BasicHeader("Authorization", "OAuth " + SFAPI.loginAccessToken) ;
				HttpClient httpclienta = new DefaultHttpClient();
				String AccountURL = SFAPI.baseUri+"/query/?q="+ URLEncoder.encode("SELECT Id,Name,Default_Warehouse__c,Default_Easel__c,Email__c  FROM Account Where Name = '"+edtuserid.getText().toString()+"'", "UTF-8");
				Log.e("URL ",AccountURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				HttpGet httpPosta = new HttpGet(AccountURL);
				httpPosta.addHeader(SFAPI.oauthHeader);
				httpPosta.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse responsea = null;
				try {
					responsea = httpclienta.execute(httpPosta);
					JSONObject jobj=new JSONObject(EntityUtils.toString(responsea.getEntity()));
					String consid="";
					JSONArray jarr=jobj.getJSONArray("records");
					for(int i=0;i<jarr.length();i++) {
						JSONObject eitems = jarr.getJSONObject(i);
						consid = eitems.getString("Id");
						String Default_Warehouse__c=eitems.getString("Default_Warehouse__c");
						String Email__c=eitems.getString("Email__c");
						String Default_Easel__c=eitems.getString("Default_Easel__c");
						String cname=eitems.getString("Name");
						String[] myName = edtuserid.getText().toString().split(" ");
						//get initials from username
						try {
							String initials = "";
							for (int ii = 0; ii < myName.length; ii++) {
								String s = myName[ii];
								initials += s.charAt(0);
							}
							pref.SetInitals(initials);
						}catch (Exception a){}
						pref.SetbaseUri(SFAPI.baseUri);
						pref.SetloginAccessToken(SFAPI.loginAccessToken);
						pref.SetloginInstanceUrl(SFAPI.loginInstanceUrl);
						pref.SetUsername(edtuserid.getText().toString());
						pref.SetUserId(consid);
						pref.SetName(cname);
						pref.SetBinNo(Default_Easel__c);
						pref.SetLocation(location);
						pref.SetWareHouse(Default_Warehouse__c);
						pref.SetAremanageremail(Email__c);
					}
					isSuccess=true;
					z ="Login Successful";
				}  catch (Exception ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "Invalid User";
				}
			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "Invalid User";	}
			return z;
		}
	}
}