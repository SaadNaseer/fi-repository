package com.fundinginnovation;

import com.Database.Databaseadapter;
import com.NavigationFragments.Home;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.preferences;
import com.navigationdrawer.NavigationActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class NewEaselConfirmationQuestion extends Activity{
	RadioGroup radioreasons;
	Button gobutton;
	EditText providedetails;
	TextView q;
	Button next;
	Button nextcancel;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.neweaselconfirmationquestionactivity);
		next=(Button)findViewById(R.id.next);
		nextcancel=(Button)findViewById(R.id.cancelnext);
		q=(TextView)findViewById(R.id.textView1);
		radioreasons=(RadioGroup)findViewById(R.id.radioreasons);
		providedetails=(EditText)findViewById(R.id.providedetails);
		gobutton=(Button)findViewById(R.id.go);
		preferences pref= new preferences(getApplicationContext());
		pref.SetLastScreen("neweaselquestion");
		final 	RelativeLayout relativelayout=(RelativeLayout)findViewById(R.id.relativeLayout1);
		
		final RadioButton radioYes=(RadioButton)findViewById(R.id.radioYes);
		final RadioButton radioNo=(RadioButton)findViewById(R.id.radioNo);
		radioYes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) 
			{
				
				ObjectAnimator fadeOut = ObjectAnimator.ofFloat(radioYes, "alpha",  1f, .3f);
				fadeOut.setDuration(300);
				ObjectAnimator fadeIn = ObjectAnimator.ofFloat(radioYes, "alpha", .3f, 1f);
				fadeIn.setDuration(300);

				final AnimatorSet mAnimationSet = new AnimatorSet();

				mAnimationSet.play(fadeIn).after(fadeOut);

				mAnimationSet.addListener(new AnimatorListenerAdapter() {
				    @Override
				    public void onAnimationEnd(Animator animation) {
				        super.onAnimationEnd(animation);
				        preferences pref= new preferences(getApplicationContext());
						pref.SetLastScreen("neweaselscreen");	   
				        Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
				    	i.putExtra("val", "val");
				    	startActivity(i);
				    	finish();
				    }
		});
		mAnimationSet.start();
				
			}

			
		});
		radioNo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) 
			{
				
				ObjectAnimator fadeOut = ObjectAnimator.ofFloat(radioNo, "alpha",  1f, .3f);
				fadeOut.setDuration(300);
				ObjectAnimator fadeIn = ObjectAnimator.ofFloat(radioNo, "alpha", .3f, 1f);
				fadeIn.setDuration(300);

				final AnimatorSet mAnimationSet = new AnimatorSet();

				mAnimationSet.play(fadeIn).after(fadeOut);

				mAnimationSet.addListener(new AnimatorListenerAdapter() {
				    @Override
				    public void onAnimationEnd(Animator animation) {
				        super.onAnimationEnd(animation);
				       			   
				       /* preferences p= new preferences(getApplicationContext());
				    	p.SetLastScreen("");
						p.Settotalbids("");
						Databaseadapter db=new Databaseadapter(getApplicationContext());
						db.DeleteRunningTransactions();  
				        Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
				        i.putExtra("val", "");
				    	startActivity(i);
				    	finish();*/
				        
				        providedetails.setVisibility(View.INVISIBLE);
						radioYes.setVisibility(View.INVISIBLE);
						radioNo.setVisibility(View.INVISIBLE);
						radioreasons.setVisibility(View.VISIBLE);
						q.setText("Reason ?");	
						if(getscreensize()>=7)
						{
							final float scale = NewEaselConfirmationQuestion.this.getResources().getDisplayMetrics().density;
							int pixelswidth = (int) (460 * scale + 0.5f);
							int pixelsheight = (int) (360 * scale + 0.5f);
							android.view.ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
							layoutParams.width = pixelswidth;
							layoutParams.height = pixelsheight;
							relativelayout.setLayoutParams(layoutParams);
						}
						else
						{
						final float scale = NewEaselConfirmationQuestion.this.getBaseContext().getResources().getDisplayMetrics().density;
						int pixelswidth = (int) (280 * scale + 0.5f);
						int pixelsheight = (int) (210 * scale + 0.5f);
						android.view.ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
						layoutParams.width = pixelswidth;
						layoutParams.height = pixelsheight;
						relativelayout.setLayoutParams(layoutParams);
						}
				    
						radioreasons.setOnCheckedChangeListener(new OnCheckedChangeListener() {

							@Override
							public void onCheckedChanged(RadioGroup group, int checkedId) {
								int id=group.getCheckedRadioButtonId();
								RadioButton rb=(RadioButton)findViewById(id);
								rb.setChecked(true);
								final String reason=rb.getText().toString();

							
									providedetails.setVisibility(View.VISIBLE);
									q.setText("Please Provide Detail");
									next.setVisibility(View.VISIBLE);
									next.setText("Submit");
									nextcancel.setVisibility(View.VISIBLE);
									
									radioreasons.setVisibility(View.INVISIBLE);
									radioYes.setVisibility(View.INVISIBLE);
									radioNo.setVisibility(View.INVISIBLE);
								
									if(getscreensize()>=7)
									{
										final float scale = NewEaselConfirmationQuestion.this.getBaseContext().getResources().getDisplayMetrics().density;
										int pixelswidth = (int) (460 * scale + 0.5f);
										int pixelsheight = (int) (360 * scale + 0.5f);
										android.view.ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
										layoutParams.width = pixelswidth;
										layoutParams.height = pixelsheight;
										relativelayout.setLayoutParams(layoutParams);
									}
									else
									{
									final float scale = NewEaselConfirmationQuestion.this.getBaseContext().getResources().getDisplayMetrics().density;
									int pixelswidth = (int) (280 * scale + 0.5f);
									int pixelsheight = (int) (210 * scale + 0.5f);
									android.view.ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
									layoutParams.width = pixelswidth;
									layoutParams.height = pixelsheight;
									relativelayout.setLayoutParams(layoutParams);
									}
									next.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) 
										{
											//Toast.makeText(NewEaselConfirmationQuestion.this, "Email Generated", Toast.LENGTH_LONG).show();
											SharedPreferences settings = NewEaselConfirmationQuestion.this.getSharedPreferences(preferences.PREF_NAME,0);
											
											String	changedartno =settings.getString(preferences.ChangedArtno,"");
											String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
											String values="";
										 if(changedartno.equals("")&&changedartdes.equals(""))
										 {
											values="Easel Location #   : "+Home.SelectedEasel+"\nEasel Location Name   : "+Home.SelectedSponsor+"\nCharity   : "+Home.SelectedCharity.trim().toString()+"\nAddress   : "+Home.SelectedStreetAddress+" "+Home.SelectedCity+" "+Home.SelectedProvince+" "+Home.SelectedPostCode+"\nProduct #   : "+Home.SelectedArt+"\nProduct Description   : "+Home.SelectedArtDescription+"\nDetails   : "+providedetails.getText().toString();
										 }
										 else
										 {
											 values="Easel Location #   : "+Home.SelectedEasel+"\nEasel Location Name   : "+Home.SelectedSponsor+"\nCharity   : "+Home.SelectedCharity.trim().toString()+"\nAddress   : "+Home.SelectedStreetAddress+" "+Home.SelectedCity+" "+Home.SelectedProvince+" "+Home.SelectedPostCode+"\nProduct #   : "+changedartno+"\nProduct Description   : "+changedartdes+"\nDetails   : "+providedetails.getText().toString();
											  
										 }
											SendEmail(values, reason);
											AlertDialog.Builder builder = new AlertDialog.Builder(
													NewEaselConfirmationQuestion.this);
											builder.setTitle("Email Generated");
											builder.setIcon(android.R.drawable.ic_dialog_email);
											builder.setMessage("Thank You for your Input");
											builder.setCancelable(false);
											builder.setPositiveButton("OK",
													new DialogInterface.OnClickListener() {

														@Override
														public void onClick(DialogInterface arg0,
																int arg1) {
															/*NavigationActivity.startturn=false;
															FragmentManager fragmentManager = getFragmentManager();
															fragmentManager.beginTransaction()
															.replace(R.id.frame_container, new HomeNew()).commit();*/
															
															preferences p= new preferences(getApplicationContext());
													    	p.SetLastScreen("");
															p.Settotalbids("");
															Databaseadapter db=new Databaseadapter(getApplicationContext());
															db.DeleteRunningTransactions();  
													        Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
													        i.putExtra("val", "");
													    	startActivity(i);
													    	finish();
															
														}
													});

											builder.create().show();
										}
									});
									nextcancel.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) 
										{
											AlertDialog.Builder builder = new AlertDialog.Builder(
													NewEaselConfirmationQuestion.this);
											builder.setTitle("Cancel ?");
											builder.setIcon(android.R.drawable.ic_dialog_alert);
											builder.setMessage("Are you sure you wish to cancel the easel entry ?");
											builder.setNegativeButton("No", null);
											builder.setPositiveButton("Yes",
													new DialogInterface.OnClickListener() {

														@Override
														public void onClick(DialogInterface arg0,
																int arg1) {
															 	preferences p= new preferences(getApplicationContext());
														    	p.SetLastScreen("");
																p.Settotalbids("");
																Databaseadapter db=new Databaseadapter(getApplicationContext());
																db.DeleteRunningTransactions();  
														        Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
														        i.putExtra("val", "");
														    	startActivity(i);
														    	finish();	}
													});

											builder.create().show();
										}
									});		
				}
				});
				    
				    }
		});
		mAnimationSet.start();
				
			}

			
		});
	}
	@SuppressWarnings("unused")
	public double getscreensize()
	{
		DisplayMetrics metrics = new DisplayMetrics();
		NewEaselConfirmationQuestion.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		float scaleFactor = metrics.density;
		float widthDp = widthPixels / scaleFactor;
				float heightDp = heightPixels / scaleFactor;
				
				float widthDpi = metrics.xdpi;
				float heightDpi = metrics.ydpi;
				float widthInches = widthPixels / widthDpi;
				float heightInches = heightPixels / heightDpi;
				double diagonalInches = Math.sqrt(
					    (widthInches * widthInches) 
					    + (heightInches * heightInches));
				
				if (diagonalInches >= 10) {
				    //Device is a 10" tablet
				} 
				else if (diagonalInches >= 7) {
				    //Device is a 7" tablet
				}
		
		return diagonalInches;
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
			 
			  @Override
			  public void run() {
			    try {
			    	GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
			    } catch (Exception e) {
			      Log.e("SendMail", e.getMessage(), e);
			      Toast.makeText(NewEaselConfirmationQuestion.this, e.getMessage(), Toast.LENGTH_LONG).show();
			  }
			}
			 
			}).start();
	}
}
