package com.fundinginnovation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Utils.ConnectionClass;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	private Button scanBtn,manualentryBtn;
	static String scanContent="";
	static String scanFormat="";
	ConnectionClass connectionClass;
	ListView listView;
	ArrayList<HashMap<String, String>> listing;
	static String Art = "Art";
	static String Location = "Location";
	static String Charity = "Charity";
	static String Quantity = "Quantity";
	static String Sponsor = "Sponsor";
	static String StreetAddress = "StreetAddress";
	static String City = "City";
	static String Province = "Province";
	static String PostalCode = "PostalCode";
	static String ArtDescription = "ArtDescription";
	static String Easel = "Easel";
	 String artnumberbar = "";
	static String SelectedArt = "SelectedArt";
	static String SelectedLocation = "SelectedLocation";
	static String SelectedCharity = "SelectedCharity";
	static String SelectedSponsor = "SelectedSponsor";
	static String SelectedEasel = "SelectedEasel";
	static String SelectedQuantity = "SelectedQuantity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		scanBtn = (Button)findViewById(R.id.scan_button);
		scanBtn.setOnClickListener(this);
		manualentryBtn = (Button)findViewById(R.id.manualentry_button);
		manualentryBtn.setOnClickListener(this);
		listing = new ArrayList<HashMap<String, String>>();
		listView=(ListView)findViewById(R.id.listview);
		listView.setCacheColorHint(Color.TRANSPARENT);
		connectionClass = new ConnectionClass();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.scan_button){

			IntentIntegrator scanIntegrator = new IntentIntegrator(this);

			scanIntegrator.initiateScan();
		}
			if(v.getId()==R.id.manualentry_button){
			
			/*Intent i = new Intent(getApplicationContext(),ManualEntry.class);
			startActivity(i);*/
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanningResult != null) {
			//artnumberbar="38-453";
			scanContent = scanningResult.getContents();
			scanFormat = scanningResult.getFormatName();
			Toast.makeText(getApplicationContext(), 
					scanContent, Toast.LENGTH_SHORT).show();

			Toast.makeText(getApplicationContext(), 
					scanFormat, Toast.LENGTH_SHORT).show();
			
			/*GetEasels geteasels = new GetEasels(); 
			geteasels.execute("");*/
			if(scanContent!=null)
			{
				try{
					if(scanContent.contains("-"))
					{
					 
					}
					else
					{
						
						scanContent=scanContent.substring(6);
						scanContent = scanContent.substring(0, 2) + "-" + scanContent.substring(2, scanContent.length());
					
					if(scanContent.length()>6)
					{
						scanContent=scanContent.substring(0, scanContent.length() - 1);
						
					}
						
					}}catch(Exception a)
					{
						Toast.makeText(getApplicationContext(), "Invalid Bar Code", Toast.LENGTH_LONG).show();
						return;
					}
				artnumberbar=scanContent;
				
				GetEasels geteasels = new GetEasels(); 
				geteasels.execute("");
				
				
			}
			else{
				Toast toast = Toast.makeText(getApplicationContext(), 
						"No Bar Code data received!", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
		else{
			Toast toast = Toast.makeText(getApplicationContext(), 
					"No Bar Code data received!", Toast.LENGTH_SHORT);
			toast.show();
		}
		

	}
	public class GetEasels extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;

		/*String userid = edtuserid.getText().toString();
		String password = edtpass.getText().toString();*/


		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(MainActivity.this);
			p.setMessage("Loading Live Easels ...");
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {
				
				Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();
				listView.setVisibility(View.VISIBLE);
				EaselAdapter   adapter = new EaselAdapter(MainActivity.this, listing);
				// Binds the Adapter to the ListView
				listView.setAdapter(adapter);
			}
			else
			{
				Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
				try {
					Connection con = connectionClass.CONN();
					if (con == null) {
						z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
					} else {
						//String query = "select * from dbo.vw_PAS_LiveEasels" ;
						String query = "select * from dbo.vw_PAS_LiveEasels where Art='" + artnumberbar + "'";
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery(query);
						 while (rs.next()) {
							/*Log.e("Art ", rs.getString("Art"));
							Log.e("Location ", rs.getString("Location"));
							Log.e("Quantity ", rs.getString("Quantity"));
							Log.e("Charity ", rs.getString("Charity"));
							Log.e("Sponsor ", rs.getString("Sponsor"));
							Log.e("Street_Address ", rs.getString("Street_Address"));
							Log.e("City ", rs.getString("City"));
							Log.e("Province ", rs.getString("Province"));
							Log.e("Postalcode ", rs.getString("Postalcode"));
							Log.e("ArtDescription ", rs.getString("ArtDescription"));
							Log.e("Easel ", rs.getString("Easel"));*/
						
							HashMap<String, String> map = new HashMap<String, String>();
							if(rs.getString("Art")!=null){
							map.put(Art, rs.getString("Art").trim().toString());
							Log.e("Art ", rs.getString("Art"));}
							else
							{
								map.put(Art, "");
							}
							if(rs.getString("Location")!=null){
								map.put(Location, rs.getString("Location"));}
								else
								{
									map.put(Location, "");
								}
							
							if(rs.getString("Charity")!=null){
								map.put(Charity, rs.getString("Charity"));}
								else
								{
									map.put(Charity, "");
								}
							
							if(rs.getString("Quantity")!=null){
								map.put(Quantity, rs.getString("Quantity"));}
								else
								{
									map.put(Quantity, "");
								}
							if(rs.getString("Sponsor")!=null){
								map.put(Sponsor, rs.getString("Sponsor").trim().toString());}
								else
								{
									map.put(Sponsor, "");
								}
							if(rs.getString("Street_Address")!=null){
								map.put(StreetAddress, rs.getString("Street_Address"));}
								else
								{
									map.put(StreetAddress, "");
								}
							if(rs.getString("City")!=null){
								map.put(City, rs.getString("City"));}
								else
								{
									map.put(City, "");
								}
							if(rs.getString("Province")!=null){
								map.put(Province, rs.getString("Province"));}
								else
								{
									map.put(Province, "");
								}
							if(rs.getString("PostalCode")!=null){
								map.put(PostalCode, rs.getString("PostalCode"));}
								else
								{
									map.put(PostalCode, "");
								}
							if(rs.getString("ArtDescription")!=null){
								map.put(ArtDescription, rs.getString("ArtDescription"));}
								else
								{
									map.put(ArtDescription, "");
								}
							if(rs.getString("ArtDescription")!=null){
								map.put(Easel, rs.getString("Easel"));}
								else
								{
									map.put(Easel, "");
								}
							
							
							
					
							listing.add(map);
		                    }
						if(!listing.isEmpty())
						{

							z = "Live Easels Found";
							isSuccess=true;
						}
						else
						{
							z = "No Live Easels Found";
							isSuccess = false;
						}

					}
				}
				catch (Exception ex)
				{
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return z;
		}
	}
	public class EaselAdapter extends BaseAdapter {

		// Declare Variables
		Context context;
		String value;
		String fontPath2 = "segoepr.ttf";
		LayoutInflater inflater;
		ArrayList<HashMap<String, String>> data;
		
		public EaselAdapter(Context context,
				ArrayList<HashMap<String, String>> arraylist) {
			this.context = context;
			data = arraylist;
			

		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@SuppressWarnings("unused")
		public View getView(final int position, View convertView, ViewGroup parent) {
			// Declare Variables
			final TextView Art;
			final TextView Location;
			final TextView Charity;
			final TextView Quantity;
			final TextView Sponsor;
			TextView StreetAddress;
			TextView City;
			TextView Province;
			TextView PostalCode;
			TextView ArtDescription;
			final TextView Easel;
			TextView listtext;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			Typeface tf2 = Typeface.createFromAsset(context.getAssets(), fontPath2);

			final View itemView = inflater.inflate(R.layout.easeldisplay, parent, false);

			// Get the position from the results
			HashMap<String, String> results = new HashMap<String, String>();
			results = data.get(position);
			Art=(TextView)itemView.findViewById(R.id.art);
			Art.setText(results.get(MainActivity.Art));
			Easel=(TextView)itemView.findViewById(R.id.easel);
			Easel.setText(results.get(MainActivity.Easel));
			Location=(TextView)itemView.findViewById(R.id.location);
			Location.setText(results.get(MainActivity.Location));
			Charity=(TextView)itemView.findViewById(R.id.charity);
			Charity.setText(results.get(MainActivity.Charity));
			Quantity=(TextView)itemView.findViewById(R.id.quantity);
			Quantity.setText(results.get(MainActivity.Quantity));
			Sponsor=(TextView)itemView.findViewById(R.id.sponsor);
			Sponsor.setText(results.get(MainActivity.Sponsor));
			StreetAddress=(TextView)itemView.findViewById(R.id.streetaddress);
			StreetAddress.setText(results.get(MainActivity.StreetAddress));
			City=(TextView)itemView.findViewById(R.id.city);
			City.setText(results.get(MainActivity.City));
			Province=(TextView)itemView.findViewById(R.id.province);
			Province.setText(results.get(MainActivity.Province));
			PostalCode=(TextView)itemView.findViewById(R.id.postalcode);
			PostalCode.setText(results.get(MainActivity.PostalCode));
			ArtDescription=(TextView)itemView.findViewById(R.id.artdescription);
			ArtDescription.setText(results.get(MainActivity.ArtDescription));
			listtext=(TextView)itemView.findViewById(R.id.listtext);
			listtext.setText(results.get(MainActivity.Easel.trim().toString())+"   "+results.get(MainActivity.Location.trim().toString())+" / "+results.get(MainActivity.Sponsor.trim().toString())+" / "+results.get(MainActivity.Art.trim().toString()));
			
	
			// Capture button clicks on ListView items
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					
					SelectedArt = Art.getText().toString();
					SelectedLocation = Location.getText().toString();
					SelectedCharity = Charity.getText().toString();
					SelectedSponsor = Sponsor.getText().toString();
					SelectedEasel = Easel.getText().toString();
					SelectedQuantity = Quantity.getText().toString();
					Intent i=new Intent(getApplicationContext(),Questionaire.class);
					startActivity(i);
				}
			});

			return itemView;
		}
	}
	@Override
	public void onBackPressed()
	{
	     
	     
	}
}
