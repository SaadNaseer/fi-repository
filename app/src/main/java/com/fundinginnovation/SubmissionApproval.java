package com.fundinginnovation;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import com.NavigationFragments.Home;
import com.Utils.FTPCredentials;
import com.Utils.FileUtils;
import com.Utils.preferences;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class SubmissionApproval extends Activity{
	
String answer="";
String filename="";
int currentimage=1;
Boolean isSuccess = false;
int remainingimages=0;
String errormessage="";
int uploadimagecount=1;
String	uniquenumber="00001";
String user2ch="";
String usernamefull="";
boolean uploadtxt=false;
boolean uploadimage=false;
boolean movefile=true;
private ArrayList<String> bidamounts ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.submissionapproval);
		Intent i = getIntent();
		bidamounts=i.getStringArrayListExtra("amounts");
		try{remainingimages=Integer.parseInt(TakePictures.totalbids);}catch(Exception y){}
		SharedPreferences pref = getApplicationContext().getSharedPreferences(preferences.PREF_NAME,0);
		
		uniquenumber =pref.getString(preferences.uniquevalue,"00001");
		usernamefull =pref.getString(preferences.username,"");
		user2ch =pref.getString(preferences.username,"");
		
		if(Home.SelectedArt.equals("SelectedArt")||Home.SelectedEasel.equals("SelectedEasel")||Home.SelectedLocation.equals("SelectedLocation"))
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
					SubmissionApproval.this);
			builder.setTitle("Error");
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setMessage("Unfortunately we have encountered an issue with the application. Please completely log out of the app and restart it");
	
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
						
						}
					});

			builder.create().show();
			return;
		}
		
		//generateodbfileandsend();
		//generatetxtfile();
	/*if(Questionaire.zerobids)
	{
		uploadimage=false;
		 UploadFile doUpload = new UploadFile(); 
		  doUpload.execute("");
	}
	else{
		 UploadImages doUpload = new UploadImages(); 
		  doUpload.execute("");}*/
		Intent ii= new Intent(getApplicationContext(),SummaryActivity.class);
		startActivity(ii);
		finish();
		final EditText providedetails=(EditText)findViewById(R.id.providedetails);
		final RadioGroup radioyesno=(RadioGroup)findViewById(R.id.radioyesno);
		final Button next=(Button)findViewById(R.id.next);
		radioyesno.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				int id=group.getCheckedRadioButtonId();
				final RadioButton rb=(RadioButton) findViewById(id);
				final AnimatorSet mAnimationSet = new AnimatorSet();
				ObjectAnimator fadeOut = ObjectAnimator.ofFloat(rb, "alpha",  1f, .3f);
				fadeOut.setDuration(300);
				ObjectAnimator fadeIn = ObjectAnimator.ofFloat(rb, "alpha", .3f, 1f);
				fadeIn.setDuration(300);

				
				mAnimationSet.play(fadeIn).after(fadeOut);

				mAnimationSet.addListener(new AnimatorListenerAdapter() {
				    @Override
				    public void onAnimationEnd(Animator animation) {
				        super.onAnimationEnd(animation);
				        
				        answer=rb.getText().toString();
						if(!answer.equals("No")&&radioyesno.getVisibility()==View.VISIBLE)
						{
							next.setVisibility(View.VISIBLE);
							generateodbfileandsend();
							return;
						}
						if(answer.equals("No")&&radioyesno.getVisibility()==View.VISIBLE)
						{
							radioyesno.setVisibility(View.GONE);
							providedetails.setVisibility(View.VISIBLE);
							next.setVisibility(View.VISIBLE);
						}
						
						else{
							radioyesno.setVisibility(View.VISIBLE);
							providedetails.setVisibility(View.GONE);
						}
				        }
				});
				mAnimationSet.start();
			}
		});
	final 	TextView q=(TextView)findViewById(R.id.textView1);
		q.setSelected(true);
		
		next.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) 
			{
				if(answer.equals(""))
				{
					Toast.makeText(getApplicationContext(), "Please Select One Option", Toast.LENGTH_LONG).show();
				return;
				}
				if(answer.equals("No")&&radioyesno.getVisibility()==View.VISIBLE)
				{
					radioyesno.setVisibility(View.GONE);
					providedetails.setVisibility(View.VISIBLE);
				}
				
				else{
					radioyesno.setVisibility(View.VISIBLE);
					providedetails.setVisibility(View.GONE);
				
					 UploadImages doUpload = new UploadImages(); 
					  doUpload.execute("");
				
				}
			}
		});
	}
@SuppressWarnings("unused")
public void generateodbfileandsend()
{
	
	try{
		 
		String m="";
		String d="";
		String modifiedmonth="";
		String modifiedday="";
		Date date = new Date(); // your date
		    Calendar cal = Calendar.getInstance();
		    cal.setTime(date);
		    int year = cal.get(Calendar.YEAR);
		    int month = cal.get(Calendar.MONTH);
		    int day = cal.get(Calendar.DAY_OF_MONTH);
		    month=month+1;
		    if(day<10)
		    {
		    	modifiedday="0"+String.valueOf(day);
		    }
		    else if(day>=10){modifiedday=String.valueOf(day);}
		    if(month<10)
		    {
		    	modifiedmonth="0"+String.valueOf(month);
		    }
		    else if(month>=10){modifiedmonth=String.valueOf(month);}
		    if(day<10)
		    {
		    	d="0"+String.valueOf(day);
		    }
		    else
		    {
		    	d=String.valueOf(day);
		    }
		   
		if(month<=9)
		{
			m=String.valueOf(month);
		}
		else if(month==10)
		{
			m="O";
			
		}
		else if(month==11)
		{
			m="N";
		
		}
		else if(month==12)
		{
			m="D";
			
		}
		// filename="I"+LoginActivity.id+LoginActivity.id+m+d;
		String user=user2ch.substring(0, Math.min(user2ch.length(), 2));
		filename="I"+user+String.valueOf(uniquenumber);
	File ff=new File(Environment.getExternalStorageDirectory()+File.separator+filename+".txt");
	if(ff.exists()){ff.delete();}
	ff.createNewFile();
	long nowpointer=0;
	String barcodeandart=Home.SelectedArt;
	String easelandart=Home.SelectedEasel+Home.SelectedArt;
	String location=Home.SelectedLocation+Home.SelectedLocation;
	String newdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
	SharedPreferences settings = this.getSharedPreferences(preferences.PREF_NAME,0);
	String newlocationfromdb=  	settings.getString(preferences.location,"");
	String newbinnofromdb=  	settings.getString(preferences.binno,"");
	String warehouse=			settings.getString(preferences.Warehouse,"");
	RandomAccessFile file = new RandomAccessFile(Environment.getExternalStorageDirectory()+File.separator+filename+".txt", "rw");
	
	file.write("IJRITH".getBytes());
	//file.seek(22);
	for(int i=0;i<16;i++)
	{
		file.write(" ".getBytes());
	}
	file.write(barcodeandart.getBytes());
	nowpointer=52-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	//file.seek(52);
	file.write(newlocationfromdb.getBytes());
	//file.seek(55);
	if(newlocationfromdb.length()<3)
	{
		file.write(" ".getBytes());
	}
	file.write(warehouse.getBytes());
	//file.seek(58);
	file.write(newdate.getBytes());
	nowpointer=106-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	//file.seek(106);
	file.write(Home.SelectedEasel.getBytes());
	nowpointer=126-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	//file.seek(126);
	file.write(/*Questionaire.Bids*/"1.0".getBytes());
	nowpointer=186-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	//file.seek(186);
	file.write(Home.SelectedEasel.getBytes());
	nowpointer=209-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	//file.seek(209);
	file.write(/*"0000"*/newbinnofromdb.getBytes());
	nowpointer=445-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	//file.seek(445);
	file.write(usernamefull.getBytes());
	file.writeBytes("\r\n");
	long pointer=file.getFilePointer();
	file.write("IJRITD".getBytes());
	//file.seek(pointer+22);
	for(int i=0;i<16;i++)
	{
		file.write(" ".getBytes());
	}
	file.write(barcodeandart.getBytes());
	nowpointer=pointer+52-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	//file.seek(pointer+52);
	file.write(newlocationfromdb.getBytes());
	if(newlocationfromdb.length()<3)
	{
		file.write(" ".getBytes());
	}
	//file.seek(pointer+55);
	file.write(warehouse.getBytes());
	//file.seek(pointer+58);
	file.write(newdate.getBytes());
	//file.seek(pointer+106);
	nowpointer=pointer+106-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	file.write(Home.SelectedEasel.getBytes());
	//file.seek(pointer+126);
	nowpointer=pointer+126-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	file.write(/*Questionaire.Bids*/"1.0".getBytes());
	//file.seek(pointer+186);
	nowpointer=pointer+186-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	file.write(Home.SelectedEasel.getBytes());
	//file.seek(pointer+209);
	nowpointer=pointer+209-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	file.write(/*"0000"*/newbinnofromdb.getBytes());
	//file.seek(pointer+445);
	nowpointer=pointer+445-file.getFilePointer();
	for(int i=0;i<nowpointer;i++)
	{
		file.write(" ".getBytes());
	}
	file.write(usernamefull.getBytes());
	file.close();
	}catch(Exception q)
	{
		Toast.makeText(getApplicationContext(), q.toString(), Toast.LENGTH_LONG).show();
	}
	File from = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".txt");
	File to = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".odb");
	from.renameTo(to);
	
}
public class UploadFile extends AsyncTask<String,String,String>
{
	
	String z = "";
	
	  String replycode="";
	  ProgressDialog p;
	@Override
	protected void onPreExecute() {
		p=new ProgressDialog(SubmissionApproval.this);
		p.setMessage("Uploading File ...");
		p.setCancelable(false);
		p.show();
		//pbbar.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onPostExecute(String r) {
		p.dismiss();
		if(isSuccess) {
			if(uploadtxt){
			
			SharedPreferences settings = getApplicationContext().getSharedPreferences(preferences.PREF_NAME,0);
			
			String	uniquenumber =settings.getString(preferences.uniquevalue,"00001");
			int incrementuniquenumber=Integer.parseInt(uniquenumber);
			incrementuniquenumber=incrementuniquenumber+1;
			preferences p= new preferences(getApplicationContext());
			p.Set5digitnumber("0000"+String.valueOf(incrementuniquenumber));
			//p.SetChangedartdes("");
			//p.SetChangedartno("");
			Toast.makeText(getApplicationContext(),"Txt File Uploaded SuccessFully",Toast.LENGTH_SHORT).show();
			 Intent i= new Intent(getApplicationContext(),SummaryActivity.class);
				startActivity(i);
				finish();}
			else{
				isSuccess=false;
				uploadtxt=true;
				
				UploadFile doUpload = new UploadFile(); 
				  doUpload.execute("");
				Toast.makeText(SubmissionApproval.this,"odb File Uploaded SuccessFully",Toast.LENGTH_SHORT).show();
			}
			}
		
		else
		{
			Toast.makeText(SubmissionApproval.this,"Error in uploading File",Toast.LENGTH_SHORT).show();
			finish();
		}

	}

	@Override
	protected String doInBackground(String... params) {
	
			try {
				isSuccess=false;
				if(uploadtxt)
				{
					movefile=true;
					uploadFile(new File(Environment.getExternalStorageDirectory()+File.separator+getbidsfilename()+".txt"));
					
				}
				else{
					movefile=false;
				uploadFile(new File(Environment.getExternalStorageDirectory()+File.separator+filename+".odb"));
				}  
				
				
			}
			catch (Exception ex)
			{
				isSuccess = false;
				replycode = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}
		
		return replycode;
	}
}

public class UploadImages extends AsyncTask<String,String,String>
{
	
	String z = "";
	ProgressDialog p;
	  String replycode="";

	@Override
	protected void onPreExecute() {
		p=new ProgressDialog(SubmissionApproval.this);
		p.setMessage("Uploading Image # "+String.valueOf(uploadimagecount)+" ...");
		p.setCancelable(false);
		p.show();
		//pbbar.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onPostExecute(String r) {
		p.dismiss();
		remainingimages--;
		uploadimagecount++;
		if(isSuccess&&remainingimages<1) {
			Toast.makeText(SubmissionApproval.this,"Images Uploaded SuccessFully",Toast.LENGTH_SHORT).show();
			uploadimage=false;
			 UploadFile doUpload = new UploadFile(); 
			  doUpload.execute("");
			  
		
		}
		else if(isSuccess&&remainingimages>=1)
		{
			UploadImages doUpload = new UploadImages(); 
			  doUpload.execute("");
		}
		
		else
		{
			Toast.makeText(SubmissionApproval.this,"Error in uploading Image",Toast.LENGTH_SHORT).show();
			finish();
		}

	}

	@Override
	protected String doInBackground(String... params) {
	
			try {
				uploadimage=true;
				uploadFile(new File(Environment.getExternalStorageDirectory()+File.separator+getimagesname()+bidamounts.get(remainingimages-1)+"_"+filename+"_"+String.valueOf(remainingimages)+".jpg"));
				 /* FTPClient client = new FTPClient();
				  FileInputStream fis = null;
				
					    client.connect("142.112.12.137");
					    client.login("fresh", "ZRnvEJYTxJ4CECmS8JA4qTut");

					    //
					    // Create an InputStream of the file to be uploaded
					    //
					    
					    fis = new FileInputStream(Environment.getExternalStorageDirectory()+File.separator+filename+".jpg");

					    //
					    // Store file to server
					    //
					    client.storeFile(filename+".jpg", fis);
					    replycode=String.valueOf(client.getReplyCode());
					    client.logout();
					    if(replycode.equals("226"))
					    {
					    	isSuccess=true;
					    }
					    else
					    {
					    	isSuccess = false;
					    }*/
				
			}
			catch (Exception ex)
			{
				isSuccess = false;
				Log.e("Error ",ex.toString());
				replycode = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}
		
		return replycode;
	}
}
public void uploadFile(File fileName){
    
    
    FTPClient client = new FTPClient();
     
   try {
        
       client.connect(FTPCredentials.FTP_HOST,21);
       client.login(FTPCredentials.FTP_USER, FTPCredentials.FTP_PASS);
       client.setType(FTPClient.TYPE_BINARY);
    if(uploadtxt||uploadimage)
     {
    	//client.changeDirectory("bid_imports/Test");
    	client.changeDirectory("bid_imports");
     }
    /*else
    {
    	 client.changeDirectory("Test");	
    }*/
       
       client.upload(fileName, new MyTransferListener());
        
   } catch (Exception e) {
       e.printStackTrace();
       errormessage=e.toString();
       Log.e("EXCEPTION ",errormessage);
       try {
           client.disconnect(true);    
       } catch (Exception e2) {
    	   errormessage=e.toString();
           e2.printStackTrace();
       }
   }
    
}

/*******  Used to file upload and show progress  **********/

public class MyTransferListener implements FTPDataTransferListener {

   public void started() {
        
   }

   public void transferred(int length) {
	
   }

   public void completed() {
        
	   isSuccess=true;
	   if(uploadtxt&&movefile)
	   {
		   //Text File
		   
				   FileUtils.MoveFile(Environment.getExternalStorageDirectory()+File.separator+getbidsfilename()+".txt",Environment.getExternalStorageDirectory()
							+ File.separator + "Bid Imports"+File.separator+getbidsfilename()+".txt");
	   }
	   else if(!uploadtxt&&movefile){
		   
		   //Images
		   FileUtils.MoveFile(Environment.getExternalStorageDirectory()+File.separator+getimagesname()+bidamounts.get(remainingimages-1)+"_"+filename+"_"+String.valueOf(remainingimages)+".jpg",Environment.getExternalStorageDirectory()
				+ File.separator + "Bid Imports"+File.separator+getimagesname()+bidamounts.get(remainingimages-1)+"_"+filename+"_"+String.valueOf(remainingimages)+".jpg");}
	   
	   else
	   {
		   //Odb File
	   }
	 
   }

   public void aborted() {
	
   }

   public void failed() {

   }

}
public void generatetxtfile()
{
	try{
	File ff=new File(Environment.getExternalStorageDirectory()+File.separator+getbidsfilename()+".txt");
	if(ff.exists()){ff.delete();}
	ff.createNewFile();
	SharedPreferences settings = SubmissionApproval.this.getSharedPreferences(preferences.PREF_NAME,0);
	
	String	changedartno =settings.getString(preferences.ChangedArtno,"");
	String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
	String art="";
	String description="";
 if(changedartno.equals("")&&changedartdes.equals(""))
 {
	art="PRODUCT: "+Home.SelectedArt;
	description="DESCRIPTION: "+Home.SelectedArtDescription;
 }
 else
 {
	art="PRODUCT: "+changedartno;
	description="DESCRIPTION: "+changedartdes;
 }
	String easel="EASEL: "+Home.SelectedEasel;
	String easelname="Easel Name: "+Home.SelectedSponsor;
	
	String bids="";
	if(Questionaire.zerobids){ bids="BIDS: "+"0";}
	else{
			 bids="BIDS: "+Questionaire.Bids;}
//	String bids="BIDS: "+Questionaire.Bids;
	
	RandomAccessFile file = new RandomAccessFile(Environment.getExternalStorageDirectory()+File.separator+getbidsfilename()+".txt", "rw");
	file.write(easel.getBytes());
	file.writeBytes("\r\n");
	file.write(easelname.getBytes());
	file.writeBytes("\r\n");
	file.write(art.getBytes());
	file.writeBytes("\r\n");
	file.write(bids.getBytes());
	file.writeBytes("\r\n");
	file.write(description.getBytes());
	file.writeBytes("\r\n");
	file.close();
	
	 
	}
	catch(Exception y)
	{
		
	}
}
@SuppressWarnings("unused")
public String getimagesname()
{
	String m="";
	String d="";
	String modifiedmonth="";
	String modifiedday="";
	Date date = new Date(); // your date
	Calendar cal = Calendar.getInstance();
	cal.setTime(date);
	int year = cal.get(Calendar.YEAR);
	int month = cal.get(Calendar.MONTH);
	int day = cal.get(Calendar.DAY_OF_MONTH);
	month=month+1;
	if(day<10)
	{
		modifiedday="0"+String.valueOf(day);
	}
	else if(day>=10){modifiedday=String.valueOf(day);}
	if(month<10)
	{
		modifiedmonth="0"+String.valueOf(month);
	}
	else if(month>=10){modifiedmonth=String.valueOf(month);}
	if(day<10)
	{
		d="0"+String.valueOf(day);
	}
	else
	{
		d=String.valueOf(day);
	}
	
	if(month<=9)
	{
		m=String.valueOf(month);
	}
	else if(month==10)
	{
		m="O";

	}
	else if(month==11)
	{
		m="N";

	}
	else if(month==12)
	{
		m="D";

	}	
	String name="";
	String currentdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
	SharedPreferences settings = SubmissionApproval.this.getSharedPreferences(preferences.PREF_NAME,0);
		
		String	changedartno =settings.getString(preferences.ChangedArtno,"");
		String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
	 if(changedartno.equals("")&&changedartdes.equals(""))
	 {
		  name =Home.SelectedEasel+"_"+Home.SelectedArt+"_"+currentdate+"_";
	 }
	 else
	 {
		  name =Home.SelectedEasel+"_"+changedartno+"_"+currentdate+"_";
	 }
		
	
	return name;
}
@SuppressWarnings("unused")
public String getbidsfilename()
{
	String m="";
	String d="";
	String modifiedmonth="";
	String modifiedday="";
	Date date = new Date(); // your date
	Calendar cal = Calendar.getInstance();
	cal.setTime(date);
	int year = cal.get(Calendar.YEAR);
	int month = cal.get(Calendar.MONTH);
	int day = cal.get(Calendar.DAY_OF_MONTH);
	month=month+1;
	if(day<10)
	{
		modifiedday="0"+String.valueOf(day);
	}
	else if(day>=10){modifiedday=String.valueOf(day);}
	if(month<10)
	{
		modifiedmonth="0"+String.valueOf(month);
	}
	else if(month>=10){modifiedmonth=String.valueOf(month);}
	if(day<10)
	{
		d="0"+String.valueOf(day);
	}
	else
	{
		d=String.valueOf(day);
	}
	
	if(month<=9)
	{
		m=String.valueOf(month);
	}
	else if(month==10)
	{
		m="O";

	}
	else if(month==11)
	{
		m="N";

	}
	else if(month==12)
	{
		m="D";

	}	
	String currentdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
SharedPreferences settings = SubmissionApproval.this.getSharedPreferences(preferences.PREF_NAME,0);
	
	String	changedartno =settings.getString(preferences.ChangedArtno,"");
	String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
	String art="";
	String description="";
	String name="";
 if(changedartno.equals("")&&changedartdes.equals(""))
 {
	 name =Home.SelectedEasel+"_"+Home.SelectedArt+"_"+currentdate+"_"+filename;
 }
 else
 {
	 name =Home.SelectedEasel+"_"+changedartno+"_"+currentdate+"_"+filename;
 }
	//String name =Home.SelectedEasel+"_"+Home.SelectedArt+"_"+currentdate+"_"+filename;
	
	return name;
}
}
