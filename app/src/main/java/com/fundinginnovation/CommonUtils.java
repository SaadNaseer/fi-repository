package com.fundinginnovation;

import android.content.Context;
import android.media.AudioManager;

public class CommonUtils {
    public static boolean isCallActive(Context context) {
        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (manager.getMode() == AudioManager.MODE_IN_CALL) {
            return true;
        } else {
            return false;
        }
    }

    public static String checkPermissionName(String permission) {

        if (permission.equalsIgnoreCase("android.permission.READ_CONTACTS")) {
            return "Contacts";
        } else if (permission.equalsIgnoreCase("android.permission.ACCESS_FINE_LOCATION")) {
            return "Location";
        } else if (permission.equalsIgnoreCase("android.permission.SEND_SMS")) {
            return "SMS";
        } else if (permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")) {
            return "Storage";
        } else if (permission.equalsIgnoreCase("android.permission.CALL_PHONE")) {
            return "Telephone";
        }
        else if (permission.equalsIgnoreCase("android.permission.CAMERA")) {
            return "Camera";
        }

        return permission;
    }
}