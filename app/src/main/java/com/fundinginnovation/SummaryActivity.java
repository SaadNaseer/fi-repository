package com.fundinginnovation;

import com.Database.Databaseadapter;
import com.NavigationFragments.EaselConfirmation;
import com.NavigationFragments.Home;
import com.NavigationFragments.LookUpEasel;
import com.Utils.HttpPatch;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.navigationdrawer.NavigationActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SummaryActivity extends Activity{
TextView summarytext;
ImageView imagethumbnail;
String barcode=MainActivity.scanContent;
static public boolean summary=false;;
public static String SelectedArt = Home.SelectedArt;
public static String SelectedLocation = Home.SelectedLocation;
public static String SelectedCharity = Home.SelectedCharity;
public static String SelectedSponsor = Home.SelectedSponsor;
public static String SelectedEasel = Home.SelectedEasel;
public static String SelectedQuantity = Home.SelectedQuantity;
public static String SelectedStreetAddress = Home.SelectedStreetAddress;
public static String SelectedCity = Home.SelectedCity;
public static String SelectedProvince= Home.SelectedProvince;
public static String SelectedPostCode= Home.SelectedPostCode;
public static String SelectedArtDescription=Home.SelectedArtDescription;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.summaryactivity);
		new GetExistingAuctionID().execute();
		preferences pref=new preferences(getApplicationContext());
		pref.SetLastScreen("summaryactivity");
		summary=true;
		summarytext=(TextView)findViewById(R.id.summarytext);
		
		imagethumbnail=(ImageView)findViewById(R.id.imagethumbnail);
		if(barcode.equals(""))
		{
			barcode=Home.SelectedEasel;
		}
		String bids="";
		if(Questionaire.zerobids)
				{
			bids="0";
				}
		else
		{
			bids=Questionaire.Bids;
		}
		SharedPreferences settings = SummaryActivity.this.getSharedPreferences(preferences.PREF_NAME,0);
		
		String	changedartno =settings.getString(preferences.ChangedArtno,"");
		String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
	 if(changedartno.equals("")&&changedartdes.equals(""))
	 {
		 summarytext.setText("Easel   : "+barcode+"\n"+"Product Number: "+Home.SelectedArt+"\n"+"Charity   : "+Home.SelectedCharity+"\n"+"Location  : "+Home.SelectedSponsor+"\n"+""+"Total Bids: "+bids);}
	 else
	 {
		 summarytext.setText("Easel   : "+barcode+"\n"+"Product Number: "+changedartno+"\n"+"Charity   : "+Home.SelectedCharity+"\n"+"Location  : "+Home.SelectedSponsor+"\n"+""+"Total Bids: "+bids);
			
	 }
	 summarytext.setTypeface(Typeface.DEFAULT_BOLD);
		//summarytext.setText("Easel   : "+barcode+"\n"+"Product Number: "+Home.SelectedArt+"\n"+"Charity   : "+Home.SelectedCharity+"\n"+"Location  : "+Home.SelectedSponsor+"\n"+""+"Total Bids: "+bids);
		/*BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		Bitmap bitmap = BitmapFactory.decodeFile(TakePictures.filename+".jpg", options);
		imagethumbnail.setImageBitmap(bitmap);*/
		
		pref.SetLastUsedEasel(Home.SelectedEasel);
		if(Home.SelectedArt.equals("SelectedArt")||Home.SelectedEasel.equals("SelectedEasel")||Home.SelectedLocation.equals("SelectedLocation"))
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
					SummaryActivity.this);
			builder.setTitle("Error");
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setMessage("Unfortunately we have encountered an issue with the application. Please completely log out of the app and restart it");
	
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
						
						}
					});

			builder.create().show();
			return;
		}
		new ChangeProduct().execute();
	}
public void ok(View a)
{
/*	Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
	i.putExtra("val", "val");
	startActivity(i);
	finish();*/
	preferences p= new preferences(getApplicationContext());
	p.SetChangedartdes("");
	p.SetChangedartno("");
	if(Questionaire.zerobids&&!Questionaire.nouppicture)
	{
		 Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
	     i.putExtra("val", "val");
	     startActivity(i);
	}
	else if(Questionaire.zerobids&&Questionaire.nouppicture)
	{
		Questionaire.nouppicture=false;
	    	p.SetLastScreen("");
			p.Settotalbids("");
			Databaseadapter db=new Databaseadapter(getApplicationContext());
			db.DeleteRunningTransactions();  
	        Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
	        i.putExtra("val", "");
	    	startActivity(i);
	    	finish();
	}
	else
	{

	Intent intent=new Intent(getApplicationContext(),NewEaselConfirmationQuestion.class);
	startActivity(intent);
	}
	finish();
}
	public class GetExistingAuctionID extends AsyncTask<String,String,String> {
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;
		String itemnumber = "";
		String itemdesc = "";


		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(SummaryActivity.this);
			p.setMessage("Loading ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if (isSuccess) {
				Toast.makeText(SummaryActivity.this, r, Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(SummaryActivity.this, r, Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String auctionid="";
			try {
				HttpClient httpclient = new DefaultHttpClient();
				SharedPreferences settings = SummaryActivity.this.getSharedPreferences(preferences.PREF_NAME, 0);
				String se = Home.SelectedEaselID;
				String EaselsURL = SFAPI.baseUri + "/query/?q=" + URLEncoder.encode("SELECT Id FROM Auction__c Where Easel_Number__c =  '" + se + "'", "UTF-8");
				Log.e("URL ", EaselsURL);
				Log.e("Header ", SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(EaselsURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse response = null;
				response = httpclient.execute(httpPost);
				String auction = EntityUtils.toString(response.getEntity());
				JSONObject jobj=new JSONObject(auction);
				String totalsize=jobj.getString("totalSize");

				if(totalsize.equalsIgnoreCase("0"))
				{
					isSuccess=false;
					z="Auction Not Found";
				}
				else {
					JSONArray jarr = jobj.getJSONArray("records");
					for (int i = 0; i < jarr.length(); i++) {

						JSONObject eitems = jarr.getJSONObject(i);
						 auctionid = eitems.getString("Id");
					}
					String turnenddatesff="";
					String uri = SFAPI.baseUri + "/sobjects/Auction__c/"+auctionid+"?_HttpMethod=PATCH";
					Log.e("Url :",uri);
					try {
						try
						{
							Calendar calendar = Calendar.getInstance();
							Date newDate = calendar.getTime();
							DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
							String dateAsString = dateFormat.format(newDate);
							String arr[]=dateAsString.split("-");
							int dayn=Integer.parseInt(arr[0]);
							int monthn=Integer.parseInt(arr[1]);
							int yearn=Integer.parseInt(arr[2]);
							turnenddatesff=yearn+"-"+monthn+"-"+dayn;
						}
						catch(Exception a)
						{

						}
						//create the JSON object containing the new lead details.
						JSONObject auctionpatch = new JSONObject();
						//auction.put("Easel_Location__c", Home.SelectedLocation);
						auctionpatch.put("Status__c", "Closed");
						auctionpatch.put("Auction_End_Date__c", turnenddatesff);

						Log.e("","JSON for auction record to be updated:\n" + auctionpatch.toString(1));
						//Toast.makeText(LoginActivity.this,"JSON for lead record to be inserted:\n" + lead.toString(1),Toast.LENGTH_LONG).show();

						//Construct the objects needed for the request
						HttpClient httpClient = new DefaultHttpClient();

						HttpPatch httppatch = new HttpPatch(uri);
						httppatch.addHeader(SFAPI.oauthHeader);
						httppatch.addHeader(SFAPI.prettyPrintHeader);

						// The message we are going to post
						StringEntity body = new StringEntity(auctionpatch.toString(1));
						body.setContentType("application/json");
						httppatch.setEntity(body);

						//Make the request
						HttpResponse httppatchresponse = httpClient.execute(httppatch);

						//Process the results
						int httppatchstatusCode = httppatchresponse.getStatusLine().getStatusCode();
						Log.e("SC ",""+httppatchstatusCode);
						if (httppatchstatusCode == 204) {

							isSuccess=true;
							z="Auction Updated";
						}

						else {
							//Toast.makeText(LoginActivity.this,"Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity()),Toast.LENGTH_LONG).show();
							//System.out.println("Insertion unsuccessful. Status code returned is " + statusCode);
							z="Auction Update unsuccessful. Status response " + EntityUtils.toString(httppatchresponse.getEntity());
							isSuccess=false;
						}
					} catch (Exception e) {
						isSuccess=false;
						//System.out.println("Issue creating JSON or processing results");
						e.printStackTrace();
					}

				}
			}catch (Exception a){}
			return z;
		}
	}


	public class ChangeProduct extends AsyncTask<String, String, String> {
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;

		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(SummaryActivity.this);
			p.setMessage("Processing ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if (isSuccess) {

				Toast.makeText(SummaryActivity.this, r, Toast.LENGTH_SHORT).show();


			} else {
				Toast.makeText(SummaryActivity.this, r, Toast.LENGTH_SHORT).show();
			}

		}

		@SuppressWarnings("unused")
		@Override
		protected String doInBackground(String... params) {

			try {
			HttpClient httpClient = new DefaultHttpClient();
			SharedPreferences settings = SummaryActivity.this.getSharedPreferences(preferences.PREF_NAME,0);
			String conid=  	settings.getString(preferences.useridd,"");
			String initials=  	settings.getString(preferences.initials,"");
			String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
			String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+initialsdef;
			//Log.e("Locations Url ",locationsapurl);
				String tolocationid="";
			HttpGet locations = new HttpGet(locationsapurl);
			locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
			locations.addHeader(SFAPI.prettyPrintHeader);
			HttpResponse locationsresponse = httpClient.execute(locations);
			JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
			if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
				JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

				for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
					JSONObject items = locationpayloadarray.getJSONObject(lp);
					tolocationid = items.getString("id");

				}
			}

			String uri = SFAPI.advanceprourl + "/api/" + SFAPI.advanceproinventoryTransfer;
			Log.e("Url :", uri);


				//create the JSON object containing the new lead details.
				JSONObject auction = new JSONObject();
				auction.put("product_id", Integer.parseInt(Home.SelectedAPProductID));
				auction.put("variant_id", 0);
				auction.put("from_location_id", Integer.parseInt(Home.SelectedFROMLOCATIONID));
				auction.put("to_location_id", Integer.parseInt(tolocationid));
				auction.put("unassign_after_transfer", true);
				auction.put("quantity", 1);
				auction.put("reason", "Change Product");


				Log.e("", "JSON for CP record to be inserted:\n" + auction.toString(1));
				//Toast.makeText(LoginActivity.this,"JSON for lead record to be inserted:\n" + lead.toString(1),Toast.LENGTH_LONG).show();

				//Construct the objects needed for the request

				HttpPost httpPost = new HttpPost(uri);
				httpPost.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				httpPost.addHeader(SFAPI.prettyPrintHeader);
				// The message we are going to post
				StringEntity body = new StringEntity(auction.toString(1));
				body.setContentType("application/json");
				httpPost.setEntity(body);

				//Make the request
				HttpResponse response = httpClient.execute(httpPost);

				//Process the results
				int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode == 201) {

					String response_string = EntityUtils.toString(response.getEntity());
					Log.e(" Response :", response_string);
					isSuccess = true;
					z="Successful";
				} else {
					//Toast.makeText(LoginActivity.this,"Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity()),Toast.LENGTH_LONG).show();
					//System.out.println("Insertion unsuccessful. Status code returned is " + statusCode);
					z = "Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity());
					isSuccess = false;
				}

			} catch (Exception ex) {
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return z;

		}

}

}
