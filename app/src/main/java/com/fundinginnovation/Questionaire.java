package com.fundinginnovation;

import java.util.ArrayList;
import java.util.HashMap;

import com.Asynctask.GetEmails;
import com.Database.Databaseadapter;
import com.NavigationFragments.Home;
import com.Utils.EmailCredentials;
import com.Utils.GMailSender;
import com.Utils.preferences;
import com.listeners.RequestListener;
import com.navigationdrawer.NavigationActivity;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class Questionaire extends Fragment{
	int p;
	String questionid;
	//ArrayList<String> questionsids = new ArrayList< String>();
	CustomViewPager viewPager;
	private Activity activity;
	@SuppressWarnings("unused")
	private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.0F);
	String fontPath2 = "segoepr.ttf";
	String qidd;;
	int pos;
	boolean picturerepairable=false;
	static String qquesid = "qquesid";
	static String qquestext = "qquestext";
	public static String Bids="Bids";
	static String answer1="";
	static String answer2="";
	static String answer3="";
	static String answer4="";
	static String answer5="";
	static String reason="";
	public static boolean zerobids=true;
	public static boolean nouppicture=false;
	boolean refresh=false;
	ArrayList<String> qids = new ArrayList< String>();
	ArrayList<HashMap<String, String>> listing;
	View rootView;
	RequestListener requestlistener;
	@SuppressWarnings("deprecation")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 
		 rootView = inflater.inflate(R.layout.slide, container, false);
		requestlistener = new RequestListener() {
			@Override
			public void onSuccess(String emails,String emailmsg,String subject)
			{
				if(emails.contains(","))
				{
					String arr[] = emails.split(",");
					for(int i=0;i<arr.length;i++)
					{
						String emailaddr=arr[i];
						SendEmailNew(emailmsg,subject,emailaddr);
						Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					SendEmailNew(emailmsg,subject,emails);
					Toast.makeText(getActivity(),"Email has been sent",Toast.LENGTH_LONG).show();
				}
			}
			@Override
			public void onError(String result) {
				Toast.makeText(getActivity(),"Error occured in sending smail ",Toast.LENGTH_LONG).show();
			}
		};
		// setRetainInstance(true);
		 preferences pref=new preferences(getActivity());
			pref.SetLastScreen("questionaire");
		 listing = new ArrayList<HashMap<String, String>>();
			for (int i = 0; i <3; i++) {



				HashMap<String, String> map = new HashMap<String, String>();
				map.put(qquesid, String.valueOf(i));
				if(i==0)
				{
					map.put(qquestext, "Is the Product coming down in good condition ?");
				}
				
				if(i==1)
				{
					map.put(qquestext, "Please Check for bids in the box. Are there bids ?");
				}
				if(i==2)
				{
					map.put(qquestext, "Are you putting another picture up on the easel ?");
				}

				
				qids.add(String.valueOf(i));

				listing.add(map);
			}
			//Toast.makeText(getActivity(), String.valueOf(listing.size()), Toast.LENGTH_LONG).show();
			this.activity=getActivity();
			viewPager = (CustomViewPager)rootView.findViewById(R.id.view_pager);
			//pos=getCategoryPos(questionid);
			viewPager.setPagingEnabled(false);
			QuestionPagerAdapter adapter = new QuestionPagerAdapter(activity, qids,listing);
			viewPager.setAdapter(adapter);

			viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				@Override
				public void onPageScrolled(int i, float v, int i2) {
					//Log.w("","POS "+i);
				}
				@Override
				public void onPageSelected(int i) {
					//	pos=getCategoryPos(questionid);
					//Log.w("","POS "+i);
					p=i;  
					viewPager.getAdapter().notifyDataSetChanged();	                
				}
				@Override
				public void onPageScrollStateChanged(int i) {
					//Log.w("","POS "+i);
				}
			});
		 return rootView;
	}
	@SuppressWarnings("unused")
	private int getCategoryPos(String category) {
		return qids.indexOf(category);
	}
	private class QuestionPagerAdapter extends PagerAdapter {
		private ArrayList<String> qids;
		ArrayList<HashMap<String, String>> qlisting;
		private Activity _activity;
		String answer="";
		public QuestionPagerAdapter(Activity activity,
				ArrayList<String> ids, ArrayList<HashMap<String, String>> listing) {
			this._activity = activity;
			this.qids = ids;
			this.qlisting=listing;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return qlisting.size();
		}
		@Override
		public boolean isViewFromObject(View view, Object object) {
			// TODO Auto-generated method stub
			return view == ((RelativeLayout) object);
		}
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE; 
		}
		@SuppressWarnings("unused")
		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Typeface tf2 = Typeface.createFromAsset(_activity.getAssets(), fontPath2);
			LayoutInflater	inflater = (LayoutInflater) _activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View viewLayout = inflater.inflate(R.layout.questionlayout,
					container, false);
			qidd = qids.get(position);
			 ImageView imageButton = (ImageView) viewLayout
						.findViewById(R.id.menuicon);
				imageButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						/*if(!NavigationActivity.draweropened){*/
							NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
							NavigationActivity.draweropened=true;
						/*}
						else{
							NavigationActivity.mDrawerLayout.closeDrawer(NavigationActivity.mDrawerList);
							NavigationActivity.draweropened=false;
						}*/
					}
				});
				ImageView homeButton = (ImageView) viewLayout
						.findViewById(R.id.homeicon);
				homeButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getActivity());
						builder.setTitle("Cancel ?");
						builder.setIcon(android.R.drawable.ic_dialog_alert);
						builder.setMessage("Are you sure you wish to cancel the transaction ?");
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0,
									int arg1) {
								
							}
						});
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										 preferences p= new preferences(getActivity());
									    	p.SetLastScreen("");
											p.Settotalbids("");
											Databaseadapter db=new Databaseadapter(getActivity());
											db.DeleteRunningTransactions(); 
											getActivity().finishAffinity();
											Intent i= new Intent(getActivity(),NavigationActivity.class);
											i.putExtra("val", "");
											startActivity(i);
										FragmentManager fragmentManager = getFragmentManager();
										/*fragmentManager.beginTransaction()
										.replace(R.id.frame_container, new HomeNew()).commit();*/
										fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
									}
								});

						builder.create().show();
					}
				});
			HashMap<String, String> questions = new HashMap<String, String>();

			questions=qlisting.get(position);
			//	TextView qoutoftotal=(TextView)viewLayout.findViewById(R.id.qqout);


			//ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
			final 	RelativeLayout relativelayout=(RelativeLayout)viewLayout.findViewById(R.id.relativeLayout1);
			int a= viewPager.getCurrentItem();
			a++;
			//	qoutoftotal.setText("Question "+a+"/"+arraysize);
			/*final TextView qi=(TextView)viewLayout.findViewById(R.id.qiid);
			qi.setText(questionid);*/
			final 	TextView selectedtext=(TextView)viewLayout.findViewById(R.id.selectedtext);
SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
			
			String	changedartno =settings.getString(preferences.ChangedArtno,"");
			String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
		 if(changedartno.equals("")&&changedartdes.equals(""))
		 {
			 selectedtext.setText("Product Name : "+Home.SelectedArt+"\n"+"Easel Location : "+Home.SelectedSponsor+"\n"+"Charity : "+Home.SelectedCharity);		
		 }
		 else
		 {
			 selectedtext.setText("Product Name : "+changedartno+"\n"+"Easel Location : "+Home.SelectedSponsor+"\n"+"Charity : "+Home.SelectedCharity);		
				
		 }
			final 	TextView q=(TextView)viewLayout.findViewById(R.id.textView1);
			q.setSelected(true);
			q.setText(questions.get(Questionaire.qquestext));
			//	q.setTypeface(tf2);
			final EditText providedetails=(EditText)viewLayout.findViewById(R.id.providedetails);
			final RadioGroup radioyesno=(RadioGroup)viewLayout.findViewById(R.id.radioyesno);
			final RadioGroup radioreasons=(RadioGroup)viewLayout.findViewById(R.id.radioreasons);
			final 	Button next=(Button)viewLayout.findViewById(R.id.next);
			final 	Button gobutton=(Button)viewLayout.findViewById(R.id.go);
			final 	Button nextcancel=(Button)viewLayout.findViewById(R.id.cancelnext);
			final 	Button confirm=(Button)viewLayout.findViewById(R.id.confirmbtn);
			final 	Button cancel=(Button)viewLayout.findViewById(R.id.cancelbtn);
			final RelativeLayout radioyesnoa=(RelativeLayout)viewLayout.findViewById(R.id.radioyesnoa);
			final RadioButton radioYesa=(RadioButton)viewLayout.findViewById(R.id.radioYesa);
			final RadioButton radioNoa=(RadioButton)viewLayout.findViewById(R.id.radioNoa);
			radioYesa.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					picturerepairable=true;
					refresh=true;
					ObjectAnimator fadeOut = ObjectAnimator.ofFloat(radioYesa, "alpha",  1f, .3f);
					fadeOut.setDuration(300);
					ObjectAnimator fadeIn = ObjectAnimator.ofFloat(radioYesa, "alpha", .3f, 1f);
					fadeIn.setDuration(300);

					final AnimatorSet mAnimationSet = new AnimatorSet();

					mAnimationSet.play(fadeIn).after(fadeOut);

					mAnimationSet.addListener(new AnimatorListenerAdapter() {
					    @Override
					    public void onAnimationEnd(Animator animation) {
					        super.onAnimationEnd(animation);
					        radioYesa.setVisibility(View.INVISIBLE);
							radioNoa.setVisibility(View.INVISIBLE);
							radioyesnoa.setVisibility(View.INVISIBLE);
							nextcancel.setVisibility(View.VISIBLE);
							nextcancel.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) 
								{
									AlertDialog.Builder builder = new AlertDialog.Builder(
											getActivity());
									builder.setTitle("Cancel ?");
									builder.setIcon(android.R.drawable.ic_dialog_alert);
									builder.setMessage("Are you sure you wish to cancel the easel entry ?");
									builder.setNegativeButton("No", null);
									builder.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(DialogInterface arg0,
														int arg1) {
													NavigationActivity.startturn=false;
													 preferences p= new preferences(getActivity());
												    	p.SetLastScreen("");
														p.Settotalbids("");
														Databaseadapter db=new Databaseadapter(getActivity());
														db.DeleteRunningTransactions(); 
														getActivity().finishAffinity();
														Intent i= new Intent(getActivity(),NavigationActivity.class);
														i.putExtra("val", "");
														startActivity(i);
													FragmentManager fragmentManager = getFragmentManager();
													/*fragmentManager.beginTransaction()
													.replace(R.id.frame_container, new HomeNew()).commit();*/
													fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
												}
											});

									builder.create().show();
								}
							});
							q.setText("Please provide detail");
							if(getscreensize()>=7)
							{
								final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
								int pixelswidth = (int) (460 * scale + 0.5f);
								int pixelsheight = (int) (360 * scale + 0.5f);
								ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
								layoutParams.width = pixelswidth;
								layoutParams.height = pixelsheight;
								relativelayout.setLayoutParams(layoutParams);
							}
							else
							{
							final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
							int pixelswidth = (int) (280 * scale + 0.5f);
							int pixelsheight = (int) (210 * scale + 0.5f);
							ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
							layoutParams.width = pixelswidth;
							layoutParams.height = pixelsheight;
							relativelayout.setLayoutParams(layoutParams);
							}
							radioyesno.setVisibility(View.INVISIBLE);
							radioreasons.setVisibility(View.INVISIBLE);
							providedetails.setVisibility(View.VISIBLE);
							next.setText("Submit");
							next.setVisibility(View.VISIBLE);
							
							next.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) 
								{
									String subject="";
									if(picturerepairable){
									 subject="Picture is Repairable";}
									else
									{
										 subject="Picture is Not Repairable";
									}
									//Send Email and move to bids section
								//	Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();
									SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
									
									String	changedartno =settings.getString(preferences.ChangedArtno,"");
									String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
									String values="";
								 if(changedartno.equals("")&&changedartdes.equals(""))
								 {
									 values="Easel Location #   : "+Home.SelectedEasel+"\nEasel Location Name   : "+Home.SelectedSponsor+"\nCharity   : "+Home.SelectedCharity.trim().toString()+"\nAddress   : "+Home.SelectedStreetAddress+" "+Home.SelectedCity+" "+Home.SelectedProvince+" "+Home.SelectedPostCode+"\nProduct #   : "+Home.SelectedArt+"\nProduct Description   : "+Home.SelectedArtDescription+"\nDetails   : "+providedetails.getText().toString();
								 }
								 else
								 {
									 values="Easel Location #   : "+Home.SelectedEasel+"\nEasel Location Name   : "+Home.SelectedSponsor+"\nCharity   : "+Home.SelectedCharity.trim().toString()+"\nAddress   : "+Home.SelectedStreetAddress+" "+Home.SelectedCity+" "+Home.SelectedProvince+" "+Home.SelectedPostCode+"\nProduct #   : "+changedartno+"\nProduct Description   : "+changedartdes+"\nDetails   : "+providedetails.getText().toString();
									 
								 }

									//SendEmail(values,subject);

									GetEmails webservice = new GetEmails(getActivity(), requestlistener,"picturerepairable", values, subject);
									webservice.execute();

									movetonext();
								}

								
							});
				   }
			});
			mAnimationSet.start();
					
				}

				
			});
			radioNoa.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					refresh=true;
					picturerepairable=false;
					ObjectAnimator fadeOut = ObjectAnimator.ofFloat(radioNoa, "alpha",  1f, .3f);
					fadeOut.setDuration(300);
					ObjectAnimator fadeIn = ObjectAnimator.ofFloat(radioNoa, "alpha", .3f, 1f);
					fadeIn.setDuration(300);

					final AnimatorSet mAnimationSet = new AnimatorSet();

					mAnimationSet.play(fadeIn).after(fadeOut);

					mAnimationSet.addListener(new AnimatorListenerAdapter() {
					    @Override
					    public void onAnimationEnd(Animator animation) {
					        super.onAnimationEnd(animation);
					        radioYesa.setVisibility(View.INVISIBLE);
							radioNoa.setVisibility(View.INVISIBLE);
							radioyesnoa.setVisibility(View.INVISIBLE);
							nextcancel.setVisibility(View.VISIBLE);
							nextcancel.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) 
								{
									AlertDialog.Builder builder = new AlertDialog.Builder(
											getActivity());
									builder.setTitle("Cancel ?");
									builder.setIcon(android.R.drawable.ic_dialog_alert);
									builder.setMessage("Are you sure you wish to cancel the easel entry ?");
									builder.setNegativeButton("No", null);
									builder.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(DialogInterface arg0,
														int arg1) {
													NavigationActivity.startturn=false;
													 preferences p= new preferences(getActivity());
												    	p.SetLastScreen("");
														p.Settotalbids("");
														Databaseadapter db=new Databaseadapter(getActivity());
														db.DeleteRunningTransactions(); 
														getActivity().finishAffinity();
														Intent i= new Intent(getActivity(),NavigationActivity.class);
														i.putExtra("val", "");
														startActivity(i);
													FragmentManager fragmentManager = getFragmentManager();
													/*fragmentManager.beginTransaction()
													.replace(R.id.frame_container, new HomeNew()).commit();*/
													fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
													}
											});

									builder.create().show();
								}
							});
							q.setText("Please provide detail");
							if(getscreensize()>=7)
							{
								final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
								int pixelswidth = (int) (460 * scale + 0.5f);
								int pixelsheight = (int) (360 * scale + 0.5f);
								ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
								layoutParams.width = pixelswidth;
								layoutParams.height = pixelsheight;
								relativelayout.setLayoutParams(layoutParams);
							}
							else
							{
							final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
							int pixelswidth = (int) (280 * scale + 0.5f);
							int pixelsheight = (int) (210 * scale + 0.5f);
							ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
							layoutParams.width = pixelswidth;
							layoutParams.height = pixelsheight;
							relativelayout.setLayoutParams(layoutParams);
							}
							radioyesno.setVisibility(View.INVISIBLE);
							radioreasons.setVisibility(View.INVISIBLE);
							providedetails.setVisibility(View.VISIBLE);
							next.setText("Submit");
							next.setVisibility(View.VISIBLE);
							
							next.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) 
								{
									String subject="";
									if(picturerepairable){
									 subject="Picture is Repairable";}
									else
									{
										 subject="Picture is Not Repairable";
									}
									//Send Email and move to bids section
									//Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();
SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
									
									String	changedartno =settings.getString(preferences.ChangedArtno,"");
									String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
									String values="";
								 if(changedartno.equals("")&&changedartdes.equals(""))
								 {
									 values="Easel Location #   : "+Home.SelectedEasel+"\nEasel Location Name   : "+Home.SelectedSponsor+"\nCharity   : "+Home.SelectedCharity.trim().toString()+"\nAddress   : "+Home.SelectedStreetAddress+" "+Home.SelectedCity+" "+Home.SelectedProvince+" "+Home.SelectedPostCode+"\nProduct #   : "+Home.SelectedArt+"\nProduct Description   : "+Home.SelectedArtDescription+"\nDetails   : "+providedetails.getText().toString();
								 }
								 else
								 {
									 values="Easel Location #   : "+Home.SelectedEasel+"\nEasel Location Name   : "+Home.SelectedSponsor+"\nCharity   : "+Home.SelectedCharity.trim().toString()+"\nAddress   : "+Home.SelectedStreetAddress+" "+Home.SelectedCity+" "+Home.SelectedProvince+" "+Home.SelectedPostCode+"\nProduct #   : "+changedartno+"\nProduct Description   : "+changedartdes+"\nDetails   : "+providedetails.getText().toString();
									  
								 }
									//SendEmail(values,subject);
									GetEmails webservice = new GetEmails(getActivity(), requestlistener,"picturerepairable", values, subject);
									webservice.execute();
									movetonext();
								}

								
							});
				   }
			});
			mAnimationSet.start();
					
				}

				
			});
			if(q.getText().toString().equals("Is the Product coming down in good condition ?")||q.getText().toString().equals("Please Check for bids in the box. Are there bids ?"))
			{
				
				nextcancel.setVisibility(View.VISIBLE);
				nextcancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) 
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getActivity());
						builder.setTitle("Cancel ?");
						builder.setIcon(android.R.drawable.ic_dialog_alert);
						builder.setMessage("Are you sure you wish to cancel the easel entry ?");
						builder.setNegativeButton("No", null);
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										NavigationActivity.startturn=false;
										 preferences p= new preferences(getActivity());
									    	p.SetLastScreen("");
											p.Settotalbids("");
											Databaseadapter db=new Databaseadapter(getActivity());
											db.DeleteRunningTransactions(); 
											getActivity().finishAffinity();
											Intent i= new Intent(getActivity(),NavigationActivity.class);
											i.putExtra("val", "");
											startActivity(i);
										FragmentManager fragmentManager = getFragmentManager();
										/*fragmentManager.beginTransaction()
										.replace(R.id.frame_container, new HomeNew()).commit();*/
										fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
									}
								});

						builder.create().show();
					}
				});}
			if(q.getText().toString().equals("Please Confirm if this is the correct location based on the scan ?")||q.getText().toString().equals("Please Confirm if this is the correct Charity based on the scan ?")||q.getText().toString().equals("Please Confirm if this is the correct Product coming down (Auction end) based on the scan ?"))
			{
				radioyesno.setVisibility(View.INVISIBLE);

				confirm.setVisibility(View.VISIBLE);
				cancel.setVisibility(View.VISIBLE);
				
				if(getscreensize()>=7)
				{
					final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
					int pixelswidth = (int) (460 * scale + 0.5f);
					int pixelsheight = (int) (200 * scale + 0.5f);
					ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
					layoutParams.width = pixelswidth;
					layoutParams.height = pixelsheight;
					relativelayout.setLayoutParams(layoutParams);
				}
				else{
				final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
				int pixelswidth = (int) (280 * scale + 0.5f);
				int pixelsheight = (int) (100 * scale + 0.5f);
				ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
				layoutParams.width = pixelswidth;
				layoutParams.height = pixelsheight;
				relativelayout.setLayoutParams(layoutParams);}
				confirm.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) 
					{
						movetonext();
					}
				});
				cancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) 
					{
						

				
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getActivity());
						builder.setTitle("Cancel ?");
						builder.setIcon(android.R.drawable.ic_dialog_alert);
						builder.setMessage("Are you sure you wish to cancel the easel entry ?");
						builder.setNegativeButton("No", null);
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										NavigationActivity.startturn=false;
										 preferences p= new preferences(getActivity());
									    	p.SetLastScreen("");
											p.Settotalbids("");
											Databaseadapter db=new Databaseadapter(getActivity());
											db.DeleteRunningTransactions(); 
											getActivity().finishAffinity();
											Intent i= new Intent(getActivity(),NavigationActivity.class);
											i.putExtra("val", "");
											startActivity(i);
										FragmentManager fragmentManager = getFragmentManager();
									/*	fragmentManager.beginTransaction()
										.replace(R.id.frame_container, new HomeNew()).commit();*/
										fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
									}
								});

						builder.create().show();
					}
				});
			}
			else
			{
				if(getscreensize()>=7)
				{
					final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
					int pixelswidth = (int) (460 * scale + 0.5f);
					int pixelsheight = (int) (360 * scale + 0.5f);
					ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
					layoutParams.width = pixelswidth;
					layoutParams.height = pixelsheight;
					relativelayout.setLayoutParams(layoutParams);
				}
				else{
				final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
				int pixelswidth = (int) (280 * scale + 0.5f);
				int pixelsheight = (int) (210 * scale + 0.5f);
				ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
				layoutParams.width = pixelswidth;
				layoutParams.height = pixelsheight;
				relativelayout.setLayoutParams(layoutParams);}
				radioyesno.setVisibility(View.VISIBLE);
				confirm.setVisibility(View.INVISIBLE);
				cancel.setVisibility(View.INVISIBLE);
			}


			radioyesno.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					int id=group.getCheckedRadioButtonId();
					final RadioButton rb=(RadioButton) rootView.findViewById(id);
					ObjectAnimator fadeOut = ObjectAnimator.ofFloat(rb, "alpha",  1f, .3f);
					fadeOut.setDuration(300);
					ObjectAnimator fadeIn = ObjectAnimator.ofFloat(rb, "alpha", .3f, 1f);
					fadeIn.setDuration(300);

					final AnimatorSet mAnimationSet = new AnimatorSet();

					mAnimationSet.play(fadeIn).after(fadeOut);

					mAnimationSet.addListener(new AnimatorListenerAdapter() {
					    @Override
					    public void onAnimationEnd(Animator animation) {
					        super.onAnimationEnd(animation);
					        rb.setChecked(true);
							answer=rb.getText().toString();
							//	Toast.makeText(getActivity(), answer, Toast.LENGTH_LONG).show();

							if(q.getText().toString().equals("Is the Product coming down in good condition ?")&&answer.equals("No")&&radioyesno.getVisibility()==View.VISIBLE)
							{
								nextcancel.setVisibility(View.VISIBLE);
								nextcancel.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) 
									{
										AlertDialog.Builder builder = new AlertDialog.Builder(
												getActivity());
										builder.setTitle("Cancel ?");
										builder.setIcon(android.R.drawable.ic_dialog_alert);
										builder.setMessage("Are you sure you wish to cancel the easel entry ?");
										builder.setNegativeButton("No", null);
										builder.setPositiveButton("Yes",
												new DialogInterface.OnClickListener() {

													@Override
													public void onClick(DialogInterface arg0,
															int arg1) {
														NavigationActivity.startturn=false;
														 preferences p= new preferences(getActivity());
													    	p.SetLastScreen("");
															p.Settotalbids("");
															Databaseadapter db=new Databaseadapter(getActivity());
															db.DeleteRunningTransactions(); 
															getActivity().finishAffinity();
															Intent i= new Intent(getActivity(),NavigationActivity.class);
															i.putExtra("val", "");
															startActivity(i);
														FragmentManager fragmentManager = getFragmentManager();
														/*fragmentManager.beginTransaction()
														.replace(R.id.frame_container, new HomeNew()).commit();*/
														fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
													}
												});

										builder.create().show();
									}
								});
								q.setText("Is this picture repairable ?");
								refresh=true;
								if(getscreensize()>=7)
								{
									final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
									int pixelswidth = (int) (460 * scale + 0.5f);
									int pixelsheight = (int) (360 * scale + 0.5f);
									ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
									layoutParams.width = pixelswidth;
									layoutParams.height = pixelsheight;
									relativelayout.setLayoutParams(layoutParams);
								}
								else
								{
								final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
								int pixelswidth = (int) (280 * scale + 0.5f);
								int pixelsheight = (int) (210 * scale + 0.5f);
								ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
								layoutParams.width = pixelswidth;
								layoutParams.height = pixelsheight;
								relativelayout.setLayoutParams(layoutParams);
								}
								radioyesno.setVisibility(View.GONE);
								radioreasons.setVisibility(View.GONE);
								radioyesnoa.setVisibility(View.VISIBLE);
								radioYesa.setVisibility(View.VISIBLE);
								radioNoa.setVisibility(View.VISIBLE);
								
								//providedetails.setVisibility(View.VISIBLE);
								//next.setText("Submit");
								//next.setVisibility(View.VISIBLE);
								
								next.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) 
									{
										//Send Email and move to bids section
										//SendEmail(providedetails.getText().toString());
										movetonext();
									}

									
								});
								

							}
							/*else if(q.getText().toString().equals("Is this picture repairable ?"))
							{
					
								

							}*/
							else	if(q.getText().toString().equals("Please Check for bids in the box. Are there bids ?")&&answer.equals("Yes")&&radioyesno.getVisibility()==View.VISIBLE)
							{
								radioYesa.setVisibility(View.INVISIBLE);
								radioNoa.setVisibility(View.INVISIBLE);
								radioyesnoa.setVisibility(View.INVISIBLE);
								radioyesno.setVisibility(View.GONE);

								providedetails.setVisibility(View.VISIBLE);
								next.setVisibility(View.VISIBLE);
								nextcancel.setVisibility(View.VISIBLE);
								next.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) 
									{
										if(providedetails.getText().toString().equals("0"))
										{
											
											/*FragmentManager fragmentManagerr = getFragmentManager();
											fragmentManagerr.beginTransaction()
											.replace(R.id.frame_container, new NewEasel()).commit();
											Questionaire.zerobids=true;*/
											providedetails.setError("Bids should be greater than 0");
											return;
										}
										Questionaire.zerobids=false;
										preferences pref=new preferences(getActivity());
										pref.Settotalbids(providedetails.getText().toString());
										pref.SetLastScreen("takepictures");
										Intent i= new Intent(getActivity(),TakePictures.class);
										i.putExtra("total", providedetails.getText().toString());
										Bids=providedetails.getText().toString();
										startActivity(i);
									}
								});
								nextcancel.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) 
									{
										AlertDialog.Builder builder = new AlertDialog.Builder(
												getActivity());
										builder.setTitle("Cancel ?");
										builder.setIcon(android.R.drawable.ic_dialog_alert);
										builder.setMessage("Are you sure you wish to cancel the easel entry ?");
										builder.setNegativeButton("No", null);
										builder.setPositiveButton("Yes",
												new DialogInterface.OnClickListener() {

													@Override
													public void onClick(DialogInterface arg0,
															int arg1) {
														NavigationActivity.startturn=false;
														 preferences p= new preferences(getActivity());
													    	p.SetLastScreen("");
															p.Settotalbids("");
															Databaseadapter db=new Databaseadapter(getActivity());
															db.DeleteRunningTransactions(); 
															getActivity().finishAffinity();
															Intent i= new Intent(getActivity(),NavigationActivity.class);
															i.putExtra("val", "");
															startActivity(i);
														FragmentManager fragmentManager = getFragmentManager();
														/*fragmentManager.beginTransaction()
														.replace(R.id.frame_container, new HomeNew()).commit();*/
														fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
													}
												});

										builder.create().show();
									}
								});
								providedetails.setHint("How Many ?");
								providedetails.setInputType(InputType.TYPE_CLASS_NUMBER);
								
							}
							else if(q.getText().toString().equals("Are you putting another picture up on the easel ?"))
							{
								if(answer.equals("No"))
								{
									providedetails.setVisibility(View.INVISIBLE);
									radioyesno.setVisibility(View.INVISIBLE);
									radioYesa.setVisibility(View.INVISIBLE);
									radioNoa.setVisibility(View.INVISIBLE);
									radioyesnoa.setVisibility(View.INVISIBLE);
									radioreasons.setVisibility(View.VISIBLE);
									q.setText("Reason ?");	
									if(getscreensize()>=7)
									{
										final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
										int pixelswidth = (int) (460 * scale + 0.5f);
										int pixelsheight = (int) (360 * scale + 0.5f);
										ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
										layoutParams.width = pixelswidth;
										layoutParams.height = pixelsheight;
										relativelayout.setLayoutParams(layoutParams);
									}
									else
									{
									final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
									int pixelswidth = (int) (280 * scale + 0.5f);
									int pixelsheight = (int) (210 * scale + 0.5f);
									ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
									layoutParams.width = pixelswidth;
									layoutParams.height = pixelsheight;
									relativelayout.setLayoutParams(layoutParams);
									}
								
									radioreasons.setOnCheckedChangeListener(new OnCheckedChangeListener() {

										@Override
										public void onCheckedChanged(RadioGroup group, int checkedId) {
											int id=group.getCheckedRadioButtonId();
											RadioButton rb=(RadioButton)rootView.findViewById(id);
											rb.setChecked(true);
											final String reason=rb.getText().toString();

										
												providedetails.setVisibility(View.VISIBLE);
												q.setText("Please Provide Detail");
												next.setVisibility(View.VISIBLE);
												next.setText("Submit");
												nextcancel.setVisibility(View.VISIBLE);
												radioyesno.setVisibility(View.INVISIBLE);
												radioreasons.setVisibility(View.INVISIBLE);
												radioYesa.setVisibility(View.INVISIBLE);
												radioNoa.setVisibility(View.INVISIBLE);
												radioyesnoa.setVisibility(View.INVISIBLE);
												if(getscreensize()>=7)
												{
													final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
													int pixelswidth = (int) (460 * scale + 0.5f);
													int pixelsheight = (int) (360 * scale + 0.5f);
													ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
													layoutParams.width = pixelswidth;
													layoutParams.height = pixelsheight;
													relativelayout.setLayoutParams(layoutParams);
												}
												else
												{
												final float scale = getActivity().getBaseContext().getResources().getDisplayMetrics().density;
												int pixelswidth = (int) (280 * scale + 0.5f);
												int pixelsheight = (int) (210 * scale + 0.5f);
												ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
												layoutParams.width = pixelswidth;
												layoutParams.height = pixelsheight;
												relativelayout.setLayoutParams(layoutParams);
												}
												next.setOnClickListener(new OnClickListener() {
													@Override
													public void onClick(View v) 
													{
														//Toast.makeText(getActivity(), "Email Generated", Toast.LENGTH_LONG).show();
														SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
														
														String	changedartno =settings.getString(preferences.ChangedArtno,"");
														String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
														String values="";
													 if(changedartno.equals("")&&changedartdes.equals(""))
													 {
														values="Easel Location #   : "+Home.SelectedEasel+"\nEasel Location Name   : "+Home.SelectedSponsor+"\nCharity   : "+Home.SelectedCharity.trim().toString()+"\nAddress   : "+Home.SelectedStreetAddress+" "+Home.SelectedCity+" "+Home.SelectedProvince+" "+Home.SelectedPostCode+"\nProduct #   : "+Home.SelectedArt+"\nProduct Description   : "+Home.SelectedArtDescription+"\nDetails   : "+providedetails.getText().toString();
													 }
													 else
													 {
														 values="Easel Location #   : "+Home.SelectedEasel+"\nEasel Location Name   : "+Home.SelectedSponsor+"\nCharity   : "+Home.SelectedCharity.trim().toString()+"\nAddress   : "+Home.SelectedStreetAddress+" "+Home.SelectedCity+" "+Home.SelectedProvince+" "+Home.SelectedPostCode+"\nProduct #   : "+changedartno+"\nProduct Description   : "+changedartdes+"\nDetails   : "+providedetails.getText().toString();
														  
													 }

														GetEmails webservice = new GetEmails(getActivity(), requestlistener,"easelremovalreason", values, reason);
														webservice.execute();
														//SendEmail(values, reason);
														AlertDialog.Builder builder = new AlertDialog.Builder(
																getActivity());
														builder.setTitle("Email Generated");
														builder.setIcon(android.R.drawable.ic_dialog_email);
														builder.setMessage("Thank You for your Input");
														builder.setCancelable(false);
														builder.setPositiveButton("OK",
																new DialogInterface.OnClickListener() {

																	@Override
																	public void onClick(DialogInterface arg0,
																			int arg1) {
																		/*NavigationActivity.startturn=false;
																		FragmentManager fragmentManager = getFragmentManager();
																		fragmentManager.beginTransaction()
																		.replace(R.id.frame_container, new HomeNew()).commit();*/
																		
																		Intent i = new Intent(getActivity(),SubmissionApproval.class);
															    		 startActivity(i);
																		Questionaire.zerobids=true;
																		Questionaire.nouppicture=true;
																		
																	}
																});

														builder.create().show();
													}
												});
												nextcancel.setOnClickListener(new OnClickListener() {
													@Override
													public void onClick(View v) 
													{
														AlertDialog.Builder builder = new AlertDialog.Builder(
																getActivity());
														builder.setTitle("Cancel ?");
														builder.setIcon(android.R.drawable.ic_dialog_alert);
														builder.setMessage("Are you sure you wish to cancel the easel entry ?");
														builder.setNegativeButton("No", null);
														builder.setPositiveButton("Yes",
																new DialogInterface.OnClickListener() {

																	@Override
																	public void onClick(DialogInterface arg0,
																			int arg1) {
																		NavigationActivity.startturn=false;
																		 preferences p= new preferences(getActivity());
																	    	p.SetLastScreen("");
																			p.Settotalbids("");
																			Databaseadapter db=new Databaseadapter(getActivity());
																			db.DeleteRunningTransactions(); 
																			getActivity().finishAffinity();
																			Intent i= new Intent(getActivity(),NavigationActivity.class);
																			i.putExtra("val", "");
																			startActivity(i);
																		FragmentManager fragmentManager = getFragmentManager();
																		/*fragmentManager.beginTransaction()
																		.replace(R.id.frame_container, new HomeNew()).commit();*/
																		fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
																	}
																});

														builder.create().show();
													}
												});		
							}
							});
								}
								else if(answer.equals("Yes"))
								{
									
								/*	FragmentManager fragmentManagerr = getFragmentManager();
									fragmentManagerr.beginTransaction()
									.replace(R.id.frame_container, new NewEasel()).commit();*/
									 Intent i = new Intent(getActivity(),SubmissionApproval.class);
						    		 startActivity(i);
									Questionaire.zerobids=true;
								}
						   
							}
							else if(q.getText().toString().equals("Please Check for bids in the box. Are there bids ?")&&answer.equals("No"))
							{
								
								Bids="1";
								movetonext();
								radioYesa.setVisibility(View.INVISIBLE);
								radioNoa.setVisibility(View.INVISIBLE);
								radioyesnoa.setVisibility(View.INVISIBLE);
								radioyesno.setVisibility(View.VISIBLE);
								providedetails.setVisibility(View.GONE);
								
								
							}
							else{
								radioYesa.setVisibility(View.INVISIBLE);
								radioNoa.setVisibility(View.INVISIBLE);
								radioyesnoa.setVisibility(View.INVISIBLE);
								radioyesno.setVisibility(View.VISIBLE);
								providedetails.setVisibility(View.GONE);
								next.setVisibility(View.GONE);
								nextcancel.setVisibility(View.GONE);
								movetonext();
								//answer="";
							}
					    }
					});
					mAnimationSet.start();
					
				}
			});

			((ViewPager) container).addView(viewLayout);
			return viewLayout;}
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((RelativeLayout) object); 	    }
	}

	public void movetonext()
	{
		int aa= viewPager.getCurrentItem();
		aa++;
		viewPager.setCurrentItem(aa);

	}
	public void movetprevious()
	{
		int aa= viewPager.getCurrentItem();
		aa--;
		viewPager.setCurrentItem(aa);

	}
	@SuppressWarnings("unused")
	public double getscreensize()
	{
		DisplayMetrics metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		float scaleFactor = metrics.density;
		float widthDp = widthPixels / scaleFactor;
				float heightDp = heightPixels / scaleFactor;
				
				float widthDpi = metrics.xdpi;
				float heightDpi = metrics.ydpi;
				float widthInches = widthPixels / widthDpi;
				float heightInches = heightPixels / heightDpi;
				double diagonalInches = Math.sqrt(
					    (widthInches * widthInches) 
					    + (heightInches * heightInches));
				
				if (diagonalInches >= 10) {
				    //Device is a 10" tablet
				} 
				else if (diagonalInches >= 7) {
				    //Device is a 7" tablet
				}
		
		return diagonalInches;
	}
	private void SendEmailNew(final String msg, final String subject,final String recepient) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, recepient);
				} catch (Exception e) {
					Log.e("SendMail", e.getMessage(), e);
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}

		}).start();
	}
	private void SendEmail(final String msg, final String subject) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
			 
			  @Override
			  public void run() {
			    try {
			    	GMailSender sender = new GMailSender(EmailCredentials.EMAIL_USER,
							EmailCredentials.EMAIL_PASS);

					sender.sendMail(/*"Test "+*/subject, msg,
							EmailCredentials.EMAIL_USER, EmailCredentials.EMAIL_RECEPIENT);
			    } catch (Exception e) {
			      Log.e("SendMail", e.getMessage(), e);
			      Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
			  }
			}
			 
			}).start();
	}
	
/*	else if(q.getText().toString().equals("Are you putting another picture up on the easel ?"))
	{
		Toast.makeText(getActivity(), answer, Toast.LENGTH_LONG).show();
		
	}
	else if(q.getText().toString().equals("Reason ?"))
	{
		
	*/
	@Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       // setOnBackPressListener();
        
    }
    private void setOnBackPressListener() {
    	 
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	boolean move=false;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                	Log.e("BACKKKKKK", "BACKKKKKKKK");
                	if(!move){
                	movetprevious();
                	move=true;
                	}
                	if(refresh)
                	{
                		refresh=false;
                		FragmentTransaction ft = getFragmentManager().beginTransaction();
                		ft.detach(Questionaire.this).attach(Questionaire.this).commit();
                	}
                	
							
                }
                return false;
            }
        });
    }
}
