package com.fundinginnovation;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;
import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.Database.Databaseadapter;
import com.GetterSetters.CharitiesGetterSetter;
import com.GetterSetters.ProductsGetterSetter;
import com.Utils.ConnectionClass;
import com.Utils.FTPCredentials;
import com.Utils.Products;
import com.Utils.SFAPI;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class RefreshService extends IntentService {
	static String charityname = "charityname";
	static String charityid = "charityid";
	static String charitylogo = "charitylogo";
	static String charityaddress = "charityaddress";
	static String charitycity = "charitycity";
	static String charityprovince = "charityprovince";
	static String charitypostcode = "charitypostcode";
	static String charitytelephone = "charitytelephone";
	static String charitycontactname = "charitycontactname";
	static String charitycontactphone = "charitycontactphone";
	static String charitycontactcell = "charitycontactcell";
	static String charitycontactemail = "charitycontactemail";
	static String itemno = "itemno";
	static String itemdescription = "itemdescription";
	ConnectionClass connectionClass;
	Databaseadapter db;
	Boolean isSuccess = false;
	String errormessage="";
	int filecount=1;
	static String NAME = "NAME";
	boolean refresh=false;
	String loadingmessage="";
	ArrayList<HashMap<String, String>> listing;
    public RefreshService() {
        super("RefreshService");
     }
     @Override
     protected void onHandleIntent(Intent intent) {
         Log.d("MyService", "About to execute MyTask");
         listing = new ArrayList<HashMap<String, String>>();
		 GetCharities getcharities = new GetCharities(); 
		 getcharities.execute("");
         
     }
     public class GetCharities extends AsyncTask<String,String,String>
 	{
 		String z = "";
 		Boolean isSuccesss = false;
 		//ProgressDialog p;
 		String name = "";
 		String id = "";
 		String address = "";
 		String city = "";
 		String province = "";
 		String postcode = "";
 		String logo = "";
 		String telephone = "";
 		String cname = "";
 		String cphone = "";
 		String ccell = "";
 		String cemail = "";
 		


 		@Override
 		protected void onPreExecute() {
 			//p=new ProgressDialog(getActivity());
 			//p.setMessage("Refreshing Charity Directory ...");
 			//p.setCancelable(false);
 			//p.show();
 		}

 		@Override
 		protected void onPostExecute(String r) {
 			//pbbar.setVisibility(View.GONE);
 			//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
 			//p.dismiss();
 			if(isSuccesss) {
 				
 				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
 			
 				refresh=true;
 				loadingmessage="Refreshing Support Materials ...";
 				
 				
 			}
 			else
 			{
 				//Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
 			}
 			GetProducts getpro = new GetProducts(); 
				getpro.execute("");
 		}

 		@Override
 		protected String doInBackground(String... params) {

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Assemble the login request URL
				String AccountURL = SFAPI.baseUri +"/sobjects"+SFAPI.Account
						;
				Log.e("URL ",AccountURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(AccountURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;
				try {
					// Execute the login POST request
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

				}
				// verify response is HTTP OK
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					//System.out.println("Error authenticating to Force.com: "+statusCode);
					//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;

				}
				else
				{
					isSuccess=true;
					z ="Success";
					String getResult = null;
					try {
						getResult = EntityUtils.toString(response.getEntity());
						//Log.e("RESULT ",getResult);
						JSONObject jobj=new JSONObject(getResult);
						JSONArray jarr=jobj.getJSONArray("recentItems");
						for(int i=0;i<jarr.length();i++)
						{
							JSONObject eitems=jarr.getJSONObject(i);
							String id=eitems.getString("Id");
							String Name=eitems.getString("Name");

							HttpClient httpclient2 = new DefaultHttpClient();
							// Assemble the login request URL
							String AccountURL2 = SFAPI.baseUri +"/sobjects"+SFAPI.Account+"/"+id
									;
							Log.e("URL ",AccountURL2);

							// Login requests must be POSTs
							HttpGet httpPost2 = new HttpGet(AccountURL2);
							httpPost2.addHeader(SFAPI.oauthHeader);
							httpPost2.addHeader(SFAPI.prettyPrintHeader);


							HttpResponse response2 = null;
							try {
								// Execute the login POST request
								response2 = httpclient2.execute(httpPost2);
							} catch (ClientProtocolException cpException) {
								cpException.printStackTrace();

							} catch (IOException ioException) {
								ioException.printStackTrace();

							}
							// verify response is HTTP OK
							final int statusCode2 = response2.getStatusLine().getStatusCode();
							if (statusCode2 != HttpStatus.SC_OK) {

							}
							else {
								JSONObject jsonObjectt=new JSONObject(EntityUtils.toString(response2.getEntity()));

								HashMap<String, String> map = new HashMap<String, String>();
								map.put(charityid, jsonObjectt.getString("Id"));
								id=jsonObjectt.getString("Id");
								map.put(charityname, jsonObjectt.getString("Name"));
								name=jsonObjectt.getString("Name");
								address=jsonObjectt.getString("BillingStreet");
								map.put(charitycity, jsonObjectt.getString("BillingCity"));
								city=jsonObjectt.getString("BillingCity");
								map.put(charityprovince, jsonObjectt.getString("BillingCountry"));
								province=jsonObjectt.getString("BillingCountry");
								map.put(charitypostcode, jsonObjectt.getString("BillingPostalCode"));
								postcode=jsonObjectt.getString("BillingPostalCode");
								map.put(charityaddress, address+","+city+","+province);
								telephone=jsonObjectt.getString("Phone");
								cname=jsonObjectt.getString("Name");
								cphone=jsonObjectt.getString("Phone");
								ccell=jsonObjectt.getString("Phone");
								cemail=jsonObjectt.getString("PersonEmail");
								String pic=jsonObjectt.getString("PhotoUrl");
								db.insertcharities(new CharitiesGetterSetter(id,name,address,city,province,postcode,telephone,cname,cphone,ccell,cemail,pic));

							}

						}
					} catch (IOException ioException) {
						ioException.printStackTrace();
					}
					JSONObject jsonObject = null;

					try {

					} catch (Exception jsonException) {
						jsonException.printStackTrace();
					}
				}



			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}
 			
 			return z;
 		}
 	}       
 	public class DownloadFile extends AsyncTask<String,String,String>
 	{
 		
 		String z = "";
 		
 		  String replycode="";
 		 // ProgressDialog p;
 		@Override
 		protected void onPreExecute() {
 			//p=new ProgressDialog(RefreshService.this);
 			//p.setMessage("Loading Marketing Materials ...");
 			//p.setCancelable(false);
 			//p.show();
 			//pbbar.setVisibility(View.VISIBLE);
 		}

 		@Override
 		protected void onPostExecute(String r) {
 			//p.dismiss();
 			if(isSuccess) {
 				
 					//Toast.makeText(RefreshService.this,"Refresh Succesfull",Toast.LENGTH_SHORT).show();
 				
 				refresh=false;
 			}
 			
 			else
 			{
 				//Toast.makeText(RefreshService.this,"Error in Downloading File",Toast.LENGTH_SHORT).show();
 				
 			}

 		}

 		@Override
 		protected String doInBackground(String... params) {
 		
 				try {
 					isSuccess=false;
 					downloadFile(new File(Environment.getExternalStorageDirectory()+File.separator+"Funding_Innovation_1_Pager"+".pdf"));
 					
 				}
 				catch (Exception ex)
 				{
 					isSuccess = false;
 					replycode = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
 				}
 			
 			return replycode;
 		}
 	}
 	public void downloadFile(File fileName){
 	    
 	    
 	    FTPClient client = new FTPClient();
 	     
 	   try {
 	        
 	       client.connect(FTPCredentials.FTP_HOST,21);
 	       client.login(FTPCredentials.FTP_USER, FTPCredentials.FTP_PASS);
 	       client.setType(FTPClient.TYPE_BINARY);
 	       client.changeDirectory("Marketing_Materials");
 	       FTPFile[] list = client.list();
 	       
 	       for (int i = 0; i < list.length; i++)
 	       {
 	    	   HashMap<String, String> map = new HashMap<String, String>();
 	    	   map.put(NAME, list[i].getName());
 	    	   listing.add(map);
 	    	   Log.e("NAME ",list[i].getName());
 	          //client.download("localFile", new java.io.File("remotefile);
 	    	   if(new File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()).exists())
 	    	   {
 	    		   if(refresh)
 		    		  {
 		    			  new File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()).delete();
 		    			  
 		    			  client.download(list[i].getName(), new File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()));
 		    		  }
 		    		  else
 		    		  {
 		    			  
 		    		  }
 	    	   }
 	    	   else
 	    	   {
 	           client.download(list[i].getName(), new File(Environment.getExternalStorageDirectory()+File.separator+list[i].getName()));
 	    	   }
 	    	   
 	       }
 	       isSuccess=true;
 	      // client.download("Funding_Innovation_1_Pager.pdf",fileName, new MyTransferListener());
 	        
 	   } catch (Exception e) {
 	       e.printStackTrace();
 	       errormessage=e.toString();
 	       isSuccess=false;
 	       Log.e("EXCEPTION ",errormessage);
 	       try {
 	           client.disconnect(true);    
 	       } catch (Exception e2) {
 	    	   errormessage=e.toString();
 	           e2.printStackTrace();
 	       }
 	   }
 	    
 	}

 	public class GetProducts extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean Success = false;
		String itemnumber="";
		String itemdesc="";

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected void onPostExecute(String r) {
		/*	if(Success) {

				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();


			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}*/
			DownloadFile down = new DownloadFile();
				down.execute("");
		}

		@Override
		protected String doInBackground(String... params) {

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Assemble the login request URL
				String EaselsURL = SFAPI.baseUri +"/sobjects"+SFAPI.Products
						;
				Log.e("URL ",EaselsURL);
				Log.e("Header ",SFAPI.oauthHeader.toString());
				// Login requests must be POSTs
				HttpGet httpPost = new HttpGet(EaselsURL);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);


				HttpResponse response = null;
				try {
					// Execute the login POST request
					response = httpclient.execute(httpPost);
				} catch (ClientProtocolException cpException) {
					cpException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

				} catch (IOException ioException) {
					ioException.printStackTrace();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

				}
				// verify response is HTTP OK
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					//System.out.println("Error authenticating to Force.com: "+statusCode);
					//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
					isSuccess = false;
					z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error authenticating to Force.com: "+statusCode;

				}
				else
				{
					isSuccess=true;
					z ="Success";
					String getResult = null;
					try {
						getResult = EntityUtils.toString(response.getEntity());
						//Log.e("RESULT ",getResult);
						JSONObject jobj=new JSONObject(getResult);
						JSONArray jarr=jobj.getJSONArray("recentItems");
						for(int i=0;i<jarr.length();i++)
						{
							JSONObject eitems=jarr.getJSONObject(i);
							String id=eitems.getString("Id");
							String Name=eitems.getString("Name");

							HttpClient httpclient2 = new DefaultHttpClient();
							// Assemble the login request URL
							String ProductsURL2 = SFAPI.baseUri +"/sobjects"+SFAPI.Products+"/"+id
									;
							Log.e("URL ",ProductsURL2);

							// Login requests must be POSTs
							HttpGet httpPost2 = new HttpGet(ProductsURL2);
							httpPost2.addHeader(SFAPI.oauthHeader);
							httpPost2.addHeader(SFAPI.prettyPrintHeader);


							HttpResponse response2 = null;
							try {
								// Execute the login POST request
								response2 = httpclient2.execute(httpPost2);
							} catch (ClientProtocolException cpException) {
								cpException.printStackTrace();

							} catch (IOException ioException) {
								ioException.printStackTrace();

							}
							// verify response is HTTP OK
							final int statusCode2 = response2.getStatusLine().getStatusCode();
							if (statusCode2 != HttpStatus.SC_OK) {

							}
							else {
								JSONObject jsonObjectt=new JSONObject(EntityUtils.toString(response2.getEntity()));
								String ProductCode=jsonObjectt.getString("StockKeepingUnit");
								String Description=jsonObjectt.getString("Name");
								itemnumber=ProductCode;
								itemdesc=Description;
								Log.e("ProductCode ",ProductCode);
								Log.e("Description ",Description);
								String pic="";
								HashMap<String, String> map = new HashMap<String, String>();
								map.put(itemno, ProductCode);
								map.put(itemdescription, Description);
								db.insertproducts(new ProductsGetterSetter(itemnumber,itemdesc,pic));
							}
						}
						if(!listing.isEmpty())
						{

							z = "Products Found";
							isSuccess=true;
						}
						else
						{
							z = "No Products Found";
							isSuccess = false;
						}
					} catch (IOException e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
					JSONObject jsonObject = null;

					try {

					} catch (Exception e) {
						isSuccess = false;
						z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + e.toString();

					}
				}



			}
			catch (Exception ex)
			{
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}
			return z;

}
	}
 }

