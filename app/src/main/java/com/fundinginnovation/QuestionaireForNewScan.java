package com.fundinginnovation;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import com.Utils.FTPCredentials;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class QuestionaireForNewScan extends Activity{
	int p;
	String questionid;
	Boolean isSuccess = false;
	CustomViewPager viewPager;
	private Activity activity;
	String filename="";
	String fontPath2 = "segoepr.ttf";
	String qidd;;
	int pos;
	static String qquesid = "qquesid";
	static String qquestext = "qquestext";
	ArrayList<String> qids = new ArrayList< String>();
	ArrayList<HashMap<String, String>> listing;
	String errormessage="";
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.slide);
		listing = new ArrayList<HashMap<String, String>>();
		for (int i = 0; i <2; i++) {



			HashMap<String, String> map = new HashMap<String, String>();
			map.put(qquesid, String.valueOf(i));
			if(i==0){
				map.put(qquestext, "Please Confirm if this is the correct Product going up (Auction Start) based on the scan ?");
			}
			if(i==1)
			{
				map.put(qquestext, "Any Additional notes/comments on this location/turn ?");
			}
			qids.add(String.valueOf(i));

			listing.add(map);
		}
		//Toast.makeText(getApplicationContext(), String.valueOf(listing.size()), Toast.LENGTH_LONG).show();
		this.activity=QuestionaireForNewScan.this;
		viewPager = (CustomViewPager) findViewById(R.id.view_pager);
		//pos=getCategoryPos(questionid);
		viewPager.setPagingEnabled(false);
		QuestionaireForNewScanPagerAdapter adapter = new QuestionaireForNewScanPagerAdapter(activity, qids,listing);
		viewPager.setAdapter(adapter);

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int i, float v, int i2) {
				//Log.w("","POS "+i);
			}
			@Override
			public void onPageSelected(int i) {
				//	pos=getCategoryPos(questionid);
				//Log.w("","POS "+i);
				p=i;  
				viewPager.getAdapter().notifyDataSetChanged();	                
			}
			@Override
			public void onPageScrollStateChanged(int i) {
				//Log.w("","POS "+i);
			}
		});
	}
	@SuppressWarnings("unused")
	private int getCategoryPos(String category) {
		return qids.indexOf(category);
	}
	private class QuestionaireForNewScanPagerAdapter extends PagerAdapter {
		private ArrayList<String> qids;
		ArrayList<HashMap<String, String>> qlisting;
		private Activity _activity;
		String answer="";
		public QuestionaireForNewScanPagerAdapter(Activity activity,
				ArrayList<String> ids, ArrayList<HashMap<String, String>> listing) {
			this._activity = activity;
			this.qids = ids;
			this.qlisting=listing;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return qlisting.size();
		}
		@Override
		public boolean isViewFromObject(View view, Object object) {
			// TODO Auto-generated method stub
			return view == ((RelativeLayout) object);
		}
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE; 
		}
		@SuppressWarnings("unused")
		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Typeface tf2 = Typeface.createFromAsset(_activity.getAssets(), fontPath2);
			LayoutInflater	inflater = (LayoutInflater) _activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View viewLayout = inflater.inflate(R.layout.questionlayout,
					container, false);
			qidd = qids.get(position);
			final 	RelativeLayout relativelayout=(RelativeLayout)viewLayout.findViewById(R.id.relativeLayout1);
			HashMap<String, String> questions = new HashMap<String, String>();

			questions=qlisting.get(position);
			//	TextView qoutoftotal=(TextView)viewLayout.findViewById(R.id.qqout);


			//ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
			int a= viewPager.getCurrentItem();
			a++;
			//	qoutoftotal.setText("Question "+a+"/"+arraysize);
			/*final TextView qi=(TextView)viewLayout.findViewById(R.id.qiid);
			qi.setText(questionid);*/
			final 	TextView q=(TextView)viewLayout.findViewById(R.id.textView1);
			q.setSelected(true);
			q.setText(questions.get(Questionaire.qquestext));
			//	q.setTypeface(tf2);
			final EditText providedetails=(EditText)viewLayout.findViewById(R.id.providedetails);
			final RadioGroup radioyesno=(RadioGroup)viewLayout.findViewById(R.id.radioyesno);
			final 	Button gobutton=(Button)viewLayout.findViewById(R.id.go);
			final Button next=(Button)viewLayout.findViewById(R.id.next);
			final 	Button confirm=(Button)viewLayout.findViewById(R.id.confirmbtn);
			final 	Button cancel=(Button)viewLayout.findViewById(R.id.cancelbtn);
			final 	Button nextcancel=(Button)viewLayout.findViewById(R.id.cancelnext);
			final 	TextView selectedtext=(TextView)viewLayout.findViewById(R.id.selectedtext);
			//selectedtext.setText("Art Name : "+MainActivity.SelectedArt+"\n"+"Location : "+MainActivity.SelectedLocation+"\n"+"Charity : "+MainActivity.SelectedCharity);
			if(q.getText().toString().equals("Please Confirm if this is the correct Product going up (Auction Start) based on the scan ?"))
			{
				radioyesno.setVisibility(View.INVISIBLE);

				confirm.setVisibility(View.VISIBLE);
				cancel.setVisibility(View.VISIBLE);
				if(getscreensize()>=7)
				{
					final float scale = getBaseContext().getResources().getDisplayMetrics().density;
					int pixelswidth = (int) (460 * scale + 0.5f);
					int pixelsheight = (int) (200 * scale + 0.5f);
					ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
					layoutParams.width = pixelswidth;
					layoutParams.height = pixelsheight;
					relativelayout.setLayoutParams(layoutParams);
				}
				else{
				final float scale = getBaseContext().getResources().getDisplayMetrics().density;
				int pixelswidth = (int) (280 * scale + 0.5f);
				int pixelsheight = (int) (100 * scale + 0.5f);
				ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
				layoutParams.width = pixelswidth;
				layoutParams.height = pixelsheight;
				relativelayout.setLayoutParams(layoutParams);}
				confirm.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) 
					{
						movetonext();
					}
				});
				cancel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) 
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(
								QuestionaireForNewScan.this);
						builder.setTitle("Cancel ?");
						builder.setIcon(android.R.drawable.ic_dialog_alert);
						builder.setMessage("Are you sure you wish to cancel the easel entry ?");
						builder.setNegativeButton("No", null);
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										finish();
									}
								});

						builder.create().show();
					}
				});
			}
			else
			{
				if(getscreensize()>=7)
				{
					final float scale = getBaseContext().getResources().getDisplayMetrics().density;
					int pixelswidth = (int) (460 * scale + 0.5f);
					int pixelsheight = (int) (260 * scale + 0.5f);
					ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
					layoutParams.width = pixelswidth;
					layoutParams.height = pixelsheight;
					relativelayout.setLayoutParams(layoutParams);
				}
				else{
				final float scale = getBaseContext().getResources().getDisplayMetrics().density;
				int pixelswidth = (int) (280 * scale + 0.5f);
				int pixelsheight = (int) (190 * scale + 0.5f);
				ViewGroup.LayoutParams layoutParams = relativelayout.getLayoutParams();
				layoutParams.width = pixelswidth;
				layoutParams.height = pixelsheight;
				relativelayout.setLayoutParams(layoutParams);}
				radioyesno.setVisibility(View.VISIBLE);
				confirm.setVisibility(View.INVISIBLE);
				cancel.setVisibility(View.INVISIBLE);
			}

			radioyesno.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					int id=group.getCheckedRadioButtonId();
					RadioButton rb=(RadioButton) findViewById(id);
					answer=rb.getText().toString();
					//	Toast.makeText(getApplicationContext(), answer, Toast.LENGTH_LONG).show();

					if(q.getText().toString().equals("Any Additional notes/comments on this location/turn ?")&&!answer.equals("No")&&radioyesno.getVisibility()==View.VISIBLE)
					{
						radioyesno.setVisibility(View.GONE);
						providedetails.setVisibility(View.VISIBLE);
						next.setVisibility(View.VISIBLE);
						nextcancel.setVisibility(View.VISIBLE);
						next.setText("Submit");
						next.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) 
							{
								/*radioyesno.setVisibility(View.VISIBLE);
								providedetails.setVisibility(View.GONE);
								gobutton.setVisibility(View.GONE);
								next.setVisibility(View.VISIBLE);*/
								
								generateodbfileandsend();
								/*movetonext();*/
								
							}
						});
						nextcancel.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) 
							{
								AlertDialog.Builder builder = new AlertDialog.Builder(
										QuestionaireForNewScan.this);
								builder.setTitle("Cancel ?");
								builder.setIcon(android.R.drawable.ic_dialog_alert);
								builder.setMessage("Are you sure you wish to cancel the easel entry ?");
								builder.setNegativeButton("No", null);
								builder.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(DialogInterface arg0,
													int arg1) {
												finish();
											}
										});

								builder.create().show();
							}
						});
					}

					else{
						radioyesno.setVisibility(View.VISIBLE);
						providedetails.setVisibility(View.GONE);
						gobutton.setVisibility(View.INVISIBLE);
						next.setVisibility(View.VISIBLE);
						next.setText("Submit");
						movetonext();
						
					}
				}
			});


			nextcancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(
							QuestionaireForNewScan.this);
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the easel entry ?");
					builder.setNegativeButton("No", null);
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									finish();
								}
							});

					builder.create().show();
				}
			});
			next.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					
					if(answer.equals(""))
					{
						Toast.makeText(getApplicationContext(), "Please Select One Option", Toast.LENGTH_LONG).show();
						return;
					}
					
					if(q.getText().toString().equals("Any Additional notes/comments on this location/turn ?")&&!answer.equals("No")&&radioyesno.getVisibility()==View.VISIBLE)
					{
						radioyesno.setVisibility(View.GONE);
						providedetails.setVisibility(View.VISIBLE);

					}

					else{
						radioyesno.setVisibility(View.VISIBLE);
						providedetails.setVisibility(View.GONE);
						movetonext();
						
					}
				}
			});
			

			((ViewPager) container).addView(viewLayout);
			return viewLayout;}
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((RelativeLayout) object); 	    }
	}
	@Override
	public void onBackPressed()
	{

		int aa= viewPager.getCurrentItem();
		aa--;
		viewPager.setCurrentItem(aa);

	}
	public void movetonext()
	{
		int aa= viewPager.getCurrentItem();
		aa++;
		viewPager.setCurrentItem(aa);

	}
	@SuppressWarnings("unused")
	public double getscreensize()
	{
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		float scaleFactor = metrics.density;
		float widthDp = widthPixels / scaleFactor;
				float heightDp = heightPixels / scaleFactor;
				
				float widthDpi = metrics.xdpi;
				float heightDpi = metrics.ydpi;
				float widthInches = widthPixels / widthDpi;
				float heightInches = heightPixels / heightDpi;
				double diagonalInches = Math.sqrt(
					    (widthInches * widthInches) 
					    + (heightInches * heightInches));
				
				if (diagonalInches >= 10) {
				    //Device is a 10" tablet
				} 
				else if (diagonalInches >= 7) {
				    //Device is a 7" tablet
				}
		
		return diagonalInches;
	}
	@SuppressWarnings("unused")
	public void generateodbfileandsend()
	{
		
		try{
			 
			String m="";
			String d="";
			String modifiedmonth="";
			String modifiedday="";
			Date date = new Date(); // your date
			    Calendar cal = Calendar.getInstance();
			    cal.setTime(date);
			    int year = cal.get(Calendar.YEAR);
			    int month = cal.get(Calendar.MONTH);
			    int day = cal.get(Calendar.DAY_OF_MONTH);
			    if(day<10)
			    {
			    	modifiedday="0"+String.valueOf(day);
			    }
			    else if(day>=10){modifiedday=String.valueOf(day);}
			    if(month<10)
			    {
			    	modifiedmonth="0"+String.valueOf(month);
			    }
			    else if(month>=10){modifiedmonth=String.valueOf(month);}
			    if(day<10)
			    {
			    	d="0"+String.valueOf(day);
			    }
			    else
			    {
			    	d=String.valueOf(day);
			    }
			    month=month+1;
			if(month<=9)
			{
				m=String.valueOf(month);
			}
			else if(month==10)
			{
				m="O";
				
			}
			else if(month==11)
			{
				m="N";
			
			}
			else if(month==12)
			{
				m="D";
				
			}
			 filename="I"+LoginActivity.id+LoginActivity.id+m+d;
		File ff=new File(Environment.getExternalStorageDirectory()+File.separator+filename+".txt");
		if(ff.exists()){ff.delete();}
		ff.createNewFile();
		
		String barcodeandart=MainActivity.SelectedArt;
		String easelandart=MainActivity.SelectedEasel+MainActivity.SelectedArt;
		String location="ZAK"+MainActivity.SelectedLocation+String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
		RandomAccessFile file = new RandomAccessFile(Environment.getExternalStorageDirectory()+File.separator+filename+".txt", "rw");
		
		file.write("IJRITH".getBytes());
		file.seek(22);
		file.write(barcodeandart.getBytes());
		file.seek(52);
		file.write(location.getBytes());
		file.seek(106);
		file.write(MainActivity.SelectedEasel.getBytes());
		file.seek(126);
		file.write(Questionaire.Bids.getBytes());
		file.seek(186);
		file.write(MainActivity.SelectedEasel.getBytes());
		file.seek(209);
		file.write("0000".getBytes());
		file.seek(445);
		file.write(LoginActivity.User.getBytes());

		file.writeBytes("\r\n");
		long pointer=file.getFilePointer();
		file.write("IJRITD".getBytes());
		//Toast.makeText(getApplicationContext(), String.valueOf(file.getFilePointer()), Toast.LENGTH_LONG).show();
		file.seek(pointer+22);
		file.write(barcodeandart.getBytes());
		file.seek(pointer+52);
		file.write(location.getBytes());
		file.seek(pointer+106);
		file.write(MainActivity.SelectedEasel.getBytes());
		file.seek(pointer+126);
		file.write(Questionaire.Bids.getBytes());
		file.seek(pointer+186);
		file.write(MainActivity.SelectedEasel.getBytes());
		file.seek(pointer+209);
		file.write("0000".getBytes());
		file.seek(pointer+445);
		file.write(LoginActivity.User.getBytes());
		file.close();
		}catch(Exception q)
		{
			Toast.makeText(getApplicationContext(), q.toString(), Toast.LENGTH_LONG).show();
		}
		File from = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".txt");
		File to = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".odb");
		from.renameTo(to);
		
		UploadFile doUpload = new UploadFile(); 
		  doUpload.execute("");
		
	}
	public class UploadFile extends AsyncTask<String,String,String>
	{
		
		String z = "";
		
		  String replycode="";
		  ProgressDialog p;
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(QuestionaireForNewScan.this);
			p.setMessage("Uploading File ...");
			p.setCancelable(false);
			p.show();
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if(isSuccess) {
				 Intent i= new Intent(getApplicationContext(),SummaryActivity.class);
					startActivity(i);
					Toast.makeText(QuestionaireForNewScan.this,"odb File Uploaded SuccessFully",Toast.LENGTH_SHORT).show();
			}
			
			else
			{
				Toast.makeText(QuestionaireForNewScan.this,"Error in uploading File",Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {
		
				try {
					isSuccess=false;
					uploadFile(new File(Environment.getExternalStorageDirectory()+File.separator+filename+".odb"));
					
				}
				catch (Exception ex)
				{
					isSuccess = false;
					replycode = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
				}
			
			return replycode;
		}
	}
	public void uploadFile(File fileName){
	    
	    
	    FTPClient client = new FTPClient();
	     
	   try {
	        
	       client.connect(FTPCredentials.FTP_HOST,21);
	       client.login(FTPCredentials.FTP_USER, FTPCredentials.FTP_PASS);
	       client.setType(FTPClient.TYPE_BINARY);
	       //client.changeDirectory("Test");
	       
	       client.upload(fileName, new MyTransferListener());
	        
	   } catch (Exception e) {
	       e.printStackTrace();
	       errormessage=e.toString();
	       try {
	           client.disconnect(true);    
	       } catch (Exception e2) {
	    	   errormessage=e.toString();
	           e2.printStackTrace();
	       }
	   }
	    
	}

	/*******  Used to file upload and show progress  **********/

	public class MyTransferListener implements FTPDataTransferListener {

	   public void started() {
	        
		 //  p.setMessage("Upload Started");
	       // Transfer started
	       //Toast.makeText(getBaseContext(), " Upload Started ...", Toast.LENGTH_SHORT).show();
	       //System.out.println(" Upload Started ...");
	   }

	   public void transferred(int length) {
		//   p.setMessage("Transferred ... " + String.valueOf(length));
	       // Yet other length bytes has been transferred since the last time this
	       // method was called
	     //  Toast.makeText(getBaseContext(), " transferred ..." + length, Toast.LENGTH_SHORT).show();
	       //System.out.println(" transferred ..." + length);
	   }

	   public void completed() {
	        
		   isSuccess=true;
		  // p.setMessage("completed");
	       // Transfer completed
		 ///  isSuccess=true;
	     //  Toast.makeText(getBaseContext(), " completed ...", Toast.LENGTH_SHORT).show();
	       //System.out.println(" completed ..." );
	   }

	   public void aborted() {
		//   p.setMessage("aborted");
		  // isSuccess=false;
	       // Transfer aborted
	       //Toast.makeText(getBaseContext()," transfer aborted ", Toast.LENGTH_SHORT).show();
	       //System.out.println(" aborted ..." );
	   }

	   public void failed() {
//		   p.setMessage("failed");
		  // isSuccess=false;
	       // Transfer failed
	     //  System.out.println(" failed ..." );
	   }

	}

}