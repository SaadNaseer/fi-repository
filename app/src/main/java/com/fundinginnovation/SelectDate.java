package com.fundinginnovation;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.NavigationFragments.Home;
import com.NavigationFragments.NewEasel;
import com.Utils.ConnectionClass;
import com.Utils.FTPCredentials;
import com.Utils.FileUtils;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.navigationdrawer.NavigationActivity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SelectDate extends Fragment{
	Button confirmbtn;
	CalendarView calendarView;
	TextView dateDisplay;
	String errormessage="";
	Boolean isSuccess = false;
	String	uniquenumber="00001";
	String user2ch="";
	String usernamefull="";
	String filename="";
	int yearn = 2017;
	int monthn = 06;
	int dayn = 11;
	String modifiedmonthn="";
	String modifieddayn="";
	boolean uploadtxt=false;
	boolean movefile =true;
	ConnectionClass connectionClass;
	String turnstartdate="";
	String turnenddate="";
	String turnenddatesff="";
	@SuppressWarnings("unused")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView;
		rootView = inflater.inflate(R.layout.easel_selectdate, container, false);
		connectionClass = new ConnectionClass();
		confirmbtn= (Button)rootView. findViewById(R.id.confirm_button);
		calendarView = (CalendarView)rootView. findViewById(R.id.calendarView);
		//Calendar calendar = Calendar.getInstance();

		dateDisplay = (TextView)rootView. findViewById(R.id.date_display);
		Calendar c = Calendar.getInstance();
		yearn = c.get(Calendar.YEAR);
		monthn = c.get(Calendar.MONTH);
		dayn = c.get(Calendar.DAY_OF_MONTH);
		monthn=monthn+1;
		long milliTime = c.getTimeInMillis();
		try{
		Date d= new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
			
			    Date mDate = sdf.parse(d.toString());
			    long timeInMilliseconds = mDate.getTime();
		calendarView.setMinDate(timeInMilliseconds);
		
		Calendar calendar = Calendar.getInstance();
	    calendar.setTime(d);
	    calendar.add(Calendar.DAY_OF_YEAR, 14);
	    Date newDate = calendar.getTime();
	    Date maxdate = sdf.parse(newDate.toString());
	    long maxtime = maxdate.getTime();
	   // calendarView.setMaxDate(maxtime);
	    calendarView.setDate (maxtime, true, true); 
	  //  dayn=Integer.valueOf(calendar.DAY_OF_MONTH);
	  //  monthn=Integer.valueOf(calendar.MONTH);
	   // monthn=monthn+1;
	   // yearn=Integer.valueOf(calendar.YEAR);
		}
		catch(Exception t)
		{
			Toast.makeText(getActivity(), "Failed to set Minimum Date"+t.toString(), Toast.LENGTH_LONG).show();
			
			Log.e("","Failed to set Minimum Date"+t.toString());
		}
		try
		{
			Calendar calendar = Calendar.getInstance();
		    calendar.add(Calendar.DAY_OF_YEAR, 14);
		    Date newDate = calendar.getTime();
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		    String dateAsString = dateFormat.format(newDate);
		    String arr[]=dateAsString.split("-");
		    dayn=Integer.parseInt(arr[0]);
		    monthn=Integer.parseInt(arr[1]);
		    yearn=Integer.parseInt(arr[2]);
		}
		catch(Exception a)
		{
			
		}
		try {
			fixBuggyCalendarview(calendarView);
		}catch (Exception aa){}
		dateDisplay.setText("Date: " + dayn + " / " + monthn + " / " + yearn);
		turnenddatesff=yearn+"-"+monthn+"-"+dayn;
		String m="";
		String d="";


		if(dayn<10)
		{
			modifieddayn="0"+String.valueOf(dayn);
		}
		else if(dayn>=10){modifieddayn=String.valueOf(dayn);}
		if(monthn<10)
		{
			modifiedmonthn="0"+String.valueOf(monthn);
		}
		else if(monthn>=10){modifiedmonthn=String.valueOf(monthn);}
		if(dayn<10)
		{
			d="0"+String.valueOf(dayn);
		}
		else
		{
			d=String.valueOf(dayn);
		}

		if(monthn<=9)
		{
			m=String.valueOf(monthn);
		}
		else if(monthn==10)
		{
			m="O";

		}
		else if(monthn==11)
		{
			m="N";

		}
		else if(monthn==12)
		{
			m="D";

		}
	
		calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
			@Override
			public void onSelectedDayChange(CalendarView calendarView, int i, int i1, int i2) {

			    try {

                    i1 = i1 + 1;
                    yearn = i;
                    monthn = i1;
                    dayn = i2;
					turnenddatesff=yearn+"-"+monthn+"-"+dayn;
                    dateDisplay.setText("Date: " + dayn + " / " + monthn + " / " + yearn);
                    String m = "";
                    String d = "";


                    if (dayn < 10) {
                        modifieddayn = "0" + String.valueOf(dayn);
                    } else if (dayn >= 10) {
                        modifieddayn = String.valueOf(dayn);
                    }
                    if (monthn < 10) {
                        modifiedmonthn = "0" + String.valueOf(monthn);
                    } else if (monthn >= 10) {
                        modifiedmonthn = String.valueOf(monthn);
                    }
                    if (dayn < 10) {
                        d = "0" + String.valueOf(dayn);
                    } else {
                        d = String.valueOf(dayn);
                    }

                    if (monthn <= 9) {
                        m = String.valueOf(monthn);
                    } else if (monthn == 10) {
                        m = "O";

                    } else if (monthn == 11) {
                        m = "N";

                    } else if (monthn == 12) {
                        m = "D";

                    }
                }catch (Exception aa){
			        Toast.makeText(getActivity(),aa.toString(),Toast.LENGTH_LONG).show();
                }
			}
		});
		confirmbtn.setOnClickListener(new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				if(modifieddayn.equals(""))
				{
					Toast.makeText(getActivity(), "Please Select Closing Date First", Toast.LENGTH_LONG).show();
					return;
				}
				SharedPreferences pref = getActivity().getSharedPreferences(preferences.PREF_NAME,0);

				uniquenumber =pref.getString(preferences.uniquevalue,"00001");
				usernamefull =pref.getString(preferences.username,"");
				user2ch =pref.getString(preferences.username,"");
				if(Home.SelectedArt.equals("SelectedArt")||Home.SelectedEasel.equals("SelectedEasel")||Home.SelectedLocation.equals("SelectedLocation")||NewEasel.SelectedEasel.equals("SelectedEasel")||NewEasel.SelectedArt.equals("SelectedArt"))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Error");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Unfortunately we have encountered an issue with the application. Please completely log out of the app and restart it");
			
					builder.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
								
								}
							});

					builder.create().show();
					return;
				}
				//generateodbfile();
				//generatetxtfile();
				//UploadFile doUpload = new UploadFile();
				//doUpload.execute("");
				GenerateAuction gauc = new GenerateAuction();
				gauc.execute("");
				//Toast.makeText(getActivity(),"In Development",Toast.LENGTH_LONG).show();

			}
		});

		return rootView;
	}

	public class GenerateAuction extends AsyncTask<String,String,String>
	{
		ProgressDialog p;
		String z = "";
		Boolean isSuccess = false;





		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Creating Auction ...");
			p.show();
			p.setCancelable(false);
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {

				ChangeProduct changeproduct = new ChangeProduct();
				changeproduct.execute("");
			}

			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			String uri = SFAPI.baseUri + "/sobjects/Auction__c/";
			Log.e("Url :",uri);
			try {

				//create the JSON object containing the new lead details.
				JSONObject auction = new JSONObject();
				auction.put("Easel_Number__c", Home.SelectedEaselID);
				//auction.put("Name", Home.SelectedEasel);
				//auction.put("Product_SKU__c", Home.SelectedArt);
				//auction.put("Easel_Location__c", Home.SelectedLocation);
				auction.put("Product_Name__c", Home.SelectedNewProductID);
				auction.put("Status__c", "Open");
				auction.put("Auction_End_Date__c", turnenddatesff);

				Log.e("","JSON for lead record to be inserted:\n" + auction.toString(1));
				//Toast.makeText(LoginActivity.this,"JSON for lead record to be inserted:\n" + lead.toString(1),Toast.LENGTH_LONG).show();

				//Construct the objects needed for the request
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(uri);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);

				// The message we are going to post
				StringEntity body = new StringEntity(auction.toString(1));
				body.setContentType("application/json");
				httpPost.setEntity(body);

				//Make the request
				HttpResponse response = httpClient.execute(httpPost);

				//Process the results
				int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode == 201) {

					String response_string = EntityUtils.toString(response.getEntity());
					Log.e("Auction Response :",response_string);
					isSuccess=true;
					z="Successful";
						}

				 else {
					//Toast.makeText(LoginActivity.this,"Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity()),Toast.LENGTH_LONG).show();
					//System.out.println("Insertion unsuccessful. Status code returned is " + statusCode);
					z="Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity());
					isSuccess=false;
				}
			} catch (JSONException e) {
				isSuccess=false;
				//System.out.println("Issue creating JSON or processing results");
				e.printStackTrace();
			} catch (IOException ioe) {
				isSuccess=false;
			} catch (NullPointerException npe) {
				isSuccess=false;
			}

			return z;
		}
	}

	@SuppressWarnings("unused")
	public void generateodbfile()
	{

		try{

			String m="";
			String d="";
			String modifiedmonth="";
			String modifiedday="";
			Date date = new Date(); // your date
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			month=month+1;
			if(day<10)
			{
				modifiedday="0"+String.valueOf(day);
			}
			else if(day>=10){modifiedday=String.valueOf(day);}
			if(month<10)
			{
				modifiedmonth="0"+String.valueOf(month);
			}
			else if(month>=10){modifiedmonth=String.valueOf(month);}
			if(day<10)
			{
				d="0"+String.valueOf(day);
			}
			else
			{
				d=String.valueOf(day);
			}

			if(month<=9)
			{
				m=String.valueOf(month);
			}
			else if(month==10)
			{
				m="O";

			}
			else if(month==11)
			{
				m="N";

			}
			else if(month==12)
			{
				m="D";

			}
			// filename="I"+LoginActivity.id+LoginActivity.id+m+d;
			String user=user2ch.substring(0, Math.min(user2ch.length(), 2));
			filename="I"+user+String.valueOf(uniquenumber);
			turnstartdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
			turnenddate=String.valueOf(yearn)+String.valueOf(modifiedmonthn)+String.valueOf(modifieddayn);
			SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
			String newlocationfromdb=  	settings.getString(preferences.location,"");
			String newbinnofromdb=  	settings.getString(preferences.binno,"");
			String warehouse=  			settings.getString(preferences.Warehouse,"");
			File ff=new File(Environment.getExternalStorageDirectory()+File.separator+filename+".txt");
			if(ff.exists()){ff.delete();}
			ff.createNewFile();
			if(SummaryActivity.summary)
			{
				//String bids=Questionaire.Bids+".0";
				long nowpointer=0;
				String bids="1.0";
				String barcodeandart=SummaryActivity.SelectedArt;
				String easelandart=SummaryActivity.SelectedEasel+SummaryActivity.SelectedArt;
				String location=SummaryActivity.SelectedLocation+SummaryActivity.SelectedLocation;
				String locationdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
				String locationnew=NewEasel.SelectedLocation+NewEasel.SelectedLocation;
				String locationnewdate=String.valueOf(yearn)+String.valueOf(modifiedmonthn)+String.valueOf(modifieddayn);
				RandomAccessFile file = new RandomAccessFile(Environment.getExternalStorageDirectory()+File.separator+filename+".txt", "rw");
				long pointer;
				//////////////////////////////////////Two Lines ODB File//////////////////////////////////////////
				file.write("IJRITH".getBytes());
				//file.seek(22);
				for(int i=0;i<16;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(NewEasel.SelectedArt.getBytes());
				nowpointer=52-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				//file.seek(52);
				file.write(/*locationnew*/warehouse.getBytes());
				//file.seek(55);
				file.write(/*locationnew*/newlocationfromdb.getBytes());
				if(newlocationfromdb.length()<3)
				{
					file.write(" ".getBytes());
				}
				//file.seek(58);
				file.write(locationdate.getBytes());
				//file.seek(106);
				nowpointer=106-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(NewEasel.SelectedEasel.getBytes());
				//file.seek(126);
				nowpointer=126-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write("1.0".getBytes());
			//	file.seek(186);
				nowpointer=186-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(/*"0000"*/newbinnofromdb.getBytes());
				//file.seek(209);
				nowpointer=209-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(NewEasel.SelectedEasel.getBytes());
				//file.seek(445);
				nowpointer=445-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(usernamefull.getBytes());
				file.writeBytes("\r\n");

				pointer=file.getFilePointer();
				file.write("IJRITD".getBytes());
				//file.seek(pointer+22);
				for(int i=0;i<16;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(NewEasel.SelectedArt.getBytes());
				//file.seek(pointer+52);
				nowpointer=pointer+52-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(/*locationnew*/warehouse.getBytes());
			//	file.seek(pointer+55);
				file.write(/*locationnew*/newlocationfromdb.getBytes());
				//file.seek(pointer+58);
				if(newlocationfromdb.length()<3)
				{
					file.write(" ".getBytes());
				}
				file.write(locationdate.getBytes());
			//	file.seek(pointer+106);
				nowpointer=pointer+106-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(NewEasel.SelectedEasel.getBytes());
				//file.seek(pointer+126);
				nowpointer=pointer+126-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write("1.0".getBytes());
				//file.seek(pointer+186);
				nowpointer=pointer+186-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(/*"0000"*/newbinnofromdb.getBytes());
				//file.seek(pointer+209);
				nowpointer=pointer+209-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(NewEasel.SelectedEasel.getBytes());
				//file.seek(pointer+445);
				nowpointer=pointer+445-file.getFilePointer();
				for(int i=0;i<nowpointer;i++)
				{
					file.write(" ".getBytes());
				}
				file.write(usernamefull.getBytes());
				file.writeBytes("\r\n");
				///////////////////////////////////////////////Four Lines ODB File////////////////////////////////////////////////////////////////////////
				/*file.write("IJRITH".getBytes());
			file.seek(22);
			file.write(barcodeandart.getBytes());
			file.seek(52);
			file.write(location.getBytes());
			file.seek(58);
			file.write(locationdate.getBytes());
			file.seek(106);
			file.write(Home.SelectedEasel.getBytes());
			file.seek(126);
			file.write(bids.getBytes());
			file.seek(186);
			file.write(Home.SelectedEasel.getBytes());
			file.seek(209);
			file.write("0000".getBytes());
			file.seek(445);
			file.write(usernamefull.getBytes());
			file.writeBytes("\r\n");

			 pointer=file.getFilePointer();
			file.write("IJRITD".getBytes());
			file.seek(pointer+22);
			file.write(barcodeandart.getBytes());
			file.seek(pointer+52);
			file.write(location.getBytes());
			file.seek(pointer+58);
			file.write(locationdate.getBytes());
			file.seek(pointer+106);
			file.write(Home.SelectedEasel.getBytes());
			file.seek(pointer+126);
			file.write(bids.getBytes());
			file.seek(pointer+186);
			file.write(Home.SelectedEasel.getBytes());
			file.seek(pointer+209);
			file.write("0000".getBytes());
			file.seek(pointer+445);
			file.write(usernamefull.getBytes());
			file.writeBytes("\r\n");


			 pointer=file.getFilePointer();
			file.write("IJRITH".getBytes());
			file.seek(pointer+22);
			file.write(NewEasel.SelectedArt.getBytes());
			file.seek(pointer+52);
			file.write(locationnew.getBytes());
			file.seek(pointer+58);
			file.write(locationdate.getBytes());
			file.seek(pointer+106);
			file.write(NewEasel.SelectedEasel.getBytes());
			file.seek(pointer+126);
			file.write("1.0".getBytes());
			file.seek(pointer+186);
			file.write("0000".getBytes());
			file.seek(pointer+209);
			file.write(NewEasel.SelectedEasel.getBytes());
			file.seek(pointer+445);
			file.write(usernamefull.getBytes());
			file.writeBytes("\r\n");


			 pointer=file.getFilePointer();
			file.write("IJRITD".getBytes());
			file.seek(pointer+22);
			file.write(NewEasel.SelectedArt.getBytes());
			file.seek(pointer+52);
			file.write(locationnew.getBytes());
			file.seek(pointer+58);
			file.write(locationdate.getBytes());
			file.seek(pointer+106);
			file.write(NewEasel.SelectedEasel.getBytes());
			file.seek(pointer+126);
			file.write("1.0".getBytes());
			file.seek(pointer+186);
			file.write("0000".getBytes());
			file.seek(pointer+209);
			file.write(NewEasel.SelectedEasel.getBytes());
			file.seek(pointer+445);
			file.write(usernamefull.getBytes());*/
				file.close();

				File from = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".txt");
				File to = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".odb");
				from.renameTo(to);
				SummaryActivity.summary=false;
				return;
			}
			//String bids=Questionaire.Bids+".0";
			long nowpointer=0;
			String bids="1.0";
			String barcodeandart=Home.SelectedArt;
			String easelandart=Home.SelectedEasel+Home.SelectedArt;
			String location=Home.SelectedLocation+Home.SelectedLocation;
			String locationdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
			String locationnew=NewEasel.SelectedLocation+NewEasel.SelectedLocation;
			String locationnewdate=String.valueOf(yearn)+String.valueOf(modifiedmonthn)+String.valueOf(modifieddayn);
			RandomAccessFile file = new RandomAccessFile(Environment.getExternalStorageDirectory()+File.separator+filename+".txt", "rw");
			long pointer;
			//////////////////////////////////////Two Lines ODB File//////////////////////////////////////////
			file.write("IJRITH".getBytes());
			//file.seek(22);
			for(int i=0;i<16;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(NewEasel.SelectedArt.getBytes());
			//file.seek(52);
			nowpointer=52-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			
			file.write(/*locationnew*/warehouse.getBytes());
			//file.seek(55);
			file.write(/*locationnew*/newlocationfromdb.getBytes());
			//file.seek(58);
			file.write(locationdate.getBytes());
			//file.seek(106);
			nowpointer=106-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(NewEasel.SelectedEasel.getBytes());
			//file.seek(126);
			nowpointer=126-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write("1.0".getBytes());
			nowpointer=186-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			//file.seek(186);
			file.write(/*"0000"*/newbinnofromdb.getBytes());
			//file.seek(209);
			nowpointer=209-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(NewEasel.SelectedEasel.getBytes());
			//file.seek(445);
			nowpointer=445-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(usernamefull.getBytes());
			file.writeBytes("\r\n");

			pointer=file.getFilePointer();
			file.write("IJRITD".getBytes());
			//file.seek(pointer+22);
			for(int i=0;i<16;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(NewEasel.SelectedArt.getBytes());
			//file.seek(pointer+52);
			nowpointer=pointer+52-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(/*locationnew*/warehouse.getBytes());
			//file.seek(pointer+55);
			file.write(/*locationnew*/newlocationfromdb.getBytes());
			//file.seek(pointer+58);
			file.write(locationdate.getBytes());
			//file.seek(pointer+106);
			nowpointer=pointer+106-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(NewEasel.SelectedEasel.getBytes());
			//file.seek(pointer+126);
			nowpointer=pointer+126-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write("1.0".getBytes());
			//file.seek(pointer+186);
			nowpointer=pointer+186-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(/*"0000"*/newbinnofromdb.getBytes());
			//file.seek(pointer+209);
			nowpointer=pointer+209-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			file.write(NewEasel.SelectedEasel.getBytes());
			nowpointer=pointer+445-file.getFilePointer();
			for(int i=0;i<nowpointer;i++)
			{
				file.write(" ".getBytes());
			}
			//file.seek(pointer+445);
			file.write(usernamefull.getBytes());
			file.writeBytes("\r\n");
			///////////////////////////////////////////////Four Lines ODB File////////////////////////////////////////////////////////////////////////
			/*file.write("IJRITH".getBytes());
		file.seek(22);
		file.write(barcodeandart.getBytes());
		file.seek(52);
		file.write(location.getBytes());
		file.seek(58);
		file.write(locationdate.getBytes());
		file.seek(106);
		file.write(Home.SelectedEasel.getBytes());
		file.seek(126);
		file.write(bids.getBytes());
		file.seek(186);
		file.write(Home.SelectedEasel.getBytes());
		file.seek(209);
		file.write("0000".getBytes());
		file.seek(445);
		file.write(usernamefull.getBytes());
		file.writeBytes("\r\n");

		 pointer=file.getFilePointer();
		file.write("IJRITD".getBytes());
		file.seek(pointer+22);
		file.write(barcodeandart.getBytes());
		file.seek(pointer+52);
		file.write(location.getBytes());
		file.seek(pointer+58);
		file.write(locationdate.getBytes());
		file.seek(pointer+106);
		file.write(Home.SelectedEasel.getBytes());
		file.seek(pointer+126);
		file.write(bids.getBytes());
		file.seek(pointer+186);
		file.write(Home.SelectedEasel.getBytes());
		file.seek(pointer+209);
		file.write("0000".getBytes());
		file.seek(pointer+445);
		file.write(usernamefull.getBytes());
		file.writeBytes("\r\n");


		 pointer=file.getFilePointer();
		file.write("IJRITH".getBytes());
		file.seek(pointer+22);
		file.write(NewEasel.SelectedArt.getBytes());
		file.seek(pointer+52);
		file.write(locationnew.getBytes());
		file.seek(pointer+58);
		file.write(locationdate.getBytes());
		file.seek(pointer+106);
		file.write(NewEasel.SelectedEasel.getBytes());
		file.seek(pointer+126);
		file.write("1.0".getBytes());
		file.seek(pointer+186);
		file.write("0000".getBytes());
		file.seek(pointer+209);
		file.write(NewEasel.SelectedEasel.getBytes());
		file.seek(pointer+445);
		file.write(usernamefull.getBytes());
		file.writeBytes("\r\n");


		 pointer=file.getFilePointer();
		file.write("IJRITD".getBytes());
		file.seek(pointer+22);
		file.write(NewEasel.SelectedArt.getBytes());
		file.seek(pointer+52);
		file.write(locationnew.getBytes());
		file.seek(pointer+58);
		file.write(locationdate.getBytes());
		file.seek(pointer+106);
		file.write(NewEasel.SelectedEasel.getBytes());
		file.seek(pointer+126);
		file.write("1.0".getBytes());
		file.seek(pointer+186);
		file.write("0000".getBytes());
		file.seek(pointer+209);
		file.write(NewEasel.SelectedEasel.getBytes());
		file.seek(pointer+445);
		file.write(usernamefull.getBytes());*/
			file.close();
		}catch(Exception q)
		{
			Toast.makeText(getActivity(), q.toString(), Toast.LENGTH_LONG).show();
		}
		File from = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".txt");
		File to = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".odb");
		from.renameTo(to);

	}
	public class UploadFile extends AsyncTask<String,String,String>
	{

		String z = "";

		String replycode="";
		ProgressDialog p;
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Uploading File ...");
			p.setCancelable(false);
			p.show();
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			if(isSuccess) {
				if(uploadtxt){
					SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);

					String	uniquenumber =settings.getString(preferences.uniquevalue,"00001");
					int incrementuniquenumber=Integer.parseInt(uniquenumber);
					incrementuniquenumber=incrementuniquenumber+1;
					preferences p= new preferences(getActivity());
					p.Set5digitnumber("0000"+String.valueOf(incrementuniquenumber));
					NavigationActivity.startturn=false;
					Toast.makeText(getActivity(),"Txt File Uploaded SuccessFully",Toast.LENGTH_SHORT).show();
					InsertRecord doInsert = new InsertRecord(); 
					doInsert.execute("");
				}
				else{
					Toast.makeText(getActivity(),"odb File Uploaded SuccessFully",Toast.LENGTH_SHORT).show();
					isSuccess=false;
					uploadtxt=true;
					UploadFile doUpload = new UploadFile(); 
					doUpload.execute("");
				}
			}

			else
			{
				Toast.makeText(getActivity(),"Error in uploading File",Toast.LENGTH_SHORT).show();

			}

		}

		@Override
		protected String doInBackground(String... params) {

			try {
				isSuccess=false;
				if(uploadtxt)
				{
					movefile=true;
					uploadFile(new File(Environment.getExternalStorageDirectory()+File.separator+getbidsfilename()/*+filename+"_bids"*/+".txt"));

				}
				else{
					movefile=false;
					uploadFile(new File(Environment.getExternalStorageDirectory()+File.separator+filename+".odb"));
				} 

			}
			catch (Exception ex)
			{
				isSuccess = false;
				replycode = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return replycode;
		}
	}
	public void uploadFile(File fileName){


		FTPClient client = new FTPClient();

		try {

			client.connect(FTPCredentials.FTP_HOST,21);
			client.login(FTPCredentials.FTP_USER, FTPCredentials.FTP_PASS);
			client.setType(FTPClient.TYPE_BINARY);
			if(uploadtxt)
			{
				//client.changeDirectory("bid_imports/Test");
				client.changeDirectory("bid_imports");
			}
			/*else
			{
				client.changeDirectory("Test");
			}*/

			client.upload(fileName, new MyTransferListener());

		} catch (Exception e) {
			e.printStackTrace();
			errormessage=e.toString();
			Log.e("EXCEPTION ",errormessage);
			try {
				client.disconnect(true);    
			} catch (Exception e2) {
				errormessage=e.toString();
				e2.printStackTrace();
			}
		}

	}

	/*******  Used to file upload and show progress  **********/

	public class MyTransferListener implements FTPDataTransferListener {

		public void started() {

		}

		public void transferred(int length) {
		}

		public void completed() {

			isSuccess=true;
			if(uploadtxt&&movefile)
			{
				//Text File

				FileUtils.MoveFile(Environment.getExternalStorageDirectory()+File.separator+getbidsfilename()+".txt",Environment.getExternalStorageDirectory()
						+ File.separator + "Bid Imports"+File.separator+getbidsfilename()+".txt");
			}

			else
			{
				//Odb File
			}

		}

		public void aborted() {

		}

		public void failed() {
		}

	}
	public void generatetxtfile()
	{
		try{
			File ff=new File(Environment.getExternalStorageDirectory()+File.separator+getbidsfilename()+".txt");
			if(ff.exists()){ff.delete();}
			ff.createNewFile();
			String easel="EASEL: "+NewEasel.SelectedEasel;
			String easelname="Easel Name: "+NewEasel.SelectedSponsor;
			String art="PRODUCT: "+NewEasel.SelectedArt;
			String bids="";
		if(Questionaire.zerobids){ bids="BIDS: "+"0";}
		else{
				 bids="BIDS: "+Questionaire.Bids;}
			
			String description="DESCRIPTION: "+NewEasel.SelectedArtDescription;
			RandomAccessFile file = new RandomAccessFile(Environment.getExternalStorageDirectory()+File.separator+getbidsfilename()+".txt", "rw");
			file.write(easel.getBytes());
			file.writeBytes("\r\n");
			file.write(easelname.getBytes());
			file.writeBytes("\r\n");
			file.write(art.getBytes());
			file.writeBytes("\r\n");
			if(Questionaire.zerobids)
			{
				
			}
			else
			{
			//file.write(bids.getBytes());
			//file.writeBytes("\r\n");
			}
			file.write(description.getBytes());
			file.writeBytes("\r\n");
			file.close();


		}
		catch(Exception y)
		{

		}
	}
	@SuppressWarnings("unused")
	public void generatehstfile()
	{
		try{

			String m="";
			String d="";
			String modifiedmonth="";
			String modifiedday="";
			Date date = new Date(); // your date
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			if(day<10)
			{
				modifiedday="0"+String.valueOf(day);
			}
			else if(day>=10){modifiedday=String.valueOf(day);}
			if(month<10)
			{
				modifiedmonth="0"+String.valueOf(month);
			}
			else if(month>=10){modifiedmonth=String.valueOf(month);}
			if(day<10)
			{
				d="0"+String.valueOf(day);
			}
			else
			{
				d=String.valueOf(day);
			}
			month=month+1;
			if(month<=9)
			{
				m=String.valueOf(month);
			}
			else if(month==10)
			{
				m="O";

			}
			else if(month==11)
			{
				m="N";

			}
			else if(month==12)
			{
				m="D";

			}
			File ff=new File(Environment.getExternalStorageDirectory()+File.separator+filename+".txt");
			if(ff.exists()){ff.delete();}
			ff.createNewFile();
			if(SummaryActivity.summary)
			{
				String bids=Questionaire.Bids+".0";
				String barcodeandart=SummaryActivity.SelectedArt;
				String easelandart=SummaryActivity.SelectedEasel+SummaryActivity.SelectedArt;
				String location=SummaryActivity.SelectedLocation+SummaryActivity.SelectedLocation;
				String locationdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
				String locationnew=NewEasel.SelectedLocation+NewEasel.SelectedLocation;
				String locationnewdate=String.valueOf(yearn)+String.valueOf(modifiedmonthn)+String.valueOf(modifieddayn);
				RandomAccessFile file = new RandomAccessFile(Environment.getExternalStorageDirectory()+File.separator+filename+".txt", "rw");
				long pointer;
				file.write("IJRITH".getBytes());
				file.seek(22);
				file.write(barcodeandart.getBytes());
				file.seek(52);
				file.write(location.getBytes());
				file.seek(58);
				file.write(locationdate.getBytes());
				file.seek(106);
				file.write(Home.SelectedEasel.getBytes());
				file.seek(126);
				file.write(bids.getBytes());
				file.seek(186);
				file.write(Home.SelectedEasel.getBytes());
				file.seek(209);
				file.write("0000".getBytes());
				file.seek(445);
				file.write(usernamefull.getBytes());
				file.writeBytes("\r\n");

				pointer=file.getFilePointer();
				file.write("IJRITD".getBytes());
				file.seek(pointer+22);
				file.write(barcodeandart.getBytes());
				file.seek(pointer+52);
				file.write(location.getBytes());
				file.seek(pointer+58);
				file.write(locationdate.getBytes());
				file.seek(pointer+106);
				file.write(Home.SelectedEasel.getBytes());
				file.seek(pointer+126);
				file.write(bids.getBytes());
				file.seek(pointer+186);
				file.write(Home.SelectedEasel.getBytes());
				file.seek(pointer+209);
				file.write("0000".getBytes());
				file.seek(pointer+445);
				file.write(usernamefull.getBytes());
				file.writeBytes("\r\n");


				pointer=file.getFilePointer();
				file.write("IJRITH".getBytes());
				file.seek(pointer+22);
				file.write(NewEasel.SelectedArt.getBytes());
				file.seek(pointer+52);
				file.write(locationnew.getBytes());
				file.seek(pointer+58);
				file.write(locationdate.getBytes());
				file.seek(pointer+106);
				file.write(NewEasel.SelectedEasel.getBytes());
				file.seek(pointer+126);
				file.write("1.0".getBytes());
				file.seek(pointer+186);
				file.write("0000".getBytes());
				file.seek(pointer+209);
				file.write(NewEasel.SelectedEasel.getBytes());
				file.seek(pointer+445);
				file.write(usernamefull.getBytes());
				file.writeBytes("\r\n");


				pointer=file.getFilePointer();
				file.write("IJRITD".getBytes());
				file.seek(pointer+22);
				file.write(NewEasel.SelectedArt.getBytes());
				file.seek(pointer+52);
				file.write(locationnew.getBytes());
				file.seek(pointer+58);
				file.write(locationdate.getBytes());
				file.seek(pointer+106);
				file.write(NewEasel.SelectedEasel.getBytes());
				file.seek(pointer+126);
				file.write("1.0".getBytes());
				file.seek(pointer+186);
				file.write("0000".getBytes());
				file.seek(pointer+209);
				file.write(NewEasel.SelectedEasel.getBytes());
				file.seek(pointer+445);
				file.write(usernamefull.getBytes());
				file.close();

				File from = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".txt");
				File to = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".odb");
				from.renameTo(to);
				SummaryActivity.summary=false;
				return;
			}
			String bids=Questionaire.Bids+".0";
			String barcodeandart=Home.SelectedArt;
			String easelandart=Home.SelectedEasel+Home.SelectedArt;
			String location=Home.SelectedLocation+Home.SelectedLocation;
			String locationdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
			String locationnew=NewEasel.SelectedLocation+NewEasel.SelectedLocation;
			String locationnewdate=String.valueOf(yearn)+String.valueOf(modifiedmonthn)+String.valueOf(modifieddayn);
			RandomAccessFile file = new RandomAccessFile(Environment.getExternalStorageDirectory()+File.separator+filename+".txt", "rw");
			long pointer;
			file.write("IJRITH".getBytes());
			file.seek(22);
			file.write(barcodeandart.getBytes());
			file.seek(52);
			file.write(location.getBytes());
			file.seek(58);
			file.write(locationdate.getBytes());
			file.seek(106);
			file.write(Home.SelectedEasel.getBytes());
			file.seek(126);
			file.write(bids.getBytes());
			file.seek(186);
			file.write(Home.SelectedEasel.getBytes());
			file.seek(209);
			file.write("0000".getBytes());
			file.seek(445);
			file.write(usernamefull.getBytes());
			file.writeBytes("\r\n");

			pointer=file.getFilePointer();
			file.write("IJRITD".getBytes());
			file.seek(pointer+22);
			file.write(barcodeandart.getBytes());
			file.seek(pointer+52);
			file.write(location.getBytes());
			file.seek(pointer+58);
			file.write(locationdate.getBytes());
			file.seek(pointer+106);
			file.write(Home.SelectedEasel.getBytes());
			file.seek(pointer+126);
			file.write(bids.getBytes());
			file.seek(pointer+186);
			file.write(Home.SelectedEasel.getBytes());
			file.seek(pointer+209);
			file.write("0000".getBytes());
			file.seek(pointer+445);
			file.write(usernamefull.getBytes());
			file.writeBytes("\r\n");


			pointer=file.getFilePointer();
			file.write("IJRITH".getBytes());
			file.seek(pointer+22);
			file.write(NewEasel.SelectedArt.getBytes());
			file.seek(pointer+52);
			file.write(locationnew.getBytes());
			file.seek(pointer+58);
			file.write(locationdate.getBytes());
			file.seek(pointer+106);
			file.write(NewEasel.SelectedEasel.getBytes());
			file.seek(pointer+126);
			file.write("1.0".getBytes());
			file.seek(pointer+186);
			file.write("0000".getBytes());
			file.seek(pointer+209);
			file.write(NewEasel.SelectedEasel.getBytes());
			file.seek(pointer+445);
			file.write(usernamefull.getBytes());
			file.writeBytes("\r\n");


			pointer=file.getFilePointer();
			file.write("IJRITD".getBytes());
			file.seek(pointer+22);
			file.write(NewEasel.SelectedArt.getBytes());
			file.seek(pointer+52);
			file.write(locationnew.getBytes());
			file.seek(pointer+58);
			file.write(locationdate.getBytes());
			file.seek(pointer+106);
			file.write(NewEasel.SelectedEasel.getBytes());
			file.seek(pointer+126);
			file.write("1.0".getBytes());
			file.seek(pointer+186);
			file.write("0000".getBytes());
			file.seek(pointer+209);
			file.write(NewEasel.SelectedEasel.getBytes());
			file.seek(pointer+445);
			file.write(usernamefull.getBytes());
			file.close();
		}catch(Exception q)
		{
			Toast.makeText(getActivity(), q.toString(), Toast.LENGTH_LONG).show();
		}
		File from = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".txt");
		File to = new File(Environment.getExternalStorageDirectory()+File.separator,filename+".HST");
		from.renameTo(to);	
	}

	public class InsertRecord extends AsyncTask<String,String,String>
	{
		String z = "";
		Boolean Success = false;
		ProgressDialog p;
		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(getActivity());
			p.setMessage("Inserting Record ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			try {
				if(p!=null &&p.isShowing()){
				p.dismiss();}
			}catch (Exception a){}
			if(Success) {
				 preferences pref= new preferences(getActivity());
					pref.SetLastScreen("");
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();

				try {
					if(p!=null &&p.isShowing()){
						p.dismiss();}

				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.frame_container, new Home()).commit();
				fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}catch (Exception a)
				{
					Intent i = new Intent(getActivity(),NavigationActivity.class);
					i.putExtra("val", "");
					startActivity(i);
				}
			}
			else
			{
				Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			try {
				Connection con = connectionClass.CONN();
				if (con == null) {
					z = "There is a problem connecting with the server. Please try again and if the issue persists, please contact app@fundinginnovation.ca";
				} else {
					String binno="";
					String itemno="";
					if(SummaryActivity.summary)
					{
						binno =SummaryActivity.SelectedEasel;
						itemno =SummaryActivity.SelectedArt;

					}
					else
					{
						binno =NewEasel.SelectedEasel;
						itemno =NewEasel.SelectedArt;
					}
					//String query = "select * from dbo.vw_PAS_LiveEasels" ;
					String query = "Insert into dbo.EaselTurns (bin_no,item_no,turn_start_date,turn_end_date,usr_id) values ('"
							+ binno + "','" + itemno+ "','" + turnstartdate+ "','" + turnenddate+ "','" + usernamefull

							+ "')";
					PreparedStatement preStmt = con.prepareStatement(query);

					preStmt.executeUpdate();

					z = "Inserted Successfully";

					Success=true;

				}
			}
			catch (Exception ex)
			{
				Success = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return z;
		}
	}
	@SuppressWarnings("unused")
	private boolean isPastDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		Date today = c.getTime();
		if (date.before(today)) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
	public String getbidsfilename()
	{
		String m="";
		String d="";
		String modifiedmonth="";
		String modifiedday="";
		Date date = new Date(); // your date
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		month=month+1;
		if(day<10)
		{
			modifiedday="0"+String.valueOf(day);
		}
		else if(day>=10){modifiedday=String.valueOf(day);}
		if(month<10)
		{
			modifiedmonth="0"+String.valueOf(month);
		}
		else if(month>=10){modifiedmonth=String.valueOf(month);}
		if(day<10)
		{
			d="0"+String.valueOf(day);
		}
		else
		{
			d=String.valueOf(day);
		}
		
		if(month<=9)
		{
			m=String.valueOf(month);
		}
		else if(month==10)
		{
			m="O";

		}
		else if(month==11)
		{
			m="N";

		}
		else if(month==12)
		{
			m="D";

		}	
		String name="";
		String currentdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
		if(SummaryActivity.summary)
		{
			name =NewEasel.SelectedEasel+"_"+NewEasel.SelectedArt+"_"+currentdate+"_"+filename;
		}
		else
		{
			name =Home.SelectedEasel+"_"+NewEasel.SelectedArt+"_"+currentdate+"_"+filename;
		}

		return name;
	}
	

	    public long milliseconds(String date) 
	    {
	        //String date_ = date;
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        try
	        {
	            Date mDate = sdf.parse(date);
	            long timeInMilliseconds = mDate.getTime();
	            System.out.println("Date in milli :: " + timeInMilliseconds);
	            return timeInMilliseconds;
	        }
	        catch (Exception e) 
	        {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	        return 0;
	    }

	public class ChangeProduct extends AsyncTask<String, String, String> {
		String z = "";
		Boolean isSuccess = false;
		ProgressDialog p;

		@Override
		protected void onPreExecute() {
			p = new ProgressDialog(getActivity());
			p.setMessage("Processing ...");
			p.setCancelable(false);
			p.show();
		}

		@Override
		protected void onPostExecute(String r) {
			p.dismiss();
			/*if (isSuccess) {

				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();


			} else {
				Toast.makeText(getActivity(), r, Toast.LENGTH_SHORT).show();
			}*/
			Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();
			preferences pref= new preferences(getActivity());
			pref.SetLastScreen("");
			Toast.makeText(getActivity(),r,Toast.LENGTH_SHORT).show();

			try {
				if(p!=null &&p.isShowing()){
					p.dismiss();}

				/*FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, new Home()).commit();
				fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);*/
				Intent i = new Intent(getActivity(),NavigationActivity.class);
				i.putExtra("val", "");
				startActivity(i);
			}catch (Exception a)
			{
				Intent i = new Intent(getActivity(),NavigationActivity.class);
				i.putExtra("val", "");
				startActivity(i);
			}

		}

		@SuppressWarnings("unused")
		@Override
		protected String doInBackground(String... params) {

			try {
				HttpClient httpClient = new DefaultHttpClient();
				SharedPreferences settings = getActivity().getSharedPreferences(preferences.PREF_NAME,0);
				String conid=  	settings.getString(preferences.useridd,"");
				String initials=  	settings.getString(preferences.initials,"");
				String initialsdef=  	settings.getString(preferences.initials,"")+"0000";
				String locationsapurl=SFAPI.advanceprourl+"/api/locations?name="+initialsdef;
				//Log.e("Locations Url ",locationsapurl);
				String tolocationid="";
				HttpGet locations = new HttpGet(locationsapurl);
				locations.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				locations.addHeader(SFAPI.prettyPrintHeader);
				HttpResponse locationsresponse = httpClient.execute(locations);
				JSONObject locationresponseobject=new JSONObject(EntityUtils.toString(locationsresponse.getEntity()));
				if(locationresponseobject.getString("success").equalsIgnoreCase("true")) {
					JSONArray locationpayloadarray = locationresponseobject.getJSONArray("payload");

					for (int lp = 0; lp < locationpayloadarray.length(); lp++) {
						JSONObject items = locationpayloadarray.getJSONObject(lp);
						tolocationid = items.getString("id");

					}
				}

				String uri = SFAPI.advanceprourl + "/api/" + SFAPI.advanceproinventoryTransfer;
				Log.e("Url :", uri);


				//create the JSON object containing the new lead details.
				JSONObject auction = new JSONObject();
				auction.put("product_id", Integer.parseInt(Home.SelectedNEWAPProductID));
				auction.put("variant_id", 0);
				auction.put("from_location_id", Integer.parseInt(tolocationid));
				auction.put("to_location_id", Integer.parseInt(Home.SelectedFROMLOCATIONID));
				auction.put("unassign_after_transfer", true);
				auction.put("quantity", 1);
				auction.put("reason", "Change Product");


				Log.e("", "JSON for CP record to be inserted:\n" + auction.toString(1));
				//Toast.makeText(LoginActivity.this,"JSON for lead record to be inserted:\n" + lead.toString(1),Toast.LENGTH_LONG).show();

				//Construct the objects needed for the request

				HttpPost httpPost = new HttpPost(uri);
				httpPost.addHeader(new BasicHeader("Authorization", "Bearer " + SFAPI.advanceprotoken));
				httpPost.addHeader(SFAPI.prettyPrintHeader);
				// The message we are going to post
				StringEntity body = new StringEntity(auction.toString(1));
				body.setContentType("application/json");
				httpPost.setEntity(body);

				//Make the request
				HttpResponse response = httpClient.execute(httpPost);

				//Process the results
				int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode == 201) {

					String response_string = EntityUtils.toString(response.getEntity());
					Log.e(" Response :", response_string);
					isSuccess = true;
					z="Successful";
				} else {
					//Toast.makeText(LoginActivity.this,"Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity()),Toast.LENGTH_LONG).show();
					//System.out.println("Insertion unsuccessful. Status code returned is " + statusCode);
					z = "Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity());
					isSuccess = false;
				}

			} catch (Exception ex) {
				isSuccess = false;
				z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ex.toString();
			}

			return z;

		}

	}

private void fixBuggyCalendarview(CalendarView cv) {
    long current = cv.getDate();
    cv.setDate(cv.getMaxDate(), false, true);
    cv.setDate(current, false, true);
}
}
