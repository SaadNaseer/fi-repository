package com.fundinginnovation;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ScanAnotherEaselcode extends Activity implements OnClickListener{
	private Button scanBtn;
	static String scananotherContent="";
	static String scananotherFormat="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scananothereaselcode);
		scanBtn = (Button)findViewById(R.id.scan_button);
		scanBtn.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.scan_button){
				IntentIntegrator scanIntegrator = new IntentIntegrator(this);
		
				scanIntegrator.initiateScan();
				
			/*Intent i = new Intent(getApplicationContext(),QuestionaireForNewScan.class);
				startActivity(i);*/
				}
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanningResult != null) {
		
			scananotherContent = scanningResult.getContents();
			scananotherFormat = scanningResult.getFormatName();
			Toast.makeText(getApplicationContext(), 
					scananotherContent, Toast.LENGTH_SHORT).show();
				   
				    Toast.makeText(getApplicationContext(), 
				    		scananotherFormat, Toast.LENGTH_SHORT).show();
				  if(scananotherContent==null)
				    {
				    	 Toast toast = Toast.makeText(getApplicationContext(), 
				 		        "No Bar Code data received!", Toast.LENGTH_SHORT);
				 		    toast.show();
				    	return;
				    }
				  
					Intent i = new Intent(getApplicationContext(),QuestionaireForNewScan.class);
					startActivity(i);
			}
		else{
		    Toast toast = Toast.makeText(getApplicationContext(), 
		        "No Bar Code data received!", Toast.LENGTH_SHORT);
		    toast.show();
		}
		
		}
}
