package com.fundinginnovation;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import com.Database.Databaseadapter;
import com.NavigationFragments.Home;
import com.Utils.CustomDigitsKeyListener;
import com.Utils.SFAPI;
import com.Utils.preferences;
import com.navigationdrawer.NavigationActivity;
import com.nekoloop.base64image.Base64Image;
import com.nekoloop.base64image.RequestEncode;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class TakePictures extends Activity{
		Button capture;
	 	private static final int CAMERA_REQUEST = 1888;
	    ImageView mimageView;
	    int currentimagenumber=1;
	    int takeimagecount=0;
	    static String totalbids="";
	    TextView header_text;
	    static Uri capturedImageUri=null;
	    static String filename="";
	    Button submit;
	    String	uniquenumber="00001";
	    String user2ch="";
	    String currentpicbidamount="";
	    private ArrayList<String> bidamounts = new ArrayList<String>();
	    String bidtitleval="";
	    String cemailval="";
		String cfnval="";
		String clnval="";
		String telnoval="";
	    String mobilenoval="";
		String shippingstreetval="";
		String shippingcityval="";
		String shippingprovinceval="";
		String shippingcountryval="";
		String shippingpcval="";
		String currentimagebase64="";
	@SuppressWarnings("unused")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.takepicture);
		try
		{
			StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
			StrictMode.setVmPolicy(builder.build());
		}
		catch (Exception a)
		{

		}
		//Toast.makeText(TakePictures.this,Home.SelectedProductID,Toast.LENGTH_LONG).show();
		capture=(Button)findViewById(R.id.take_image_from_camera);
		SharedPreferences pref = getApplicationContext().getSharedPreferences(preferences.PREF_NAME,0);
		
			uniquenumber =pref.getString(preferences.uniquevalue,"00001");
			user2ch =pref.getString(preferences.username,"");
		Intent i=getIntent();
		totalbids=i.getStringExtra("total");
		 mimageView = (ImageView) this.findViewById(R.id.image_from_camera);
		 submit = (Button) this.findViewById(R.id.next);
	        Button button = (Button) this.findViewById(R.id.take_image_from_camera);
	        header_text=(TextView)this.findViewById(R.id.header_text);
	        header_text.setText("Click the camera button to take photo of Bid "+String.valueOf(currentimagenumber));
	        ImageView imageButton = (ImageView) findViewById(R.id.menuicon);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					NavigationActivity.mDrawerLayout.openDrawer(NavigationActivity.mDrawerList);
					NavigationActivity.draweropened=true;
				}
			});
			ImageView homeButton = (ImageView) findViewById(R.id.homeicon);
			homeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							TakePictures.this);
					builder.setTitle("Cancel ?");
					builder.setIcon(android.R.drawable.ic_dialog_alert);
					builder.setMessage("Are you sure you wish to cancel the transaction ?");
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0,
								int arg1) {
							
						}
					});
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									 preferences p= new preferences(TakePictures.this);
								    	p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(TakePictures.this);
										db.DeleteRunningTransactions(); 
										 Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
									        i.putExtra("val", "");
									    	startActivity(i);
									    	finish();
								}
							});

					builder.create().show();
				}
			});
	}
	 public void takeImageFromCamera(View view) {
if(totalbids.equalsIgnoreCase(""))

{
	Toast.makeText(getApplicationContext(), "Unfortunately something went wrong. please try again later", Toast.LENGTH_LONG).show();
	return;

}
		 if(takeimagecount==Integer.parseInt(totalbids)){
			 Toast.makeText(getApplicationContext(), "All Bids Images Taken", Toast.LENGTH_LONG).show();
			 return;
			 
		 }
		 takeimagecount++;
		
			
			// filename="I"+LoginActivity.id+LoginActivity.id+m+d;
			String user=user2ch.substring(0, Math.min(user2ch.length(), 2));
			filename="I"+user+String.valueOf(uniquenumber);
			
		 File file = new File(Environment.getExternalStorageDirectory(),  (filename/*+*//*"_"+currentimagenumber*/+"_"+String.valueOf(takeimagecount)+".jpg"));
		     if(!file.exists()){
		     try {
		         file.createNewFile();
		     } catch (IOException e) {
		     // TODO Auto-generated catch block
		         e.printStackTrace();
		     }
		     }else{
		        file.delete();
		     try {
		        file.createNewFile();
		     } catch (IOException e) {
		     // TODO Auto-generated catch block
		         e.printStackTrace();
		     }
		     }
		     capturedImageUri = Uri.fromFile(file);

	        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		 	cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
	        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
	        startActivityForResult(cameraIntent, CAMERA_REQUEST);
	        
	    }

	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
	        	try{
	        	Bitmap mphoto = MediaStore.Images.Media.getBitmap( getApplicationContext().getContentResolver(),  capturedImageUri);
	         
	        	mimageView.setImageBitmap(mphoto);
	        	compressImage(mphoto);
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					mphoto.compress(Bitmap.CompressFormat.JPEG, 20, out);
					Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

					Base64Image.with(this)
							.encode(decoded)
							.into(new RequestEncode.Encode() {
								@Override
								public void onSuccess(String base64) {
									currentimagebase64=base64;
								}

								@Override
								public void onFailure() {

								}
							});
	        	}catch(Exception j)
	        	{
	        		Toast.makeText(getApplicationContext(), j.toString(), Toast.LENGTH_LONG).show();
	        	}
	        	final AlertDialog.Builder builder=new AlertDialog.Builder(TakePictures.this); 
          		 LayoutInflater li = LayoutInflater.from(TakePictures.this);
          			View promptsView = li.inflate(R.layout.dialogpassword_layout, null);
          			builder.setView(promptsView);
          			builder.setCancelable(false);
          			//android:digits="0123456789."
          			builder.setTitle("Enter Bid Amount ");
          			final EditText qval = (EditText)promptsView.
          		 				findViewById(R.id.qval);
				final EditText bidtitle = (EditText)promptsView.
						findViewById(R.id.bidtitle);
				bidtitle.requestFocus();
				final EditText cemail = (EditText)promptsView.
						findViewById(R.id.cemail);
				final EditText cfn = (EditText)promptsView.
						findViewById(R.id.cfn);
				final EditText cln = (EditText)promptsView.
						findViewById(R.id.cln);
				final EditText telno = (EditText)promptsView.
						findViewById(R.id.telno);
				final EditText mobileno = (EditText)promptsView.
						findViewById(R.id.mobileno);
				final EditText shippingstreet = (EditText)promptsView.
						findViewById(R.id.shippingstreet);
				final EditText shippingcity = (EditText)promptsView.
						findViewById(R.id.shippingcity);
				final EditText shippingprovince = (EditText)promptsView.
						findViewById(R.id.shippingprovince);
				final EditText shippingcountry = (EditText)promptsView.
						findViewById(R.id.shippingcountry);
				final EditText shippingpc = (EditText)promptsView.
						findViewById(R.id.shippingpc);

				qval.setKeyListener(new CustomDigitsKeyListener(true,true));
          			qval.setHint("Please enter the bid amount");
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@SuppressWarnings("unused")
					@Override
					public void onClick(DialogInterface dialog, int which) {

						AlertDialog.Builder builder = new AlertDialog.Builder(
								TakePictures.this);
						builder.setTitle("Cancel ?");
						builder.setIcon(android.R.drawable.ic_dialog_alert);
						builder.setMessage("Are you sure you wish to cancel the transaction ?");
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
														int arg1) {
takeimagecount--;
currentimagenumber--;
										//header_text.setText("Click the below button to take photo of Bid "+String.valueOf(currentimagenumber));
									}
								});
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
														int arg1) {
										preferences p= new preferences(TakePictures.this);
										p.SetLastScreen("");
										p.Settotalbids("");
										Databaseadapter db=new Databaseadapter(TakePictures.this);
										db.DeleteRunningTransactions();
										Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
										i.putExtra("val", "");
										startActivity(i);
										finish();
									}
								});

						builder.create().show();
					}
				});
          			builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
          				@SuppressWarnings("unused")
						@Override
          				public void onClick(DialogInterface dialog, int which) {
          					if(qval.getText().toString().isEmpty())
          					{
          						currentpicbidamount="0";
          					}
          					currentpicbidamount=qval.getText().toString();
          					bidamounts.add(currentpicbidamount);
          					 String m="";
          					String d="";
          					String modifiedmonth="";
          					String modifiedday="";
          					Date date = new Date(); // your date
          					Calendar cal = Calendar.getInstance();
          					cal.setTime(date);
          					int year = cal.get(Calendar.YEAR);
          					int month = cal.get(Calendar.MONTH);
          					int day = cal.get(Calendar.DAY_OF_MONTH);
          					month=month+1;
          					if(day<10)
          					{
          						modifiedday="0"+String.valueOf(day);
          					}
          					else if(day>=10){modifiedday=String.valueOf(day);}
          					if(month<10)
          					{
          						modifiedmonth="0"+String.valueOf(month);
          					}
          					else if(month>=10){modifiedmonth=String.valueOf(month);}
          					if(day<10)
          					{
          						d="0"+String.valueOf(day);
          					}
          					else
          					{
          						d=String.valueOf(day);
          					}
          					
          					if(month<=9)
          					{
          						m=String.valueOf(month);
          					}
          					else if(month==10)
          					{
          						m="O";

          					}
          					else if(month==11)
          					{
          						m="N";

          					}
          					else if(month==12)
          					{
          						m="D";

          					}	
          					String currentdate=String.valueOf(year)+String.valueOf(modifiedmonth)+String.valueOf(modifiedday);
          					File from = new File(Environment.getExternalStorageDirectory(),  (filename+"_"+String.valueOf(takeimagecount)+".jpg"));
          					SharedPreferences settings = TakePictures.this.getSharedPreferences(preferences.PREF_NAME,0);
          					
          					String	changedartno =settings.getString(preferences.ChangedArtno,"");
          					String	changedartdes =settings.getString(preferences.ChangedArtDes,"");
          				 if(changedartno.equals("")&&changedartdes.equals(""))
          				 {
          					File to = new File(Environment.getExternalStorageDirectory(),  (Home.SelectedEasel+"_"+Home.SelectedArt+"_"+currentdate+"_"+currentpicbidamount+"_"+filename+"_"+String.valueOf(takeimagecount)+".jpg"));
          					from.renameTo(to); 
          				 }
          				 else
          				 {
          					File to = new File(Environment.getExternalStorageDirectory(),  (Home.SelectedEasel+"_"+changedartno+"_"+currentdate+"_"+currentpicbidamount+"_"+filename+"_"+String.valueOf(takeimagecount)+".jpg"));
          					from.renameTo(to);
          				 }
          					//generate Lead
          					//capture.performClick();
						/*	if(bidtitle.getText().toString().isEmpty())
							{
								bidtitle.setError("Title must be entered");
							}
							else if(cemail.getText().toString().isEmpty())
							{
								cemail.setError("Email must be entered");
							}
							else if(cfn.getText().toString().isEmpty())
							{
								cfn.setError("FirstName must be entered");
							}
							else if(cln.getText().toString().isEmpty())
							{
								cln.setError("LastName must be entered");
							}
							else if(telno.getText().toString().isEmpty())
							{
								telno.setError("Telephone # must be entered");
							}
							else if(mobileno.getText().toString().isEmpty())
							{
								mobileno.setError("Mobile # must be entered");
							}
							else if(shippingstreet.getText().toString().isEmpty())
							{
								shippingstreet.setError("Shipping Street must be entered");
							}
							else if(shippingcity.getText().toString().isEmpty())
							{
								shippingcity.setError("Shipping City must be entered");
							}
							else if(shippingprovince.getText().toString().isEmpty())
							{
								shippingprovince.setError("Shipping Province must be entered");
							}
							else if(shippingcountry.getText().toString().isEmpty())
							{
								shippingcountry.setError("Shipping Country must be entered");
							}
							else if(shippingpc.getText().toString().isEmpty())
							{
								shippingpc.setError("Shipping Postal Code must be entered");
							}
							else
								{*/
						//Toast.makeText(TakePictures.this,"Address "+Home.SelectedStreetAddress+" City "+Home.SelectedCity+" Province "+Home.SelectedProvince,Toast.LENGTH_LONG).show();
									bidtitleval=bidtitle.getText().toString();
									cemailval=cemail.getText().toString();
									cfnval=cfn.getText().toString();
									clnval="lastname";
									telnoval=telno.getText().toString();
									mobilenoval=mobileno.getText().toString();
									shippingstreetval=Home.SelectedStreetAddress;
									shippingcityval=Home.SelectedCity;
									shippingcountryval=shippingcountry.getText().toString();
									shippingprovinceval=Home.SelectedProvince;
									shippingpcval=shippingpc.getText().toString();
									new GenerateLead().execute();
								/*}*/

          				}
          			});
          		
          			builder.show();
          			 qval.setOnFocusChangeListener(new OnFocusChangeListener() {
          		        @Override
          		        public void onFocusChange(View v, boolean hasFocus) {
          		            qval.post(new Runnable() {
          		                @Override
          		                public void run() {
          		                    InputMethodManager inputMethodManager= (InputMethodManager) TakePictures.this.getSystemService(Context.INPUT_METHOD_SERVICE);
          		                    inputMethodManager.showSoftInput(qval, InputMethodManager.SHOW_IMPLICIT);
          		                }
          		            });
          		        }
          		    });
          		   // qval.requestFocus();
	            if(currentimagenumber<Integer.parseInt(totalbids)){
	            currentimagenumber++;
	            header_text.setText("Click the below button to take photo of Bid "+String.valueOf(currentimagenumber));
	        }
	            if(takeimagecount==Integer.parseInt(totalbids))
		    	 {
	            	submit.setText("Submit");
		    	 }   
	        }
	    }
	    @SuppressWarnings("deprecation")
		public String compressImage(Bitmap imageUri) {

	       /* String filePath = getRealPathFromURI(imageUri);
	        Bitmap scaledBitmap = null;

	        BitmapFactory.Options options = new BitmapFactory.Options();

//	      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//	      you try the use the bitmap here, you will get null.
	        options.inJustDecodeBounds = true;
	        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

	        int actualHeight = options.outHeight;
	        int actualWidth = options.outWidth;

//	      max Height and width values of the compressed image is taken as 816x612

	        float maxHeight = 816.0f;
	        float maxWidth = 612.0f;
	        float imgRatio = actualWidth / actualHeight;
	        float maxRatio = maxWidth / maxHeight;

//	      width and height values are set maintaining the aspect ratio of the image

	        if (actualHeight > maxHeight || actualWidth > maxWidth) {
	            if (imgRatio < maxRatio) {
	                imgRatio = maxHeight / actualHeight;
	                actualWidth = (int) (imgRatio * actualWidth);
	                actualHeight = (int) maxHeight;
	            } else if (imgRatio > maxRatio) {
	                imgRatio = maxWidth / actualWidth;
	                actualHeight = (int) (imgRatio * actualHeight);
	                actualWidth = (int) maxWidth;
	            } else {
	                actualHeight = (int) maxHeight;
	                actualWidth = (int) maxWidth;

	            }
	        }

//	      setting inSampleSize value allows to load a scaled down version of the original image

	        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//	      inJustDecodeBounds set to false to load the actual bitmap
	        options.inJustDecodeBounds = false;

//	      this options allow android to claim the bitmap memory if it runs low on memory
	        options.inPurgeable = true;
	        options.inInputShareable = true;
	        options.inTempStorage = new byte[16 * 1024];

	        try {
//	          load the bitmap from its path
	            bmp = BitmapFactory.decodeFile(filePath, options);
	        } catch (OutOfMemoryError exception) {
	            exception.printStackTrace();

	        }
	        try {
	            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
	        } catch (OutOfMemoryError exception) {
	            exception.printStackTrace();
	        }

	        float ratioX = actualWidth / (float) options.outWidth;
	        float ratioY = actualHeight / (float) options.outHeight;
	        float middleX = actualWidth / 2.0f;
	        float middleY = actualHeight / 2.0f;

	        Matrix scaleMatrix = new Matrix();
	        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

	        Canvas canvas = new Canvas(scaledBitmap);
	        canvas.setMatrix(scaleMatrix);
	        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//	      check the rotation of the image and display it properly
	        ExifInterface exif;
	        try {
	            exif = new ExifInterface(filePath);

	            int orientation = exif.getAttributeInt(
	                    ExifInterface.TAG_ORIENTATION, 0);
	            Log.d("EXIF", "Exif: " + orientation);
	            Matrix matrix = new Matrix();
	            if (orientation == 6) {
	                matrix.postRotate(90);
	                Log.d("EXIF", "Exif: " + orientation);
	            } else if (orientation == 3) {
	                matrix.postRotate(180);
	                Log.d("EXIF", "Exif: " + orientation);
	            } else if (orientation == 8) {
	                matrix.postRotate(270);
	                Log.d("EXIF", "Exif: " + orientation);
	            }
	            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
	                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
	                    true);
	        } catch (IOException e) {
	            e.printStackTrace();
	            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
	        }*/

	        FileOutputStream out = null;
	        String filenamee = Environment.getExternalStorageDirectory()+File.separator+filename+"_"+String.valueOf(takeimagecount)+".jpg";
	        try {
	            out = new FileOutputStream(filenamee);

//	          write the compressed bitmap at the destination specified by filename.
	            imageUri.compress(Bitmap.CompressFormat.JPEG, 80, out);

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        }

	        return filenamee;

	    }

	    private String getRealPathFromURI(String contentURI) {
	        Uri contentUri = Uri.parse(contentURI);
	        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
	        if (cursor == null) {
	            return contentUri.getPath();
	        } else {
	            cursor.moveToFirst();
	            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
	            return cursor.getString(index);
	        }
	    }

	    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	        final int height = options.outHeight;
	        final int width = options.outWidth;
	        int inSampleSize = 1;

	        if (height > reqHeight || width > reqWidth) {
	            final int heightRatio = Math.round((float) height / (float) reqHeight);
	            final int widthRatio = Math.round((float) width / (float) reqWidth);
	            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	        }
	        final float totalPixels = width * height;
	        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
	        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
	            inSampleSize++;
	        }

	        return inSampleSize;
	    }
	    public void next(View a)
	    {
	    	 if(takeimagecount==Integer.parseInt(totalbids))
	    	 {
	    		 Intent i = new Intent(getApplicationContext(),SubmissionApproval.class);
	    		 i.putStringArrayListExtra("amounts", bidamounts);
	    		 startActivity(i);
	    		 finish();
	    	 }
	    	 
	    	 else 
	    	 {
	    		 Toast.makeText(getApplicationContext(), "Pleast Take Images of Bids First", Toast.LENGTH_LONG).show();
				 return; 
	    	 }
	    	
	    }
	    @Override
	    public void onBackPressed() 
	    {
	    	 Intent i=new Intent(getApplicationContext(),NavigationActivity.class);
		        i.putExtra("val", "q");
		    	startActivity(i);
	    }


	public class GenerateLead extends AsyncTask<String,String,String>
	{
		ProgressDialog p;
		String z = "";
		Boolean isSuccess = false;





		@Override
		protected void onPreExecute() {
			p=new ProgressDialog(TakePictures.this);
			p.setMessage("Generating Lead ...");
			p.show();
			p.setCancelable(false);
			//pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(String r) {
			//pbbar.setVisibility(View.GONE);
			//Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();
			p.dismiss();
			if(isSuccess) {
				Toast.makeText(TakePictures.this,"Lead Generated Successfully\n"+r,Toast.LENGTH_SHORT).show();
			}

			else
			{
				Toast.makeText(TakePictures.this,r,Toast.LENGTH_SHORT).show();
				takeimagecount--;
				currentimagenumber--;
				//header_text.setText("Click the below button to take photo of Bid "+String.valueOf(currentimagenumber));

			}
			capture.performClick();

		}

		@Override
		protected String doInBackground(String... params) {

			String uri = SFAPI.baseUri + "/sobjects/Lead/";
			Log.e("Url :",uri);
			try {

				//create the JSON object containing the new lead details.
				JSONObject lead = new JSONObject();
				lead.put("Bid_Amount__c", currentpicbidamount);
				//lead.put("Easel_Location__c", Home.SelectedLocation);
				lead.put("Easel_Number__c", Home.SelectedEaselID);
				lead.put("Email", cemailval);
				lead.put("FirstName", cfnval);
				lead.put("LastName", clnval);
				//lead.put("Name", cfnval+" "+clnval);
				lead.put("Status", "New");
				lead.put("MobilePhone", mobilenoval);
				lead.put("Phone", telnoval);
				lead.put("LeadSource", "FI App");
				//lead.put("Product_SKU__c", Home.SelectedArt);
				lead.put("Product_Name__c", Home.SelectedProductID);
				lead.put("Payment_Method__c","");
				lead.put("Shipping_and_Handling_Amount__c", "0");
				lead.put("Shipping_City__c", shippingcityval);
				lead.put("Shipping_Country__c", shippingcountryval);
				lead.put("Shipping_Street__c", shippingstreetval);
				lead.put("Shipping_Zip_Postal_Code__c", shippingpcval);
				lead.put("Shipping_State_Province__c",shippingprovinceval);
				lead.put("Title", bidtitleval);
				Log.e("","JSON for lead record to be inserted:\n" + lead.toString(1));
				//Toast.makeText(LoginActivity.this,"JSON for lead record to be inserted:\n" + lead.toString(1),Toast.LENGTH_LONG).show();

				//Construct the objects needed for the request
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(uri);
				httpPost.addHeader(SFAPI.oauthHeader);
				httpPost.addHeader(SFAPI.prettyPrintHeader);
				// The message we are going to post
				StringEntity body = new StringEntity(lead.toString(1));
				body.setContentType("application/json");
				httpPost.setEntity(body);

				//Make the request
				HttpResponse response = httpClient.execute(httpPost);

				//Process the results
				int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode == 201) {
					p.setMessage("Uploading Bid Slip Image ...");

					String response_string = EntityUtils.toString(response.getEntity());
					Log.e("Lead Response :",response_string);
					JSONObject json = new JSONObject(response_string);
					// Store the retrieved lead id to use when we update the lead.
					String leadId = json.getString("id");
					//System.out.println("New Lead id from response: " + leadId);
					isSuccess=true;
					z="Lead Id : "+leadId;

					String attachmenturl = SFAPI.baseUri + "/sobjects/ContentVersion";
					Log.e("Url :",attachmenturl);
					//Log.e("currentimagebase64 :",currentimagebase64);
					try {
						String filenamee = Environment.getExternalStorageDirectory()+File.separator+filename+"_"+String.valueOf(takeimagecount)+".jpg";
						//create the JSON object containing the new lead details.
						JSONObject image = new JSONObject();
						image.put("Title", filename+"_"+String.valueOf(takeimagecount));
						image.put("PathOnClient", filename+"_"+String.valueOf(takeimagecount)+".jpg");
						image.put("ContentLocation", "S");
						image.put("FirstPublishLocationId",leadId);
						//image.put("OwnerId","0056s000000HznYAAS");
						//image.put("PublishStatus","P");
						image.put("VersionData",currentimagebase64);
						Log.e("","Content Version Request :\n" + image.toString(1));
							//Construct the objects needed for the request
						HttpClient httpClient2 = new DefaultHttpClient();

						HttpPost httpPost2 = new HttpPost(attachmenturl);
						httpPost2.addHeader(SFAPI.oauthHeader);
						httpPost2.addHeader(SFAPI.prettyPrintHeader);
						// The message we are going to post
						StringEntity body2 = new StringEntity(image.toString(1));
						body2.setContentType("application/json");
						httpPost2.setEntity(body2);

						//Make the request
						HttpResponse response2 = httpClient2.execute(httpPost2);

						//Process the results
						int statusCode2 = response2.getStatusLine().getStatusCode();
						if (statusCode2 == 201) {
							z="Lead Generated... Bid Slip Uploaded ";
							isSuccess=true;
							/*JSONObject jsonobjcd = new JSONObject(EntityUtils.toString(response2.getEntity()));
							String ccid = jsonobjcd.getString("id");


							HttpClient httpclientgetcid = new DefaultHttpClient();
							// Assemble the login request URL
							String getcid = SFAPI.baseUri+ "/query/?q=" + URLEncoder.encode("SELECT ContentDocumentId FROM ContentVersion Where Id = '"+ccid+"'", "UTF-8");
							Log.e("URL ", getcid);
							Log.e("Header ", SFAPI.oauthHeader.toString());
							// Login requests must be POSTs
							HttpGet httpPostgetcid = new HttpGet(getcid);
							httpPostgetcid.addHeader(SFAPI.oauthHeader);
							httpPostgetcid.addHeader(SFAPI.prettyPrintHeader);


							HttpResponse responsegetcid = null;
							try {
								// Execute the login POST request
								responsegetcid = httpclientgetcid.execute(httpPostgetcid);
							} catch (ClientProtocolException cpException) {
								cpException.printStackTrace();
								isSuccess = false;
								z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + cpException.toString();

							} catch (IOException ioException) {
								ioException.printStackTrace();
								isSuccess = false;
								z = "The following unexpected errors have occured. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + ioException.toString();

							}
							// verify response is HTTP OK
							final int statusCodegetcid = responsegetcid.getStatusLine().getStatusCode();
							if (statusCodegetcid != HttpStatus.SC_OK) {
								//System.out.println("Error authenticating to Force.com: "+statusCode);
								//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
								isSuccess = false;
								z = "Bid Slip Image Upload Failed. Please try again and if the issue persists, please contact app@fundinginnovation.ca. Error Message: " + "Error : "+statusCode;

							}
							else
								{
									isSuccess=true;
									z ="Success";
									String getResultgetcid = null;
									try {
										getResultgetcid = EntityUtils.toString(responsegetcid.getEntity());
										//Log.e("RESULT ",getResult);
										JSONObject jobj = new JSONObject(getResultgetcid);
										JSONArray jarr = jobj.getJSONArray("records");
										String ContentDocumentId="";
										for (int i = 0; i < jarr.length(); i++) {
											JSONObject eitems = jarr.getJSONObject(i);
											 ContentDocumentId=eitems.getString("ContentDocumentId");
										}
										String ContentDocumentLinkurl = SFAPI.baseUri + "/sobjects/ContentDocumentLink";
										Log.e("Url :",ContentDocumentLinkurl);
										Log.e("ContentDocumentId ",ContentDocumentId);
										Log.e("LinkedEntityId ",leadId);
										try {
											JSONObject linkcd = new JSONObject();
											linkcd.put("ContentDocumentId", ContentDocumentId);
											linkcd.put("LinkedEntityId", leadId);
											linkcd.put("Visibility","AllUsers");
											//Construct the objects needed for the request
											HttpClient httpClientlinkcd = new DefaultHttpClient();
											Log.e("linkcd ",linkcd.toString());
											HttpPost httpPostlinkcd = new HttpPost(ContentDocumentLinkurl);
											httpPostlinkcd.addHeader(SFAPI.oauthHeader);
											httpPostlinkcd.addHeader(SFAPI.prettyPrintHeader);
											// The message we are going to post
											StringEntity bodylinkcd = new StringEntity(linkcd.toString(1));
											bodylinkcd.setContentType("application/json");
											httpPostlinkcd.setEntity(bodylinkcd);

											//Make the request
											HttpResponse responselinkcd = httpClientlinkcd.execute(httpPostlinkcd);

											//Process the results
											int statusCodelinkcd = responselinkcd.getStatusLine().getStatusCode();
											if (statusCodelinkcd != HttpStatus.SC_OK) {
												//System.out.println("Error authenticating to Force.com: "+statusCode);
												//Toast.makeText(LoginActivity.this,"Error authenticating to : "+statusCode,Toast.LENGTH_LONG).show();
												isSuccess = false;
												z = EntityUtils.toString(responselinkcd.getEntity());

											}
											else
												{
													isSuccess = true;
													z = "Bid Slip Image Uploaded Successfully";

												}
										}
										catch (Exception a){}

									}catch (Exception e)
									{

									}
								}*/
						}
						else
							{
								isSuccess=false;
								z="Lead Generated... Bid Slip Upload Failed " + EntityUtils.toString(response2.getEntity());
							}

					}catch (Exception ee)
					{
						Log.e("Exception ",ee.toString());
					}
				} else {
					//Toast.makeText(LoginActivity.this,"Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity()),Toast.LENGTH_LONG).show();
					//System.out.println("Insertion unsuccessful. Status code returned is " + statusCode);
					z="Insertion unsuccessful. Status response " + EntityUtils.toString(response.getEntity());
					isSuccess=false;
				}
			} catch (JSONException e) {
				isSuccess=false;
				//System.out.println("Issue creating JSON or processing results");
				e.printStackTrace();
			} catch (IOException ioe) {
				isSuccess=false;
			} catch (NullPointerException npe) {
				isSuccess=false;
			}

			return z;
		}
	}


}
