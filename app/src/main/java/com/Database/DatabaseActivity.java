package com.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseActivity extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 4;
	private static final String DATABASE_NAME = "fi.db";
	public DatabaseActivity(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE charities (" 
				+ "CharityID varchar," 
				+ "Name varchar," 
				+ "Address1 varchar," 
				+ "City varchar," 
				+ "Province varchar," 
				+ "PostCode varchar," 
				+ "Telephone varchar," 
				+ "ContactName varchar," 
				+ "cnt_f_tel varchar," 
				+ "Cellphone varchar," 
				+ "EMail varchar," 
				+ "Logo varchar"+");");
		
		db.execSQL("CREATE TABLE products (" 
				+ "Name varchar," 
				+ "Description varchar," 
				+ "Logo varchar"+");");
		db.execSQL("CREATE TABLE inventoryscan (" 
				+ "itemno varchar," 
				+ "quantity varchar," 
				+ "description varchar," 
				+ "idate varchar"+");");
		db.execSQL("CREATE TABLE requestsupplies (" 
				+ "itemname varchar," 
				+ "quantity varchar," 
				+ "description varchar"+");");
		
		
		
		db.execSQL("CREATE TABLE runningtransactions (" 
				+ "art varchar," 
				+ "location varchar," 
				+ "charity varchar," 
				+ "sponsor varchar," 
				+ "easel varchar," 
				+ "quantity varchar," 
				+ "streetaddress varchar," 
				+ "city varchar," 
				+ "province varchar," 
				+ "postalcode varchar," 
				+ "artdescription varchar"+");");
		
	}
	


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		try
		{
		db.execSQL("CREATE TABLE charities (" 
				+ "CharityID varchar," 
				+ "Name varchar," 
				+ "Address1 varchar," 
				+ "City varchar," 
				+ "Province varchar," 
				+ "PostCode varchar," 
				+ "Telephone varchar," 
				+ "ContactName varchar," 
				+ "cnt_f_tel varchar," 
				+ "Cellphone varchar," 
				+ "EMail varchar," 
				+ "Logo varchar"+");");		
		db.execSQL("CREATE TABLE IF NOT EXISTS products (" 
				+ "Name varchar," 
				+ "Description varchar," 
				+ "Logo varchar"+");");
		db.execSQL("CREATE TABLE IF NOT EXISTS inventoryscan (" 
				+ "itemno varchar," 
				+ "quantity varchar," 
				+ "description varchar," 
				+ "idate varchar"+");");
		db.execSQL("CREATE TABLE IF NOT EXISTS requestsupplies (" 
				+ "itemname varchar," 
				+ "quantity varchar," 
				+ "description varchar"+");");		
		db.execSQL("CREATE TABLE IF NOT EXISTS runningtransactions (" 
				+ "art varchar," 
				+ "location varchar," 
				+ "charity varchar," 
				+ "sponsor varchar," 
				+ "easel varchar," 
				+ "quantity varchar," 
				+ "streetaddress varchar," 
				+ "city varchar," 
				+ "province varchar," 
				+ "postalcode varchar," 
				+ "artdescription varchar"+");");   
		
		}
		catch(Exception r){}
	}
}