package com.Database;
import java.util.ArrayList;
import java.util.List;

import com.GetterSetters.CharitiesGetterSetter;
import com.GetterSetters.ProductsGetterSetter;
import com.GetterSetters.RequestSuppliesGetterSetter;
import com.GetterSetters.RunningtransactionsGetterSetter;
import com.GetterSetters.ScanInventoryGetterSetter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Databaseadapter {
	SQLiteDatabase database;
	DatabaseActivity dbhelper;
	//private static final String TABLE_NAME = "menu";

	public Databaseadapter(Context context){
		dbhelper=new DatabaseActivity(context);
	}
	public void open(){
		database=dbhelper.getWritableDatabase();
	}
	public void getdata(){
		database=dbhelper.getReadableDatabase();
	}
	public void  close(){
		database.close();
	}
	 public int getCount() {
		 int count = 0;
		    String countQuery = "SELECT  * FROM " + "charities";
		    Cursor cursor = database.rawQuery(countQuery, null);
		    if(cursor != null && !cursor.isClosed()){
		        count = cursor.getCount();
		        cursor.close();
		    }   
		    return count;
	    }
	 
	 public int getProductsCount() {
		 int count = 0;
		    String countQuery = "SELECT  * FROM " + "products";
		    Cursor cursor = database.rawQuery(countQuery, null);
		    if(cursor != null && !cursor.isClosed()){
		        count = cursor.getCount();
		        cursor.close();
		    }   
		    return count;
	    }
	 public void DeleteCharities() {
		    
		    database.delete("charities", null, null);
	    }
	 public void DeleteProducts() {
		    
		    database.delete("products", null, null);
	    }
	 public void DeleteInventories() {
		    
		    database.delete("inventoryscan", null, null);
	    }
	 public void DeleteInventory(String itemno)
	 {
		 database.delete("inventoryscan", "itemno = ?", new String[] { itemno });
	 }
	 public void DeleteRequestSuplies(String itemno)
	 {
		 database.delete("requestsupplies", "itemname = ?", new String[] { itemno });
	 }
	 public void DeleteRequestSupplies() {
		 
		    database.delete("requestsupplies", null, null);
	    }
	 public void UpdateInventory(String olditemno,String newitemno,String newquantity,String des) {
		    
		 ContentValues cv = new ContentValues();
		 cv.put("itemno",newitemno); //These Fields should be your String values of actual column names
		 cv.put("quantity",newquantity);
		 cv.put("description",des);
	
		 database.update("inventoryscan", cv, "itemno = ?", new String[]{olditemno});
	    }
	 public void UpdateRequestSupplies(String olditemno,String newitemno,String newquantity,String des) {
		    
		 ContentValues cv = new ContentValues();
		 cv.put("itemname",newitemno); //These Fields should be your String values of actual column names
		 cv.put("quantity",newquantity);
		 cv.put("description",des);
	
		 database.update("requestsupplies", cv, "itemname = ?", new String[]{olditemno});
	    }
	 public List<CharitiesGetterSetter> getcharities() {
		    List<CharitiesGetterSetter> localhistoryList = new ArrayList<CharitiesGetterSetter>();
		    // Select All Query
		    String selectQuery = "SELECT  * FROM " + "charities";
		   
		    Cursor cursor = database.rawQuery(selectQuery, null);
		 
		    // looping through all rows and adding to list
		    if (cursor.moveToFirst()) {
		        do {
		    				
		        	CharitiesGetterSetter charities = new CharitiesGetterSetter();
		        	charities.SetCharityId(cursor.getString(0));
		        	charities.SetCharityName(cursor.getString(1));
		        	charities.SetAddress(cursor.getString(2));
		        	charities.SetCity(cursor.getString(3));
		        	charities.SetProvine(cursor.getString(4));
		        	charities.SetPostCode(cursor.getString(5));
		        	charities.SetTelephone(cursor.getString(6));
		        	charities.SetContactName(cursor.getString(7));
		        	charities.Setcnt_f_tel(cursor.getString(8));
		        	charities.SetCellPhone(cursor.getString(9));
		        	charities.SetEmail(cursor.getString(10));
		        	charities.SetLogo(cursor.getString(11));
		            // Adding menus to list
		        	localhistoryList.add(charities);
		        } while (cursor.moveToNext());
		    }
		 
		    // return localhistoryList 
		    return localhistoryList;
		}

	 public List<ProductsGetterSetter> getproducts() {
		    List<ProductsGetterSetter> pro = new ArrayList<ProductsGetterSetter>();
		    // Select All Query
		    String selectQuery = "SELECT  * FROM " + "products";
		   
		    Cursor cursor = database.rawQuery(selectQuery, null);
		 
		    // looping through all rows and adding to list
		    if (cursor.moveToFirst()) {
		        do {
		    				
		        	ProductsGetterSetter products = new ProductsGetterSetter();
		        	products.SetProductName(cursor.getString(0));
		        	products.SetProductDescription(cursor.getString(1));
		        	products.SetProductPicture(cursor.getString(2));
		        
		            // Adding menus to list
		        	pro.add(products);
		        } while (cursor.moveToNext());
		    }
		 
		    // return localhistoryList 
		    return pro;
		}
	 public List<ProductsGetterSetter> get5products(int limit) {
		    List<ProductsGetterSetter> pro = new ArrayList<ProductsGetterSetter>();
		    // Select All Query
	
		  String selectQuery = "SELECT  * FROM " + "products limit "+String.valueOf(limit)+", "+String.valueOf(5)+"";
		   
		    Cursor cursor = database.rawQuery(selectQuery, null);
		 
		    // looping through all rows and adding to list
		    if (cursor.moveToFirst()) {
		        do {
		    				
		        	ProductsGetterSetter products = new ProductsGetterSetter();
		        	products.SetProductName(cursor.getString(0));
		        	products.SetProductDescription(cursor.getString(1));
		        	products.SetProductPicture(cursor.getString(2));
		        
		            // Adding menus to list
		        	pro.add(products);
		        } while (cursor.moveToNext());
		    }
		 
		    // return localhistoryList 
		    return pro;
		}
	 public List<RequestSuppliesGetterSetter> getrequestsupplies() {
		    List<RequestSuppliesGetterSetter> scan = new ArrayList<RequestSuppliesGetterSetter>();
		    // Select All Query
		    String selectQuery = "SELECT  * FROM " + "requestsupplies";
		   
		    Cursor cursor = database.rawQuery(selectQuery, null);
		 
		    // looping through all rows and adding to list
		    if (cursor.moveToFirst()) {
		        do {
		    				
		        	RequestSuppliesGetterSetter scanned = new RequestSuppliesGetterSetter();
		        	scanned.SetItemName(cursor.getString(0));
		        	scanned.SetItemQuantity(cursor.getString(1));
		        	scanned.SetItemDescription(cursor.getString(2));;
		        	scan.add(scanned);
		        } while (cursor.moveToNext());
		    }
		 
		    // return localhistoryList 
		    return scan;
		}
	 public List<ScanInventoryGetterSetter> getscannedinventory() {
		    List<ScanInventoryGetterSetter> scan = new ArrayList<ScanInventoryGetterSetter>();
		    // Select All Query
		    String selectQuery = "SELECT  * FROM " + "inventoryscan";
		   
		    Cursor cursor = database.rawQuery(selectQuery, null);
		 
		    // looping through all rows and adding to list
		    if (cursor.moveToFirst()) {
		        do {
		    				
		        	ScanInventoryGetterSetter scanned = new ScanInventoryGetterSetter();
		        	scanned.SetItemNo(cursor.getString(0));
		        	scanned.SetItemQuantity(cursor.getString(1));
		        	scanned.SetItemDescription(cursor.getString(2));
		        	scanned.SetItemDate(cursor.getString(3));
		        	scan.add(scanned);
		        } while (cursor.moveToNext());
		    }
		 
		    // return localhistoryList 
		    return scan;
		}
	public void insertcharities(CharitiesGetterSetter CharitiesGetterSetters){
		ContentValues cv=new ContentValues();
		cv.put("CharityID",CharitiesGetterSetters.GetCharityid());
		cv.put("Name",CharitiesGetterSetters.GetCharityName());
		cv.put("Address1",CharitiesGetterSetters.GetAddress());
		cv.put("City",CharitiesGetterSetters.GetCity());
		cv.put("Province",CharitiesGetterSetters.GetProvince());
		cv.put("PostCode",CharitiesGetterSetters.GetPostCode());
		cv.put("Telephone",CharitiesGetterSetters.GetTelephone());
		cv.put("ContactName",CharitiesGetterSetters.GetContactName());
		cv.put("cnt_f_tel",CharitiesGetterSetters.Getcnt_f_tel());
		cv.put("Cellphone",CharitiesGetterSetters.GetCellPhone());
		cv.put("EMail",CharitiesGetterSetters.GetEmail());
		cv.put("Logo",CharitiesGetterSetters.GetLogo());
		database.insert("charities", "", cv);}
	public void insertproducts(ProductsGetterSetter ProductsGetterSetter){
		ContentValues cv=new ContentValues();
		cv.put("Name",ProductsGetterSetter.GetProductName());
		cv.put("Description",ProductsGetterSetter.GetProductDescription());
		cv.put("Logo",ProductsGetterSetter.GetProductPicture());

		database.insert("products", "", cv);}
	public void insertscaninventory(ScanInventoryGetterSetter scannedinventory){
		ContentValues cv=new ContentValues();
		cv.put("itemno",scannedinventory.GetItemNo());
		cv.put("quantity",scannedinventory.GetItemQuantity());
		cv.put("description",scannedinventory.GetItemDescription());
		cv.put("idate", scannedinventory.GetItemDate());
		database.insert("inventoryscan", "", cv);}
	public void insertrequestsupplies(RequestSuppliesGetterSetter requestinventory){
		ContentValues cv=new ContentValues();
		cv.put("itemname",requestinventory.GetItemName());
		cv.put("quantity",requestinventory.GetItemQuantity());
		cv.put("description",requestinventory.GetItemDescription());
		
		database.insert("requestsupplies", "", cv);}
	
	public int getRunningTransactionsCount() {
		 int count = 0;
		    String countQuery = "SELECT  * FROM " + "runningtransactions";
		    Cursor cursor = database.rawQuery(countQuery, null);
		    if(cursor != null && !cursor.isClosed()){
		        count = cursor.getCount();
		        cursor.close();
		    }   
		    return count;
	    }

public void DeleteRunningTransactions()
	 {
	open();
	getdata();
		   database.delete("runningtransactions", null, null);
	 }

public List<RunningtransactionsGetterSetter> getRunningTransactions() {
		    List<RunningtransactionsGetterSetter> runnintransactions = new ArrayList<RunningtransactionsGetterSetter>();
		    // Select All Query
		    String selectQuery = "SELECT  * FROM " + "runningtransactions";
		   
		    Cursor cursor = database.rawQuery(selectQuery, null);
		 
		    // looping through all rows and adding to list
		    if (cursor.moveToFirst()) {
		        do {
		    				
		        	RunningtransactionsGetterSetter rt = new RunningtransactionsGetterSetter();
		        	rt.SetArt(cursor.getString(0));
		        	rt.SetLocation(cursor.getString(1));
		        	rt.SetCharity(cursor.getString(2));
		        	rt.SetSponsor(cursor.getString(3));
		        	rt.SetEasel(cursor.getString(4));
		        	rt.SetQuantity(cursor.getString(5));
		        	rt.SetStreeAddress(cursor.getString(6));
		        	rt.SetCity(cursor.getString(7));
		        	rt.SetProvince(cursor.getString(8));
		        	rt.SetPostalCode(cursor.getString(9));
		        	rt.SetArtDescription(cursor.getString(10));
		        	runnintransactions.add(rt);
		        } while (cursor.moveToNext());
		    }
		 
		    // return runnintransactionslist 
		    return runnintransactions;
		}

public void insertrunningtransactions(RunningtransactionsGetterSetter rt){
		ContentValues cv=new ContentValues();
		cv.put("art",rt.GetArt());
		cv.put("location",rt.GetLocation());
		cv.put("charity",rt.GetCharity());
		cv.put("sponsor",rt.GetSponsor());
		cv.put("easel",rt.GetEasel());
		cv.put("quantity",rt.GetQuantity());
		cv.put("streetaddress",rt.GetStreeAddress());
		cv.put("city",rt.GetCity());
		cv.put("province",rt.GetProvince());
		cv.put("postalcode",rt.GetPostalCode());
		cv.put("artdescription",rt.GetArtDescription());
		database.insert("runningtransactions", "", cv);}
}